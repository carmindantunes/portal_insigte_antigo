﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;
using iTextSharp.text;
using iTextSharp.text.pdf;


namespace Insigte
{
    public partial class Charts : BasePage
    {

        Boolean GRconcorrencia = false;
        String Concorrencia = "";
        Int32 TotalConc = 1;
        String TemaCores = "";
        String[] cores = new String[] { "#00add9", "#00add9", "#00add9", "#00add9", "#00add9" };

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(3, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }

            TemaCores = Global.InsigteManager[Session.SessionID].CorCliente;
            cores = new String[] { TemaCores, TemaCores, TemaCores, TemaCores, TemaCores };
            //fabio 20140808

            if (!Page.IsPostBack)
            {
                fillddpais();
                calGraf();
            }
        }

        protected void calGraf()
        {
            String subjectid_Concorrencia = "";

            foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
            {
                if (pair.Value.Name == "Concorrência" || pair.Value.Name == "Competitors")
                {
                    GRconcorrencia = true;

                    String[] Conc = pair.Value.SubTemas.Split(';');

                    //fabio 20140808
                    subjectid_Concorrencia = pair.Value.IdTema.Substring(0, 4).ToString();

                    foreach (String x in Conc)
                    {
                        TotalConc++;
                        Concorrencia += "'" + x.ToString().Substring(4, 3) + "',";
                    }
                    //fabio 20140808
                    Concorrencia = Concorrencia.TrimEnd(',');
                    //Concorrencia = Concorrencia + "'" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "'";
                    TemaCores = pair.Value.TemaCores;
                    cores = TemaCores.Split(',');
                }
            }

            if (GRconcorrencia == false)
            {
                ChartConco.Visible = false;
                LBConco.Visible = false;
            }
            else
            {
                //fabio 20140808
                CreateChartACNConcorrencia(subjectid_Concorrencia);
                //CreateChartACNConcorrencia();
            }

            if (Global.InsigteManager[Session.SessionID].inUser.DashVisability.Substring(0, 1) == "1")
                CreateChartACN();

            if (Global.InsigteManager[Session.SessionID].inUser.DashVisability.Substring(1, 1) == "1")
                CreateChartROIAVE();

            if (Global.InsigteManager[Session.SessionID].inUser.DashVisability.Substring(2, 1) == "1")
                CreateChartOTS();

            if (Global.InsigteManager[Session.SessionID].inUser.DashVisability.Substring(3, 1) == "1")
                CreateChartACN5Editors();

            if (Global.InsigteManager[Session.SessionID].inUser.DashVisability.Substring(4, 1) == "1")
                CreateChartACN5Jornalistas();

            if (Global.InsigteManager[Session.SessionID].inUser.DashVisability.Substring(6, 1) == "1")
                CreateChartPie();

        }

        protected void fillddpais()
        {
            String SQL = "";

            SQL += " select distinct country from editors with(nolock) where country in ('Angola','Portugal','Moçambique') order by 1  ";

            DataTable pais = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "getddpais");
            string language = Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt" ? "All" : "Todos";
            System.Web.UI.WebControls.ListItem x = new System.Web.UI.WebControls.ListItem(language, "-1");
            //ddpais.Items.Clear();
            ddpais.Items.Add(x);
            foreach (DataRow row in pais.Rows)
            {
                ddpais.Items.Add(new System.Web.UI.WebControls.ListItem(row["country"].ToString(), row["country"].ToString()));
            }

            ddpais.SelectedValue = "-1";
        }

        protected void ddpais_SelectedIndexChanged(object sender, EventArgs e)
        {
            calGraf();
        }

        private void CreateChartACN()
        {
            lblNN.Visible = true;
            ChartNN.Visible = true;

            TemaCores = Global.InsigteManager[Session.SessionID].CorCliente;
            cores = new String[] { TemaCores, TemaCores, TemaCores, TemaCores, TemaCores };

            try
            {
                //String Month_des = "tm.DES_MONTH_PT";
                //if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                //{
                //    Month_des = "tm.DES_MONTH_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " ";
                //}

                string setLG = "Set language Portuguese ";
                if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                {
                    setLG = "Set language English ";
                }

                string cmdQuery = setLG + " SELECT TOP 12 year(m.date) as Year, month(m.date) as month_1, cast(left(UPPER(datename(MONTH,m.date)),1) as varchar) + cast(right(datename(MONTH,m.date), LEN(datename(MONTH,m.date))-1) as varchar) as Month, COUNT(ca.id) AS num, left(convert(varchar,m.date,112),6) as YMdate ";
                cmdQuery += "FROM clientarticles ca with(nolock) ";
                cmdQuery += " join metadata m with(nolock) on ca.id = m.id ";
                cmdQuery += " join editors edt with(nolock) on edt.editorid = m.editorId ";
                cmdQuery += " join time_month tm with(nolock) on convert(varchar,m.date,112) = COD_DAY ";
                cmdQuery += "WHERE ca.clientid = '" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "'  ";
                cmdQuery += " AND ca.subjectid = '" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "'  ";
                cmdQuery += " AND left(convert(varchar(8),m.date,112),6) > left(convert(varchar(8),dateadd(mm,-12,getdate()),112),6) ";
                if (ddpais.SelectedValue != "-1")
                {
                    cmdQuery += " and edt.country = '" + ddpais.SelectedValue + "' ";
                }
                else
                {
                    cmdQuery += " and edt.country in ('Angola','Portugal','Moçambique')";
                }

                cmdQuery += "GROUP BY year(m.date),month(m.date), DateName(mm,m.date), ca.subjectid, left(convert(varchar,m.date,112),6) ";
                cmdQuery += "ORDER BY Year, month(m.date)  ";

                DataTable GrConc = Global.InsigteManager[Session.SessionID].getTableQuery(cmdQuery, "GrConc");

                Int32[] yValues = new Int32[GrConc.Rows.Count];
                string[] xValues = new string[GrConc.Rows.Count];


                int counter = 0;
                foreach (DataRow row in GrConc.Rows)
                {
                    yValues[counter] = Convert.ToInt32(row["num"].ToString());
                    xValues[counter] = row["Month"].ToString();
                    counter++;
                }

                ChartNN.Series["Series1"].Points.DataBindXY(xValues, yValues);


                counter = 0;
                foreach (DataRow row in GrConc.Rows)
                {
                    ChartNN.Series["Series1"].Points[counter].Color = ColorTranslator.FromHtml(cores[0]);
                    counter++;
                }

                // Set series chart type
                ChartNN.Series["Series1"].ChartType = SeriesChartType.Column;
                // Set series point width
                ChartNN.Series["Series1"]["PointWidth"] = "0.5";
                // Show data points labels
                ChartNN.Series["Series1"].IsValueShownAsLabel = true;
                // Set data points label style
                ChartNN.Series["Series1"]["BarLabelStyle"] = "Center";
                // Show as 3D
                //Chart1.ChartAreas["Series1"].Area3DStyle.Enable3D = false;
                // Draw as 3D Cylinder
                ChartNN.Series["Series1"]["DrawingStyle"] = "Default";
                ChartNN.Series["Series1"].ToolTip = "#VALX [#VALY]";
                ChartNN.Series["Series1"].IsXValueIndexed = false;
                ChartNN.Series["Series1"].IsVisibleInLegend = true;
                ChartNN.ChartAreas["ChartArea1"].AxisX.LabelStyle.Enabled = true;
                ChartNN.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                ChartNN.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineWidth = 0;
                ChartNN.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineWidth = 0;
                counter = 0;
                foreach (DataRow row in GrConc.Rows)
                {
                    ChartNN.Series["Series1"].Points[counter].Url = "~/Subject.aspx?clid=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "&tema=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "&ym=" + row["YMdate"].ToString() + "&fon=0";
                    counter++;
                }
            }
            catch (Exception e)
            {
            }

        }

        private void CreateChartROIAVE()
        {
            lblRoiAve.Visible = true;
            ChartRoiAve.Visible = true;

            TemaCores = Global.InsigteManager[Session.SessionID].CorCliente;
            cores = new String[] { TemaCores, TemaCores, TemaCores, TemaCores, TemaCores };

            try
            {

                //String Month_des = "tm.DES_MONTH_PT";
                //if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                //{
                //    Month_des = "tm.DES_MONTH_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " ";
                //}

                string setLG = "Set language Portuguese ";
                if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                {
                    setLG = "Set language English ";
                }
                
                string cmdQuery = "";
                cmdQuery += setLG;

                cmdQuery += " select x.month_1,sum(x.ave_usd) as num,x.Month from ( ";
                cmdQuery += " select   left(convert(varchar(8),m.date,112),6) as month_1 ";
                cmdQuery += " ,(select AVE_USD from [dbo].[fnCalMediaValue] (m.id))  as ave_usd  ";
                //cmdQuery += " --,(select OTS from [dbo].[fnCalMediaValue] (m.id)) as OTS ";
                cmdQuery += " ,cast(left(UPPER(datename(MONTH,m.date)),1) as varchar) + cast(right(datename(MONTH,m.date), LEN(datename(MONTH,m.date))-1) as varchar) as Month   ";
                cmdQuery += " from metadata m (nolock)   ";
                cmdQuery += " join clientarticles c with(nolock) on c.id = m.id   ";
                cmdQuery += " join editors edt with(nolock) on edt.editorid = m.editorId  ";
                
                cmdQuery += " where cast(c.clientid as nvarchar(4))+cast(subjectid as nvarchar(3)) = " + Global.InsigteManager[Session.SessionID].TemaCliente.ToString();
                cmdQuery += " AND left(convert(varchar(8),m.date,112),6) > left(convert(varchar(8),dateadd(mm,-12,getdate()),112),6) ";

                if (ddpais.SelectedValue != "-1")
                {
                    cmdQuery += " and edt.country = '" + ddpais.SelectedValue + "' ";
                }
                else
                {
                    cmdQuery += " and edt.country in ('Angola','Portugal','Moçambique')";
                }
                cmdQuery += " ) x ";
                cmdQuery += " group by x.month_1, x.Month ";
                cmdQuery += " order by x.month_1 ";

                //cmdQuery += " SELECT TOP 13 year(m.date) as Year, month(m.date) as month_1, " + Month_des + " as Month, COUNT(ca.id) AS num, left(convert(varchar,m.date,112),6) as YMdate ";
                //cmdQuery += " FROM clientarticles ca ";
                //cmdQuery += "	 inner join metadata m ";
                //cmdQuery += "			 on ca.id = m.id ";
                //cmdQuery += "	 inner join time_month tm ";
                //cmdQuery += "			 on convert(varchar,m.date,112) = COD_DAY ";
                //cmdQuery += " WHERE ca.clientid = '" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "'  ";
                //cmdQuery += "  AND ca.subjectid = '" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "'  ";
                //cmdQuery += "  AND m.date >= getdate()-365  ";
                //cmdQuery += " GROUP BY year(m.date),month(m.date), " + Month_des + ", ca.subjectid, left(convert(varchar,m.date,112),6)  ";
                //cmdQuery += " ORDER BY Year, month(m.date)  ";

                //string cmdQuery = "SELECT TOP 13 year(metadata.date) as Year, month(metadata.date) as month_1, DATENAME(mm,metadata.date) as Month, COUNT(clientarticles.id) AS num, left(convert(varchar,metadata.date,112),6) as YMdate ";
                //cmdQuery += "FROM clientarticles, subject, metadata ";
                //cmdQuery += "WHERE iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.id ";
                //cmdQuery += "AND clientarticles.clientid = '" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "' ";
                //cmdQuery += "AND clientarticles.subjectid = '" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "' ";
                //cmdQuery += "AND clientarticles.clientid = subject.clienteid ";
                //cmdQuery += "AND clientarticles.subjectid = subject.subjectid ";
                //cmdQuery += "AND metadata.date >= getdate()-365 ";
                //cmdQuery += "GROUP BY year(metadata.date),month(metadata.date), DATENAME(mm,metadata.date), clientarticles.subjectid,left(convert(varchar,metadata.date,112),6) ";
                //cmdQuery += "ORDER BY Year, month(metadata.date) ";


                //#region Conta val mancha
                //DataTable dt_ValMancha = new DataTable();
                //dt_ValMancha = Global.InsigteManager[Session.SessionID].getTableQuery(cmdQuery, "ValMancha");

                //if (dt_ValMancha.Rows.Count)
                //{
                //    foreach

                //}

                //#endregion

                DataTable GrConc = new DataTable();
                GrConc = Global.InsigteManager[Session.SessionID].getTableQuery(cmdQuery, "GrConc");

                Int32[] yValues = new Int32[GrConc.Rows.Count];
                string[] xValues = new string[GrConc.Rows.Count];


                int counter = 0;
                foreach (DataRow row in GrConc.Rows)
                {
                    yValues[counter] = Convert.ToInt32(row["num"].ToString());
                    xValues[counter] = row["Month"].ToString();
                    counter++;
                }

                ChartRoiAve.Series["Series1"].Points.DataBindXY(xValues, yValues);

                counter = 0;
                foreach (DataRow row in GrConc.Rows)
                {
                    ChartRoiAve.Series["Series1"].Points[counter].Color = ColorTranslator.FromHtml(cores[1]);
                    counter++;
                }

                // Set series chart type
                ChartRoiAve.Series["Series1"].ChartType = SeriesChartType.Column;
                // Set series point width
                ChartRoiAve.Series["Series1"]["PointWidth"] = "0.5";
                // Show data points labels
                ChartRoiAve.Series["Series1"].IsValueShownAsLabel = true;
                // Set data points label style
                ChartRoiAve.Series["Series1"]["BarLabelStyle"] = "Center";
                // Show as 3D
                //Chart1.ChartAreas["Series1"].Area3DStyle.Enable3D = false;
                // Draw as 3D Cylinder
                ChartRoiAve.Series["Series1"]["DrawingStyle"] = "Default";
                ChartRoiAve.Series["Series1"].ToolTip = "#VALX [#VALY]";
                ChartRoiAve.Series["Series1"].IsXValueIndexed = false;
                ChartRoiAve.Series["Series1"].IsVisibleInLegend = true;
                ChartRoiAve.ChartAreas["ChartArea1"].AxisX.LabelStyle.Enabled = true;
                ChartRoiAve.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                ChartRoiAve.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineWidth = 0;
                ChartRoiAve.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineWidth = 0;
                counter = 0;
                foreach (DataRow row in GrConc.Rows)
                {
                    ChartRoiAve.Series["Series1"].Points[counter].Url = "~/Subject.aspx?clid=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "&tema=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "&ym=" + row["YMdate"].ToString() + "&fon=0";
                    counter++;
                }
            }

            catch (Exception e)
            {
            }

        }

        private void CreateChartOTS()
        {
            lblOts.Visible = true;
            ChartOTS.Visible = true;

            TemaCores = Global.InsigteManager[Session.SessionID].CorCliente;
            cores = new String[] { TemaCores, TemaCores, TemaCores, TemaCores, TemaCores };

            try
            {

                //String Month_des = "tm.DES_MONTH_PT";
                //if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                //{
                //    Month_des = "tm.DES_MONTH_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " ";
                //}

                //string setLG = "Set language Portuguese ";
                //if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                //{
                //    setLG = "Set language English ";
                //}
                //string valdef = "32394";
                //string cmdQuery = "";

                //cmdQuery += setLG;
                //cmdQuery += " select SUM(x.num) as num, x.mes as Month from ( ";
                //cmdQuery += " select convert(int, round( count(m.id) * isnull(VAL_CIRCULACAO," + valdef + ") * isnull(VAL_PAIS,3),1)) as num, ";
                //cmdQuery += " datename(month,m.date) as mes, LEFT(convert(varchar(8),m.date,112),6) as anomes ";
                //cmdQuery += " from metadata m with(nolock)  ";
                //cmdQuery += " join editors edt with(nolock) on edt.editorid = m.editorId ";
                //cmdQuery += " join clientarticles c with(nolock) on c.id = m.id ";
                //cmdQuery += " join NW10F_editors_detail e with(nolock) on m.editorId = e.ID_EDITOR ";
                //cmdQuery += " where cast(c.clientid as nvarchar(4))+cast(c.subjectid as nvarchar(3))= " + Global.InsigteManager[Session.SessionID].TemaCliente.ToString();
                //cmdQuery += " AND left(convert(varchar(8),m.date,112),6) > left(convert(varchar(8),dateadd(mm,-12,getdate()),112),6) ";
                //if (ddpais.SelectedValue != "-1")
                //{
                //    cmdQuery += " and edt.country = '" + ddpais.SelectedValue + "' ";
                //}
                //else
                //{
                //    cmdQuery += " and edt.country in ('Angola','Portugal','Moçambique')";
                //}
                //cmdQuery += " group by LEFT(convert(varchar(8),m.date,112),6), datename(month,m.date), isnull(VAL_CIRCULACAO," + valdef + "), isnull(VAL_PAIS,3) ";
                //cmdQuery += " ) x  ";
                //cmdQuery += " group by x.anomes, x.mes ";
                //cmdQuery += " order by x.anomes ";

                string setLG = "Set language Portuguese ";
                if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                {
                    setLG = "Set language English ";
                }

                string cmdQuery = "";
                cmdQuery += setLG;

                cmdQuery += " select x.month_1,sum(x.OTS) as num,x.Month from ( ";
                cmdQuery += " select  left(convert(varchar(8),m.date,112),6) as month_1 ";
               // cmdQuery += " --,(select AVE_USD from [dbo].[fnCalMediaValue] (m.id))  as ave_usd  ";
                cmdQuery += " ,(select OTS from [dbo].[fnCalMediaValue] (m.id)) as OTS ";
                cmdQuery += " ,cast(left(UPPER(datename(MONTH,m.date)),1) as varchar) + cast(right(datename(MONTH,m.date), LEN(datename(MONTH,m.date))-1) as varchar) as Month   ";
                cmdQuery += " from metadata m (nolock)   ";
                cmdQuery += " join clientarticles c with(nolock) on c.id = m.id   ";
                cmdQuery += " join editors edt with(nolock) on edt.editorid = m.editorId  ";

                cmdQuery += " where cast(c.clientid as nvarchar(4))+cast(subjectid as nvarchar(3)) = " + Global.InsigteManager[Session.SessionID].TemaCliente.ToString();
                cmdQuery += " AND left(convert(varchar(8),m.date,112),6) > left(convert(varchar(8),dateadd(mm,-12,getdate()),112),6) ";
                if (ddpais.SelectedValue != "-1")
                {
                    cmdQuery += " and edt.country = '" + ddpais.SelectedValue + "' ";
                }
                else
                {
                    cmdQuery += " and edt.country in ('Angola','Portugal','Moçambique')";
                }
                cmdQuery += " ) x ";
                cmdQuery += " group by x.month_1, x.Month ";
                cmdQuery += " order by x.month_1  ";

                DataTable GrConc = new DataTable();
                GrConc = Global.InsigteManager[Session.SessionID].getTableQuery(cmdQuery, "GrConc");

                Int32[] yValues = new Int32[GrConc.Rows.Count];
                string[] xValues = new string[GrConc.Rows.Count];


                int counter = 0;
                foreach (DataRow row in GrConc.Rows)
                {
                    yValues[counter] = Convert.ToInt32(row["num"].ToString());
                    xValues[counter] = row["Month"].ToString();
                    counter++;
                }

                ChartOTS.Series["Series1"].Points.DataBindXY(xValues, yValues);

                counter = 0;
                foreach (DataRow row in GrConc.Rows)
                {
                    ChartOTS.Series["Series1"].Points[counter].Color = ColorTranslator.FromHtml(cores[2]);
                    counter++;
                }

                // Set series chart type
                ChartOTS.Series["Series1"].ChartType = SeriesChartType.Column;
                // Set series point width
                ChartOTS.Series["Series1"]["PointWidth"] = "0.5";
                // Show data points labels
                ChartOTS.Series["Series1"].IsValueShownAsLabel = true;
                // Set data points label style
                ChartOTS.Series["Series1"]["BarLabelStyle"] = "Center";
                // Show as 3D
                //Chart1.ChartAreas["Series1"].Area3DStyle.Enable3D = false;
                // Draw as 3D Cylinder
                ChartOTS.Series["Series1"]["DrawingStyle"] = "Default";
                ChartOTS.Series["Series1"].ToolTip = "#VALX [#VALY]";
                ChartOTS.Series["Series1"].IsXValueIndexed = false;
                ChartOTS.Series["Series1"].IsVisibleInLegend = true;
                ChartOTS.ChartAreas["ChartArea1"].AxisX.LabelStyle.Enabled = true;
                ChartOTS.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                ChartOTS.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineWidth = 0;
                ChartOTS.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineWidth = 0;
                counter = 0;
                foreach (DataRow row in GrConc.Rows)
                {
                    ChartOTS.Series["Series1"].Points[counter].Url = "~/Subject.aspx?clid=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "&tema=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "&ym=" + row["YMdate"].ToString() + "&fon=0";
                    counter++;
                }
            }
            catch (Exception e)
            {
            }

        }

        private void CreateChartACN5Editors()
        {
            lblEdit.Visible = true;
            ChartEdit.Visible = true;

            TemaCores = Global.InsigteManager[Session.SessionID].CorCliente;
            cores = new String[] { TemaCores, TemaCores, TemaCores, TemaCores, TemaCores };

            try
            {
                string cmdQuery = "SELECT TOP 5 COUNT(clientarticles.id) AS num, metadata.editor As EDITOR, metadata.editorid As EDid  ";
                cmdQuery += "FROM clientarticles with(nolock), subject with(nolock), metadata with(nolock), editors with(nolock) ";
                cmdQuery += "WHERE iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.id ";
                cmdQuery += "AND clientarticles.clientid = '" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "' ";
                cmdQuery += "AND clientarticles.subjectid = '" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "' ";
                cmdQuery += "AND clientarticles.clientid = subject.clienteid ";
                cmdQuery += "AND clientarticles.subjectid = subject.subjectid ";
                cmdQuery += "AND metadata.editorId = editors.editorid ";
                cmdQuery += "AND left(convert(varchar(8),metadata.date,112),6) > left(convert(varchar(8),dateadd(mm,-12,getdate()),112),6) ";

                if (ddpais.SelectedValue != "-1")
                {
                    cmdQuery += " and editors.country = '" + ddpais.SelectedValue + "' ";
                }
                else
                {
                    cmdQuery += " and editors.country in ('Angola','Portugal','Moçambique')";
                }

                cmdQuery += "GROUP BY clientarticles.subjectid, metadata.editor, metadata.editorid ";
                cmdQuery += "ORDER BY num Desc ";


                DataTable GrConc = new DataTable();
                GrConc = Global.InsigteManager[Session.SessionID].getTableQuery(cmdQuery, "GrConc");

                Int32[] yValues = new Int32[GrConc.Rows.Count];
                string[] xValues = new string[GrConc.Rows.Count];
                string[] sEditor = new string[GrConc.Rows.Count];

                int counter = 0;
                foreach (DataRow row in GrConc.Rows)
                {
                    yValues[counter] = Convert.ToInt32(row[0].ToString());
                    xValues[counter] = row[1].ToString();
                    counter++;
                }

                ChartEdit.Series["Series1"].Points.DataBindXY(xValues, yValues);

                counter = 0;
                foreach (DataRow row in GrConc.Rows)
                {
                    ChartEdit.Series["Series1"].Points[counter].Color = ColorTranslator.FromHtml(cores[0]);
                    counter++;
                }

                // Set series chart type
                ChartEdit.Series["Series1"].ChartType = SeriesChartType.Column;
                // Set series point width
                ChartEdit.Series["Series1"]["PointWidth"] = "0.5";
                // Show data points labels
                ChartEdit.Series["Series1"].IsValueShownAsLabel = true;
                // Set data points label style
                ChartEdit.Series["Series1"]["BarLabelStyle"] = "Center";
                // Show as 3D
                ChartEdit.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
                // Draw as 3D Cylinder
                ChartEdit.Series["Series1"]["DrawingStyle"] = "Default";
                //Chart2.Series["Series2"].ToolTip = "#VALX [#VALY]";

                counter = 0;
                foreach (DataRow row in GrConc.Rows)
                {
                    ChartEdit.Series["Series1"].Points[counter].ToolTip = "#VALX [#VALY]";
                    counter++;
                }

                ChartEdit.Series["Series1"].IsXValueIndexed = false;
                ChartEdit.Series["Series1"].IsVisibleInLegend = true;
                ChartEdit.ChartAreas["ChartArea1"].AxisX.LabelStyle.Enabled = true;
                ChartEdit.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineWidth = 0;
                ChartEdit.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineWidth = 0;
                counter = 0;
                foreach (DataRow row in GrConc.Rows)
                {
                    ChartEdit.Series["Series1"].Points[counter].Url = "~/Subject.aspx?clid=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "&tema=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "&ed=" + row["EDid"].ToString() + "&fon=0&dd=365";
                    counter++;
                }
            }
            catch (Exception e)
            {
            }
        }

        private void CreateChartACN5Jornalistas()
        {
            lblJorn.Visible = true;
            ChartJorn.Visible = true;

            TemaCores = Global.InsigteManager[Session.SessionID].CorCliente;
            cores = new String[] { TemaCores, TemaCores, TemaCores, TemaCores, TemaCores };

            try
            {
                string cmdQuery = " ";
                cmdQuery += "SELECT TOP 5 COUNT(ca.id) AS num, ca.subjectid AS ID, s.name AS NAME, left(m.author,charindex(' ',m.author,0)) + substring(m.author,charindex(' ',m.author,0),3) + '.' AS AUTHOR , m.authorid as AUID ";

                cmdQuery += "FROM clientarticles ca with(nolock) ";
                cmdQuery += "     inner join  subject s with(nolock) ";
                cmdQuery += "			 on ca.clientid = s.clienteid  ";
                cmdQuery += "			AND ca.subjectid = s.subjectid ";
                cmdQuery += "     inner join metadata m with(nolock) ";
                cmdQuery += "			 on ca.id = m.id ";
                cmdQuery += " join editors edt with(nolock) on edt.editorid = m.editorId ";
                cmdQuery += "	 inner join authors a with(nolock) ";
                cmdQuery += "			 on m.authorid = a.authorid ";
                cmdQuery += "WHERE ca.clientid = '" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "' AND ca.subjectid = '" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "' ";
                cmdQuery += "  AND left(convert(varchar(8),m.date,112),6) > left(convert(varchar(8),dateadd(mm,-12,getdate()),112),6) ";
                cmdQuery += "  AND m.authorid is not null ";
                cmdQuery += "  AND len(rtrim(ltrim(m.authorid)))>0 ";
                if (ddpais.SelectedValue != "-1")
                {
                    cmdQuery += " and edt.country = '" + ddpais.SelectedValue + "' ";
                }
                else
                {
                    cmdQuery += " and edt.country in ('Angola','Portugal','Moçambique')";
                }
                cmdQuery += " GROUP BY ca.subjectid, s.name, left(m.author,charindex(' ',m.author,0)) + substring(m.author,charindex(' ',m.author,0),3) + '.', m.authorid  ";

                cmdQuery += " ORDER BY num DESC ";


                DataTable GrConc2 = new DataTable();
                GrConc2 = Global.InsigteManager[Session.SessionID].getTableQuery(cmdQuery, "GrConc2");

                Int32[] yValues2 = new Int32[GrConc2.Rows.Count];
                string[] xValues2 = new string[GrConc2.Rows.Count];

                int counter = 0;
                foreach (DataRow row in GrConc2.Rows)
                {
                    yValues2[counter] = Convert.ToInt32(row[0].ToString());
                    xValues2[counter] = row[3].ToString();
                    counter++;
                }

                ChartJorn.Series["Series2"].Points.DataBindXY(xValues2, yValues2);

                counter = 0;
                foreach (DataRow row in GrConc2.Rows)
                {
                    ChartJorn.Series["Series2"].Points[counter].Color = ColorTranslator.FromHtml(cores[3]);
                    counter++;
                }

                //ChartJorn.Series["Series2"].Color = Color.FromArgb(0, 63, 86);
                // Set series chart type
                ChartJorn.Series["Series2"].ChartType = SeriesChartType.Column;
                // Set series point width
                ChartJorn.Series["Series2"]["PointWidth"] = "0.5";
                // Show data points labels
                ChartJorn.Series["Series2"].IsValueShownAsLabel = true;
                // Set data points label style
                ChartJorn.Series["Series2"]["BarLabelStyle"] = "Center";
                // Show as 3D
                ChartJorn.ChartAreas["ChartArea2"].Area3DStyle.Enable3D = false;
                // Draw as 3D Cylinder
                ChartJorn.Series["Series2"]["DrawingStyle"] = "Default";
                //Chart2.Series["Series2"].ToolTip = "#VALX [#VALY]";

                counter = 0;
                foreach (DataRow row in GrConc2.Rows)
                {
                    ChartJorn.Series["Series2"].Points[counter].ToolTip = "#VALX [#VALY]";
                    counter++;
                }

                ChartJorn.Series["Series2"].IsXValueIndexed = false;
                ChartJorn.Series["Series2"].IsVisibleInLegend = true;
                ChartJorn.ChartAreas["ChartArea2"].AxisX.LabelStyle.Enabled = true;
                ChartJorn.ChartAreas["ChartArea2"].AxisX.MajorGrid.LineWidth = 0;
                ChartJorn.ChartAreas["ChartArea2"].AxisY.MajorGrid.LineWidth = 0;
                counter = 0;
                foreach (DataRow row in GrConc2.Rows)
                {
                    ChartJorn.Series["Series2"].Points[counter].Url = "~/Subject.aspx?clid=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "&tema=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "&at=" + row["AUID"].ToString() + "&fon=0&dd=365";
                    counter++;
                }
            }
            catch (Exception e)
            {
            }
        }

        private void CreateChartACNConcorrencia(String subjectid_Concorrencia)
        {
            LBConco.Visible = true;
            ChartConco.Visible = true;

            TemaCores = Global.InsigteManager[Session.SessionID].CorCliente;
            cores = new String[] { TemaCores, TemaCores, TemaCores, TemaCores, TemaCores };

            try
            {
                string cmdQuery = "SELECT COUNT(clientarticles.id) AS num, clientarticles.subjectid AS ID, subject.name AS NAME,clientarticles.clientid as CLID, clientarticles.subjectid as SBID, subject.tema_cores as COR ";
                //cmdQuery += " ,case cast(clientarticles.clientid as varchar) + cast(clientarticles.subjectid as varchar) when '" + Global.InsigteManager[Session.SessionID].TemaCliente + "' then 0 else 1 end as client ";
                cmdQuery += " FROM clientarticles with(nolock), subject with(nolock), metadata with(nolock), editors with(nolock) ";
                cmdQuery += " WHERE iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.id ";
                // Siemens Concorrência
                if (Global.InsigteManager[Session.SessionID].IdClient == "1037")
                {
                    cmdQuery += " AND clientarticles.clientid = '1017' ";
                    cmdQuery += " AND clientarticles.subjectid IN (131,109,172,173,174,175) ";
                }
                else
                {
                    //cmdQuery += " AND cast(clientarticles.clientid as varchar) + cast(clientarticles.subjectid as varchar) in  ('1095101') ";
                    cmdQuery += " AND ( ( clientarticles.clientid = '" + subjectid_Concorrencia + "' ";
                    cmdQuery += " AND clientarticles.subjectid IN (" + Concorrencia + ")  )";
                    cmdQuery += " or ( clientarticles.clientid = '" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "' ";
                    cmdQuery += " AND clientarticles.subjectid IN (" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + ") ) ) ";
                }
                //cmdQuery += "AND clientarticles.clientid = '" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "' ";
                cmdQuery += " AND clientarticles.clientid = subject.clienteid ";
                cmdQuery += " AND clientarticles.subjectid = subject.subjectid ";
                cmdQuery += " AND metadata.editorId = editors.editorid ";
                cmdQuery += " AND left(convert(varchar(8),metadata.date,112),6) > left(convert(varchar(8),dateadd(mm,-12,getdate()),112),6) ";
                if (ddpais.SelectedValue != "-1")
                {
                    cmdQuery += " and editors.country = '" + ddpais.SelectedValue + "' ";
                }
                else
                {
                    cmdQuery += " and editors.country in ('Angola','Portugal','Moçambique') ";
                }
                cmdQuery += " GROUP BY  clientarticles.subjectid, subject.name,clientarticles.clientid, clientarticles.subjectid, subject.tema_cores ";
                cmdQuery += " ORDER BY num DESC, id ";

                //lblDataToday.Visible = true;
                //lblDataToday.Text = cmdQuery;

                DataTable GrConc = new DataTable();
                GrConc = Global.InsigteManager[Session.SessionID].getTableQuery(cmdQuery, "GrConc");

                Int32[] yValues = new Int32[GrConc.Rows.Count];
                string[] xValues = new string[GrConc.Rows.Count];

                int counter = 0;
                foreach (DataRow row in GrConc.Rows)
                {
                    yValues[counter] = Convert.ToInt32(row[0].ToString());
                    xValues[counter] = row[2].ToString();
                    counter++;
                }

                ChartConco.Series["Series1"].Points.DataBindXY(xValues, yValues);

                counter = 0;
                foreach (DataRow row in GrConc.Rows)
                {
                    if (Global.InsigteManager[Session.SessionID].IdClient == "1017")
                    {
                        ChartConco.Series["Series1"].Points[counter].Color = ColorTranslator.FromHtml(row["COR"].ToString());
                    }
                    else
                    {

                        //if (Global.InsigteManager[Session.SessionID].IdClient == "1099")
                        //{
                        Color colour = ColorTranslator.FromHtml(cores[4]);
                        Color transparent = Color.FromArgb(200, colour);

                        //if (row["client"].ToString() == "0")
                        if ((row["CLID"].ToString() + row["SBID"].ToString()) == Global.InsigteManager[Session.SessionID].TemaCliente)
                            ChartConco.Series["Series1"].Points[counter].Color = colour;
                        else
                            ChartConco.Series["Series1"].Points[counter].Color = transparent;
                        //}
                        //else
                        //{ ChartConco.Series["Series1"].Points[counter].Color = ColorTranslator.FromHtml(cores[4]); }

                    }
                    counter++;
                }

                // Set series chart type
                ChartConco.Series["Series1"].ChartType = SeriesChartType.Column;
                // Set series point width
                ChartConco.Series["Series1"]["PointWidth"] = "0.5";
                // Show data points labels
                ChartConco.Series["Series1"].IsValueShownAsLabel = true;
                // Set data points label style
                ChartConco.Series["Series1"]["BarLabelStyle"] = "Center";
                // Show as 3D
                //Chart1.ChartAreas["Series1"].Area3DStyle.Enable3D = false;
                // Draw as 3D Cylinder
                ChartConco.Series["Series1"]["DrawingStyle"] = "Default";
                ChartConco.Series["Series1"].ToolTip = "#VALX [#VALY]";
                ChartConco.Series["Series1"].IsXValueIndexed = false;
                ChartConco.Series["Series1"].IsVisibleInLegend = true;
                ChartConco.ChartAreas["ChartArea1"].AxisX.LabelStyle.Enabled = true;
                ChartConco.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                ChartConco.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineWidth = 0;
                ChartConco.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineWidth = 0;


                counter = 0;
                foreach (DataRow row in GrConc.Rows)
                {
                    ChartConco.Series["Series1"].Points[counter].Url = "~/Subject.aspx?clid=" + row["CLID"].ToString() + "&tema=" + row["SBID"].ToString() + "&fon=0&dd=365";
                    counter++;
                }
            }
            catch (Exception e)
            {
            }

        }

        private void CreateChartPie()
        {
            ChartMerc.Visible = true;
            lblMerc.Visible = true;

            TemaCores = Global.InsigteManager[Session.SessionID].CorCliente;
            cores = new String[] { TemaCores, TemaCores, TemaCores, TemaCores, TemaCores };

            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                string cmdQuery = "SELECT COUNT(clientarticles.id) AS num ";
                cmdQuery += "FROM clientarticles with(nolock), subject with(nolock), metadata with(nolock), editors with(nolock) ";
                cmdQuery += "WHERE iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.id ";
                cmdQuery += "AND clientarticles.clientid = '1084' ";
                cmdQuery += "AND clientarticles.clientid = subject.clienteid ";
                cmdQuery += "AND clientarticles.subjectid = subject.subjectid ";
                cmdQuery += "AND metadata.editorId = editors.editorid ";
                cmdQuery += "AND left(convert(varchar(8),metadata.date,112),6) > left(convert(varchar(8),dateadd(mm,-12,getdate()),112),6) ";
                if (ddpais.SelectedValue != "-1")
                {
                    cmdQuery += " and editors.country = '" + ddpais.SelectedValue + "' ";
                }

                SqlCommand cmd = new SqlCommand(cmdQuery);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();

                Int32[] yValues = new Int32[2];
                string[] xValues = new string[2];

                int counter = 0;

                while (reader.Read())
                {
                    yValues[counter] = Convert.ToInt32(reader.GetValue(0).ToString());
                    xValues[counter] = "Angola";

                    counter++;
                }

                cmd.Connection.Close();
                cmd.Connection.Dispose();

                string cmdQuery2 = "SELECT COUNT(clientarticles.id) AS num ";
                cmdQuery2 += "FROM clientarticles with(nolock), subject with(nolock), metadata with(nolock), editors with(nolock) ";
                cmdQuery2 += "WHERE iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.id ";
                cmdQuery2 += "AND clientarticles.clientid = '1014' ";
                cmdQuery2 += "AND clientarticles.clientid = subject.clienteid ";
                cmdQuery2 += "AND clientarticles.subjectid = subject.subjectid ";
                cmdQuery2 += " AND metadata.editorId = editors.editorid ";
                cmdQuery2 += "AND left(convert(varchar(8),metadata.date,112),6) > left(convert(varchar(8),dateadd(mm,-12,getdate()),112),6) ";
                if (ddpais.SelectedValue != "-1")
                {
                    cmdQuery2 += " and editors.country = '" + ddpais.SelectedValue + "' ";
                }

                SqlConnection conn2 = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
                conn2.Open();
                SqlCommand cmd2 = new SqlCommand(cmdQuery2);
                cmd2.Connection = conn2;
                cmd2.CommandType = CommandType.Text;

                SqlDataReader reader2 = cmd2.ExecuteReader();

                while (reader2.Read())
                {
                    yValues[counter] = Convert.ToInt32(reader2.GetValue(0).ToString());
                    xValues[counter] = "Portugal";

                    counter++;
                }

                counter = 0;

                ChartMerc.Series["Series2"].Points.DataBindXY(xValues, yValues);

                ChartMerc.Series["Series2"].ChartType = SeriesChartType.Pie;// Set the Pie width
                ChartMerc.Series["Series2"].Points[0].Color = Color.FromArgb(41, 90, 27);
                ChartMerc.Series["Series2"].Points[1].Color = Color.FromArgb(207, 200, 197);
                ChartMerc.Series["Series2"]["PointWidth"] = "0.5";// Show data points labels
                ChartMerc.Series["Series2"].IsValueShownAsLabel = true;// Set data points label style
                ChartMerc.Series["Series2"].Label = "#VALY";
                ChartMerc.Series["Series2"]["BarLabelStyle"] = "Center";// Show chart as 3D
                ChartMerc.ChartAreas["ChartArea2"].Area3DStyle.Enable3D = false;// Draw chart as 3D 
                ChartMerc.Series["Series2"]["DrawingStyle"] = "Cylinder";
                //Chart1.Legends[0].Enabled = false;

                ChartMerc.Legends.Add("Legend1");
                ChartMerc.Legends[0].Enabled = true;
                ChartMerc.Legends[0].Docking = Docking.Bottom;
                ChartMerc.Legends[0].Alignment = System.Drawing.StringAlignment.Center;

                // Show labels in the legend in the format "Name (### %)"
                ChartMerc.Series[0].LegendText = "#VALX [#VALY] (#PERCENT)";

                ChartMerc.Series["Series2"].ToolTip = "#VALX [#PERCENT]";

                ChartMerc.Series["Series2"].Points[0].Url = "~/Subject.aspx?clid=1084&tema=000";//Angola
                ChartMerc.Series["Series2"].Points[1].Url = "~/Subject.aspx?clid=1014&tema=000";//Portugal

            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }
        }

        protected void export_Click(object sender, EventArgs e)
        {

            //Document pdfDoc = new Document(PageSize.A4.Rotate(), 10f, 10f, 10f, 10f);
            //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            //pdfDoc.Open();

            try
            {
                string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                string path = @"G:\pdf\" + Global.InsigteManager[Session.SessionID].IdClient.ToString() + "\\";
                String PDFFinal = "Dashboard_" + timestamp + ".pdf";
                String PDFFinalpath = path + PDFFinal;


                var Doc = new Document(PageSize.A4, 10f, 10f, 10f, 10f);
                //PdfWriter.GetInstance(Doc, Response.OutputStream);
                PdfWriter.GetInstance(Doc, new FileStream(PDFFinal, FileMode.Create));
                Doc.Open();

                Doc.Add(new Paragraph("Este es mi primer PDF al vuelo"));


                //using (MemoryStream memoryStream = new MemoryStream())
                //{
                //    ChartConco.SaveImage(memoryStream, ChartImageFormat.Png);
                //    iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(memoryStream.GetBuffer());
                //    img.ScalePercent(75f);
                //    Doc.Add(img);

                //}



                //img.SetAbsolutePosition(526f, 795f);
                //Doc.Add(img);

                Doc.Close();

                try
                {
                    //System.IO.Directory.CreateDirectory(path);

                    

                    //Response.Clear();
                    //Response.ContentType = "application/pdf";
                    //Response.AddHeader("Content-Disposition", "attachment; filename=\"" + PDFFinal + "\"");
                    //Response.WriteFile(PDFFinal);
                    //Response.End();
                    Response.Clear();
                    Response.ContentType = "application/octet-stream";
                    Response.BufferOutput = true;
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + PDFFinal);
                    Response.AddHeader("Content-Length", PDFFinal.Length.ToString());

                    Response.TransmitFile(PDFFinalpath);
                    Response.Flush();
                }
                catch (Exception teste)
                {
                    lblDataToday.Visible = true;
                    lblDataToday.Text = "Morangos : " + teste.Message.ToString();
                }

                //Response.ContentType = "application/pdf";
                //Response.AddHeader("content-disposition", "attachment;filename=Chart.pdf");
                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //Response.Write(Doc);

                //Response.End();
            }
            catch (Exception ex)
            {
                ex.Message.ToString();

            }
        }
        private iTextSharp.text.Image ChartToImg(Chart chart)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                chart.SaveImage(memoryStream, ChartImageFormat.Png);
                iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(memoryStream.GetBuffer());
                return img;
            }
        }

    }
}