﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using APToolkitNET;

namespace Insigte
{
    public partial class ListAdd : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        protected void btAdicionar_Click(object sender, EventArgs e)
        {

            String Query = " insert into dbo.CL10D_LISTS (ID_CLIENT, ID_CLIENT_USER, NOM_LIST, DES_LIST, DAT_CRIACAO, DAT_MODIFICACAO) ";
            Query += " select " + Global.InsigteManager[Session.SessionID].IdClient + ", " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + ",'" + NomePasta.Text + "', '" + tbNota.Text + "', getdate(), getdate() ";

            DataTable insList = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "InsertList");

            //tbNota.Text = Query;

            Response.Redirect("listscfg.aspx");
            //Response.Write("<script language='javascript'> { self.close() }</script>");

        }

        protected void cancelar_click(object sender, EventArgs e)
        {
            Response.Redirect("listscfg.aspx");
        }


    }
}