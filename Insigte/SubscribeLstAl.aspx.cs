﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using INInsigteManager;

namespace Insigte
{
    public partial class SubscribeLstAl : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            rp_AlertsDisp.PagerStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[0].ToString());
            rp_AlertsDisp.HeaderStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[1].ToString());
            

            //user not loged
            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            //user without acess
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(1, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }

            if (!Page.IsPostBack)
            {
                FillRepeater();
                getChecked();
            }
        }

        //temas que podem escolher
        protected void FillRepeater()
        {
            string query = "";
            query += " select * from CL10H_CLIENT_USER_ALERTS cua with(nolock) ";
            query += " inner join subject s with(nolock) on cast(s.clienteid as varchar) +  cast(s.subjectid as varchar) =  cast(cua.COD_ALERT_TEMAS as varchar) ";
            query += " where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " and ID_CLIENT_USER = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString();
            //lblDataToday.Text = query;
            try
            {
                DataTable dt = Global.InsigteManager[Session.SessionID].getTableQuery(query, "GetTemasToAlerts");

                rp_AlertsDisp.DataSource = dt.DefaultView;
                rp_AlertsDisp.DataBind();

            }
            catch (Exception erro)
            {
                //lblDataToday.Visible = false;
                //lblDataToday.Text = erro.Message.ToString();
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {          
            foreach (DataGridItem drv in rp_AlertsDisp.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
                if (chk.Checked)// adicionar ha tabela
                {
                    #region if Checked

                    string query = "";
                    query += " select * from clientmailonfly with(nolock) ";
                    query += " where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient.ToString();
                    query += " and clienteid = " + chk.Attributes["NewsId"].ToString().Substring(0, 4);
                    query += " and subjectid = " + chk.Attributes["NewsId"].ToString().Substring(4, 3);

                    DataTable dt = Global.InsigteManager[Session.SessionID].getTableQuery(query, "GetAlerts");

                    if (dt.Rows.Count.Equals(1))
                    {
                        //exite row
                        string strEmailList = dt.Rows[0]["email"].ToString();
                        //check se email consta na coluna email
                        if (!strEmailList.Contains(Global.InsigteManager[Session.SessionID].inUser.Email))
                        {//Nao consta

                            string querye = "";
                            querye += " update clientmailonfly ";
                            querye += " set email = '" + strEmailList.Trim() + "," + Global.InsigteManager[Session.SessionID].inUser.Email.Trim() + "'";
                            querye += " where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient.ToString();
                            querye += " and clienteid = " + chk.Attributes["NewsId"].ToString().Substring(0, 4);
                            querye += " and subjectid = " + chk.Attributes["NewsId"].ToString().Substring(4, 3);

                            try
                            {
                                DataTable dtUpd = Global.InsigteManager[Session.SessionID].getTableQuery(querye, "updMailOnFly");
                            }
                            catch (Exception)
                            {

                                throw;
                            }
                        }
                    }
                    else
                    {//nao existe row
                        string queryins = "";
                        queryins += " insert into clientmailonfly ";
                        queryins += " (clienteid, subjectid, email, emailsubject, emailkeywords, ID_CLIENT) ";
                        queryins += " values ";
                        queryins += " ( " + chk.Attributes["NewsId"].ToString().Substring(0, 4);
                        queryins += " , " + chk.Attributes["NewsId"].ToString().Substring(4, 3);
                        queryins += " , 'janetebento@insigte.com," + Global.InsigteManager[Session.SessionID].inUser.Email.Trim() + "'";
                        queryins += " , '" + chk.Attributes["NewsName"].ToString() + " | Email Alert'";
                        queryins += " , '" + chk.Attributes["NewsName"].ToString().ToLower() + "'";
                        queryins += " , " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " ) ";

                        try
                        {
                            DataTable dtins = Global.InsigteManager[Session.SessionID].getTableQuery(queryins, "insMailOnFly");
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                    #endregion
                }
                else
                {
                    #region if not Checked

                    string query = "";
                    query += " select * from clientmailonfly with(nolock) ";
                    query += " where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient.ToString();
                    query += " and clienteid = " + chk.Attributes["NewsId"].ToString().Substring(0, 4);
                    query += " and subjectid = " + chk.Attributes["NewsId"].ToString().Substring(4, 3);

                    DataTable dt = Global.InsigteManager[Session.SessionID].getTableQuery(query, "GetAlerts");

                    if (dt.Rows.Count.Equals(1))
                    {
                        //exite row
                        string strEmailList = dt.Rows[0]["email"].ToString();
                        //check se email consta na coluna email
                        if (strEmailList.Contains(Global.InsigteManager[Session.SessionID].inUser.Email))
                        {//remove email

                            string querye = "";
                            querye += " update clientmailonfly ";
                            querye += " set email = '" + strEmailList.Trim().Replace(","+Global.InsigteManager[Session.SessionID].inUser.Email.Trim(),"") + "'";
                            querye += " where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient.ToString();
                            querye += " and clienteid = " + chk.Attributes["NewsId"].ToString().Substring(0, 4);
                            querye += " and subjectid = " + chk.Attributes["NewsId"].ToString().Substring(4, 3);

                            try
                            {
                                DataTable dtUpd = Global.InsigteManager[Session.SessionID].getTableQuery(querye, "updMailOnFly");
                            }
                            catch (Exception)
                            {

                                throw;
                            }
                        }
                    }
                   
                    #endregion
                }
            }
        }

        protected void getChecked()
        {
            try
            {
                foreach (DataGridItem drv in rp_AlertsDisp.Items)
                {
                    CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
                    string x = chk.Attributes["NewsId"].ToString();
                    string query = "";

                    query += " select * from clientmailonfly with(nolock) ";
                    query += " where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient.ToString();
                    query += " and clienteid = " + chk.Attributes["NewsId"].ToString().Substring(0, 4);
                    query += " and subjectid = " + chk.Attributes["NewsId"].ToString().Substring(4, 3);

                    DataTable dt = Global.InsigteManager[Session.SessionID].getTableQuery(query, "check");

                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow drow in dt.Rows)
                        {
                            chk.Checked = drow["email"].ToString().Contains(Global.InsigteManager[Session.SessionID].inUser.Email) ? true : false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void chkSelAll_changed(object sender, EventArgs e)
        {
            CheckBox chk2 = (CheckBox)sender;
            //CheckBox chk1 = (CheckBox)dgPasta.FindControl("chkSelAll");
            foreach (DataGridItem drv in rp_AlertsDisp.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");

                chk.Checked = chk2.Checked;
            }
        }
    }
}