﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace Insigte
{
    public partial class login : BasePage
    {
        string info = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CultureInfo userInfo;

            try
            {

                userInfo = CultureInfo.CreateSpecificCulture(Request.UserLanguages[0]);

            }

            catch
            {

                userInfo = CultureInfo.CurrentCulture;

            }


            info = userInfo.TwoLetterISOLanguageName;
            if (info.Equals("pt"))
            {
                TrataSiteLangPT();
            }
            else
            {
                TrataSiteLangEN();
            }

            //endereco();

        }

        //protected string endereco()
        //{
        //    Uri intraclient = Request.UrlReferrer;
        //    try
        //    {
        //        string endereco = intraclient.ToString();

        //        //if (endereco.Substring(11, 7) == "insigte")
        //        //{
        //        //    Global.InsigteManager[Session.SessionID].Login("intranet@pwc", "intrapwc162");
        //        //    Response.Redirect("Default.aspx");
        //        //}

        //        if (endereco.Substring(8, 81) == "pwc-spark.com/groups/internal-portal-pwc-portugal/projects/external-research-news")
        //            {
        //            Global.InsigteManager[Session.SessionID].Login("intranet@pwc", "intrapwc162");
        //            Response.Redirect("Default.aspx");
        //            }

        //        return endereco;

        //    }
        //    catch 
        //    {
        //        return null;
        //    }
        //}
        
        private void TrataSiteLangPT()
        {
            FormTitlea.InnerText = "Clientes";
            FormUsera.InnerText = "utilizador";
            FormPWa.InnerText = "código";
            //DescTitle1a.InnerText = "media intelligence company";
            //DescTitle2a.InnerText = "collecting small pieces to deliver the Big Picture";
            //Adicionados DescTitle3-5 para nova página login_carma.aspx
            DescTitle3a.InnerText = "Insigte now is Carma";
            DescTitle4a.InnerText = "Delivering what matters";
            DescTitle5a.InnerText = "Saiba mais";

            DescTitleTeca.InnerText = "Nova tecnologia, novos INSIGTHS";
            TecTitle1a.InnerText = "REAL TIME";
            TecText1a.InnerText = "Recepção de informação em tempo real de forma a permitir uma atitude proactiva, eficaz e assertiva";
            TecTitle2a.InnerText = "EARLY WARNING";
            TecText2a.InnerText = "Entender as tendências, antecipar as necessidades e monitorizar o ambiente competitivo";
            TecTitle3a.InnerText = "PERFORMANCE";
            TecText3a.InnerText = "Partilhar o retorno financeiro das acções de comunicação, as pessoas atingidas e o share mediático";
            TecTitle4a.InnerText = "BENCHMARK";
            TecText4a.InnerText = "Conhecer a performance mediática de cada concorrente e identificar as acções";
            TecTitle5a.InnerText = "BRANDMINING";
            TecText5a.InnerText = "Analisar o comportamento da marca, defender a reputação e realinhar o planeamento estratégico";
            TecTitle6a.InnerText = "GEO-MKT";
            TecText6a.InnerText = "Conhecer a performance geográfica da organização, em cada província e o respectivo benchmark";
            TecTitle7a.InnerText = "MULTI-FORMAT";
            TecText7a.InnerText = "Plataforma acessível através de qualquer dispositivo, seja o computador, o tablet ou o telefone";
            TecTitle8a.InnerText = "SHARE";
            TecText8a.InnerText = "Partilhar os factos relevantes por toda a organização de forma a gerar valor";
            DescTitleOuta.InnerText = "OUTPUTS que geram INSIGTHS";
            OutTitle1a.InnerText = "MANAGEMENT";
            OutText1a.InnerHtml = "Sumário Executivo<br />Diagnóstico reputacional<br />Relatório de media<br />Relatório BI<br />";
            OutTitle2a.InnerText = "MKT & COMM";
            OutText2a.InnerHtml = "Sumário Executivo<br />Diagnóstico reputacional<br />Relatório de media<br />Relatório BI<br />Newsletter & Alertas<br />Portal";
            OutTitle3a.InnerText = "NEW BUSINESS";
            OutText3a.InnerHtml = "Sumário Executivo<br / >Newsletter<br />Alertas | Prospects<br />";
            OutTitle4a.InnerText = "CLIENT CARE";
            OutText4a.InnerHtml = "Sumário Executivo<br />Newsletter<br />Alertas | Clientes";
            OutTitle5a.InnerText = "RESEARCH";
            OutText5a.InnerHtml = "Sumário Executivo<br />Diagnóstico reputacional<br />Newsletter<br />Relatório BI<br />Portal";
            OutTitle6a.InnerText = "TECHNOLOGY";
            OutText6a.InnerHtml = "Sumário Executivo<br />Newsletter | TECH";
            OutTitle7a.InnerText = "PROJECTS";
            OutText7a.InnerHtml = "Sumário Executivo<br />Newsletter<br />Alertas | Project";
            OutTitle8a.InnerText = "GLOBAL";
            OutText8a.InnerHtml = "Sumário Executivo<br />Newsletter diária<br />Alertas<br />Research";
            CountryName1a.InnerText = "Angola";
            CountryName2a.InnerText = "Cabo Verde";
            CountryName3a.InnerText = "Moçambique";
            CountryName4a.InnerText = "Portugal";
            CountryName5a.InnerText = "África do Sul";
            CountryName6a.InnerText = "Namíbia";
        }

        private void TrataSiteLangEN()
        {
            FormTitlea.InnerText = "Login";
            FormUsera.InnerText = "User";
            FormPWa.InnerText = "Password";
            //DescTitle1a.InnerText = "media intelligence company";
            //DescTitle2a.InnerText = "collecting small pieces to deliver the Big Picture";
            //Adicionados DescTitle3-5 para nova página login_carma.aspx
            DescTitle3a.InnerText = "Insigte now is Carma";
            DescTitle4a.InnerText = "Delivering what matters";
            DescTitle5a.InnerText = "Know more";

            DescTitleTeca.InnerText = "New technology, new INSIGTHS";
            TecTitle1a.InnerText = "REAL TIME";
            TecText1a.InnerText = "Receiving information in real time so as to allow proactive, effective and assertive";
            TecTitle2a.InnerText = "EARLY WARNING";
            TecText2a.InnerText = "Understand the trends, anticipate needs and monitor the competitive environment";
            TecTitle3a.InnerText = "PERFORMANCE";
            TecText3a.InnerText = "Share the financial return of publicity measures, the affected people and the media share";
            TecTitle4a.InnerText = "BENCHMARK";
            TecText4a.InnerText = "Knowing the media performance of each competitor and identify actions";
            TecTitle5a.InnerText = "BRANDMINING";
            TecText5a.InnerText = "Analyze the behavior of the brand, defend the reputation and realign the strategic planning";
            TecTitle6a.InnerText = "GEO-MKT";
            TecText6a.InnerText = "Knowing the geographical performance of the organization in each province and its benchmark";
            TecTitle7a.InnerText = "MULTI-FORMAT";
            TecText7a.InnerText = "Accessible platform through any device, be it computer, tablet or phone";
            TecTitle8a.InnerText = "SHARE";
            TecText8a.InnerText = "Share the relevant facts throughout the organization in order to generate value";
            DescTitleOuta.InnerText = "OUTPUTS generating INSIGTHS";
            OutTitle1a.InnerText = "MANAGEMENT";
            OutText1a.InnerHtml = "Executive Summary<br />Reputational Diagnosis <br />Media report<br />BI Report<br />";
            OutTitle2a.InnerText = "MKT & COMM";
            OutText2a.InnerHtml = "Executive Summary<br />Reputational Diagnosis<br />Media report<br />BI Report <br />Newsletter & Alerts<br />Portal";
            OutTitle3a.InnerText = "NEW BUSINESS";
            OutText3a.InnerHtml = "Executive Summary<br />Newsletter<br />Alerts | Prospects<br />";
            OutTitle4a.InnerText = "CLIENT CARE";
            OutText4a.InnerHtml = "Executive Summary<br />Newsletter<br />Alerts | Customers<br />";
            OutTitle5a.InnerText = "RESEARCH";
            OutText5a.InnerHtml = "Executive Summary<br />reputational Diagnostic<br />Newsletter<br />BI Report<br />Portal";
            OutTitle6a.InnerText = "TECHNOLOGY";
            OutText6a.InnerHtml = "Executive Summary<br />Newsletter | TECH";
            OutTitle7a.InnerText = "PROJECTS";
            OutText7a.InnerHtml = "Executive Summary<br />Newsletter<br />Alerts | Project";
            OutTitle8a.InnerText = "GLOBAL";
            OutText8a.InnerHtml = "Executive Summary<br />daily Newsletter<br />Alerts<br />Research";
            CountryName1a.InnerText = "Angola";
            CountryName2a.InnerText = "Cape Verde";
            CountryName3a.InnerText = "Mozambique";
            CountryName4a.InnerText = "Portugal";
            CountryName5a.InnerText = "South Africa";
            CountryName6a.InnerText = "Namibia";

        }

        public static CultureInfo ResolveCulture()
        {

            string[] languages = HttpContext.Current.Request.UserLanguages;



            if (languages == null || languages.Length == 0)

                return null;



            try
            {

                string language = languages[0].ToLowerInvariant().Trim();

                return CultureInfo.CreateSpecificCulture(language);

            }

            catch (ArgumentException)
            {

                return null;

            }

        }

        protected void timerAO_Tick(object sender, EventArgs e)
        {
            //lb_tmAo.Text = AoTime();
            //lb_tmMz.Text = MzTime();
            //lb_tmPt.Text = PtTime();

            //lb_tmAS.Text = MzTime();
            //lb_tmNB.Text = MzTime();
        }

        protected String PtTime()
        {

            var remoteTimeZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");
            var remoteTime = TimeZoneInfo.ConvertTime(DateTime.Now, remoteTimeZone);

            string seg = remoteTime.Second.ToString().Length > 1 ? remoteTime.Second.ToString() : "0" + remoteTime.Second.ToString();

            return remoteTime.ToShortTimeString() + ":" + seg;
            //return "Time in " + remoteTimeZone.Id + " is " + remoteTime.TimeOfDay.ToString();
        }

        protected String MzTime()
        {

            var remoteTimeZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");
            var remoteTime = TimeZoneInfo.ConvertTime(DateTime.Now, remoteTimeZone);

            string seg = remoteTime.Second.ToString().Length > 1 ? remoteTime.Second.ToString() : "0" + remoteTime.Second.ToString();

            return remoteTime.AddHours(2).ToShortTimeString() + ":" + seg;
            //return "Time in " + remoteTimeZone.Id + " is " + remoteTime.TimeOfDay.ToString();
        }

        protected String AoTime()
        {

            var remoteTimeZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");
            var remoteTime = TimeZoneInfo.ConvertTime(DateTime.Now, remoteTimeZone);

            string seg = remoteTime.Second.ToString().Length > 1 ? remoteTime.Second.ToString() : "0" + remoteTime.Second.ToString();

            return remoteTime.AddHours(1).ToShortTimeString() + ":" + seg;
            //return "Time in " + remoteTimeZone.Id + " is " + remoteTime.TimeOfDay.ToString();
        }

        protected String getTop5AO()
        {

            String topStr = "";
            String SQLquery = "";
            //foreach (var pair in Global.InsigteManager)
            //{
            //    topStr += "Session : " + pair.Key.ToString() + "<br>";
            //    topStr += "ID Adr : " + pair.Value.inUser.IPAddress + "<br>";
            //    topStr += "ID Lan Adr : " + pair.Value.inUser.IPLanAddress + "<br>";
            //    topStr += "Nome : " + pair.Value.inUser.Name + "<br>";
            //    topStr += "Cod : " + pair.Value.CodUser + "<br>";
            //}


            //String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata with(nolock)  ";
            //SQLquery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            //SQLquery += " WHERE clientarticles.clientid='1084' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            SQLquery += " SELECT TOP 5 m.TITLE"+ ( info.Equals("pt") ? "" :"_EN" ) + " as TITLE, m.EDITOR, m.DATE, m.page_url ";
            SQLquery += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles  ca with(nolock) ON ca.id = m.ID ";
            SQLquery += " WHERE ca.clientid='1084'  and isnull(page_url,'') != ''  ";
            SQLquery += " and m.cod_dia_insert >= convert(varchar(8),getdate()-3,112) ";
            SQLquery += " and isnull(m.title_en,'') <> '' ";
            SQLquery += " order by m.insert_date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri,Arial' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri,Arial' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }

            return topStr;
        }

        protected String getTop5NB()
        {

            String topStr = "";
            String SQLquery = "";
            //foreach (var pair in Global.InsigteManager)
            //{
            //    topStr += "Session : " + pair.Key.ToString() + "<br>";
            //    topStr += "ID Adr : " + pair.Value.inUser.IPAddress + "<br>";
            //    topStr += "ID Lan Adr : " + pair.Value.inUser.IPLanAddress + "<br>";
            //    topStr += "Nome : " + pair.Value.inUser.Name + "<br>";
            //    topStr += "Cod : " + pair.Value.CodUser + "<br>";
            //}


            //String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata with(nolock) ";
            //SQLquery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            //SQLquery += " WHERE clientarticles.clientid='1087' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            SQLquery += " SELECT TOP 5 m.TITLE" + (info.Equals("pt") ? "" : "_EN") + " as TITLE, m.EDITOR, m.DATE, m.page_url ";
            SQLquery += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles  ca with(nolock) ON ca.id = m.ID ";
            SQLquery += " WHERE ca.clientid='1087'  and isnull(page_url,'') != ''  ";
            SQLquery += " and m.cod_dia_insert >= convert(varchar(8),getdate()-3,112) ";
            SQLquery += " and isnull(m.title_en,'') <> '' ";
            SQLquery += " order by m.insert_date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri,Arial' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri,Arial' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }

            return topStr;
        }

        protected String getTop5AS()
        {

            String topStr = "";
            String SQLquery = "";
            //String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata with(nolock) ";
            //SQLquery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            //SQLquery += " WHERE clientarticles.clientid='1099' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            SQLquery += " SELECT TOP 5 m.TITLE" + (info.Equals("pt") ? "" : "_EN") + " as TITLE, m.EDITOR, m.DATE, m.page_url ";
            SQLquery += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles  ca with(nolock) ON ca.id = m.ID ";
            SQLquery += " WHERE ca.clientid='1099'  and isnull(page_url,'') != ''  ";
            SQLquery += " and m.cod_dia_insert >= convert(varchar(8),getdate()-3,112) ";
            SQLquery += " and isnull(m.title_en,'') <> '' ";
            SQLquery += " order by m.insert_date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri,Arial' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri,Arial' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }

            return topStr;
        }

        protected String getTop5MZ()
        {

            String topStr = "";
            String SQLquery = "";
            //String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata with(nolock) ";
            //SQLquery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            //SQLquery += " WHERE clientarticles.clientid='1085' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            SQLquery += " SELECT TOP 5 m.TITLE" + (info.Equals("pt") ? "" : "_EN") + " as TITLE, m.EDITOR, m.DATE, m.page_url ";
            SQLquery += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles  ca with(nolock) ON ca.id = m.ID ";
            SQLquery += " WHERE ca.clientid='1085'  and isnull(page_url,'') != ''  ";
            SQLquery += " and m.cod_dia_insert >= convert(varchar(8),getdate()-3,112) ";
            SQLquery += " and isnull(m.title_en,'') <> '' ";
            SQLquery += " order by m.insert_date desc ";
            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri,Arial' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri,Arial' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }
            return topStr;
        }

        protected String getTop5PT()
        {

            String topStr = "";
            String SQLquery = "";
            //String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata with(nolock)  ";
            //SQLquery += " INNER JOIN iadvisers.dbo.clientarticles ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            //SQLquery += " WHERE clientarticles.clientid='1014' with(nolock) and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            SQLquery += " SELECT TOP 5 m.TITLE" + (info.Equals("pt") ? "" : "_EN") + " as TITLE, m.EDITOR, m.DATE, m.page_url ";
            SQLquery += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles  ca with(nolock) ON ca.id = m.ID ";
            SQLquery += " WHERE ca.clientid='1014'  and isnull(page_url,'') != ''  ";
            SQLquery += " and m.cod_dia_insert >= convert(varchar(8),getdate()-3,112) ";
            SQLquery += " and isnull(m.title_en,'') <> '' ";
            SQLquery += " order by m.insert_date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri,Arial' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri,Arial' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }
            return topStr;
        }

        protected String getTop5CV()
        {
            String topStr = "";
            String SQLquery = "";
            //String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata  ";
            //SQLquery += " INNER JOIN iadvisers.dbo.clientarticles ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            //SQLquery += " WHERE clientarticles.clientid='1089' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            SQLquery += " SELECT TOP 5 m.TITLE" + (info.Equals("pt") ? "" : "_EN") + " as TITLE, m.EDITOR, m.DATE, m.page_url ";
            SQLquery += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles  ca with(nolock) ON ca.id = m.ID ";
            SQLquery += " WHERE ca.clientid='1089'  and isnull(page_url,'') != ''  ";
            SQLquery += " and m.cod_dia_insert >= convert(varchar(8),getdate()-3,112) ";
            SQLquery += " and isnull(m.title_en,'') <> '' ";
            SQLquery += " order by m.insert_date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri,Arial' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri,Arial' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }
            return topStr;
        }

        protected String getTop5BW()
        {
            String topStr = "";
            String SQLquery = "";
            //String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata  ";
            //SQLquery += " INNER JOIN iadvisers.dbo.clientarticles ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            //SQLquery += " WHERE clientarticles.clientid='1101' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            SQLquery += " SELECT TOP 5 m.TITLE" + (info.Equals("pt") ? "" : "_EN") + " as TITLE, m.EDITOR, m.DATE, m.page_url ";
            SQLquery += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles  ca with(nolock) ON ca.id = m.ID ";
            SQLquery += " WHERE ca.clientid='1101'  and isnull(page_url,'') != '' ";
            SQLquery += " and m.cod_dia_insert >= convert(varchar(8),getdate()-30,112) ";
            SQLquery += " and isnull(m.title_en,'') <> '' ";
            SQLquery += " order by m.insert_date desc ";


            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri,Arial' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri,Arial' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }
            return topStr;
        }

        protected String getTop5ZB()
        {
            String topStr = "";
            String SQLquery = "";
            //String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata  ";
            //SQLquery += " INNER JOIN iadvisers.dbo.clientarticles ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            //SQLquery += " WHERE clientarticles.clientid='1100' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            SQLquery += " SELECT TOP 5 m.TITLE" + (info.Equals("pt") ? "" : "_EN") + " as TITLE, m.EDITOR, m.DATE, m.page_url ";
            SQLquery += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles  ca with(nolock) ON ca.id = m.ID ";
            SQLquery += " WHERE ca.clientid='1100'  and isnull(page_url,'') != ''  ";
            SQLquery += " and m.cod_dia_insert >= convert(varchar(8),getdate()-7,112) ";
            SQLquery += " and isnull(m.title_en,'') <> '' ";
            SQLquery += " order by m.insert_date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri,Arial' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri,Arial' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }
            return topStr;
        }

        protected void Ib_login_Click(object sender, EventArgs e)
        {
            Global.InsigteManager[Session.SessionID].Login(txt_user.Value, txt_pass.Value);
            Response.Redirect("Default.aspx");
            //Response.Redirect("login.aspx");
        }

    }
}