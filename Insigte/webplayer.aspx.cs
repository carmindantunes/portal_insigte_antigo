﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;

namespace Insigte
{
    public partial class webplayer : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string qsIDArtigo = Request.QueryString["ID"];

            
            if (!Page.IsPostBack)
            {
                getArtigo(qsIDArtigo);
            }
        }

        protected void getArtigo(string ID)
        {
            String SQLquery = "select * from metadata with(nolock) where id = " + ID;
            DataTable video = new DataTable();
            video = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "video");

            foreach (DataRow x in video.Rows)
            {
                LBVideo.Text = " <object classid=\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\" codebase=\"http://www.apple.com/qtactivex/qtplugin.cab\" height=\"300\" width=\"400\"> ";
                LBVideo.Text+= "<param name=\"src\" value=\"http://insigte.com/ficheiros/" + x["filepath"].ToString() + "\"> ";
                LBVideo.Text+= "<param name=\"autoplay\" value=\"true\"> ";
                LBVideo.Text += "<param name=\"CONTROLLER\" VALUE=\"True\"> ";
                LBVideo.Text += "<embed height=\"300\" pluginspage=\"http://www.apple.com/quicktime/download/\" src=\"http://insigte.com/ficheiros/" + x["filepath"].ToString() + "\" type=\"video/quicktime\" width=\"400\" CONTROLLER=\"True\" autoplay=\"True\"> ";
                LBVideo.Text+= "</object> ";
            }

        }
    }
}