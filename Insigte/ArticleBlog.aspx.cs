﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;
using System.Text.RegularExpressions;

namespace Insigte
{
    public partial class ArticleBlog : BasePage
    {
        string SearchActive = "0";
        string qsIDArtigo = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            qsIDArtigo = Request.QueryString["ID"];
            SearchActive = Request.QueryString["sh"];

            //ImgShare.ImageUrl = "Cli/" + Global.InsigteManager[Session.SessionID].CodCliente + "Imgs/icons/black/png/share_icon_16.png";
            //ImgPrint.ImageUrl = "Cli/" + Global.InsigteManager[Session.SessionID].CodCliente + "Imgs/icons/black/png//print_icon_16.png";
            //imgBackArticles.ImageUrl = "Cli/" + Global.InsigteManager[Session.SessionID].CodCliente + "Imgs/icons/black/png/arrow_left_icon_16.png";

            //ImgShare.ImageUrl = "Imgs/icons/black/png/share_icon_16.png";
            ImgPrint.ImageUrl = "Imgs/icons/black/png//print_icon_16.png";
            imgBackArticles.ImageUrl = "Imgs/icons/black/png/arrow_left_icon_16.png";


            if (!Page.IsPostBack)
            {
                getArtigoBlog(qsIDArtigo);
            }
        }

        private void getArtigoBlog(string qsIDArtigo)
        {
            String Title = "txt_title";
            String text = "txt_content";
            //para traduçao
            //if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            //{
            //    Title = "TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
            //    text = "text_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as text";
            //}

            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;User ID=in_app;Initial Catalog=iadvisers;Password=in.app.2013");
            try
            {
                //
                //SqlConnection conn2 = new SqlConnection("Data Source=167.114.209.37;User ID=in_app;Initial Catalog=iadvisers;Password=in.app.2013");
                //conn2.Open();

                //string entidade = " select id_entidade, cod_terms from iadvisers.dbo.dim_entidade ";

                //SqlCommand cmdEnt = new SqlCommand(entidade);
                //cmdEnt.Connection = conn2;
                //cmdEnt.CommandType = CommandType.Text;
                //SqlDataReader readerEnt = cmdEnt.ExecuteReader();

                //List<ListItem> Entidades = new List<ListItem>();
                //while (readerEnt.Read())
                //{
                //    ListItem x = new ListItem(readerEnt["cod_terms"].ToString(), readerEnt["id_entidade"].ToString());
                //    Entidades.Add(x);
                //}

                //conn2.Close();
                //conn2.Dispose();


                conn.Open();

                string cmdQuery = " SELECT m." + Title + " as TITULO,b.NOM_BLOG as BLOG,convert( varchar,dat_publish,103), " + text + " as CONTEUDO, txt_url ";
                cmdQuery += " FROM [NW10F_METADATA_BLOG] m with(nolock)  ";
                cmdQuery += " inner join [dbo].[NW10D_BLOG] b with(nolock) ";
		              cmdQuery += " on m.id_blog = b.id_blog ";
                      cmdQuery += " where m.id_news = '" + qsIDArtigo + "'";

                //string cmdQuery = "SELECT " + text + ", " + Title + ", EDITOR, DATE, iadvisers.dbo.metadata.ID, filepath, page_url ";
                //cmdQuery += "FROM iadvisers.dbo.metadata INNER JOIN iadvisers.dbo.contents ON iadvisers.dbo.metadata.ID=iadvisers.dbo.contents.ID ";
                //cmdQuery += "where iadvisers.dbo.metadata.ID='" + qsIDArtigo + "'";

                SqlCommand cmd = new SqlCommand(cmdQuery);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    lblArticleTitle.Text = reader.GetValue(0).ToString();
                    lblArticleEditorData.Text = "(" + reader.GetValue(1).ToString() + " | " + reader.GetValue(2).ToString() + ")";
                    lblArticleText.Text = reader.GetValue(3).ToString().Replace(Environment.NewLine, "<br />");

                //    if (SearchActive == "1")
                //    {

                //        String Words = Session["ADVSearchTxt"].ToString();

                //        if (Session["ADVSearchTxt"].ToString().Contains("\""))
                //        {
                //            //@"(?<="")[^""]+(?="")|[^\s""]\S*"
                //            foreach (Match m in Regex.Matches(Session["ADVSearchTxt"].ToString(), "\"([^\".]+)\""))
                //            {
                //                //lblArticleTitle.Text += "<br>Word: " + m.Value;
                //                Words = Words.Replace(m.Value, " ");
                //            }
                //        }

                //        //Words = Regex.Replace(Words, "or", " ", RegexOptions.IgnoreCase);
                //        //Words = Regex.Replace(Words, "NOT", " ", RegexOptions.IgnoreCase);
                //        //Words = Regex.Replace(Words, "AND", " ", RegexOptions.IgnoreCase);
                //        //Words = Regex.Replace(Words, "NEAR", " ", RegexOptions.IgnoreCase);

                //        //String Words = Session["ADVSearchTxt"].ToString();
                //        Words = Words.Replace("(", " ").Replace("\"", "").Replace(")", " ");
                //        Words = Words.Replace("  ", " ").Replace("  ", " ").Replace("  ", " ");
                //        Words = Words.Replace(" OR ", " ").Replace(" or ", " ").Replace(" Or ", " ");
                //        Words = Words.Replace(" NOT ", " ").Replace(" not ", " ").Replace(" Not ", " ");
                //        Words = Words.Replace(" AND ", " ").Replace(" and ", " ").Replace(" And ", " ");
                //        Words = Words.Replace(" NEAR ", " ").Replace(" near ", " ").Replace(" Near ", " ");

                //        Words = Words.Replace("OR", " ").Replace("or", " ").Replace("Or", " ");
                //        Words = Words.Replace("NOT", " ").Replace("not", " ").Replace("Not", " ");
                //        Words = Words.Replace("AND", " ").Replace("and", " ").Replace("And", " ");
                //        Words = Words.Replace("NEAR", " ").Replace("near", " ").Replace("Near", " ");

                //        Words = Words.Replace("  ", " ").Replace("  ", " ").Replace("  ", " ");

                //        String[] Replaces = Words.Split(' ');
                //        foreach (String x in Replaces)
                //        {
                //            if (x.Length > 0)
                //            {
                //                lblArticleText.Text = lblArticleText.Text.Replace(x, "<b>" + x + "</b>").Replace(Environment.NewLine, "<br />");
                //                lblArticleText.Text = lblArticleText.Text.Replace(x.Substring(0, 1).ToUpper() + x.Substring(1, x.Length - 1), "<b>" + x.Substring(0, 1).ToUpper() + x.Substring(1, x.Length - 1) + "</b>").Replace(Environment.NewLine, "<br />");
                //                lblArticleText.Text = lblArticleText.Text.Replace(x.ToUpper(), "<b>" + x.ToUpper() + "</b>").Replace(Environment.NewLine, "<br />");
                //                lblArticleText.Text = lblArticleText.Text.Replace(x.ToLower(), "<b>" + x.ToLower() + "</b>").Replace(Environment.NewLine, "<br />");
                //            }
                //        }

                //        foreach (Match m in Regex.Matches(Session["ADVSearchTxt"].ToString(), "\"([^\".]+)\""))
                //        {
                //            String x = m.Value.Replace("\"","");

                //            if (x.Length > 0)
                //            {
                //                String Cap = Global.InsigteManager[Session.SessionID].FirstCharToUpper(x);

                //                //lblArticleTitle.Text += "<br>Word: " + Cap;

                //                lblArticleText.Text = lblArticleText.Text.Replace(Cap, "<b>" + Cap + "</b>").Replace(Environment.NewLine, "<br />");

                //                lblArticleText.Text = lblArticleText.Text.Replace(x, "<b>" + x + "</b>").Replace(Environment.NewLine, "<br />");
                //                lblArticleText.Text = lblArticleText.Text.Replace(x.Substring(0, 1).ToUpper() + x.Substring(1, x.Length - 1), "<b>" + x.Substring(0, 1).ToUpper() + x.Substring(1, x.Length - 1) + "</b>").Replace(Environment.NewLine, "<br />");
                //                lblArticleText.Text = lblArticleText.Text.Replace(x.ToUpper(), "<b>" + x.ToUpper() + "</b>").Replace(Environment.NewLine, "<br />");
                //                lblArticleText.Text = lblArticleText.Text.Replace(x.ToLower(), "<b>" + x.ToLower() + "</b>").Replace(Environment.NewLine, "<br />");
                //            }
                //        }
                //    }
                //    else
                //    {
                //        lblArticleText.Text = lblArticleText.Text.Replace(Global.InsigteManager[Session.SessionID].NomCliente, "<b>" + Global.InsigteManager[Session.SessionID].NomCliente + "</b>").Replace(Environment.NewLine, "<br />");
                //    }

                //    foreach (ListItem enti in Entidades)
                //    {
                //        String[] term = enti.Text.Split(';');
                //        foreach (String x in term)
                //            lblArticleText.Text = lblArticleText.Text.Replace(x, "<a class=\"ent\" href=\"ArticleInfo.aspx?ID=" + qsIDArtigo + "&etd=" + enti.Value + "\">" + x + "</a>").Replace(Environment.NewLine, "<br />");
                //    }

                   //lblEmailLink.Text = "<a id=\"share-news\" onclick=\"shareTheNews('" + reader.GetValue(4).ToString() + "','" + reader.GetValue(1).ToString().Replace("'", "") + "')\" style='font-family:Arial; font-size:12px; text-decoration:none; color:#000000;' href=\"#\" >" + Resources.insigte.language.shareFormDesc + "</a>";
                    
                //    if (reader.GetValue(6).ToString() == null || reader.GetValue(6).ToString() == string.Empty)
                //    {
                //        imgDLorLINK.ImageUrl = "Imgs/icons/black/png/download_icon_16.png";
                //        lblPath.Text = "<a href='http://www.insigte.com/ficheiros/" + reader.GetValue(5).ToString() + "' target='_blank' style='font-family:Arial; font-size:12px; text-decoration:none; color:#000000;'>Download</a>";
                //    }
                //    else
                //    {
                    imgDLorLINK.ImageUrl = "Imgs/icons/black/png/link_icon_16.png";
                    imgDLorLINK.Height = 16;
                    lblPath.Text = "<a href='" + reader.GetValue(4).ToString() + "' target='_blank' style='font-family:Arial; font-size:12px; text-decoration:none; color:#000000;'>Link</a>";
                //    }

                //    if (chkMyNewsArchive(reader.GetValue(4).ToString()))
                //    {
                //        imgAddMyArticles.ImageUrl = "Imgs/icons/black/png/clipboard_copy_icon_16_sel.png";
                //        imgAddMyArticles.ToolTip = Resources.insigte.language.arColClipsGuardados;
                //        lblAddMyArticle.Text = Resources.insigte.language.arColClipsGuardados;
                //    }
                //    else
                //    {
                //        imgAddMyArticles.ImageUrl = "Imgs/icons/black/png/clipboard_copy_icon_16.png";
                //        imgAddMyArticles.ToolTip = Resources.insigte.language.arColClipsAdicionar;
                //        lblAddMyArticle.Text = Resources.insigte.language.arColClipsAdicionar;                        
                //    }

                //    if (chkMyPDFArchive(reader.GetValue(4).ToString()))
                //    {
                //        imgPDFMyArticles.ImageUrl = "Imgs/icons/black/png/book_side_icon_16_sel.png";
                //        imgPDFMyArticles.ToolTip = Resources.insigte.language.ucArcLink3;
                //        lblPDFMyArticle.Text = Resources.insigte.language.ucArcLink3;
                //    }
                //    else
                //    {
                //        imgPDFMyArticles.ImageUrl = "Imgs/icons/black/png/book_side_icon_16.png";
                //        imgPDFMyArticles.ToolTip = Resources.insigte.language.ucArcLink3;
                //        lblPDFMyArticle.Text = Resources.insigte.language.ucArcLink3;
                //    }
                }

                cmd.Connection.Close();
                cmd.Connection.Dispose();
            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }
        }

        private bool chkMyNewsArchive(string idArticle)
        {
            if (HttpContext.Current.Request.Cookies["CookieinsigteSys"] == null)
            {
                return false;
            }
            else
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["ID"] == null)
                {
                    return false;
                }
                else
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["ID"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(idArticle))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (idVal == idArticle)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
        }

        private bool chkMyPDFArchive(string idArticle)
        {
            if (HttpContext.Current.Request.Cookies["CookieinsigteSys"] == null)
            {
                return false;
            }
            else
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] == null)
                {
                    return false;
                }
                else
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(idArticle))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (idVal == idArticle)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
        }

        protected void bt_AddPDF_Command(Object sender, CommandEventArgs e)
        {
            addLinkPDF(qsIDArtigo);
            imgPDFMyArticles.ImageUrl = "Imgs/icons/black/png/book_side_icon_16_sel.png";
        }

        protected void bt_AddArquivo_Command(Object sender, CommandEventArgs e)
        {
            addNews(qsIDArtigo);

            imgAddMyArticles.ImageUrl = "Imgs/icons/black/png/clipboard_copy_icon_16_sel.png";
            lblAddMyArticle.Text = Resources.insigte.language.arColClipsGuardados;
        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        protected void addNews(string qsIDArtigo)
        {
            string ValCookie = string.Empty;
            string PDFCookie = string.Empty;
            string DelCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

            if (cookie.Values["ID"] != "")
            {
                ValCookie = cookie.Values["ID"] + "|" + qsIDArtigo;
            }
            else
            {
                ValCookie = qsIDArtigo;
            }

            DelCookie = cookie.Values["DELNOT"];
            PDFCookie = cookie.Values["PDF"];

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = PDFCookie;
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(360);
            Response.Cookies.Add(newCookie);

        }

        protected void addLinkPDF(string qsPDFArtigo)
        {

            string ValCookie = string.Empty;
            string PDFCookie = string.Empty;
            string DelCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

            if (cookie.Values["PDF"] != "")
            {
                PDFCookie = cookie.Values["PDF"] + "|" + qsPDFArtigo;
            }
            else
            {
                PDFCookie = qsPDFArtigo;
            }

            DelCookie = cookie.Values["DELNOT"];
            ValCookie = cookie.Values["ID"];

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = PDFCookie;
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(360);
            Response.Cookies.Add(newCookie);

        }

    }
}