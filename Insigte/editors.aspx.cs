﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using APToolkitNET;

namespace Insigte
{
    public partial class editors : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string qsIDEditor = Request.QueryString["ID"];

                getEditor(qsIDEditor);
                getLastNews(qsIDEditor);
            }
        }

        protected void getEditor(String ID)
        {
            DataTable gEdt = Global.InsigteManager[Session.SessionID].getTableQuery("select * from editors with(nolock) where editorid = " + ID, "getAutor");

            foreach (DataRow x in gEdt.Rows)
            {
                lt_Editor_photo.Text = "<img src='../ficheiros/editores/default.png' alt=\"\" height=\"42px\" width=\"42px\" />";
                lt_Editor_name.Text = x["name"].ToString();

                if (x["email"].ToString().Length > 0)
                    lt_Editor_Email.Text = "(" + x["email"].ToString() + ")";

                if (x["facebook"].ToString().Length > 0)
                    fb_desc.Text = "<a href=\"" + (x["facebook"].ToString().Substring(0, 4) == "http" ? x["facebook"].ToString() + "\" target=\"_blank\">" + x["facebook"].ToString() : "http://" + x["facebook"].ToString() + "\" target=\"_blank\">" + x["facebook"].ToString()) + "</a>";
                else
                    fb_desc.Text = "n/a";

                if (x["twitter"].ToString().Length > 0)
                    tw_desc.Text = "<a href=\"" + (x["twitter"].ToString().Substring(0, 4) == "http" ? x["twitter"].ToString() + "\" target=\"_blank\">" + x["twitter"].ToString() : "http://" + x["twitter"].ToString() + "\" target=\"_blank\">" + x["twitter"].ToString()) + "</a>";
                else
                    tw_desc.Text = "n/a";

                if (x["linkedin"].ToString().Length > 0)
                    lk_desc.Text = "<a href=\"" + (x["linkedin"].ToString().Substring(0, 4) == "http" ? x["linkedin"].ToString() + "\" target=\"_blank\">" + x["linkedin"].ToString() : "http://" + x["linkedin"].ToString() + "\" target=\"_blank\">" + x["linkedin"].ToString()) + "</a>";
                else
                    lk_desc.Text = "n/a";

                if (x["phone"].ToString().Length > 0)
                    lt_telefone.Text = x["phone"].ToString();
                else
                    lt_telefone.Text = "n/a";

                if (x["adress"].ToString().Length > 0)
                    lt_morada.Text = x["adress"].ToString();
                else
                    lt_morada.Text = "n/a";


            }
        }

        protected void getLastNews(String IdEditor)
        {
            DataTable gNews = Global.InsigteManager[Session.SessionID].getTableQuery("select top 5 m.id, m.date, rtrim(ltrim(m.title)) as title, substring(c.text,1,500) + '...' as text, m.editor from metadata m with(nolock) inner join contents c with(nolock) on m.id = c.id where m.editorid = " + IdEditor + " order by m.id desc ", "getAutor");

            rep_last5news.DataSource = gNews;
            rep_last5news.DataBind();
        }
    }
}