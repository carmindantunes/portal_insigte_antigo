﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;
using System.Web.Mail;

namespace Insigte
{
    public partial class Reports : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(4, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }

            if (!Page.IsPostBack)
            {
                FillDDDocuments();
                FillRepeater();
            }
        }

        protected void FillDDDocuments()
        {
            DataTable tempTbl = Global.InsigteManager[Session.SessionID].getTableQuery("select * from [dbo].[CL10D_DOCUMENT_TYPE] with(nolock)", "getDocuments");

            string DES_DOCUMENT_TYPE = Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt" ? "DES_DOCUMENT_TYPE_EN" : "DES_DOCUMENT_TYPE";
        

            foreach (DataRow x in tempTbl.Rows)
            {
                if (Global.InsigteManager[Session.SessionID].IdClient == "3003")
                {
                    DDDocuments.Items.Add(new ListItem(x[DES_DOCUMENT_TYPE].ToString().Replace("Dossier", "Resenha"), x["COD_DOCUMENT_TYPE"].ToString()));
                }
                else
                { 
                    DDDocuments.Items.Add(new ListItem(x[DES_DOCUMENT_TYPE].ToString(), x["COD_DOCUMENT_TYPE"].ToString()));
                }
            }
        }

        protected void FillRepeater()
        {

            DataTable tempTbl = Global.InsigteManager[Session.SessionID].getTableQuery("select * from CL10H_CLIENT_DOCUMENTS with(nolock) where IND_ACTIVE = 1 and COD_DOCUMENT_TYPE=" + DDDocuments.SelectedValue.ToString() + " and ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and isnull(ID_CLIENT_USER," + Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString() + ") = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString() + " order by VAL_DOCUMENT_ORDER  ", "getDocuments");

            foreach (DataRow x in tempTbl.Rows)
            {

            }

            DataView tempView = tempTbl.DefaultView;
            tempView.Sort = ("VAL_DOCUMENT_ORDER asc, DAT_CREATE desc");
            DataTable tempTblSorted = tempView.ToTable();

            repGroups.DataSource = tempTblSorted;
            repGroups.DataBind();
        }

        protected void DDDocuments_itemChange(object sender, EventArgs e)
        {

            FillRepeater();

        }
        protected string getId()
        {
            if (!DDDocuments.SelectedValue.Equals("4"))
            {
                //return "http://Insigte.com/Ficheiros/";
                return "";
            }

            string ret = "http://Insigte.com/Docs/"+ Global.InsigteManager[Session.SessionID].IdClient.ToString() + "/";

            return ret;
        }

        protected void lnk_download_Command(object sender, CommandEventArgs e)
        {
            //lblDataToday.Visible = true;
            //lblDataToday.Text = e.CommandArgument.ToString();

            string fileNetpath = "";
            string fileRealPath = "";
            string fileUserName = "";

            DataTable tempTbl = Global.InsigteManager[Session.SessionID].getTableQuery("select * from CL10H_CLIENT_DOCUMENTS with(nolock) where IND_ACTIVE = 1 and ID_DOCUMENT=" + e.CommandArgument.ToString(), "getDocumentID");

            foreach (DataRow x in tempTbl.Rows)
            {
                String Directnet = "ficheiros";
               
                if (DDDocuments.SelectedValue == "4")
                {
                    Directnet = "Docs/" + Global.InsigteManager[Session.SessionID].IdClient;
                }

                //lblDataToday.Text += "<br>" + "../ficheiros/" + x["FILE_PATH"].ToString().Replace("http://insigte.com", "");
                //filepath = Server.MapPath(@"../"+ Direct +"/" + x["FILE_PATH"].ToString());
                fileNetpath = "http://insigte.com/" + Directnet + "/" + x["FILE_PATH"].ToString();
                fileUserName = x["NOM_DOCUMENT"].ToString();
            }

            fileRealPath = fileNetpath.Contains("Docs") ? fileNetpath.Replace("http://insigte.com/Docs/", @"G:/Documents/") : fileNetpath.Replace("http://insigte.com/ficheiros",@"G:/files");
            
            FileInfo myfile = new FileInfo(fileRealPath);
            if (myfile.Exists)
            {
                // Clear the content of the response
                Response.ClearContent();

                // Add the file name and attachment, which will force the open/cancel/save dialog box to show, to the header
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileUserName.Replace(" ", "_"));

                // Add the file size into the response header
                Response.AddHeader("Content-Length", myfile.Length.ToString());

                // Set the ContentType
                Response.ContentType = ReturnExtension(myfile.Extension.ToLower());

                // Write the file into the response (TransmitFile is for ASP.NET 2.0. In ASP.NET 1.1 you have to use WriteFile instead)
                Response.TransmitFile(myfile.FullName);

                // End the response
                Response.End();
            }
            else
            {
                lblDataToday.Visible = true;
                lblDataToday.Text += Resources.insigte.language.filenotfound;
            }

        }

        private string ReturnExtension(string fileExtension)
        {
            switch (fileExtension)
            {
                case ".htm":
                case ".html":
                case ".log":
                    return "text/HTML";
                case ".txt":
                    return "text/plain";
                case ".doc":
                case ".docx":
                    return "application/ms-word";
                case ".tiff":
                case ".tif":
                    return "image/tiff";
                case ".asf":
                    return "video/x-ms-asf";
                case ".avi":
                    return "video/avi";
                case ".zip":
                    return "application/zip";
                case ".xls":
                case ".xlsx":
                case ".csv":
                    return "application/vnd.ms-excel";
                case ".gif":
                    return "image/gif";
                case ".jpg":
                case "jpeg":
                    return "image/jpeg";
                case ".bmp":
                    return "image/bmp";
                case ".wav":
                    return "audio/wav";
                case ".mp3":
                    return "audio/mpeg3";
                case ".mpg":
                case "mpeg":
                    return "video/mpeg";
                case ".rtf":
                    return "application/rtf";
                case ".asp":
                    return "text/asp";
                case ".pdf":
                    return "application/pdf";
                case ".fdf":
                    return "application/vnd.fdf";
                case ".ppt":
                    return "application/mspowerpoint";
                case ".dwg":
                    return "image/vnd.dwg";
                case ".msg":
                    return "application/msoutlook";
                case ".xml":
                case ".sdxl":
                    return "application/xml";
                case ".xdp":
                    return "application/vnd.adobe.xdp+xml";
                default:
                    return "application/octet-stream";
            }
        }

        protected void repGroups_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //var myLbEditor = (ListBox)e.Item.FindControl("LbEditors");
            //var myLbEditorAO = (ListBox)e.Item.FindControl("LbEditorsAO");
            //var myLbEditorMZ = (ListBox)e.Item.FindControl("LbEditorsMZ");
            //var myLbEditorPT = (ListBox)e.Item.FindControl("LbEditorsPT");
            //var myLbEditorOU = (ListBox)e.Item.FindControl("LbEditorsOU");
            ////myLbEditor.ToolTip = e.Item.DataItem.ToString();//e.Item.ItemIndex.ToString();

            //myLbEditor.Items.Add(Resources.insigte.language.geralTagTodos);
            //foreach (DataRow x in Editors.Rows)
            //{
            //    switch (x["country"].ToString())
            //    {
            //        case "Portugal":
            //            myLbEditorPT.Items.Add(x["name"].ToString());
            //            break;
            //        case "Angola":
            //            myLbEditorAO.Items.Add(x["name"].ToString());
            //            break;
            //        case "Moçambique":
            //            myLbEditorMZ.Items.Add(x["name"].ToString());
            //            break;
            //        default:
            //            myLbEditorOU.Items.Add(x["name"].ToString());
            //            break;
            //    }

            //    myLbEditor.Items.Add(x["name"].ToString());
            //}


        }

        public void Share_Pdf_Click(object sender, EventArgs e)
        {
            string MailHTMLBody = string.Empty;
            string MailEnviadoPor_Comment = string.Empty;
            string MailSubject = string.Empty;

            //if (txt_aux_sharePdf_sendby.Value != "" || txt_aux_sharePdf_sendby.Value != string.Empty)
            //{
            //    MailEnviadoPor_Comment += Resources.insigte.language.defEmailSend + " " + txt_aux_sharePdf_sendby.Value + "<br />" + Environment.NewLine;
            //}

            //if (txt_aux_sharePdf_comment.Value != "" || txt_aux_sharePdf_comment.Value != string.Empty)
            //{
            //    MailEnviadoPor_Comment += Resources.insigte.language.defEmailComment + " " + txt_aux_sharePdf_comment.Value + "<br />" + Environment.NewLine;
            //}

            //MailHTMLBody += "<body style='font-family:Arial;'>";

            MailSubject += Global.InsigteManager[Session.SessionID].NomCliente + " | Dossier de Imprensa";
            MailHTMLBody += getBodyHtml(txt_aux_sharePdf_link.Value, txt_aux_sharePdf_sendby.Value, txt_aux_sharePdf_comment.Value);

            //MailHTMLBody += "<br />" + Environment.NewLine + "<a href=\"" + txt_aux_sharePdf_link.Value + "\"><font face='Arial' size='2' color='#000000'>" + txt_aux_sharePdf_id.Value + "</font></a>";
            //MailHTMLBody += "</body>";

            MailMessage mail = new MailMessage();
            mail.To = txt_aux_sharePdf_email.Value;
            mail.From = "insigte <info@insigte.com>";
            mail.Bcc = "";
            mail.Subject = MailSubject;
            mail.BodyFormat = MailFormat.Html;
            mail.Body = MailHTMLBody;

            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", 25);
            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", 2);
            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", 1);
            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "info@insigte.com");
            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "jQY4qhQk");
            mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", "false");

            try
            {
                SmtpMail.SmtpServer = "mail.insigte.com";
                SmtpMail.Send(mail);

                lblDataToday.Visible= true;
                lblDataToday.Text = "Email Enviado";
                //Global.InsigteManager[Session.SessionID].inUser.EmailShareSend = "1";
                //Session["EmailSend"] = "1";
                //lbDashboard.Text = "OMFG " + Session["EmailSend"].ToString();
            }
            catch (Exception ex)
            {
                lblDataToday.Visible = true;
                lblDataToday.Text = "Email não Enviado:"+ ex.Message.ToString();
                //Global.InsigteManager[Session.SessionID].inUser.EmailShareSend = "0";
            }

            //Response.Redirect(Request.RawUrl);

        }

        public string getBodyHtml(String _Nome, String _sendBy, String _commnent)
        {

            string textotop = "Carma Delivery Services | " + Resources.insigte.language.shareDossier.ToString();
            string textoBot = "© Carma 2015 | Delivering what matters";


            string rtn = "";

            rtn += "<body bgcolor='#ffffff' leftMargin='0' link='#000000' vlink='#000000' alink='#000000' style='margin-top: 0; margin-bottom:0; font-family:Arial; font-size:26px; '>";
            rtn += "</br>";
            rtn += "<table border='0' width='100%' style='font-size:26px; '>";
            rtn += "<tr>";
            //rtn += "<td colspan='5'>";
            //rtn += "<table width='100%' style='font-size:26px; font-family:Arial;'>";
            //rtn += "<td width='10px'></td>";
            rtn += "<td colspan='5' style='font-size:26px; font-family:Arial;'>" + textotop + " </td>";
            //rtn += "</table>";
            //rtn += "</td>";
            rtn += "</tr>";
            rtn += "<tr><td colspan='5' height='10px'></td></tr>";
            rtn += "<tr>";
            rtn += "<td colspan='5' style='color:white; '>";
            rtn += "<table height='50px' bgcolor='#00add9' width='100%'>";
            rtn += "<td width='10px'></td>";
            rtn += "<td style='color:white; font-size:26px; font-family:Arial;'>" + Resources.insigte.language.sharedPDF.ToString() + ": <a style='color:white; font-family:Arial; text-decoration:none;' href='" + _Nome + "'>" + txt_aux_sharePdf_id.Value + " </a></td>";
            rtn += "</table>";
            rtn += "</td>";
            rtn += "</tr>";
            rtn += "<tr>";
            rtn += "<tr><td colspan='5' height='10px'></td></tr>";
            rtn += "<td width='45%' bgcolor='#002d56' style='color:white;'>";
            rtn += "<table bgcolor='#002d56' >";
            rtn += "<td rowspan='3' width='10px'></td>";
            rtn += "<tr>";
            rtn += "<td style='color:white; font-size:26px; font-family:Arial;'>" + Resources.insigte.language.shareSent.ToString() + "</td></tr>";
            rtn += "<tr><td style='color:white; font-size:18px; font-family:Arial;'>" + _sendBy + "</td></tr>";
            rtn += "</table>";
            rtn += "</td>";
            rtn += "<td width='10px'></td>";
            rtn += "<td width='45%' bgcolor='#002d56' style='color:white;'>";
            rtn += "<table>";
            rtn += "<td rowspan='3' width='10px'></td>";
            rtn += "<tr>";
            rtn += "<td style='color:white; ; font-size:26px; font-family:Arial;'>" + Resources.insigte.language.shareComment.ToString() + "</td></tr>";
            rtn += "<tr><td style='color:white; ; font-size:18px; font-family:Arial;'>" + _commnent + "</td></tr>";
            rtn += "</table>";
            rtn += "</td>";
            rtn += "<td width='10px'></td>";
            rtn += "<td width='10%'>";
            rtn += "<img src='http://insigte.com/login/Imgs/logo_carma_trans_small.png'/></td>";
            rtn += "<tr><td colspan='3' height='10px'></td></tr>";
            rtn += "<tr>";
            //rtn += "<td colspan='5'>";
            //rtn += "<table width='100%'>";
            //rtn += "<td width='10px'></td>";
            rtn += "<td colspan='5' style='font-size:14px; font-family:Arial;'>" + textoBot + "</td>";
            //rtn += "</table>";
            //rtn += "</td>";
            rtn += "</tr>";
            rtn += "</tr></table></body>";


            //rtn += "<body bgcolor='#ffffff' leftMargin='0' link='#000000' vlink='#000000' alink='#000000' style='margin-top: 0; margin-bottom:0; font-family:Arial; font-size:26; '>";
            //rtn += "</br>";
            //rtn += "<table border='0' width='100%' style='font-size:26; '>";
            //rtn += "<tr>";
            //rtn += "<td colspan='5' style='padding-left:20; font-family:Arial;'>" + textotop + "</td>";
            //rtn += "</tr>";
            //rtn += "<tr><td colspan='5' height='10px' ></td></tr>";
            //rtn += "<tr>";
            //rtn += "<td colspan='5' bgcolor='#00add9' style='color:white; padding-left:20; font-family:Arial; height:10px;' >";
            //rtn +=  Resources.insigte.language.shareDossier.ToString() + " <a style='color:white; font-family:Arial; text-decoration:none;' href='" + _Nome + "'>" + txt_aux_sharePdf_id.Value + "</a>";
            //rtn += "</td>";
            //rtn += "</tr>";
            //rtn += "<tr><td colspan='5' height='10px' ></td></tr>";
            //rtn += "<tr>";
            //rtn += "<td width='45%' bgcolor='#002d56' style='color:white; padding-left:10;'>";
            //rtn += "<table>";
            //rtn += "<tr>";
            //rtn += "<td style='color:white; padding-left:10; font-size:26; font-family:Arial;'>" + Resources.insigte.language.shareSent.ToString() + "</td>";
            //rtn += "</tr>";
            //rtn += "<tr>";
            //rtn += "<td style='color:white; padding-left:10; font-size:18; font-family:Arial;'>" + _sendBy + "</td>";
            //rtn += "</tr>";
            //rtn += "</table>";
            //rtn += "</td>";
            //rtn += "<td width='10px'></td>";
            //rtn += "<td width='45%' bgcolor='#002d56' style='color:white; padding-left:10; '>";
            //rtn += "<table>";
            //rtn += "<tr>";
            //rtn += "<td style='color:white; padding-left:10; font-size:26; font-family:Arial;'>" + Resources.insigte.language.shareComment.ToString() + "</td>";
            //rtn += "</tr>";
            //rtn += "<tr>";
            //rtn += "<td style='color:white; padding-left:10; font-size:18; font-family:Arial;'>" + _commnent + "</td>";
            //rtn += "</tr>";
            //rtn += "</table>";
            //rtn += "</td><td width='10px'></td>";
            //rtn += "<td width='10%'>";
            //rtn += "<img src='http://insigte.com/login/Imgs/logo_insigte_trans_small.png'/></td>";
            //rtn += "<tr><td colspan='4' height='10px'></td></tr>";
            //rtn += "<td colspan='5' style='padding-left:20; font-family:Arial;font-size:14;'>" + textoBot + "</td>";
            //rtn += "</tr>";
            //rtn += "</table>";
            //rtn += "</body>";

            return rtn;
        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        protected String getFormTitle()
        {
            return Resources.insigte.language.shareFormTitulo;
        }
    }
}