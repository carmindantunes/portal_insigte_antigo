﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;

namespace Insigte
{
    public partial class pr : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            lbl_re.Visible = false;
            lbl_ra.Visible = false;

            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(9, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }

            if (!Page.IsPostBack)
            {
                FillDDFontes();
            }
        }

        protected void FillRepeater()
        {
            String SQLQuery = " select top 20 a.authorid as ID, a.name, a.email, ee.name as editor, photo, sum(1) as total ";
            SQLQuery += "  from metadata m with(nolock) ";
            SQLQuery += "	   inner join authors a with(nolock) ";
            SQLQuery += "			   on m.authorid = a.authorid ";
            SQLQuery += "	   inner join contents c with(nolock) ";
            SQLQuery += "			   on m.id = c.id ";
            SQLQuery += "	   inner join editors e with(nolock) ";
            SQLQuery += "			   on m.editorid = e.editorid ";
            SQLQuery += "	   left outer join editors ee with(nolock) ";
            SQLQuery += "			   on substring(a.cod_editorid + ';',0, charindex(';',a.cod_editorid + ';')) = ee.editorid ";
            SQLQuery += " where m.authorid is not null ";
            //Regra posta pelo Luis dia 21-10-2014 para aparecer apenas os Autores com os *** na intranet
            SQLQuery += "   and len(rtrim(ltrim(a.email))) > 0 and len(rtrim(ltrim(a.subjects))) > 0 and len(rtrim(ltrim(a.cod_editorid))) > 0 ";
            if (txt_pesquisa.Value.Trim() != "")
                SQLQuery += "   and CONTAINS (c.text, '\"" + txt_pesquisa.Value.Replace("'", "''") + "\"') ";
            SQLQuery += "   and convert(varchar,insert_date,112) > convert(varchar,getdate()-180,112) ";
            if (DDfontes.SelectedValue != "-1")
                SQLQuery += "   and m.editorId = " + DDfontes.SelectedValue;
            if (DDPais.SelectedValue != "-1")
                SQLQuery += "   and e.country = '" + DDPais.SelectedValue + "' ";
            SQLQuery += " group by a.authorid, a.name, a.email, ee.name, photo";
            SQLQuery += " order by total desc ";

            //lblDataToday.Visible = true;
            //lblDataToday.Text = SQLQuery;

            DataTable tempTbl = Global.InsigteManager[Session.SessionID].getTableQuery(SQLQuery, "getAutorea");

            foreach (DataRow x in tempTbl.Rows)
            {

            }

            DataView tempView = tempTbl.DefaultView;
            tempView.Sort = ("total desc");
            DataTable tempTblSorted = tempView.ToTable();
            repGroups_autores.DataSource = tempTblSorted;
            repGroups_autores.DataBind();

            SQLQuery = "select top 20 e.editorid as ID, e.name, e.email, sum(1) as total ";
            SQLQuery += "  from metadata m with(nolock) ";
            SQLQuery += "	   inner join editors e with(nolock) ";
            SQLQuery += "			   on m.editorid = e.editorid ";
            SQLQuery += "	   inner join contents c with(nolock) ";
            SQLQuery += "			   on m.id = c.id ";
            SQLQuery += " where m.authorid is not null ";
            if (txt_pesquisa.Value.Trim() != "")
            SQLQuery += "   and CONTAINS (c.text, '\"" + txt_pesquisa.Value.Replace("'", "''") + "\"') ";
            if (DDfontes.SelectedValue != "-1")
                SQLQuery += "   and m.editorId = " + DDfontes.SelectedValue;
            SQLQuery += "   and convert(varchar,insert_date,112) > convert(varchar,getdate()-180,112) ";
            SQLQuery += " group by e.editorid, e.name, e.email ";
            SQLQuery += " order by total desc ";

            DataTable tempTbl2 = Global.InsigteManager[Session.SessionID].getTableQuery(SQLQuery, "getEditores");

            foreach (DataRow x in tempTbl2.Rows)
            {

            }


            DataView tempView2 = tempTbl2.DefaultView;
            tempView2.Sort = ("total desc");
            DataTable tempTblSorted2 = tempView2.ToTable();
            repGroups_editores.DataSource = tempTblSorted2;
            repGroups_editores.DataBind();

        }

        protected String getEditorLink(String ID, String Img, String Name)
        {
            if (Img == "1")
                return "<a style=\"border-width:0px;text-decoration:none;\" href=\"javascript:window.open('addList.aspx?ID=" + ID + "','CustomPopUp', 'width=420, height=680, menubar=no, resizeble=no, location=no'); void(0)\" target=\"_self\"><img src=\"Imgs/icons/black/png/round_plus_icon_16.png\" alt=\"Adcionar á lista\" /></a>";
            else
                return "<a style=\"border-width:0px;text-decoration:none;\" href=\"javascript:window.open('addList.aspx?ID=" + ID + "','CustomPopUp', 'width=420, height=680, menubar=no, resizeble=no, location=no'); void(0)\" target=\"_self\">" + Name + "</a>";
        }

        protected String getPubLink(String ID, String Img, String Name)
        {
            if (Img == "1")
                return "<a style=\"border-width:0px;text-decoration:none;\" href=\"javascript:window.open('editors.aspx?ID=" + ID + "','CustomPopUp', 'width=420, height=680, menubar=no, resizeble=no, location=no'); void(0)\" target=\"_self\"><img src=\"Imgs/icons/black/png/round_plus_icon_16.png\" alt=\"Ver Informação\" /></a>";
            else
                return "<a style=\"border-width:0px;text-decoration:none;\" href=\"javascript:window.open('editors.aspx?ID=" + ID + "','CustomPopUp', 'width=420, height=680, menubar=no, resizeble=no, location=no'); void(0)\" target=\"_self\">" + Name + "</a>";
        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        protected void FillDDFontes()
        {
            DataTable tempTbl = Global.InsigteManager[Session.SessionID].getTableQuery("select * from [dbo].[editors] with(nolock) where editorid in (select case when charindex(';',cod_editorid,0) > 0 then substring(cod_editorid,0,charindex(';',cod_editorid,0)) else cod_editorid end from authors with(nolock)) order by name", "getDocuments");
            DDfontes.Items.Add(new ListItem(Resources.insigte.language.advSMainEditoresT1, "-1"));
            foreach (DataRow x in tempTbl.Rows)
            {
                DDfontes.Items.Add(new ListItem(x["name"].ToString(), x["editorid"].ToString()));
            }
        }

        protected void Pesquisar_Click (object sender, EventArgs e)
        {
            lbl_re.Visible = true;
            lbl_ra.Visible = true;
            FillRepeater();
        }

        protected void repGroups_autores_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
        }

        protected void repGroups_editores_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
        }

        protected void lnk_download_Command(object sender, CommandEventArgs e)
        {
        }
    }
}