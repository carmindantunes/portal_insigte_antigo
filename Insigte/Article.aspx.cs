﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Web;
//using System.Web.UI;

//using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;
using System.Text.RegularExpressions;
using INinsigteManager;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web;
using System.Threading;


namespace Insigte
{
    public partial class Article : BasePage
    {
        string SearchActive = "0";
        string qsIDArtigo = "";
        string qsNRP = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            //this.upcont1.Load +=new EventHandler(upcont1_Load);    

            if (Global.InsigteManager[Session.SessionID].inUser.EmailShareSend != null)
            {
                if (Global.InsigteManager[Session.SessionID].inUser.EmailShareSend == "1")
                {
                    lbInfo.Visible = true;
                    lbInfo.Text = "Email enviado.";
                    Global.InsigteManager[Session.SessionID].inUser.EmailShareSend = null;
                }
                else
                {
                    lbInfo.Visible = true;
                    lbInfo.Text = "Email não enviado.";

                    Global.InsigteManager[Session.SessionID].inUser.EmailShareSend = null;
                }
            }

            qsIDArtigo = Request.QueryString["ID"];
            SearchActive = Request.QueryString["sh"];
            qsNRP = Request.QueryString["NRP"];

            //ImgShare.ImageUrl = "Cli/" + Global.InsigteManager[Session.SessionID].CodCliente + "Imgs/icons/black/png/share_icon_16.png";
            //ImgPrint.ImageUrl = "Cli/" + Global.InsigteManager[Session.SessionID].CodCliente + "Imgs/icons/black/png//print_icon_16.png";
            //imgBackArticles.ImageUrl = "Cli/" + Global.InsigteManager[Session.SessionID].CodCliente + "Imgs/icons/black/png/arrow_left_icon_16.png";

            ImgShare.ImageUrl = "Imgs/icons/black/png/share_icon_16.png";
            ImgPrint.ImageUrl = "Imgs/icons/black/png/print_icon_16.png";
            imgBackArticles.ImageUrl = "Imgs/icons/black/png/arrow_left_icon_16.png";

            if (!Page.IsPostBack)
            {
                getArtigo(qsIDArtigo);

                DataTable Research = Global.InsigteManager[Session.SessionID].getTableQuery(" select * from CL10D_RESEARCH  with(nolock) where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and ID_CLIENT_USER = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser, "getResearchLists");

                DDL_research.DataSource = Research;
                DDL_research.DataTextField = "NOM_RESEARCH";
                DDL_research.DataValueField = "ID_RESEARCH";
                DDL_research.DataBind();

                DataTable Research_tema = Global.InsigteManager[Session.SessionID].getTableQuery(" select * from CL10D_RESEARCH_TEMA  with(nolock) where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and ID_CLIENT_USER = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and ID_RESEARCH = " + DDL_research.SelectedValue.ToString(), "getResearchTemaLists");

                DDL_tema.DataSource = Research_tema;
                DDL_tema.DataTextField = "NOM_RESEARCH_TEMA";
                DDL_tema.DataValueField = "ID_RESEARCH_TEMA";
                DDL_tema.DataBind();
            }
            else
            {
                seltext.Style.Add("display", "block");
            }
        }

        protected String GetNewsRelatedAuth()
        {
            string rtn = "";
            rtn = rtnNewsRelatedUC();
            return rtn;
        }
        private string rtnNewsRelatedUC()
        {
            return "<ucNewsRelated:getNewsRelated ID=\"getNewsRelated1\" runat=\"server\" Visible=\"true\" />";
        }

        protected void upcont1_Load(object sender, EventArgs e)
        {
            //upcont1.Update();
        }

        protected void DDL_research_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable Research = Global.InsigteManager[Session.SessionID].getTableQuery(" select * from CL10D_RESEARCH_TEMA  with(nolock) where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and ID_CLIENT_USER = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and ID_RESEARCH = " + DDL_research.SelectedValue.ToString(), "getResearchTemaLists");

            DDL_tema.DataSource = Research;
            DDL_tema.DataTextField = "NOM_RESEARCH_TEMA";
            DDL_tema.DataValueField = "ID_RESEARCH_TEMA";
            DDL_tema.DataBind();
        }

        protected void btn_ins_research_Click(object sender, EventArgs e)
        {
            int error = 0;
            String Msg = "";

            if (selectedtext.Value.Trim().Length == 0)
            {
                error++;
                Msg += "Texto em falta!<br>";
            }

            if (DDL_research.SelectedValue.Length == 0)
            {
                error++;
                Msg += "Precisa de criar uma pasta primeiro!<br>";
            }

            if (DDL_tema.SelectedValue.Length == 0)
            {
                error++;
                Msg += "Precisa de criar um tema primeiro!<br>";
            }

            if (error == 0)
            {
                String Query = "";

                Query += " insert into CL10H_NEWS_RESEARCH (ID_CLIENT, ID_CLIENT_USER, ID_RESEARCH, ID_NEWS, ID_RESEARCH_TEMA, TXT_RESEARCH, TXT_TITULO, TXT_NOTA, DAT_CRIACAO, DAT_MODIFICACAO) ";
                Query += " select " + Global.InsigteManager[Session.SessionID].IdClient + ", " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + ", " + DDL_research.SelectedValue.ToString() + ", " + qsIDArtigo + ", " + DDL_tema.SelectedValue.ToString() + ", '" + selectedtext.Value.Replace("'", "''") + "', '" + txt_titulo.Value.Replace("'", "''") + "', '" + txt_nota.Value.Replace("'", "''") + "', getdate(), getdate() ";

                DataTable InsRes = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "InsRes");

                selectedtext.Value = "";
                txt_nota.Value = "";
                lbInfo.Text = "Guardado com sucesso!";
            }
            else
            {
                lbInfo.Text = Msg;
            }
        }

        private void getArtigo(string qsIDArtigo)
        {
            String Title = "TITLE";
            String text = "text";
            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            {
                Title = "TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
                text = "text_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as text";
            }

            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                SqlConnection conn2 = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
                conn2.Open();

                string entidade = " select id_entidade, cod_terms from iadvisers.dbo.dim_entidade  with(nolock) ";

                SqlCommand cmdEnt = new SqlCommand(entidade);
                cmdEnt.Connection = conn2;
                cmdEnt.CommandType = CommandType.Text;
                SqlDataReader readerEnt = cmdEnt.ExecuteReader();

                List<ListItem> Entidades = new List<ListItem>();
                while (readerEnt.Read())
                {
                    ListItem x = new ListItem(readerEnt["cod_terms"].ToString(), readerEnt["id_entidade"].ToString());
                    Entidades.Add(x);
                }

                conn2.Close();
                conn2.Dispose();

                conn.Open();

                string cmdQuery = "SELECT " + text + ", " + Title + ", EDITOR, DATE, iadvisers.dbo.metadata.ID, filepath, page_url, tipo ";
                if (Global.InsigteManager[Session.SessionID].IdClient == "1096")
                {
                    cmdQuery += " ,(select AVE_USD from [dbo].[fnCalMediaValue] (iadvisers.dbo.metadata.id) ) as AVE ";
                    cmdQuery += " ,(select OTS from [dbo].[fnCalMediaValue] (iadvisers.dbo.metadata.id) ) as OTS ";
                }
                cmdQuery += "FROM iadvisers.dbo.metadata with(nolock) INNER JOIN iadvisers.dbo.contents with(nolock) ON iadvisers.dbo.metadata.ID=iadvisers.dbo.contents.ID ";
                cmdQuery += "where iadvisers.dbo.metadata.ID='" + qsIDArtigo + "'";

                SqlCommand cmd = new SqlCommand(cmdQuery);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    lblArticleTitle.Text = reader.GetValue(1).ToString();
                    txt_titulo.Value = reader.GetValue(1).ToString();
                    lblArticleEditorData.Text = "(" + reader.GetValue(2).ToString() + " | " + reader.GetValue(3).ToString().Substring(0, 10) + ")";
                    lblArticleText.Text = reader.GetValue(0).ToString().Replace(Environment.NewLine, "<br />");

                    if (Global.InsigteManager[Session.SessionID].IdClient == "1096")
                    {
                        int getave = Convert.ToInt32(string.IsNullOrEmpty(reader.GetValue(8).ToString()) ? "0" : reader.GetValue(8).ToString());
                        int getots = Convert.ToInt32(string.IsNullOrEmpty(reader.GetValue(8).ToString()) ? "0" : reader.GetValue(9).ToString());
                        Thread.CurrentThread.CurrentCulture = new CultureInfo("pt");
                        string ave = String.Format("{0:#,#}", getave);
                        string ots = String.Format("{0:#,#}", getots);
                        lblArticleAVE.Text = "AVE: " + ave + " € <br/>OTS: " + ots;
                    }
                    else
                    {
                        lblArticleAVE.Text = " ";
                    }

                    if (SearchActive == "1")
                    {

                        String Words = Session["ADVSearchTxt"].ToString();

                        if (Session["ADVSearchTxt"].ToString().Contains("\""))
                        {
                            //@"(?<="")[^""]+(?="")|[^\s""]\S*"
                            foreach (Match m in Regex.Matches(Session["ADVSearchTxt"].ToString(), "\"([^\".]+)\""))
                            {
                                //lblArticleTitle.Text += "<br>Word: " + m.Value;
                                Words = Words.Replace(m.Value, " ");
                            }
                        }

                        //Words = Regex.Replace(Words, "or", " ", RegexOptions.IgnoreCase);
                        //Words = Regex.Replace(Words, "NOT", " ", RegexOptions.IgnoreCase);
                        //Words = Regex.Replace(Words, "AND", " ", RegexOptions.IgnoreCase);
                        //Words = Regex.Replace(Words, "NEAR", " ", RegexOptions.IgnoreCase);

                        //String Words = Session["ADVSearchTxt"].ToString();
                        Words = Words.Replace("(", " ").Replace("\"", "").Replace(")", " ");
                        Words = Words.Replace("  ", " ").Replace("  ", " ").Replace("  ", " ");
                        Words = Words.Replace(" OR ", " ").Replace(" or ", " ").Replace(" Or ", " ");
                        Words = Words.Replace(" NOT ", " ").Replace(" not ", " ").Replace(" Not ", " ");
                        Words = Words.Replace(" AND ", " ").Replace(" and ", " ").Replace(" And ", " ");
                        Words = Words.Replace(" NEAR ", " ").Replace(" near ", " ").Replace(" Near ", " ");

                        //Words = Words.Replace("OR", " ").Replace("or", " ").Replace("Or", " ");
                        //Words = Words.Replace("NOT", " ").Replace("not", " ").Replace("Not", " ");
                        //Words = Words.Replace("AND", " ").Replace("and", " ").Replace("And", " ");
                        //Words = Words.Replace("NEAR", " ").Replace("near", " ").Replace("Near", " ");

                        Words = Words.Replace("  ", " ").Replace("  ", " ").Replace("  ", " ");

                        String[] Replaces = Words.Split(' ');
                        foreach (String x in Replaces)
                        {
                            if (x.Length > 0)
                            {
                                lblArticleText.Text = lblArticleText.Text.Replace(x, "<b>" + x + "</b>").Replace(Environment.NewLine, "<br />");
                                lblArticleText.Text = lblArticleText.Text.Replace(x.Substring(0, 1).ToUpper() + x.Substring(1, x.Length - 1), "<b>" + x.Substring(0, 1).ToUpper() + x.Substring(1, x.Length - 1) + "</b>").Replace(Environment.NewLine, "<br />");
                                lblArticleText.Text = lblArticleText.Text.Replace(x.ToUpper(), "<b>" + x.ToUpper() + "</b>").Replace(Environment.NewLine, "<br />");
                                lblArticleText.Text = lblArticleText.Text.Replace(x.ToLower(), "<b>" + x.ToLower() + "</b>").Replace(Environment.NewLine, "<br />");
                            }
                        }

                        foreach (Match m in Regex.Matches(Session["ADVSearchTxt"].ToString(), "\"([^\".]+)\""))
                        {
                            String x = m.Value.Replace("\"", "");

                            if (x.Length > 0)
                            {
                                String Cap = Global.InsigteManager[Session.SessionID].FirstCharToUpper(x);

                                //lblArticleTitle.Text += "<br>Word: " + Cap;

                                lblArticleText.Text = lblArticleText.Text.Replace(Cap, "<b>" + Cap + "</b>").Replace(Environment.NewLine, "<br />");

                                lblArticleText.Text = lblArticleText.Text.Replace(x, "<b>" + x + "</b>").Replace(Environment.NewLine, "<br />");
                                lblArticleText.Text = lblArticleText.Text.Replace(x.Substring(0, 1).ToUpper() + x.Substring(1, x.Length - 1), "<b>" + x.Substring(0, 1).ToUpper() + x.Substring(1, x.Length - 1) + "</b>").Replace(Environment.NewLine, "<br />");
                                lblArticleText.Text = lblArticleText.Text.Replace(x.ToUpper(), "<b>" + x.ToUpper() + "</b>").Replace(Environment.NewLine, "<br />");
                                lblArticleText.Text = lblArticleText.Text.Replace(x.ToLower(), "<b>" + x.ToLower() + "</b>").Replace(Environment.NewLine, "<br />");
                            }
                        }
                    }
                    else
                    {
                        
                        //marcar a bold
                        #region Fabio 20150202
                        try
                        {
                            //marcar a bold todas sd palavras do tema que estao na tablela BI10X_MARCACAO_TEMA
                            string query = "";
                            query += " SELECT distinct COD_WORDS ";
                            query += " FROM [iadvisers].[dbo].[BI10X_MARCACAO_TEMA] with(nolock) ";
                            query += " WHERE cast(COD_TEMA as varchar(4))  + cast(cod_subtema as varchar(3))  = '" + Global.InsigteManager[Session.SessionID].TemaCliente.ToString() + "' ";
                            query += " and ind_bold = 1 ";

                            DataTable dtLstWords = Global.InsigteManager[Session.SessionID].getTableQuery(query, "dtLstWords");

                            if (dtLstWords != null)
                            {
                                if (dtLstWords.Rows.Count > 0)
                                {
                                    foreach(DataRow row in dtLstWords.Rows)
                                    {
                                        foreach(string str in row["COD_WORDS"].ToString().Split(';'))
                                        {
                                            //if (str.Contains(Global.InsigteManager[Session.SessionID].NomCliente))
                                            //    str.Replace(" " + Global.InsigteManager[Session.SessionID].NomCliente, "");

                                            lblArticleText.Text = lblArticleText.Text.Replace(str, "<font face='Arial' style='background-color:#DCDCDC; font-size=12px'><b>" + str + "</b></font>");
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception er)
                        {
                            er.Message.ToString();
                        }
                        #endregion

                        lblArticleText.Text = lblArticleText.Text.Replace(Global.InsigteManager[Session.SessionID].NomCliente, "<b>" + Global.InsigteManager[Session.SessionID].NomCliente + "</b>").Replace(Environment.NewLine, "<br />");
                    }

                    try
                    {
                        foreach (ListItem enti in Entidades)
                        {
                            String[] term = enti.Text.Split(';');
                            foreach (String x in term)
                                lblArticleText.Text = lblArticleText.Text.Replace(x, "<a class=\"ent\" href=\"ArticleInfo.aspx?ID=" + qsIDArtigo + "&etd=" + enti.Value + "\">" + x + "</a>").Replace(Environment.NewLine, "<br />");
                        }
                    }
                    catch (Exception er)
                    {
                        er.Message.ToString();
                    }

                    lblEmailLink.Text = "<a id=\"share-news\" onclick=\"shareTheNews('" + reader.GetValue(4).ToString() + "','" + reader.GetValue(1).ToString().Replace("'", "") + "')\" style='font-family:Arial; font-size:12px; text-decoration:none; color:#000000;' href=\"#\" >" + Resources.insigte.language.shareFormDesc + "</a>";

                    if (reader.GetValue(6).ToString() == null || reader.GetValue(6).ToString() == string.Empty)
                    {
                        String FileHelperPath = "ficheiros";
                        if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1" && reader.GetValue(7).ToString() == "Imprensa")
                        {
                            if (File.Exists(@"G:\cfiles\" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + reader.GetValue(5).ToString().Replace("/", "\\")))
                            {
                                FileHelperPath = "cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient;
                            }
                        }

                        imgDLorLINK.ImageUrl = "Imgs/icons/black/png/download_icon_16.png";
                        lblPath.Text = "<a href='http://www.insigte.com/" + FileHelperPath + "/" + reader.GetValue(5).ToString() + "' target='_blank' style='font-family:Arial; font-size:12px; text-decoration:none; color:#000000;'>Download</a>";
                    }
                    else
                    {
                        imgDLorLINK.ImageUrl = "Imgs/icons/black/png/link_icon_16.png";
                        imgDLorLINK.Height = 16;
                        lblPath.Text = "<a href='" + reader.GetValue(6).ToString() + "' target='_blank' style='font-family:Arial; font-size:12px; text-decoration:none; color:#000000;'>Link</a>";
                    }

                    //ANEXOS
                    try
                    {
                        string query = " select * from metadata_attach with(nolock) where id_news = " + qsIDArtigo;
                        DataTable dtAnexo = Global.InsigteManager[Session.SessionID].getTableQuery(query, "GetAnexos");

                        if (dtAnexo != null)
                        {
                            if (dtAnexo.Rows.Count > 0)
                            {
                                //tcAnexosImg.Visible = true;
                                //tcAnexos.Visible = true;
                                divAnexos.Visible = true;
                                //imgAnexos.Visible = true;
                                lblAnexos.Visible = true;
                                lblAnexos.Text = (Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt" ? "Anexos" : "attachments");

                                //imgAnexos.ImageUrl = "Imgs/icons/black/png/clip_icon_16.png";
                                imgAnexos.Height = 16;
                                string inner = "";
                                foreach (DataRow row in dtAnexo.Rows)
                                {
                                    inner += "<div style=\"float:left; margin-top:5px; margin-left: 15px; width:100%;\"><a href='http://www.insigte.com/Ficheiros/" + row["file_path"].ToString() + "' target='_blank' style='font-family:Arial; font-size:12px; text-decoration:none; color:#000000;'>" + row["file_path"].ToString().Split('/').Last() + "</a></div>";
                                }
                                lstAnexos.InnerHtml = inner;
                            }
                        }
                        else
                        {
                            //tcAnexosImg.Visible = false;
                            //tcAnexos.Visible = false;
                            divAnexos.Visible = false;
                            //imgAnexos.Visible = false;
                            //lblAnexos.Visible = false;
                        }
                    }
                    catch (Exception er)
                    {
                        er.Message.ToString();
                    }
                    if (chkMyNewsArchive(reader.GetValue(4).ToString()))
                    {
                        imgAddMyArticles.ImageUrl = "Imgs/icons/black/png/clipboard_copy_icon_16_sel.png";
                        imgAddMyArticles.ToolTip = Resources.insigte.language.arColClipsGuardados;
                        lblAddMyArticle.Text = Resources.insigte.language.arColClipsGuardados;
                    }
                    else
                    {
                        imgAddMyArticles.ImageUrl = "Imgs/icons/black/png/clipboard_copy_icon_16.png";
                        imgAddMyArticles.ToolTip = Resources.insigte.language.arColClipsAdicionar;
                        lblAddMyArticle.Text = Resources.insigte.language.arColClipsAdicionar;
                    }

                    if (chkMyPDFArchive(reader.GetValue(4).ToString()))
                    {
                        imgPDFMyArticles.ImageUrl = "Imgs/icons/black/png/book_side_icon_16_sel.png";
                        imgPDFMyArticles.ToolTip = Resources.insigte.language.ucArcLink3;
                        lblPDFMyArticle.Text = Resources.insigte.language.ucArcLink3;
                    }
                    else
                    {
                        imgPDFMyArticles.ImageUrl = "Imgs/icons/black/png/book_side_icon_16.png";
                        imgPDFMyArticles.ToolTip = Resources.insigte.language.ucArcLink3;
                        lblPDFMyArticle.Text = Resources.insigte.language.ucArcLink3;
                    }
                }
                cmd.Connection.Close();
                cmd.Connection.Dispose();
            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }
        }

        private bool chkMyNewsArchive(string idArticle)
        {
            if (HttpContext.Current.Request.Cookies["CookieinsigteSys"] == null)
            {
                return false;
            }
            else
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["ID"] == null)
                {
                    return false;
                }
                else
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["ID"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(idArticle))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (idVal == idArticle)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
        }

        private bool chkMyPDFArchive(string idArticle)
        {
            if (HttpContext.Current.Request.Cookies["CookieinsigteSys"] == null)
            {
                return false;
            }
            else
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] == null)
                {
                    return false;
                }
                else
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(idArticle))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (idVal == idArticle)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
        }

        protected void bt_AddPDF_Command(Object sender, CommandEventArgs e)
        {
            addLinkPDF(qsIDArtigo);
            imgPDFMyArticles.ImageUrl = "Imgs/icons/black/png/book_side_icon_16_sel.png";
        }

        protected void bt_AddArquivo_Command(Object sender, CommandEventArgs e)
        {
            addNews(qsIDArtigo);

            imgAddMyArticles.ImageUrl = "Imgs/icons/black/png/clipboard_copy_icon_16_sel.png";
            lblAddMyArticle.Text = Resources.insigte.language.arColClipsGuardados;
        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        protected void addNews(string qsIDArtigo)
        {
            string ValCookie = string.Empty;
            string PDFCookie = string.Empty;
            string DelCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

            if (cookie.Values["ID"] != "")
            {
                ValCookie = cookie.Values["ID"] + "|" + qsIDArtigo;
            }
            else
            {
                ValCookie = qsIDArtigo;
            }

            DelCookie = cookie.Values["DELNOT"];
            PDFCookie = cookie.Values["PDF"];

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = PDFCookie;
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(360);
            Response.Cookies.Add(newCookie);

        }

        protected void addLinkPDF(string qsPDFArtigo)
        {

            string ValCookie = string.Empty;
            string PDFCookie = string.Empty;
            string DelCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

            if (cookie.Values["PDF"] != "")
            {
                PDFCookie = cookie.Values["PDF"] + "|" + qsPDFArtigo;
            }
            else
            {
                PDFCookie = qsPDFArtigo;
            }

            DelCookie = cookie.Values["DELNOT"];
            ValCookie = cookie.Values["ID"];

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = PDFCookie;
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(360);
            Response.Cookies.Add(newCookie);

        }

        protected String getSelectJava()
        {
            String JavaCode = "";

            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(11, 1) == "0")
            {
                JavaCode = "";
            }
            else
            {
                //JavaCode += "            //---- SELECT TEXT -------------------- ";

                JavaCode += "        function getSelected() { " + Environment.NewLine;
                JavaCode += "            if (window.getSelection) { " + Environment.NewLine;
                JavaCode += "                return window.getSelection(); " + Environment.NewLine;
                JavaCode += "            } " + Environment.NewLine;
                JavaCode += "            else if (document.getSelection) { " + Environment.NewLine;
                JavaCode += "                return document.getSelection(); " + Environment.NewLine;
                JavaCode += "            } " + Environment.NewLine;
                JavaCode += "            else { " + Environment.NewLine;
                JavaCode += "                var selection = document.selection && document.selection.createRange(); " + Environment.NewLine;
                JavaCode += "                if (selection.text) { " + Environment.NewLine;
                JavaCode += "                    return selection.text; " + Environment.NewLine;
                JavaCode += "                } " + Environment.NewLine;
                JavaCode += "                return false; " + Environment.NewLine;
                JavaCode += "            } " + Environment.NewLine;
                JavaCode += "            return false; " + Environment.NewLine;
                JavaCode += "        } " + Environment.NewLine;

                JavaCode += "        function chkSelected() { " + Environment.NewLine;
                JavaCode += "            var texto = ''; " + Environment.NewLine;

                JavaCode += "            if (window.getSelection) { " + Environment.NewLine;
                JavaCode += "       texto = window.getSelection(); " + Environment.NewLine;
                JavaCode += "   } " + Environment.NewLine;
                JavaCode += "   else if (document.getSelection) { " + Environment.NewLine;
                JavaCode += "       texto = document.getSelection(); " + Environment.NewLine;
                JavaCode += "   } " + Environment.NewLine;
                JavaCode += "   else { " + Environment.NewLine;
                JavaCode += "       var selection = document.selection && document.selection.createRange(); " + Environment.NewLine;
                JavaCode += "       if (selection.text) { " + Environment.NewLine;
                JavaCode += "           texto = selection.text; " + Environment.NewLine;
                JavaCode += "       } " + Environment.NewLine;
                JavaCode += "       return false; " + Environment.NewLine;
                JavaCode += "   } " + Environment.NewLine;

                JavaCode += "   if (texto.toString().length > 0) { " + Environment.NewLine;
                JavaCode += "       return true; " + Environment.NewLine;
                JavaCode += "   } " + Environment.NewLine;
                JavaCode += "   else { " + Environment.NewLine;
                JavaCode += "       return false; " + Environment.NewLine;
                JavaCode += "   } " + Environment.NewLine;
                JavaCode += "        } " + Environment.NewLine;

                JavaCode += "        $(document).ready(function () { " + Environment.NewLine;
                JavaCode += "   var selectionImage; " + Environment.NewLine;
                JavaCode += "   $('#mensagem').mouseup(function (e) { " + Environment.NewLine;
                JavaCode += "       var selection = getSelected(); " + Environment.NewLine;
                JavaCode += "       var isSelected = chkSelected(); " + Environment.NewLine;

                JavaCode += "       if (!selectionImage && isSelected) { " + Environment.NewLine;

                JavaCode += "           var txtBox = document.getElementById('MainContent_seltext'); " + Environment.NewLine;
                JavaCode += "           txtBox.setAttribute(\"style\", \"display:block;\"); " + Environment.NewLine;

                JavaCode += "           selectionImage = $('<button>').attr({ " + Environment.NewLine;
                JavaCode += "               type: 'button', " + Environment.NewLine;
                JavaCode += "               title: 'Guardar Texto seleccionado', " + Environment.NewLine;
                JavaCode += "               id: 'quote-place', " + Environment.NewLine;
                JavaCode += "               class: 'btn-pdfs' " + Environment.NewLine;

                JavaCode += "                    }).html(\"Guardar\").css({ " + Environment.NewLine;
                JavaCode += "                        \"color\": \"#FFFFFF\" " + Environment.NewLine;
                JavaCode += "                    }).hide(); " + Environment.NewLine;

                JavaCode += "                    $(document.body).append(selectionImage); " + Environment.NewLine;

                JavaCode += "                } " + Environment.NewLine;

                JavaCode += "                $(\"#quote-place\").unbind(\"click\").click(function quote() { " + Environment.NewLine;
                JavaCode += "                    var txt = ''; " + Environment.NewLine;

                JavaCode += "                    if (window.getSelection) { " + Environment.NewLine;
                JavaCode += "                        txt = window.getSelection(); " + Environment.NewLine;
                JavaCode += "                    } " + Environment.NewLine;
                JavaCode += "                    else if (document.getSelection) { " + Environment.NewLine;
                JavaCode += "                        txt = document.getSelection(); " + Environment.NewLine;
                JavaCode += "                    } " + Environment.NewLine;
                JavaCode += "                    else if (document.selection) { " + Environment.NewLine;
                JavaCode += "                        txt = document.selection.createRange().text; " + Environment.NewLine;
                JavaCode += "                    } " + Environment.NewLine;
                JavaCode += "                    else { " + Environment.NewLine;
                JavaCode += "                        return; " + Environment.NewLine;
                JavaCode += "                    } " + Environment.NewLine;

                JavaCode += "                    var txt_save = document.getElementById('MainContent_selectedtext') " + Environment.NewLine;
                JavaCode += "                    txt_save.value += txt; " + Environment.NewLine;


                JavaCode += "                }).mousedown(function () { " + Environment.NewLine;

                JavaCode += "                    if (selectionImage) { " + Environment.NewLine;
                JavaCode += "                        selectionImage.fadeOut(); " + Environment.NewLine;
                JavaCode += "                    } " + Environment.NewLine;
                JavaCode += "                }); " + Environment.NewLine;

                JavaCode += "                if (isSelected) { " + Environment.NewLine;
                JavaCode += "                    selectionImage.css({ " + Environment.NewLine;
                JavaCode += "                        top: e.pageY - 30, " + Environment.NewLine;
                //JavaCode += "                        //offsets " + Environment.NewLine;
                JavaCode += "                        left: e.pageX - 13 " + Environment.NewLine;
                JavaCode += "                    }).fadeIn(); " + Environment.NewLine;
                JavaCode += "                } else { " + Environment.NewLine;
                JavaCode += "                    selectionImage.css({ " + Environment.NewLine;
                JavaCode += "                        top: e.pageY - 30, " + Environment.NewLine;
                //JavaCode += "                        //offsets " + Environment.NewLine;
                JavaCode += "                        left: e.pageX - 13  " + Environment.NewLine;
                JavaCode += "                    }).fadeOut(); " + Environment.NewLine;

                JavaCode += "                } " + Environment.NewLine;
                JavaCode += "            }); " + Environment.NewLine;
                JavaCode += "        }); " + Environment.NewLine;
            }

            return JavaCode;
        }
    }
}