﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GeoMZ.aspx.cs" Inherits="Insigte.GeoMZ" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
    #section.sector
    {
        float:left;
        border :1px solid Black;
        width: 100%;
        min-height: 300px;
        min-width:400px;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
        padding:0px;
        margin:0px;
    }
    
    .form_search
    {
        float:left;
        width: 100%;
        height:10px;
        padding-top: 4px;
        padding-bottom: 10px;
        margin: 0 auto 5px auto;
        background-color: #323232;
        color: #FFFFFF;
    } 
    
    .mapMz
    {
       float:left;
       /*border:1px solid black;*/
       margin:15px 5px 10px 15px;
       width:360px;
       height:556px;
    }

    .info
    {
        /*border:1px solid black;*/
        display:block; 
        position:absolute;               
        border-radius:5px; 
        font-family:Arial;
        font-size:12px;
    }

    .infoNome
    {    
        color:white;        
        background-color:#323232;
        padding:2px;
        border-top-left-radius:5px;
        border-top-right-radius:5px; 
                
    }
    
    .infoDesc
    {
        padding:2px;
        background-color:#f0f0f0; 
        border-bottom-left-radius:5px;
        border-bottom-right-radius:5px;            
    }
    
    .regionInfo
    {
        float:right;
        border:0px solid black;
        margin:15px 15px 10px 0px;
       /* height:556px;*/
        width:400px;
        border-top-left-radius:5px;
        border-top-right-radius:5px;     
    } 
   
    .divChart1
    {
        text-align:left; 
        color:black; 
        font-size:14px; 
        font-family:Arial; 
        margin-left: 13px; 
        font-weight:bold;    
    }
    
    .Region
    {
        fill:#323232;
        
    }
    .Region_Over
    {
        /* fill:#00add9;*/
        fill:#f89c3e;
        fill-opacity:0.20;
       
    }
    .Region_select
    {
        fill:#f89c3e;    
    }
    .progress
    {
        text-align:center;
        vertical-align:middle;
        position: relative;
        float:right;
        border-style:outset;
        border-color:silver;
        background-color:Silver;
        white-space:nowrap;
        padding:5px;
    }
    
    #dialog_form_help_blog
    {
        
        display:none;
        height:auto;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family: Arial;
    }
     
     #dialog_form_help_blog p.newDesc { color: #000; }
   
    .omfgover
    {
       cursor:pointer;
       border:1px solid #d5d5d5; 
       border-width:1px;
       
    }
    
    .omfg
    {
       border-width: 1px;
       cursor:pointer;
    }
     
</style>
<script type="text/javascript">
    var posx=0; var posy=0;
    var clicked;
    var chartid = "";
   
    function chartmouseover(divnome)
    {  
            divnome.setAttribute("class", "omfgover");
            divnome.setAttribute("style", "border-width: 1px"); 
 
    }

      function chartmouseout(divnome)
    {   
            divnome.setAttribute("class", "omfg"); 
            divnome.setAttribute("fill", "#f89c3e");
    }
   

    function teste(chart) 
    {
        if(clicked != null)
        {
            var testse  = clicked.getAttribute('nome').toString();
            var parameters = "ChartZoom;" + chart.getAttribute('value') + ";" + clicked.getAttribute('nome').toString();
            __doPostBack('<%=UpdatePanel1.ClientID %>', parameters);
            $("#dialog_form_help_blog").dialog("open");
        }else
        {
            var parameters = "ChartZoom;" + chart.getAttribute('value');
            __doPostBack('<%=UpdatePanel1.ClientID %>', parameters);
            $("#dialog_form_help_blog").dialog("open");
        }
    } 
    
    $(function () 
    {
        $("#dialog_form_help_blog").dialog({
        resizable: false,
        autoOpen: false,
        height: 'auto',
        width: 'auto',
        modal: true,
        beforeClose: 'destroy'      
        });
    });


    $(document).mousemove(function(e){  
        var $div = $("#mapMz");       
        posx= e.pageX - $div.offset().left,
        posy= e.pageY - $div.offset().top       
    });

    function trataFill(){ 
     
        <%= getTrataFill() %>
    }
       
    function mouseOver(Region){
        Region.setAttribute("fill", "#f89c3e");
        Region.setAttribute("fill-opacity", "0.20"); 
        Region.setAttribute("cursor", "pointer"); 
        
    }

    function mouseOut(Region){
        Region.setAttribute("fill-opacity", "0.01");
        var t = document.getElementById("info");
        t.setAttribute("style", "display:none;");
        trataFill();
       
    }

    function SetValues(Region) {
       var _info = document.getElementById('info');
        _info.setAttribute("style", " top: " + (posy + 270) + "px; left: " + (posx+25)  + "px; ");
        var _infoNome = document.getElementById('infoNome');      
       _infoNome.textContent = Region.getAttribute('nome').toString();
        _infoNome.setAttribute("class", "infoNome");                  
        var _infoText = document.getElementById('infoDesc');         
        _infoText.innerHTML = Region.getAttribute('value').toString() + " Article(s)";
        _infoText.setAttribute("class", "infoDesc");
          
    }

    function MouseClick(Region){
        clicked = Region;      
        var nome = Region.getAttribute('nome');
        var id = Region.getAttribute('id');       
        Region.setAttribute("fill", "#f89c3e");
        Region.setAttribute("fill-opacity", "0.60");        
        var parameters = nome + ";" + id;
        __doPostBack('<%=upcont1.ClientID %>', parameters);       
        trataFill(); 
    }

     function restartMap(t){
        clicked = null;
        var regStatus = document.getElementById('regionInfo');
        regStatus.setAttribute("style", "display:block;");
        var parameters = "restart";
        __doPostBack('<%=upcont1.ClientID %>', parameters);
        trataFill();     
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="dialog_form_help_blog" title="Insigte Charts">
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" >
  <ContentTemplate> 
    
	    <p class="newDesc">&nbsp;</p>
         <asp:Chart ID="chZoom" runat="server" Width="900px" Height="400px" Palette="Chocolate">
            <Titles>
            <asp:Title Name="ttZoom"></asp:Title>
            </Titles>
            <Series>
                <asp:Series Name="srZoom">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="caZoom">
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
        <%--<img id="imgtest" runat="server" alt="" />--%>
    
  </ContentTemplate>            
</asp:UpdatePanel>
</div>
<div id="Geral" style="float:left; padding:0px; margin:0px; height:100%; width:800px;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%" > 
        <div style="margin-top:12px">
            <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false" />
        </div>
    </div>
    <div id="section" class="sector ui-corner-top" style="float:left;width:100%;">
        <div id="titulo" class="form_search headbg ui-corner-top" style="float:left;width:100%;">
            <div id="caixa" style="float:left;width:80%;">
              <div id="lblTitleAdvSearch"  style="font-family:Arial; cursor:pointer; float:left; font-size:12px; color:#FFFFFF;" onclick=' restartMap(this); ' >&nbsp;Geo-MKT | Beta 1.0</div>
            </div>
            <div id="help" style="float:left;width:20%; ">
                <div style="float:right;padding-right:5px; display: none;">
                    <img alt="Ajuda" src="Imgs/icons/white/png/info_icon_16.png" />
                </div>
            </div>
        </div>      
        <div id="mapMz" class="mapMz">
            <div style="text-align:left; color:#f89c3e; font-size:12px; font-family:Arial;">Moçambique</div>
            <svg  width="338.789" height="522.018" viewBox="0 0 181.00 278.00" enable-background="new 0 0 181.00 278.00" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">	          
                <%--<image x="0" y="0" width="362" style="display:none;" height="556" transform="matrix(1.00,0.00,0.00,1.00,0.00,0.00)" xlink:href="mozambiquer_files/image2.png"/>--%>	      
	            <%= getPaths()%>
            </svg>
            <div id="info" class="info" style="display:none;">
	            <div id="infoNome" class="infoNome"></div>
                <div id="infoDesc" class="infoDesc"></div>
            </div> 
        </div> 
         
        <asp:UpdatePanel ID="upcont1" runat="server" UpdateMode="Conditional" >
        
            <ContentTemplate>  
            
                <div id="regionInfo" class="regionInfo" style="display:block;">
                        <div id="porvNome" class="divChart1" runat="server" visible="false"></div>
                        
                        <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="upcont1" runat="server" >
                                <ProgressTemplate>
                               <%-- <div class="progress">please wait...</div>--%>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        
                        
                            <asp:Chart ID="chEvolReg" CssClass="omfg" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                                <Titles>
                                <asp:Title Name="ttEvolReg">
                                </asp:Title>
                                </Titles>
                                <Series>
                                    <asp:Series Name="srEvolReg">
                                    </asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="caEvolReg">
                                    </asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>
                            <asp:UpdateProgress ID="UpdateProgress2" AssociatedUpdatePanelID="upcont1" runat="server">
                                <ProgressTemplate>
                               <%-- <div class="progress">please wait...</div>--%>
                                </ProgressTemplate>
                                
                            </asp:UpdateProgress>
                            <asp:Chart ID="chEvolRegCli" CssClass="omfg" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                                <Titles>
                                <asp:Title Name="ttEvolRegCli"></asp:Title>
                                </Titles>
                                <Series>
                                    <asp:Series Name="srEvolRegCli">
                                    </asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="caEvolRegCli">
                                    </asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>
                        
                    
                </div>            
                <div id="DivChartCon6M" runat="server" style="float:left; border:0px solid black; margin:5px; width:789px; height:320px;">
                    <div id="grc" runat="server">
                        <asp:UpdateProgress ID="UpdateProgress3" AssociatedUpdatePanelID="upcont1" runat="server" >
                                <ProgressTemplate>
                                <%--<div class="progress">please wait...</div>--%>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        
                        <asp:Chart ID="chcGeral" CssClass="omfg" runat="server" Width="789px" Height="320px" Palette="Chocolate">
                            <Titles>
                                <asp:Title Name="ttcGeral" >
                            </asp:Title>
                            </Titles>
                            <Series>
                                <asp:Series Name="srcGeral">
                                </asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="cacGeral">
                                </asp:ChartArea>
                            </ChartAreas>
                        </asp:Chart>
                        
                    </div> 
                </div>
                <%--<div id="DivChartCon6M1" runat="server" style="float:left; border:1px solid black; margin:5px; width:789px; height:320px;">
                    <div id="grd" runat="server">
                        <asp:UpdateProgress ID="UpdateProgress4" AssociatedUpdatePanelID="upcont1" runat="server">
                                <ProgressTemplate>
                                  UpdatePanel1 updating...
                                  
                                </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:Chart ID="chdGeral" runat="server" Width="789px" Height="320px" Palette="Chocolate">
                            <Titles>
                            <asp:Title Name="ttdGeral" >
                            </asp:Title>
                            </Titles>
                            <ChartAreas>
                                <asp:ChartArea Name="cadGeral">
                                </asp:ChartArea>
                            </ChartAreas>
                        </asp:Chart>
                    </div>
                </div>--%>
            </ContentTemplate>            
        </asp:UpdatePanel>
    </div>
</div>

</asp:Content>
