﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using RestSharp;
using RestSharp.Authenticators;
using System.Threading.Tasks;


namespace Insigte
{
    public partial class NewsChoose : BasePage
    {
        private static string username = "InApi2015";
        private static string password = "Insigte2015Api";


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(1, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }

            if (!Page.IsPostBack)
            {

                getNews();
            }
        }

        private void getNews()
        {
            try
            {
                string query = string.Empty;
                string emailDaily = (Global.InsigteManager[Session.SessionID].IdClient != "1099") ? Global.InsigteManager[Session.SessionID].inUser.Email : "admin@garrigues.com";
                query = "select distinct cast(isnull(editors,'*') as nvarchar(max)) as editors from clientmaildaily with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and cast(email as nvarchar(max)) = '" + emailDaily + "'";

                DataTable newseditors = Global.InsigteManager[Session.SessionID].getTableQuery(query, "editoresFiltro");
                string lst = string.Empty;

                foreach (DataRow row in newseditors.Rows)
                {
                    lst += "'" + row["editors"].ToString() + "',";
                }

                lst = lst.TrimEnd(',').Replace(",", "','");

                query = "iadvisers.dbo.[NewsChoose] " + Global.InsigteManager[Session.SessionID].IdClient + "," + Global.InsigteManager[Session.SessionID].inUser.IdClientUser;

                DataTable dt = Global.InsigteManager[Session.SessionID].getTableQuery(query, "Noticiaparasair");
                Session["NewsToSendCount"] = 0;
                if (dt.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(lst) && !lst.Contains("*"))
                    {

                        foreach (DataRow row in dt.Rows)
                        {
                            if (!lst.Contains(row["editorId"].ToString()))
                                try
                                {
                                    row.Delete();
                                }
                                catch (Exception er)
                                {
                                    er.Message.ToString();
                                }
                        }

                    }

                    dt.AcceptChanges();

                    Session["NewsToSendCount"] = dt.Rows.Count;

                    if (dt.Rows.Count > 0)
                    {
                        lblDataToday.Visible = true;
                        lblDataToday.Text = "Total ( " + dt.Rows.Count.ToString() + " )";
                        Session["NewsToSend"] = dt;

                        DataView view1 = dt.DefaultView.ToTable(true, "tema", "name", "name_en").DefaultView;
                        view1.Sort = "name ASC";

                        //System.Windows.Forms.BindingSource source1 = new System.Windows.Forms.BindingSource();
                        //source1.DataSource = view1;

                        //source1.Sort = "name ASC";
                        rp_NewsTemas.DataSource = view1;

                        rp_NewsTemas.DataBind();

                        //rp_NewsTemas.DataSource = dt.DefaultView.ToTable(true, "tema", "name", "name_en").DefaultView;

                        //rp_NewsTemas.DataBind();
                    }
                    else
                    {
                        lblDataToday.Visible = true;
                        lblDataToday.Text = Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt" ? "Sem notícias para escolher" : "No news to select";
                        //lnk_guardarBottom.Style.Add("display", "none");
                    }
                }
                else
                {
                    lblDataToday.Visible = true;
                    lblDataToday.Text = Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt" ? "Sem notícias para escolher" : "No news to select";
                    //lnk_guardarBottom.Style.Add("display", "none");
                }

                string user = Global.InsigteManager[Session.SessionID].IdClient.Equals("1099") ? "971" : Global.InsigteManager[Session.SessionID].inUser.IdClientUser;

                query = " select * from NL10F_NEWSSELECTED (nolock) where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + user + " and CODDAY = convert(varchar(8),getdate(),112) ";
                DataTable dts = Global.InsigteManager[Session.SessionID].getTableQuery(query, "noticias marcadas");
                Session["NewsToselected"] = dts.Rows.Count;
            }
            catch (Exception er)
            {
                er.Message.ToString();
            }
        }

        protected string getname(string name, string name_en, string tema)
        {
            try
            {
                //int countdiftema = ((DataTable)Session["NewsToSend"]).Select("name = " + name).Count(c => c["tema"].ToString() != tema);

                //string pa = "";
                //if (countdiftema > 1)
                //{
                //    var sss = ((DataTable)Session["NewsToSend"]).Select("name_intranet").FirstOrDefault(t => t["tema"].ToString() == tema);
                //}

                //foreach (DataRow row in ((DataTable)Session["NewsToSend"]).Select("name = " + name))
                //{

                //}

                return Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt" ? name : name_en;
            }
            catch (Exception er)
            {
                er.Message.ToString();
                return null;

            }
        }

        protected bool hasSend()
        {
            try
            {
                if (!Global.InsigteManager[Session.SessionID].IdClient.Equals("1013") && !Global.InsigteManager[Session.SessionID].IdClient.Equals("1103"))
                    return true;

                string idclient = "";
                if (Global.InsigteManager[Session.SessionID].IdClient.Equals("1013") || Global.InsigteManager[Session.SessionID].IdClient.Equals("1103"))
                    idclient = "1013,1103";
                else
                    idclient = Global.InsigteManager[Session.SessionID].IdClient;

                string query = "select id_news from iadvisers.dbo.NL10F_NEWSSELECTED ns (nolock) where ns.ID_CLIENT in ( " + idclient + " ) and ns.CODDAY = convert(varchar(8)  ,getdate(),112)";
                DataTable dt = Global.InsigteManager[Session.SessionID].getTableQuery(query, "news");

                if (dt == null) return false;

                if (dt.Rows.Count > 0) return false;

                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }

        protected bool checkClient()
        {
            return (!Global.InsigteManager[Session.SessionID].IdClient.Equals("1099") && !Global.InsigteManager[Session.SessionID].IdClient.Equals("1000"));
        }

        protected bool hasSendApi()
        {
            try
            {
                string user = Global.InsigteManager[Session.SessionID].IdClient.Equals("1099") ? "971" : Global.InsigteManager[Session.SessionID].inUser.IdClientUser;

                string query = " select * from [iadvisers].[dbo].[ML10L_CLIENT_NEWSLETTER] with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + user + " and convert(varchar(8),dat_start,112) = convert(varchar(8),getdate(),112) ";
                DataTable dt = Global.InsigteManager[Session.SessionID].getTableQuery(query, "Log");

                if (dt == null) return false;

                if (dt.Rows.Count > 0) return false;

                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }

        protected bool hasNews()
        {
            return (((int)Session["NewsToSendCount"]) > 0) ? true : false;
        }

        protected int hasNewsSelected()
        {
            //return (((int)Session["NewsToselected"]) > 0) ? true : false;
            return (int)Session["NewsToselected"];
        }

        protected DataTable FillRp_news(String ID)
        {
            try
            {
                DataTable tempTbl = new DataTable();
                tempTbl.Columns.Add("id", typeof(String));
                tempTbl.Columns.Add("title", typeof(String));
                tempTbl.Columns.Add("date", typeof(String));
                tempTbl.Columns.Add("editor", typeof(String));
                tempTbl.Columns.Add("url", typeof(String));
                tempTbl.Columns.Add("tipo", typeof(String));
                tempTbl.Columns.Add("tema", typeof(String));


                foreach (DataRow row in ((DataTable)Session["NewsToSend"]).Select("tema = " + ID, "date desc"))
                {
                    //String Icon = "";
                    //String Link = "";

                    string titulo = Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt" ? row["title"].ToString() : row["title_en"].ToString();
                    //if (row["tipo"].ToString() == "Imprensa")
                    //{
                    //    Icon = "doc_export_icon_24.png";
                    //    Link = "http://insigte.com/ficheiros/" + row["filepath"].ToString();
                    //}
                    //else if (row["tipo"].ToString() == "Online")
                    //{
                    //    Icon = "globe_1_icon_24.png";
                    //    Link = row["page_url"].ToString();
                    //}
                    //else if (row["tipo"].ToString() == "Rádio")
                    //{
                    //    Icon = "headphones_icon_24.png";
                    //    Link = "http://insigte.com/ficheiros/" + row["filepath"].ToString();
                    //}
                    //else if (row["tipo"].ToString() == "Televisão")
                    //{
                    //    Icon = "movie_icon_24.png";
                    //    Link = "http://insigte.com/ficheiros/" + row["filepath"].ToString();
                    //}



                    string url = !string.IsNullOrEmpty(row["page_url"].ToString()) ? row["page_url"].ToString() : "http://insigte.com/ficheiros/" + row["filepath"].ToString();
                    tempTbl.Rows.Add(row["id"].ToString(), titulo, row["date"].ToString(), row["editor"].ToString(), url, row["tipo"].ToString(), row["tema"].ToString());
                }

                return tempTbl;
            }
            catch (Exception er)
            {
                er.Message.ToString();
                return null;
            }
        }


        protected void EnviarNewsLetter(object sender, EventArgs e)
        {
            try
            {
                //Verificar se já nao existe um registo de enviado ou a enviar
                lblDataToday.Visible = true;
                if (!hasSendApi())
                {
                    lblDataToday.Text = Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt" ? "A Enviar..." : "Sending...";
                    return;
                }

                lblDataToday.Text = Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt" ? "A processar..." : "Processing...";

                string user = Global.InsigteManager[Session.SessionID].IdClient.Equals("1099") ? "971" : Global.InsigteManager[Session.SessionID].inUser.IdClientUser;

                string SQLQUERY = "";
                SQLQUERY += " INSERT INTO [dbo].[ML10L_CLIENT_NEWSLETTER] ";
                SQLQUERY += " ([ID_CLIENT],[ID_CLIENT_USER],[USER_CREATE]) ";
                SQLQUERY += " VALUES ";
                SQLQUERY += " ( " + Global.InsigteManager[Session.SessionID].IdClient;
                SQLQUERY += " , " + user;
                SQLQUERY += " ," + user + ") ";
                SQLQUERY += " select @@identity as ID_LOG Set Nocount off ";

                DataTable insertControl = Global.InsigteManager[Session.SessionID].getTableQuery(SQLQUERY, "InertInControl");

                if (insertControl == null)
                {
                    lblDataToday.Text = Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt" ? "Erro a Processar" : "Erro on Processing ";
                    return;
                }

            
                


                startsend(Global.InsigteManager[Session.SessionID].IdClient, user, insertControl.Rows[0][0].ToString());

                //var client = new RestClient("http://insigte.com/inapi/api/NewsLetter/");
                //client.Authenticator = new HttpBasicAuthenticator(username, password);

                //var request = new RestRequest("SendNewsLetterFromPortal/", Method.GET);
                //request.AddParameter("IdClient", Global.InsigteManager[Session.SessionID].IdClient);
                //request.AddParameter("IdClientUser", user);
                //request.AddParameter("IdLog", insertControl.Rows[0][0].ToString());
                //client.Execute(request);

                lblDataToday.Text = Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt" ? "A enviar Newsletter" : "Sending Newsletter";

                Response.Redirect("NewsChoose.aspx");

            }
            catch (Exception er)
            {
                er.Message.ToString();
                lblDataToday.Visible = false;
            }
            finally
            {

            }
        }

        private void startsend(string IdClient, string IdUser, string IdLog)
        {
            try
            {

                IRestResponse loginResponse = new RestResponse();
                TaskCompletionSource<IRestResponse> tcs = new TaskCompletionSource<IRestResponse>();

                var client = new RestClient("http://insigte.com/inapi/api/NewsLetter/");
                client.Authenticator = new HttpBasicAuthenticator(username, password);
                var request = new RestRequest("SendNewsLetterFromPortal/", Method.GET);

                request.AddParameter("IdClient", IdClient);
                request.AddParameter("IdClientUser", IdUser);
                request.AddParameter("IdLog", IdLog);
                request.AddParameter("NewsSelected", true);
                request.AddParameter("SendEmail", true);
                request.AddParameter("SendFrom", 2);

                //client.ExecuteAsync(request);
                Task task = new Task(() => client.ExecuteAsync<RestResponse>(request, tcs.SetResult)); // Lambda and named method                    
                task.Start();
            }
            catch (Exception er)
            {
                er.Message.ToString();
                throw;
            }
        }


        protected void teste_Click(object sender, EventArgs e)
        {
            try
            {
                //try
                //{

                //    DataTable dv = Global.InsigteManager[Session.SessionID].getTableQuery("dbo.JOBRUN 1099 , '_TESTES_PT_MLCLI1099_GARRIGUES (EXEC BY USER)'", "execjob");

                //    //DataTable insert = Global.InsigteManager[Session.SessionID].getTableQuery("[RUN_DTSX] 1099, '_TESTES_PT_MLCLI1099_GARRIGUES (EXEC BY USER)'", "execsp");
                //}
                //catch (Exception er)
                //{
                //    er.Message.ToString();
                //}

                //lnk_guardarBottom.Style.Add("display", "none");
                lblDataToday.Visible = true;
                lblDataToday.Text = Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt" ? "A processar..." : "Processing ...";

                IList<listcheck> lstchked = new List<listcheck>();

                //Lista de noticias não checadas só server para a deloitte
                IList<listnotcheck> lstnotchked = new List<listnotcheck>();

                //String Seleccionados = "";
                for (int i = 0; i < rp_NewsTemas.Items.Count; i++)
                {
                    try
                    {
                        Repeater rpt = (Repeater)rp_NewsTemas.Items[i].FindControl("rp_news1");
                        if (rpt != null)
                        {
                            foreach (RepeaterItem li in rpt.Items)
                            {

                                CheckBox chk = (CheckBox)li.FindControl("Chk_temas");
                                if (chk != null)
                                {
                                    //foreach (ListItem lichk in chk.Items)
                                    //{
                                    if (chk.Checked)
                                    {

                                        listcheck item = new listcheck();
                                        item.idnews = chk.Attributes["data-id"].ToString();
                                        item.grupo = chk.Attributes["data-grupo"].ToString();
                                        lstchked.Add(item);
                                        //Seleccionados += chk.Value + ",";
                                    }
                                    else
                                    {
                                        listnotcheck item = new listnotcheck();
                                        item.idnews = chk.Attributes["data-id"].ToString();
                                        item.grupo = chk.Attributes["data-grupo"].ToString();
                                        lstnotchked.Add(item);
                                    }
                                    //}
                                }
                            }
                        }
                    }
                    catch (Exception er)
                    {
                        //lnk_guardarBottom.Style.Add("display", "block");
                        lblDataToday.Visible = false;
                        er.Message.ToString();
                    }
                }

                //if (lstchked.Count() == 0)
                //{
                //    lblDataToday.Visible = true;
                //    lblDataToday.Text = Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt" ? "Necessário escolher no minimo uma notícia." : "Must choose at least one news.";
                //    //lnk_guardarBottom.Style.Add("display", "block");
                //    return;
                //}

                int inserted = 0;

                //Quando o cliente não é deloitte "1013" e "1103" o sistema vai marcar na tabela NL10F_NEWSSELECTED as noticias que o cliente vai receber
                if (!Global.InsigteManager[Session.SessionID].IdClient.Equals("1013") && !Global.InsigteManager[Session.SessionID].IdClient.Equals("1103"))
                {
                    foreach (listcheck item in lstchked)
                    {
                        String query = "";
                        query += " INSERT INTO [dbo].[NL10F_NEWSSELECTED] ";
                        query += " ( [ID_CLIENT],[ID_CLIENT_USER],[ID_NEWS],[COD_TEMA],[CODDAY] ) ";
                        query += "  VALUES ";
                        query += " ( ";
                        query += Global.InsigteManager[Session.SessionID].IdClient;

                        //User
                        if (Global.InsigteManager[Session.SessionID].IdClient.Equals("1099"))
                            query += " , 971";
                        else
                            query += " , " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser;

                        query += " , " + item.idnews;
                        query += " , " + item.grupo;
                        query += " , convert(nvarchar(8),getdate(),112)";
                        query += " ) ";

                        DataTable insert = Global.InsigteManager[Session.SessionID].getTableQuery(query, "inseririncontrol");

                        if (insert != null)
                        {
                            inserted++;
                        }
                    }
                }
                else
                {
                    //para a deloite o sistema vai inserir na tabela NL10F_NEWSSELECTED as noticias que o cliente tirou o pisco
                    foreach (listnotcheck item in lstnotchked)
                    {
                        String query = "";
                        query += " INSERT INTO [dbo].[NL10F_NEWSSELECTED] ";
                        query += " ([ID_CLIENT],[ID_CLIENT_USER],[ID_NEWS],[COD_TEMA],[CODDAY]) ";
                        query += " VALUES ";
                        query += " ( ";
                        query += "1103";
                        query += " , " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser;
                        query += " , " + item.idnews;
                        query += " , " + item.grupo;
                        query += " , convert(nvarchar(8),getdate(),112)";
                        query += " ) ";

                        DataTable insert = Global.InsigteManager[Session.SessionID].getTableQuery(query, "inseririncontrol");

                        if (insert != null)
                        {
                            inserted++;
                        }
                    }
                }

                if (inserted > 0)
                {
                    lblDataToday.Visible = true;
                    //lblDataToday.Text = Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt" ? "Notícias adicionadas para newsletter." : "News added for newsletter.";
                    Response.Redirect("NewsChoose.aspx");
                }

            }
            catch (Exception er)
            {
                er.Message.ToString();
                lblDataToday.Visible = false;
            }
            finally
            {

            }

        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }
    }

    public class listcheck
    {
        public string grupo { get; set; }
        public string idnews { get; set; }
    }

    public class listnotcheck
    {
        public string grupo { get; set; }
        public string idnews { get; set; }
    }

}