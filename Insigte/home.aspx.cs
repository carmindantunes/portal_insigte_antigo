﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;

namespace Insigte
{
    public partial class home : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                getPublicacoes();
            }
        }

        protected void repGroups_noticias_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
        }

        protected void Limpar_Click(object sender, EventArgs e)
        {
            
        }

        private void getPublicacoes()
        {

            String Title = "m.TITLE";
            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            {
                Title = "m.TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
            }

            String DataToShow = "m.DATE";
            String DataToShowConfig = "m.DATE";

            if (Global.InsigteManager[Session.SessionID].IdClient == "1000" || Global.InsigteManager[Session.SessionID].inUser.IdClientUser == "178")
            {
                DataToShow = "m.insert_date as DATE";
                DataToShowConfig = "m.insert_date";
            }

            String SQLTemas = "SELECT top 30 m.ID as IDMETA, " + Title + ", m.EDITOR, " + DataToShow + ", m.filepath, m.page_url, m.tipo";
            SQLTemas += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLTemas += " INNER JOIN iadvisers.dbo.clientarticles a with(nolock) ";
            SQLTemas += " ON a.id = m.ID WHERE ( ";

            try
            {

                foreach (var tema in Global.InsigteManager[Session.SessionID].inUser.Temas)
                {
                    SQLTemas += " ( ";
                    String filtroEditors = (tema.Value.FILTER_EDITORS_HOMEPAGE == "*" ? Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter : tema.Value.FILTER_EDITORS_HOMEPAGE);

                    //lblDataToday.Text += "<br>" + tema.Value.IdTema + ":" + filtroEditors;

                    if (tema.Value.IdTema.Substring(4, 3).ToString() != "000" && tema.Value.SubTemas.Length == 0)
                    {
                        SQLTemas += " a.clientid='" + tema.Value.IdTema.Substring(0, 4).ToString() + "' and a.subjectid = '" + tema.Value.IdTema.Substring(4, 3).ToString() + "' and " + DataToShowConfig + " >= getdate()-" + tema.Value.BackDays.ToString();
                        if (filtroEditors != "*")
                            SQLTemas += " and m.editorId in ( " + filtroEditors + " ) ";
                    }
                    if (tema.Value.IdTema.Substring(4, 3).ToString() == "000" && tema.Value.IdTema == tema.Value.SubTemas)
                    {
                        SQLTemas += " a.clientid='" + tema.Value.IdTema.Substring(0, 4).ToString() + "' and " + DataToShowConfig + " >= getdate()-" + tema.Value.BackDays.ToString();
                        if (filtroEditors != "*")
                            SQLTemas += " and m.editorId in ( " + filtroEditors + " ) ";
                    }
                    if (tema.Value.IdTema.Substring(4, 3).ToString() == "000" && tema.Value.IdTema != tema.Value.SubTemas && tema.Value.IdTema.Length <= tema.Value.SubTemas.Length)
                    {
                        String[] subtemas = tema.Value.SubTemas.Split(';');
                        String AuxTemas = "";
                        foreach (String y in subtemas)
                        {
                            AuxTemas += "'" + y.Substring(4, 3) + "',";
                        }
                        AuxTemas = AuxTemas.TrimEnd(',');
                        SQLTemas += " a.clientid='" + tema.Value.IdTema.Substring(0, 4).ToString() + "' and a.subjectid in (" + AuxTemas + ") and " + DataToShowConfig + " >= getdate()-" + tema.Value.BackDays.ToString();
                        if (filtroEditors != "*")
                            SQLTemas += " and m.editorId in ( " + filtroEditors + " ) ";
                    }
                    SQLTemas += ") OR ";
                }
                SQLTemas = SQLTemas.Substring(0, (SQLTemas.Length - 4)) + " ) ";
            }
            catch (Exception e)
            {
            }
            //// FILTRO DAS FONTES GERAIS
            //if (Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter != "*")
            //    SQLTemas += " and m.editorId in ( " + Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter + " ) ";

            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            {
                SQLTemas += " and m.TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " is not null ";
            }


            SQLTemas += " order by " + DataToShowConfig + " desc, IDMETA desc";

            DataTable Noticias = Global.InsigteManager[Session.SessionID].getTableQuery(SQLTemas, "getnoticiasDefault");

            repGroups_noticias.DataSource = Noticias;
            repGroups_noticias.DataBind();

        }

        protected string sReturnIconTipoPasta(string url, string tipo, string file)
        {
            //Imprensa
            //Insigte
            //Online
            //Rádio
            //Televisão
            switch (tipo)
            {
                case "Imprensa":
                    String FileHelperPath = "ficheiros";
                    if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                    {
                        string auxfile = @"G:\cfiles\" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file.Replace("/", "\\");
                        bool urlexists = INinsigteManager.Utils.URLExists("http://insigte.com/cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file.ToString());

                        if (File.Exists(auxfile) || urlexists)
                        {
                            FileHelperPath = "cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient;
                        }

                        //if (File.Exists( @"G:\cfiles\" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file.Replace("/","\\")))
                        //{
                        //    FileHelperPath = "cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient;
                        //}
                    }
                    return string.Format("<a href=\"http://insigte.com/" + FileHelperPath + "/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    //return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    break;
                case "Online":
                    return string.Format("<a href=\"{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/globe_1_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Link\" style=\"border-width:0px;\" title=\"Link\" /></a>", url);
                    break;
                case "Rádio":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/headphones_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP3\" style=\"border-width:0px;\" title=\"Download MP3\" /></a>", file);
                    break;
                case "Televisão":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/movie_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP4\" style=\"border-width:0px;\" title=\"Download MP4\" /></a>", file);
                    break;
                default:
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    break;
            }
            return null;
        }

    }
}