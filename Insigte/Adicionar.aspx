﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Adicionar.aspx.cs" Inherits="Insigte.Adicionar" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>insigte | media intelligence</title>
</head>
<body onunload='javaScript:window.opener.location=window.opener.location;'>
    <form id="form1" runat="server">
        <% = "<div style=\"width: 400px; float: left; clear: both; color: #ffffff; height: 67px; background-image: url('Cli/" + global_asax.InsigteManager[Session.SessionID].CodCliente + "/fundo_small.png'); background-repeat:no-repeat; background-color: #003f56;\"> "%>
        </div>
        <div id="divMain" style="width: auto; height:auto; text-align: justify; clear: both; padding-bottom: 10px;">
            <asp:Table ID="Table1" runat="server">
                <asp:TableRow ID="TableRow2" runat="server">
                    <asp:TableCell ID="TableCell3" runat="server" HorizontalAlign="Left">
                        <asp:Label ID="Label1" Text="" runat="server"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell4" runat="server">
                        <asp:Label ID="lbInfo" Text="" runat="server"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="TableRow1" runat="server">
                    <asp:TableCell ID="TableCell1" runat="server" HorizontalAlign="Left">
                        <asp:Label ID="lbFrom" runat="server"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell2" runat="server">
                        <asp:DropDownList ID="ddTo" runat="server" AutoPostBack="True"></asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" ID="TableRow5">
                    <asp:TableCell ID="TableCell9" runat="server">
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell10" runat="server">        
                        <asp:Button runat="server" Text="<%$Resources:insigte.language,defAdicionar%>" Font-Names="Arial" Font-Size="12px" ID="btAdicionar" OnClick="btAdicionar_Click" CssClass="btn-pdfs"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>
    </form>
</body>
</html>
