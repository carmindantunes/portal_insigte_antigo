﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="home.aspx.cs" Inherits="Insigte.home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
    #section.sector
    {
        float:left;
        width: 100%;
        min-height: 300px;
        min-width:400px;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
        padding:0px;
        margin:0px;
    }
    .form_search
    {
        float:left;
        width: 100%;
        height:10px;
        padding-top: 4px;
        padding-bottom: 10px;
        margin: 0 auto 20px auto;
        background-color: #323232;
        color: #FFFFFF;
    }
    
    .news_body
    {
        float:left;
        display:block;
        width:31%;
        margin: 5px;
        height:200px;
        background-color: #00add9;
        color: #FFFFFF;
    }
    
    .news_body:hover
    {
        
    }
    
    .news_body_title
    {
        float:left;
        width:50%;
        color: #000;
    }
    
    .nBody
    {
        text-decoration: none;
        font-size: 16px;
        text-align:left;
        font-family:Arial;
    }
    
    .news_body_data
    {
        float:left;
        width:50%;
        color: #000;
        text-align:right;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
    }
    
    .news_body_source
    {
        float:left;
        width:50%;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
    }
    
    .news_body_actions
    {
        float:left;
        width:50%;
        text-align:right;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
    }
    
    .actionBtn
    {
        width: 36px;
        height: 36px;
        padding: 1px 2px 3px 2px;
        border: none;
        background: transparent url(Imgs/action/email.png);
    }    
    .actionBtn-snone {
      background: transparent url(Imgs/action/email.png);
    }

    .actionBtn-snone:hover {
      background: transparent url(Imgs/action/email_gray.png);
    }

    .actionBtn-snone:active {
      background: transparent url(Imgs/action/email.png);
    }
    
</style>

<script type="text/javascript">
    $(function () {
        $('.show_on_hover').hide();
        $('.news_body').hover(function () {
            $(this).find('.show_on_hover').stop(true, true).fadeIn(400);
        },
        function () {
            $(this).find('.show_on_hover').stop(true, true).fadeOut(400);
        }
        );
    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="Geral" style="float:left; padding:0px; margin:0px; height:100%; width:800px;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:12px">
            <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false" />
        </div>
    </div>
    <div id="section" class="sector ui-corner-top">
        
        <div id="news_block" style=" float:left; width:100%;">

            <asp:Repeater id="repGroups_noticias" runat="server" OnItemDataBound='repGroups_noticias_OnItemDataBound'>
                <ItemTemplate>

                    <div id="nb" class="news_body">
                        <div style="padding:2px 2px 2px 2px; margin:2px 2px 2px 2px;">
                            <div class="head" style="height:25px;">
                                <div class="news_body_title">
                                    <%# sReturnIconTipoPasta(DataBinder.Eval(Container.DataItem, "page_url").ToString(), DataBinder.Eval(Container.DataItem, "tipo").ToString(), DataBinder.Eval(Container.DataItem, "filepath").ToString())%>&nbsp;
                                </div>
                                <div class="news_body_data"><%# Convert.ToDateTime(Eval("DATE")).ToString("dd-MM-yyyy") %></div>
                            </div>
                            <div class="nBody" style="height:155px;">
                                <%# Eval("TITLE").ToString() %>
                            </div>
                            <div class="bottoms" style="height:15px;">
                                <div class="news_body_source"><%# Eval("EDITOR").ToString() %></div>
                            </div>
                            <div id="btn_action" class="show_on_hover" style="position: relative; z-index: 110; top: -100px; display: block;">
                                <div style="width:100%; text-align:center">
                                    <asp:Button ToolTip="limpar todos" ID="btn_eml" CssClass="actionBtn actionBtn-snone" runat="server" CommandName="limpar" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"IDMETA").ToString() %>' OnClick="Limpar_Click"  />&nbsp;
                                    <asp:Button ToolTip="limpar todos" ID="Button1" CssClass="actionBtn actionBtn-snone" runat="server" CommandName="limpar" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"IDMETA").ToString() %>' OnClick="Limpar_Click"  />
                                </div>
                            </div>
                        </div>
                    </div>

                </ItemTemplate>
            </asp:Repeater>

        </div>

    </div>
</div>


<script type="text/javascript">

    $('.news_body').toggleClick(function () {
        $(this).animate({ 'height': '176px', 'width': '100%' }, 100);
    }, function () {
        $(this).animate({ 'height': '88px', 'width': '49%' }, 100);
    });


</script>

</asp:Content>
