﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pastascfg.aspx.cs" Inherits="Insigte.pastascfg" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Carma |  Insigte is now Carma</title>
    <link rel="icon" type="ico" href="https://www.carma.com/app/themes/carma/assets/images/favicon.ico" />
    <link href="Styles/StyleSheet.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    function openWindow(url) {
        var e = document.getElementById("tvPastas");
        var strUser = e.options[e.selectedIndex].value;
        var w = window.open(url + strUser, '', 'width=400,height=400,toolbar=0,status=0,location=0,menubar=0,directories=0,resizable=1,scrollbars=1');
        w.focus();
    }
</script>

</head>
<body>
    <form id="form1" runat="server">
        <% = "<div style=\"width: 400px; float: left; clear: both; color: #ffffff; height: 67px; background-image: url('Cli/" + global_asax.InsigteManager[Session.SessionID].CodCliente + "/fundo_small.png'); background-repeat:no-repeat; background-color: #003f56;\"> "%>
        </div>
        <div style="float:left; width: 180px; padding-top:5px; margin-left:10px; margin-top:5px; ">
            <asp:Table ID="tblArchives" runat="server" CellPadding="2" CellSpacing="5">
                <asp:TableRow ID="TableRow3" runat="server">
                    <asp:TableCell ID="TableCell6" runat="server" VerticalAlign="Middle" >
                        <asp:Label ID="lb" runat="server" Text="<%$Resources:insigte.language,ucPastasTitulo%>" style="color:#000000; font-size:12px; font-family:Arial;"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell7" runat="server">
                        <asp:DropDownList ID="tvPastas" runat="server" AutoPostBack="True" ></asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="TableRow4" runat="server">
                    <asp:TableCell ID="TableCell8" runat="server" VerticalAlign="Middle" HorizontalAlign="Right" >
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell9" runat="server" VerticalAlign="Middle" HorizontalAlign="Left">
                        &nbsp;
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="TableRow10" runat="server">
                    <asp:TableCell ID="TableCell0" runat="server" VerticalAlign="Middle" HorizontalAlign="Right" >
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Imgs/icons/black/png/folder_plus_icon_16.png" Width="16px" Height="16px" ToolTip='<%# getResource("defPastaCriar") %>' />
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell1" runat="server" VerticalAlign="Middle" HorizontalAlign="Left">
                        <asp:LinkButton ID="Lblnova" CssClass="btn-pdfs" runat="server" Text="<%$Resources:insigte.language,defPastaCriar%>" OnClick="nova_click" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" Font-Underline="false"></asp:LinkButton>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="TableRow1" runat="server">
                    <asp:TableCell ID="TableCell4" runat="server" VerticalAlign="Middle" HorizontalAlign="Right">
                        <asp:Image ID="imgModificar" runat="server" ImageUrl="~/Imgs/icons/black/png/folder_arrow_icon_16.png" Width="16px" Height="16px" ToolTip='<%# getResource("defPastaMod") %>' />
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell5" runat="server" VerticalAlign="Middle" HorizontalAlign="Left">
                        <asp:LinkButton ID="lnkModificar" CssClass="btn-pdfs" runat="server" Text="<%$Resources:insigte.language,defPastaMod%>" OnClick="mod_click" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" Font-Underline="false"></asp:LinkButton>
                        
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="TableRow2" runat="server">
                    <asp:TableCell ID="TableCell2" runat="server" VerticalAlign="Middle" HorizontalAlign="Right">
                        <asp:Image ID="imgDelete" runat="server" ImageUrl="~/Imgs/icons/black/png/folder_delete_icon_16.png" Width="16px" Height="16px" ToolTip='<%# getResource("defPastaApagar") %>' />
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell3" runat="server" VerticalAlign="Middle" HorizontalAlign="Left">
                        <asp:LinkButton ID="lnkDelete" CssClass="btn-pdfs" runat="server" Text="<%$Resources:insigte.language,defPastaApagar%>" OnClick="del_click" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" Font-Underline="false"></asp:LinkButton>
                        
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>
    </form>
</body>
</html>
