﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;


namespace Insigte
{
    public partial class Blogs : BasePage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            //lblDataToday.Visible = true;

            dgNewsBLOG.PagerStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[0].ToString());
            dgNewsBLOG.HeaderStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[1].ToString());

            if (!Page.IsPostBack)
            {
                preEncheBlogNews(null, null);

                try
                {
                    string query = "select * from [iadvisers].[dbo].[BL10X_AUTO_MARCACAO_TAG] with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString();
                    DataTable TAGS = Global.InsigteManager[Session.SessionID].getTableQuery(query, "ClienteWithBlogs");

                    if (TAGS.Rows.Count > 0)
                    {
                        preEncheTemas();
                    }
                    else
                    {
                        DropDl_select_Tema.Visible = false;
                        DropDl_select_tag.Visible = true;
                    }

                }
                catch (Exception)
                {


                }



                preEncheBlogTags();
                preEncheBlogLst();
            }
            else
            {
                //lblDataToday.Text = this.Request["__EVENTARGUMENT"];
                //remover id tag and blog
                removeTag(this.Request["__EVENTARGUMENT"]);

            }

        }

        protected void removeTag(string tag)
        {
            if(tag.Equals("")) return;

            if (DropDl_select_tag.Items.FindByText(tag) != null)
            {
                ListItem i = DropDl_select_tag.Items.FindByText(tag);

                String[] omfg = txt_tags.Value.Split(',');
                String AuxOMFG = "";
                foreach (String omfffffg in omfg)
                {
                    if (omfffffg != i.Value)
                        AuxOMFG += omfffffg + ",";

                }
                txt_tags.Value = AuxOMFG.TrimEnd(',');

                //txt_tags.Value = txt_tags.Value.Replace(i.Value, "");
                //txt_tags.Value = txt_tags.Value.Replace(",,", ",");
                //txt_tags.Value = txt_tags.Value.TrimEnd(',');
                //txt_tags.Value = txt_tags.Value.TrimStart(',');
            }

            if (DropDl_select_Blog.Items.FindByText(tag) != null)
            {
                ListItem i = DropDl_select_Blog.Items.FindByText(tag);

                String[] omfg = txt_blogs.Value.Split(',');
                String AuxOMFG = "";
                foreach (String omfffffg in omfg)
                {
                    if (omfffffg != i.Value)
                        AuxOMFG += omfffffg + ",";

                }
                txt_blogs.Value = AuxOMFG.TrimEnd(',');

                //txt_blogs.Value = txt_blogs.Value.Replace(i.Value, "");
                //txt_blogs.Value = txt_blogs.Value.Replace(",,", ",");
                //txt_blogs.Value = txt_blogs.Value.TrimEnd(',');
                //txt_blogs.Value = txt_blogs.Value.TrimStart(',');
            }

            //if (lst_tagsRmv.Items.FindByText(tag) != null)
            //{
            //    try
            //    {

            //        DropDl_select_tag.Items.FindByText(tag).Enabled = true;
            //        lst_tagsRmv.Items.Remove(lst_tagsRmv.Items.FindByText(tag));

            //    }
            //    catch (Exception)
            //    {


            //    }
            //}

            string query = "select * from [iadvisers].[dbo].[BL10X_AUTO_MARCACAO_TAG] with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString();
            DataTable TAGS = Global.InsigteManager[Session.SessionID].getTableQuery(query, "ClienteWithBlogs");

            if (TAGS.Rows.Count > 0)
            {
                preEncheBlogNews1(txt_tags.Value, txt_blogs.Value);
            }
            else
            {
                preEncheBlogNews(txt_tags.Value, txt_blogs.Value);
            }

        }



        protected void dgNewsBLOG_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgNewsBLOG.CurrentPageIndex = e.NewPageIndex;
            if (ta_editores_codes.Value == "")
                preEncheBlogNews(null, null);
            else
                preEncheBlogNews(txt_tags.Value, txt_blogs.Value);
        }

        protected void preEncheBlogNews(String tags, String blogs)
        {
            blogs = blogs == null ? "" : blogs;
            tags = tags == null ? "" : tags;

            String SQLBLOGNEWS = "";

            SQLBLOGNEWS = " SELECT  m.id_news as ID_NEWS,m.txt_title as TITULO,b.NOM_BLOG as BLOG,ab.NOM_AUTOR as AUTOR,dat_publish as DATA, m.txt_url URL ";
            SQLBLOGNEWS += " FROM [NW10F_METADATA_BLOG] m with(nolock) ";
            SQLBLOGNEWS += " inner join [dbo].[NW10D_AUTOR_BLOG] ab with(nolock) ";
            SQLBLOGNEWS += " on m.id_autor = ab.id_autor ";
            SQLBLOGNEWS += " inner join [dbo].[NW10D_BLOG] b with(nolock) ";
            SQLBLOGNEWS += " on m.id_blog = b.id_blog ";
            SQLBLOGNEWS += " where 1 = 1 ";
            if (tags.Length > 0)
                SQLBLOGNEWS += " and m.id_news in (select distinct [ID_NEWS] from BL10F_MARCACAO_TAG with(nolock) where ID_TAG in ('" + tags.Replace(",", "','") + "')) ";
            if (blogs.Length > 0)
                SQLBLOGNEWS += " and b.id_blog in ('" + blogs.Replace(",", "','") + "')";
            SQLBLOGNEWS += " order by DATA desc ";

            //lblDataToday.Text = SQLBLOGNEWS + "<BR><br> tags: " + tags + "<BR><br> Blogs: " + blogs;

            DataTable news = Global.InsigteManager[Session.SessionID].getTableQuery(SQLBLOGNEWS, "GetNews");

            dgNewsBLOG.DataSource = news;
            dgNewsBLOG.DataBind();
            dgNewsBLOG.Visible = true;
        }

        protected void preEncheBlogNews1(String tags, String blogs)
        {
            String SQLBLOGNEWS = "";
            try
            {

                blogs = blogs == null ? "" : blogs;
                tags = tags == null ? "" : tags;

                SQLBLOGNEWS = " SELECT  m.id_news as ID_NEWS,m.txt_title as TITULO,b.NOM_BLOG as BLOG,ab.NOM_AUTOR as AUTOR,dat_publish as DATA, m.txt_url URL ";
                SQLBLOGNEWS += " FROM [NW10F_METADATA_BLOG] m with(nolock) ";
                SQLBLOGNEWS += " inner join [dbo].[NW10D_AUTOR_BLOG] ab with(nolock) ";
                SQLBLOGNEWS += " on m.id_autor = ab.id_autor ";
                SQLBLOGNEWS += " inner join [dbo].[NW10D_BLOG] b with(nolock) ";
                SQLBLOGNEWS += " on m.id_blog = b.id_blog ";
                SQLBLOGNEWS += " where 1 = 1 ";
                if (tags.Length > 0)
                    SQLBLOGNEWS += " and m.id_news in (select distinct [ID_NEWS] from  [iadvisers].[dbo].[BL10F_CLIENT_BLOG_ARTICLE] with(nolock) where ID_MARCACAO_TAG in ('" + tags.Replace(",", "','") + "')) ";
                if (blogs.Length > 0)
                    SQLBLOGNEWS += " and b.id_blog in ('" + blogs.Replace(",", "','") + "')";
                SQLBLOGNEWS += " order by DATA desc ";

                //lblDataToday.Text = SQLBLOGNEWS + "<BR><br> tags: " + tags + "<BR><br> Blogs: " + blogs;

                DataTable news = Global.InsigteManager[Session.SessionID].getTableQuery(SQLBLOGNEWS, "GetNews");

                dgNewsBLOG.DataSource = news;
                dgNewsBLOG.DataBind();
                dgNewsBLOG.Visible = true;
            }
            catch (Exception)
            {
                //lblDataToday.Visible = true;
                //lblDataToday.Text = SQLBLOGNEWS;
            }
        }

        protected void preEncheBlogLst()
        {
            try
            {
                String SQLBLOGS = " select id_blog, nom_blog ";
                SQLBLOGS += " from [dbo].[NW10D_BLOG] with(nolock) order by nom_blog";
                //SQLBLOGS += " union all select -1, 'Selecionar Blog'  order by nom_blog";

                DataTable BLOGS = Global.InsigteManager[Session.SessionID].getTableQuery(SQLBLOGS, "GetBlogs");
                DropDl_select_Blog.DataSource = null;
                DropDl_select_Blog.DataSource = BLOGS;
                DropDl_select_Blog.DataTextField = "nom_blog";
                DropDl_select_Blog.DataValueField = "id_blog";
                DropDl_select_Blog.DataBind();
                DropDl_select_Blog.Items.Insert(0, Resources.insigte.language.geralSelectBlogue.ToString());
                DropDl_select_Blog.SelectedValue = "0";

            }
            catch (Exception)
            {

            }

        }

        protected void preEncheBlogTags()
        {
            try
            {

                String SQLBLOGTAGS = " select [ID_MARCACAO_TAG],des_tag ";
                SQLBLOGTAGS += " from [dbo].[BI10X_MARCACAO_TAG] with(nolock) ";
                SQLBLOGTAGS += " where [ID_MARCACAO_TAG] in (  ";
                SQLBLOGTAGS += " SELECT  distinct TOP 15 x.id_tag ";
                SQLBLOGTAGS += " FROM (select top 1000 mt.id_tag ";
                SQLBLOGTAGS += " from [iadvisers].[dbo].[BL10F_MARCACAO_TAG] mt with(nolock) ";
                SQLBLOGTAGS += " inner join [dbo].[NW10F_METADATA_BLOG] nb with(nolock) ";
                SQLBLOGTAGS += " on mt.id_news = nb.id_news ";
                SQLBLOGTAGS += " order by nb.[DAT_PUBLISH] desc ";
                SQLBLOGTAGS += " ) x ";
                SQLBLOGTAGS += " ) ";
                //SQLBLOGTAGS += " union all select -1, 'Selecionar Tag'  order by des_tag";
                SQLBLOGTAGS += "order by des_tag ";

                DataTable TAGS = Global.InsigteManager[Session.SessionID].getTableQuery(SQLBLOGTAGS, "GetTags");
                DropDl_select_tag.DataSource = null;
                DropDl_select_tag.DataSource = TAGS;
                DropDl_select_tag.DataTextField = "des_tag";
                DropDl_select_tag.DataValueField = "ID_MARCACAO_TAG";
                DropDl_select_tag.DataBind();
                DropDl_select_tag.Items.Insert(0, Resources.insigte.language.geralSelectTag.ToString());
                DropDl_select_tag.SelectedValue = "0";
            }
            catch (Exception)
            {
            }
        }

        protected void preEncheTemas()
        {
            try
            {
                String SQLBLOGTAGS = "";
                SQLBLOGTAGS += " select distinct COD_TAG,Des_tag from [iadvisers].[dbo].[BL10X_AUTO_MARCACAO_TAG] with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString();
                SQLBLOGTAGS += " order by des_tag ";
                DataTable TAGS = Global.InsigteManager[Session.SessionID].getTableQuery(SQLBLOGTAGS, "GetTags");
                DropDl_select_Tema.DataSource = null;
                DropDl_select_Tema.DataSource = TAGS;
                DropDl_select_Tema.DataTextField = "Des_tag";
                DropDl_select_Tema.DataValueField = "COD_TAG";
                DropDl_select_Tema.DataBind();
                //ListItem item = new ListItem(-1,"Select Tema")
                DropDl_select_Tema.Items.Insert(0, Resources.insigte.language.geralSelectTema.ToString());
                DropDl_select_Tema.SelectedValue = "0";
            }
            catch (Exception)
            {
            }
        }

        protected void preEncheTags1(String _Tema)
        {
            DropDl_select_tag.Visible = true;

            try
            {

                String SQLBLOGTAGS = "";
                SQLBLOGTAGS += " SELECT * FROM [iadvisers].[dbo].[BL10X_AUTO_MARCACAO_TAG] with(nolock) where cod_tag = " + _Tema;


                DataTable TAGS = Global.InsigteManager[Session.SessionID].getTableQuery(SQLBLOGTAGS, "GetTags");
                DropDl_select_tag.DataSource = TAGS;
                DropDl_select_tag.DataTextField = "COD_WORD";
                DropDl_select_tag.DataValueField = "ID_MARCACAO_TAG";
                DropDl_select_tag.DataBind();
                DropDl_select_tag.Items.Insert(0, "Select Tag");
                DropDl_select_tag.SelectedValue = "-1";

            }
            catch (Exception er)
            {
                //lblDataToday.Visible = true;
                //lblDataToday.Text = SQLBLOGTAGS;

            }
        }

        protected void DropDl_select_Tema_change(object sender, EventArgs e)
        {
            if (DropDl_select_Tema.SelectedIndex.Equals(0))
            {
                DropDl_select_tag.Visible = false;
                return;
            }

            preEncheTags1(DropDl_select_Tema.SelectedItem.Value);
        }

        protected void DropDl_select_tag_change(object sender, EventArgs e)
        {
            if (DropDl_select_tag.SelectedIndex.Equals(0)) return;
            if (ta_editores_codes.Value.Contains(DropDl_select_tag.SelectedItem.Text)) return;

            ta_editores_codes.Value += (ta_editores_codes.Value.Length > 0 ? "," : "") + DropDl_select_tag.SelectedItem.Text;
            txt_tags.Value += (txt_tags.Value.Length > 0 ? "," : "") + DropDl_select_tag.SelectedItem.Value;
            //DropDl_select_tag.Items.Remove(DropDl_select_tag.SelectedItem);
            try
            {
                string query = "select * from [iadvisers].[dbo].[BL10X_AUTO_MARCACAO_TAG] with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString();
                DataTable TAGS = Global.InsigteManager[Session.SessionID].getTableQuery(query, "ClienteWithBlogs");

                if (TAGS.Rows.Count > 0)
                {
                    preEncheBlogNews1(txt_tags.Value, txt_blogs.Value);
                }
                else
                {
                    preEncheBlogNews(txt_tags.Value, txt_blogs.Value);
                }
            }
            catch (Exception er)
            {


            }

            //ListItem item = new ListItem(DropDl_select_tag.SelectedItem.Text, DropDl_select_tag.SelectedItem.Value);
            //DropDl_select_tag.Items.FindByValue(DropDl_select_tag.SelectedItem.Value).Enabled = false;
            ////DropDl_select_tag.Items.Remove(DropDl_select_tag.Items.FindByValue(DropDl_select_tag.SelectedItem.Value));
            //lst_tagsRmv.Items.Add(item);

        }

        protected void DropDl_select_Blog_change(object sender, EventArgs e)
        {
            if (DropDl_select_Blog.SelectedIndex.Equals(0)) return;
            if (ta_editores_codes.Value.Contains(DropDl_select_Blog.SelectedItem.Text)) return;
            
            ta_editores_codes.Value += (ta_editores_codes.Value.Length > 0 ? "," : "") + DropDl_select_Blog.SelectedItem.Text;
            txt_blogs.Value += (txt_blogs.Value.Length > 0 ? "," : "") + DropDl_select_Blog.SelectedItem.Value;

            try
            {
                string query = "select * from [iadvisers].[dbo].[BL10X_AUTO_MARCACAO_TAG] with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString();
                DataTable TAGS = Global.InsigteManager[Session.SessionID].getTableQuery(query, "ClienteWithBlogs");

                if (TAGS.Rows.Count > 0)
                {
                    preEncheBlogNews1(txt_tags.Value, txt_blogs.Value);
                }
                else
                {
                    preEncheBlogNews(txt_tags.Value, txt_blogs.Value);
                }
            }
            catch (Exception er)
            {


            }

            //DropDl_select_Blog.Items.Remove(DropDl_select_Blog.Items.FindByValue("-1"));
        }

    }
}