﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;
using INInsigteManager;
using System.Net;
using System.Xml.Linq;

namespace Insigte.UserControls
{
    public partial class cpub : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //loadTemas();
            if (!Page.IsPostBack)
            {
                if (Global.InsigteManager[Session.SessionID].CodUser.ToLower() == "admin")
                {
                    Lb_NlCp.Visible = false;
                }

                if (ChkCPNewsletter())
                {
                    Lb_NlCp.Text = Resources.insigte.language.ucCpNl3_2;
                }
                else
                {
                    Lb_NlCp.Text = Resources.insigte.language.ucCpNl3; 
                }
            }
        }

        protected void loadTemas()
        {
            DataTable temas = Global.InsigteManager[Session.SessionID].getTableQuery("Select * from NW10D_CP_TEMA with(nolock) ", "CpTemas");

            cpubRep.DataSource = temas;
            cpubRep.DataBind();

        }

        protected void NewsL_Click(object sender, EventArgs e)
        {
            String SQL = "select COD_EMAIL from CL10H_CP_NEWSLETTER with(nolock) where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient;
            DataTable chkEmail = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "chkCPnewsletter");

            if (chkEmail.Rows.Count > 0)
            {
                foreach(DataRow row in chkEmail.Rows)
                {
                    String StrEmailList = row["COD_EMAIL"].ToString();

                    if (!StrEmailList.Contains(Global.InsigteManager[Session.SessionID].inUser.Email.Trim()))
                    {
                        StrEmailList += "," + Global.InsigteManager[Session.SessionID].inUser.Email;
                        StrEmailList = StrEmailList.TrimStart(',');

                        String cmdUpd = " update CL10H_CP_NEWSLETTER set COD_EMAIL = '" + StrEmailList + "' where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient;
                        DataTable updCpNews = Global.InsigteManager[Session.SessionID].getTableQuery(cmdUpd, "UpdCPnewsletter");
                        Lb_NlCp.Text = Resources.insigte.language.ucCpNl3_2;
                    }
                    else
                    {
                        StrEmailList = StrEmailList.Replace(Global.InsigteManager[Session.SessionID].inUser.Email, "");
                        StrEmailList = StrEmailList.Replace(",,", ",");
                        StrEmailList = StrEmailList.TrimEnd(',');

                        String cmdUpd = " update CL10H_CP_NEWSLETTER set COD_EMAIL = '" + StrEmailList + "' where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient;
                        DataTable updCpNews = Global.InsigteManager[Session.SessionID].getTableQuery(cmdUpd, "UpdCPnewsletter");
                        Lb_NlCp.Text = Resources.insigte.language.ucCpNl3;
                    }

                }

            }
            else
            {
                //inserir registo.
                String insReg = " insert into CL10H_CP_NEWSLETTER (ID_CLIENT, COD_TEMA, COD_EMAIL, IND_ACTIVE) ";
                insReg += " select " + Global.InsigteManager[Session.SessionID].IdClient + ", '" + Global.InsigteManager[Session.SessionID].TemaCliente + "', '" + Global.InsigteManager[Session.SessionID].inUser.Email + "', 1 ";

                DataTable inst = Global.InsigteManager[Session.SessionID].getTableQuery(insReg, "InsertRegCPNews");
                Lb_NlCp.Text = Resources.insigte.language.ucCpNl3_2;
            }
        }

        protected Boolean ChkCPNewsletter()
        {
            String SQL = "select * from CL10H_CP_NEWSLETTER with(nolock) where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient;
            DataTable chkEmail = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "chkCPnewsletter");

            String StrEmailList = "";

            foreach (DataRow row in chkEmail.Rows)
            {
                StrEmailList = row["COD_EMAIL"].ToString();
            }

            if (!StrEmailList.Contains(Global.InsigteManager[Session.SessionID].inUser.Email))
            {
                return false;
            }
            else
            {
                return true;
            }

        }

    }
}