﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Insigte.UserControls
{
    public partial class Reports : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            carregaVals();
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            Global.InsigteManager[Session.SessionID].inUser.RSVLastReport = "RPT_Media_Measurement";
            Session["Report"] = "RPT_Media_Measurement";

            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallopenReportWindowA4", "openReportWindowA4('PrevMediaReport.aspx?rld=RPT_Media_Measurement')", true);
        }

        private void carregaVals()
        {
            Session["Report"] = "RPT_Media_Measurement";
            Global.InsigteManager[Session.SessionID].inUser.RSVLastReport = "RPT_Media_Measurement";
        }

        

        protected string getEncrypt()
        {
            return Encryp();
        }

        private string Encryp()
        {
            return Global.InsigteManager[Session.SessionID].geEncrypt(Session["Report"].ToString());
        }
    }
}