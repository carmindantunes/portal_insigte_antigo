﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Insigte.UserControls
{
    public partial class Research : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            Global.InsigteManager[Session.SessionID].inUser.RSVLastReport = "/RL_Research";

            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallopenReportWindowA4", "openReportWindowA4('reportingServ.aspx')", true);

            //Session["Report"] = "/RL_Research";

        }
    }
}