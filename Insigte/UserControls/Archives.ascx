﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Archives.ascx.cs" Inherits="Insigte.UserControls.Archives" %>

<div class="ui-corner-top in-cli-usercontrols" style="height: 25px; width: 180px;">
    <div style="padding-top: 4px; padding-left: 5px;float:left;">
        <asp:Label runat="server" Text="<%$Resources:insigte.language,ucArcTitulo%>" ID="Lb_Titulo" ForeColor="white" Font-Names="Arial" Font-Size="12px" />
    </div>
    <div style="padding-top: 4px; padding-left: 5px; padding-right: 5px; float:right">
        <asp:Label runat="server" ForeColor="#f89c3e" Text="" Font-Bold="true" ID="Label1"  Font-Names="Arial" Font-Size="12px" />
    </div>
</div>
<div class="ui-corner-bottom" style="width: 180px; padding-top:5px; background-color: #d7d7d7;">
    <asp:Table ID="tblArchives" runat="server">
        <asp:TableRow ID="TableRow1" runat="server">
            <asp:TableCell ID="TableCell0" runat="server" VerticalAlign="Middle" >
                <%--<asp:Image ID="Image1" runat="server" ImageUrl="~/Imgs/folderplus32.png" Width="16px" Height="16px" ToolTip="Notícias" />--%>
            </asp:TableCell>
            <asp:TableCell ID="TableCell1" runat="server"><a href="NewsArchive.aspx" style=" color:#000000; font-size:12px; font-family:Arial;" target="_self"><asp:Literal ID="LtLink1" runat="server" Text="<%$Resources:insigte.language,ucArcLink1%>" /></a></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow3" runat="server">
            <asp:TableCell ID="TableCell4" runat="server" VerticalAlign="Middle" >
                <%--<asp:Image ID="Image2" runat="server" ImageUrl="~/Imgs/recycle32.png" Width="16px" Height="16px" ToolTip="Items eliminados"/>--%>
            </asp:TableCell>
            <asp:TableCell ID="TableCell5" runat="server"><a href="Recycle.aspx" style=" color:#000000; font-size:12px; font-family:Arial;" target="_self"><asp:Literal ID="LtLink2" runat="server" Text="<%$Resources:insigte.language,ucArcLink2%>" /></a></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow2" runat="server">
            <asp:TableCell ID="TableCell2" runat="server" VerticalAlign="Middle" >
                <%--<asp:Image ID="Image3" runat="server" ImageUrl="~/Imgs/linedpaper32.png" Width="16px" Height="16px" ToolTip="Compilar PDF"/>--%>
            </asp:TableCell>
            <asp:TableCell ID="TableCell3" runat="server"><a href="ListPDF.aspx" style=" color:#000000; font-size:12px; font-family:Arial;" target="_self">
                 <asp:Literal ID="LtLink3" Visible="false" runat="server" Text=" <%$Resources:insigte.language,ucArcLink3%> "/>
                 <asp:Literal ID="LtLink3EA" Visible="false" runat="server" Text=" <%$Resources:insigte.language,ucArcLink3EA%> "/>
            </a></asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</div>

