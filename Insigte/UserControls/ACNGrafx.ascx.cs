﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;

namespace Insigte.UserControls
{
    public partial class ACNGrafx : System.Web.UI.UserControl
    {
        Boolean GRconcorrencia = false;
        String Concorrencia = "";
        Int32 TotalConc = 1;
        String TemaCores = "#F7AD34";
        String[] cores = new String[] { "#FCDEB8", "#F7AD34 ", "#92BEB7", "#ABD4D6", "#505050" };

        protected void Page_Load(object sender, EventArgs e)
        {

            foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
            {
                if (pair.Value.Name == "Concorrência" || pair.Value.Name == "Competitors")
                {
                    GRconcorrencia = true;

                    String[] Conc = pair.Value.SubTemas.Split(';');

                    foreach (String x in Conc)
                    {
                        TotalConc++;
                        Concorrencia += "'" + x.ToString() + "',";
                    }

                    Concorrencia = Concorrencia.TrimEnd(',');
                    TemaCores = pair.Value.TemaCores;
                    cores = TemaCores.Split(',');
                }
            }

            

            if (GRconcorrencia == false)
            {
                Chart1.Visible = false;
                lblACNVCC.Visible = false;

            }
            else
            {
                CreateChartACNvsOP();
            }

            CreateChartACN5AUTHORS();
        }


        private void CreateChartACNvsOP()
        {



            string cmdQuery = "select COUNT(ca.id) AS num ";
            cmdQuery += "	  ,cast(ca.clientid as varchar) CLID  ";
            cmdQuery += "	  ,cast(ca.subjectid as varchar) AS ID ";
            cmdQuery += "	  ,s.name AS NAME  ";
            cmdQuery += "	  ,s.tema_cores as COR ";
            cmdQuery += "  FROM clientarticles ca with(nolock) ";
            cmdQuery += "       inner join subject s with(nolock) ";
            cmdQuery += "			   on ca.clientid = s.clienteid  ";
            cmdQuery += "			  AND ca.subjectid = s.subjectid  ";
            cmdQuery += "	   inner join metadata m with(nolock) ";
            cmdQuery += "			   on ca.id = m.id  ";
            cmdQuery += " where cast(ca.clientid as varchar) + cast(ca.subjectid as varchar) = '" + Global.InsigteManager[Session.SessionID].TemaCliente + "' ";
            cmdQuery += "   and m.date >= getdate()-90  ";
            cmdQuery += " GROUP BY ca.subjectid, cast(ca.clientid as varchar) , cast(ca.subjectid as varchar), s.name, s.tema_cores ";
            cmdQuery += "union all ";
            cmdQuery += "select * from ( select top 4 COUNT(ca.id) AS num ";
            cmdQuery += "	  ,cast(ca.clientid as varchar) CLID  ";
            cmdQuery += "	  ,cast(ca.subjectid as varchar) AS ID ";
            cmdQuery += "	  ,s.name AS NAME  ";
            cmdQuery += "	  ,s.tema_cores as COR ";
            cmdQuery += "  FROM clientarticles ca with(nolock) ";
            cmdQuery += "       inner join subject s with(nolock) ";
            cmdQuery += "			   on ca.clientid = s.clienteid  ";
            cmdQuery += "			  AND ca.subjectid = s.subjectid  ";
            cmdQuery += "	   inner join metadata m with(nolock) ";
            cmdQuery += "			   on ca.id = m.id  ";
            cmdQuery += " where cast(ca.clientid as varchar) + cast(ca.subjectid as varchar) in (" + Concorrencia + ") ";
            cmdQuery += "   and m.date >= getdate()-90  ";
            cmdQuery += " GROUP BY ca.subjectid, cast(ca.clientid as varchar) , cast(ca.subjectid as varchar), s.name, s.tema_cores ";
            cmdQuery += " ORDER BY 1 desc ) X";
            cmdQuery += " ORDER BY 1 desc ";

            try
            {
                DataTable GrConc = new DataTable();
                GrConc = Global.InsigteManager[Session.SessionID].getTableQuery(cmdQuery, "GrConc");

                Int32[] yValues = new Int32[GrConc.Rows.Count];
                string[] xValues = new string[GrConc.Rows.Count];


                int counter = 0;
                foreach (DataRow row in GrConc.Rows)
                {
                    yValues[counter] = Convert.ToInt32(row[0].ToString());
                    xValues[counter] = row[3].ToString();
                    counter++;
                }

                Chart1.Series["Series1"].Points.DataBindXY(xValues, yValues);
                Chart1.Series["Series1"].ChartType = SeriesChartType.Pie;// Set the Pie width
                Chart1.Series["Series1"]["PointWidth"] = "0.5";// Show data points labels
                Chart1.Series["Series1"].IsValueShownAsLabel = true;// Set data points label style
                Chart1.Series["Series1"].Label = "#VALY";
                Chart1.Series["Series1"]["BarLabelStyle"] = "Center";// Show chart as 3D
                Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;// Draw chart as 3D 
                Chart1.Series["Series1"]["DrawingStyle"] = "Cylinder";

                //String[] cores = TemaCores.Split(',');


                //Chart1.Series["Series1"].Points[0].Color = ColorTranslator.FromHtml("#2a9046"); //Color.FromArgb(42, 144, 70);
                //Chart1.Series["Series1"].Points[1].Color = ColorTranslator.FromHtml("#e07646"); //Color.FromArgb(224, 118, 70);
                //Chart1.Series["Series1"].Points[2].Color = ColorTranslator.FromHtml("#7bc059"); //Color.FromArgb(123, 192, 89);
                //Chart1.Series["Series1"].Points[3].Color = ColorTranslator.FromHtml("#5aaae2"); //Color.FromArgb(90, 170, 226);
                //Chart1.Series["Series1"].Points[4].Color = ColorTranslator.FromHtml("#ebdb17"); //Color.FromArgb(235, 219, 23);
               string[] lstcli = {"1017","1099"};

                //COR AUTO
                counter = 0;
                foreach (DataRow row in GrConc.Rows)
                {
                    if (lstcli.Contains( Global.InsigteManager[Session.SessionID].IdClient.ToString()))
                    {
                        Chart1.Series["Series1"].Points[counter].Color = ColorTranslator.FromHtml(row["COR"].ToString());
                    }
                    else
                    {
                        Chart1.Series["Series1"].Points[counter].Color = ColorTranslator.FromHtml(cores[counter]);
                    }
                    counter++;

                }

                //Chart1.Legends[0].Enabled = false;

                Chart1.Legends.Add("Legend1");
                Chart1.Legends[0].Enabled = true;
                Chart1.Legends[0].Docking = Docking.Bottom;
                Chart1.Legends[0].Alignment = System.Drawing.StringAlignment.Center;

                // Show labels in the legend in the format "Name (### %)"
                Chart1.Series[0].LegendText = "#VALX [#VALY] (#PERCENT)";
                Chart1.Series["Series1"].ToolTip = "#VALX [#PERCENT]";

                counter = 0;
                foreach (DataRow row in GrConc.Rows)
                {
                    Chart1.Series["Series1"].Points[counter].Url = "~/Subject.aspx?clid=" + row[1] + "&tema=" + row[2];
                    counter++;
                }
            }
            catch (Exception e)
            {
            }
            
        }

        private void CreateChartACN5AUTHORS()
        {
            
            TemaCores = Global.InsigteManager[Session.SessionID].CorCliente;

            try
            {
                
                string cmdQuery = "SELECT TOP 5 COUNT(ca.id) AS num, ca.subjectid AS ID, s.name AS NAME, m.author AS AUTHOR, m.editor As EDITOR , m.authorid as AUID ";
                cmdQuery += "FROM clientarticles ca with(nolock) ";
                cmdQuery += "     inner join  subject s with(nolock) ";
                cmdQuery += "			 on ca.clientid = s.clienteid  ";
                cmdQuery += "			AND ca.subjectid = s.subjectid ";
                cmdQuery += "     inner join metadata m with(nolock) ";
                cmdQuery += "			 on ca.id = m.id ";
                cmdQuery += "	 inner join authors a with(nolock) ";
                cmdQuery += "			 on m.authorid = a.authorid ";
                cmdQuery += "WHERE ca.clientid = '" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "' AND ca.subjectid = '" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "' ";
                cmdQuery += "  AND m.date >= getdate()-365  ";
                cmdQuery += "  AND m.authorid is not null ";
                cmdQuery += "  AND len(rtrim(ltrim(m.authorid)))>0 ";
                cmdQuery += "GROUP BY ca.subjectid, s.name, m.author, m.editor, m.authorid  ";
                cmdQuery += "ORDER BY num DESC ";


                DataTable GrConc2 = new DataTable();
                GrConc2 = Global.InsigteManager[Session.SessionID].getTableQuery(cmdQuery, "GrConc2");

                Int32[] yValues = new Int32[GrConc2.Rows.Count];
                string[] xValues = new string[GrConc2.Rows.Count];
                string[] sEditor = new string[GrConc2.Rows.Count];

                int counter = 0;
                foreach (DataRow row in GrConc2.Rows)
                {
                    yValues[counter] = Convert.ToInt32(row[0].ToString());
                    xValues[counter] = row[3].ToString();
                    sEditor[counter] = row[4].ToString();
                    counter++;
                }
                counter = 0;

                Chart2.Series["Series2"].Points.DataBindXY(xValues, yValues);

                String[] cores = TemaCores.Split(',');

                // Set series chart type
                Chart2.Series["Series2"].ChartType = SeriesChartType.Column;

                foreach (DataRow row in GrConc2.Rows)
                {
                    Chart2.Series["Series2"].Points[counter].Color = ColorTranslator.FromHtml(cores[0]);
                    counter++;
                }
                counter = 0;

                Chart2.Series["Series2"]["PointWidth"] = "0.6";
                // Show data points labels
                Chart2.Series["Series2"].IsValueShownAsLabel = false;
                // Set data points label style
                Chart2.Series["Series2"]["BarLabelStyle"] = "Center";
                // Show as 3D
                Chart2.ChartAreas["ChartArea2"].Area3DStyle.Enable3D = false;
                // Draw as 3D Cylinder
                Chart2.Series["Series2"]["DrawingStyle"] = "Default";
                //Chart2.Series["Series2"].ToolTip = "#VALX [#VALY]";

                foreach (DataRow row in GrConc2.Rows)
                {
                    Chart2.Series["Series2"].Points[counter].ToolTip = "#VALX [#VALY]" + "[" + sEditor[counter] + "]";
                    counter++;
                }
                counter = 0;

                Chart2.Series["Series2"].IsXValueIndexed = false;
                Chart2.Series["Series2"].IsVisibleInLegend = false;
                Chart2.ChartAreas["ChartArea2"].AxisX.LabelStyle.Enabled = false;

                foreach (DataRow row in GrConc2.Rows)
                {
                    Chart2.Series["Series2"].Points[counter].Url = "~/Subject.aspx?clid=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "&tema=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "&at=" + row["AUID"].ToString() + "&fon=0&dd=365";
                    counter++;
                }

            }
            catch (Exception e)
            {
            }
        }

        protected void Chart1_Load(object sender, EventArgs e)
        {

        }
    }
}