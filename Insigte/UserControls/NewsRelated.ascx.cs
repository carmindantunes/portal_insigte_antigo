﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Insigte.UserControls
{
    public partial class NewsRelated : System.Web.UI.UserControl
    {
        string qsIDArtigo = "";
        string qsNRP = "-1";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            #region Permissão
            //string idsAut = "1,2";
            //if (!idsAut.Contains(Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString()))
            //    return; 
            #endregion

            if (!Page.IsPostBack)
            {
                qsIDArtigo = Request.QueryString["ID"];
                qsNRP = Request.QueryString["NRP"];

                getNewsRelatedArtigos(qsIDArtigo);
            }

        }

        public string getcolor()
        {
            string cor = Global.InsigteManager[Session.SessionID].CorCliente != "" && Global.InsigteManager[Session.SessionID].CorCliente.ToString().Contains("#") ? Global.InsigteManager[Session.SessionID].CorCliente : "#00add9";
            return cor;
        }

        public string getcolor1(string id)
        {

            string _class = "nicecolor";

            if (id.Equals(qsNRP))
                _class = "nicecolor1";

            return _class;
        }

        public string getcolor2(string id)
        {
            string _class = "#F0F0F0";

            if (id.Equals(qsNRP))
                _class = "#707070";

            return _class;
        }

        public string getStyle()
        {
            string rtn = "float: left; width: 95%;";

            if (lst_NewsRelated.Items.Count > 9)
                rtn = "float: left; width: 95%; height: 600px; overflow-y: scroll;";
            return rtn;
        }

        /// <summary>
        /// Carrega tabela news related em sessao
        /// </summary>
        /// <param name="idnews"></param>
        protected void getNewsRelatedArtigos(String idnews)
        {

            string query = "";
            string idconf = "1";
            try
            {
                string _idclient = Global.InsigteManager[Session.SessionID].IdClient.ToString();
                string _iduser = Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString();

                #region Vai buscar a config do utilizador se a tiver
                string getConfQuery = "";

                getConfQuery += " select ID_CONFIG ";
                getConfQuery += " from NR10H_NEWSRELATED_CLIENT with(nolock) ";
                getConfQuery += " where ID_CLIENT = " + _idclient;
                getConfQuery += " and ID_CLIENT_USER = " + _iduser;
                getConfQuery += " and COD_DATE_START <= convert(varchar(10),getdate(),112) ";
                getConfQuery += " and COD_DATE_END >= convert(varchar(10),getdate(),112) ";

                DataTable dtconf = Global.InsigteManager[Session.SessionID].getTableQuery(getConfQuery, "NewsRelatedConf");

                idconf = dtconf.Rows.Count == 1 ? dtconf.Rows[0]["ID_CONFIG"].ToString() : "1";

                #endregion

                #region Vai buscar a noticias Relacionadas e carrega a lista

                string codlg = Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt" ? "_en" : "";

                //query += " select m.id,left(convert(nvarchar,m.date,111),10)  as date,m.title" + codlg + " as title,m.editor,m.tipo,cast(c.text" + codlg + " as varchar(1000)) + ' ...' as text from metadata m with(nolock) ";
                //query += " join contents c with (nolock) on c.id = m.id ";
                //query += " where m.id in ( select ID_NEWSRELATED from NR10F_NEWSRELATED with(nolock) where  ID_CONFIG = " + idconf + " and id_news = " + qsIDArtigo + " and ID_NEWSRELATED != " + qsIDArtigo + "  ) order by m.insert_date desc ";

                //exec sp

                DataTable dtTema = Global.InsigteManager[Session.SessionID].getTableQuery("SP_GETTEMASUSER " + _idclient + " , " + _iduser, "temas");

                string temas = "";

                foreach (DataRow row in dtTema.Rows)
                {
                    temas += ",'" + row[0].ToString() + "'";
                }

                temas = temas.TrimStart(',');

                query += " select distinct m.insert_date, m.id,left(convert(nvarchar,m.date,111),10)  as date,m.title" + codlg + " as title,m.editor,m.tipo,cast(c.text" + codlg + " as varchar(1000)) + ' ...' as text ";
                query += " from metadata m with(nolock) ";
                query += " join contents c with (nolock) on c.id = m.id  ";
                query += " join clientarticles ca on ca.id = m.id and (cast(ca.clientid as varchar) + cast(ca.subjectid as varchar) ) in (  "+temas+" ) ";
                query += " where m.id in ( select ID_NEWSRELATED from NR10F_NEWSRELATED with(nolock) where  ID_CONFIG = " + idconf + " and id_news = " + qsIDArtigo + " and ID_NEWSRELATED != " + qsIDArtigo + "  ) ";
                query += " order by m.insert_date desc ";

                //String filtroEditors = (tema.Value.FILTER_EDITORS_HOMEPAGE == "*" ? Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter : tema.Value.FILTER_EDITORS_HOMEPAGE);

                //if (filtroEditors != "*")
                //    SQLTemas += " and m.editorId in ( " + filtroEditors + " ) ";

                //query += " where m.id in ( select ID_NEWSRELATED from NR10F_NEWSRELATED with(nolock) where ID_CONFIG= 1 and id_news = " + qsIDArtigo + ")";

                Global.InsigteManager[Session.SessionID].dtNewsRelated = Global.InsigteManager[Session.SessionID].getTableQuery(query, "NewsRelated");

                //var utemas = from temas in Global.InsigteManager[Session.SessionID].inUser.Temas
                //             select temas.Value;

                //string _temas = "";
                //foreach (var tema in utemas)
                //{
                //    if(tema.IdTema
                //    _temas += "," + tema.IdTema
                //}


                if (Global.InsigteManager[Session.SessionID].dtNewsRelated.Rows.Count > 0)
                {

                    lst_NewsRelated.DataSource = Global.InsigteManager[Session.SessionID].dtNewsRelated;
                    lst_NewsRelated.DataBind();
                }
                #endregion

            }
            catch (Exception er)
            {
                er.Message.ToString();
            }


        }

        protected string TransTipo(string tipo)
        {
            return trataTipo(tipo);
        }

        private string trataTipo(string tipo)
        {
            string codlg = Global.InsigteManager[Session.SessionID].inUser.CodLanguage;
            if (codlg != "pt")
            {
                switch (tipo)
                {
                    case "Online":
                        return tipo;
                    case "Imprensa":
                        return "Press";
                    case "Rádio":
                        return "Radio";
                    case "Insigte":
                        return tipo;
                    case "Televisão":
                        return "Television";
                    default:
                        return tipo;
                }
            }
            else
                return tipo;
        }

        public String newsNRP()
        {
            return (qsIDArtigo != "") ? "&NRP=" + qsIDArtigo : "";
        }
    }
}

