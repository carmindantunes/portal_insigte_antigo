﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;

namespace Insigte.UserControls
{
    public partial class Newsletters : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //admin não pode ver alertas pois o email de admin é admin@CODCLIENTE
                //e como os alertas e com o email de utilizador este nao é compativel
                if (Global.InsigteManager[Session.SessionID].CodUser.ToLower() == "admin")
                {
                    Lb_AlertaRow.Visible = false;
                    Lb_Alerta.Visible = false;
                }
                //Newsletter Composta
                if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(7, 1) == "1")
                {
                    Lb_CompostaRow.Visible = true;
                    Lb_Composta.Visible = true;
                }


                //}

                //Newsletter Choose
                try
                {
                    if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(15, 1) == "1")
                    {
                        NewsChooseRow.Visible = true;
                        NewsChoose.Visible = true;
                    }
                }
                catch (Exception er)
                {
                }


                if (ChkAlertaActive())
                {
                    Lb_Alerta.Text = Resources.insigte.language.ucNwlLink3_1;
                }
                else
                {
                    Lb_Alerta.Text = Resources.insigte.language.ucNwlLink3;
                }
            }

        }

        protected void Alertas_Click(object sender, EventArgs e)
        {

            String SQLquery = "SELECT email FROM clientmailonfly with(nolock) WHERE cast(clienteid as varchar) + cast(subjectid as varchar) = '" + Global.InsigteManager[Session.SessionID].TemaCliente + "' ";
            DataTable UpdChKAlert = new DataTable();
            UpdChKAlert = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "UpdChKAlert");


            string strEmailList = string.Empty;
            foreach (DataRow row in UpdChKAlert.Rows)
            {
                strEmailList = row["email"].ToString();
            }

            if (!strEmailList.Contains(Global.InsigteManager[Session.SessionID].inUser.Email.Trim()))
            {

                strEmailList += "," + Global.InsigteManager[Session.SessionID].inUser.Email;

                String cmdUptQuery = "UPDATE clientmailonfly SET email='" + strEmailList + "' WHERE cast(clienteid as varchar) + cast(subjectid as varchar) = ltrim(rtrim('" + Global.InsigteManager[Session.SessionID].TemaCliente + "')) ";
                DataTable UpdAddAlert = new DataTable();
                UpdAddAlert = Global.InsigteManager[Session.SessionID].getTableQuery(cmdUptQuery, "UpdAddAlert");

                Lb_Alerta.Text = "Cancelar Alertas";

            }
            else
            {

                strEmailList = strEmailList.Replace(Global.InsigteManager[Session.SessionID].inUser.Email, "");
                strEmailList = strEmailList.Replace(",,", ",");
                strEmailList = strEmailList.TrimEnd(',');

                String cmdUptQuery = "UPDATE clientmailonfly SET email='" + strEmailList + "' WHERE cast(clienteid as varchar) + cast(subjectid as varchar) = ltrim(rtrim('" + Global.InsigteManager[Session.SessionID].TemaCliente + "')) ";
                DataTable UpdRmvAlert = new DataTable();
                UpdRmvAlert = Global.InsigteManager[Session.SessionID].getTableQuery(cmdUptQuery, "UpdRmvAlert");

                Lb_Alerta.Text = "Alertas real-time";
            }

        }

        protected Boolean ChkAlertaActive()
        {
            String SQLquery = "SELECT email FROM clientmailonfly with(nolock) WHERE cast(clienteid as varchar) + cast(subjectid as varchar) = ltrim(rtrim('" + Global.InsigteManager[Session.SessionID].TemaCliente + "')) ";
            DataTable UpdChKAlert = new DataTable();
            UpdChKAlert = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "UpdChKAlert");


            string strEmailList = string.Empty;
            foreach (DataRow row in UpdChKAlert.Rows)
            {
                strEmailList = row["email"].ToString();
            }

            if (!strEmailList.Contains(Global.InsigteManager[Session.SessionID].inUser.Email))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void LinkButton1_click(object sender, EventArgs e)
        {

            if (IMSsubscribe())
                Response.Redirect("SubscribeNEW.aspx", true);
            else
                Response.Redirect("SubscribeNl.aspx", true);
        }

        private bool IMSsubscribe()
        {
            try
            {
                bool xpto = false;
                String SQLquery = "SELECT IND_IMS FROM iadvisers..CL10D_CLIENT with(nolock) WHERE ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient;
                DataTable IMSenabled = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "IMSenabled");
                foreach (DataRow x in IMSenabled.Rows)
                {
                    xpto = (bool)x["IND_IMS"];
                }
                return xpto;
            }
            catch (Exception e)
            {
                return false;
                //throw;
            }

        }
    }
}