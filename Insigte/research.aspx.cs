﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;
using System.Text.RegularExpressions;
using iTextSharp.text;
using iTextSharp.text.pdf;


namespace Insigte
{
    public partial class research : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Research
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(11, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }

            if (!Page.IsPostBack)
            {
                getResearch();
                getResearchTema();
                getNewsResearch();
            }
        }

        protected void getResearch()
        {
            DataTable Research = Global.InsigteManager[Session.SessionID].getTableQuery(" select * from CL10D_RESEARCH with(nolock) where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and ID_CLIENT_USER = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser, "getResearchLists");
            DDResearch.DataSource = Research;
            DDResearch.DataTextField = "NOM_RESEARCH";
            DDResearch.DataValueField = "ID_RESEARCH";
            DDResearch.DataBind();
        }

        protected void getResearchTema()
        {
            DataTable Research = Global.InsigteManager[Session.SessionID].getTableQuery(" select * from CL10D_RESEARCH_TEMA with(nolock) where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and ID_CLIENT_USER = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and ID_RESEARCH = " + DDResearch.SelectedValue.ToString(), "getResearchTemaLists");

            DDResearch_Tema.DataSource = Research;
            DDResearch_Tema.DataTextField = "NOM_RESEARCH_TEMA";
            DDResearch_Tema.DataValueField = "ID_RESEARCH_TEMA";
            DDResearch_Tema.DataBind();

        }

        protected void getNewsResearch()
        {
            String Query = "";

            Query += " SELECT r.id_news, r.txt_research, cast(r.txt_research as varchar(100)) as txt_research_short, r.txt_nota, m.filepath, m.tipo, m.page_url, e.name ";
            Query += "  FROM CL10H_NEWS_RESEARCH r with(nolock) inner join metadata m with(nolock) on r.id_news = m.id inner join editors e with(nolock) on e.editorid = m.editorid ";
            Query += " where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and ID_CLIENT_USER = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and ID_RESEARCH = " + DDResearch.SelectedValue.ToString() + " and ID_RESEARCH_TEMA = " + DDResearch_Tema.SelectedValue.ToString();

            DataTable Research = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "getResearchTemaLists");

            rep_Research.DataSource = Research;
            rep_Research.DataBind();

        }

        protected String getUrl(String filepath, String page_url, String tipo, String name)
        {
            String BaseUrl = "http://www.insigte.com/ficheiros/";
            String Url = "";
            Url = "<a href=\"" + (filepath.Length > 0 ? BaseUrl + filepath : page_url) + "\" >" + tipo + " - " + name + "</a>";
            return Url;
        }

        protected void DDResearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            getResearchTema();
            getNewsResearch();
        }

        protected void DDResearch_Tema_SelectedIndexChanged(object sender, EventArgs e)
        {
            getNewsResearch();
        }

        protected void btnBuildPDF_Click(object sender, EventArgs e)
        {
            String SQL = "";
            SQL += "select m.tipo, m.date, m.title, e.name Editor ";
            SQL += "	   ,r.*  ";
            SQL += "  from CL10H_NEWS_RESEARCH r with(nolock) ";
            SQL += "	   inner join metadata m with(nolock) on r.ID_NEWS = m.id ";
            SQL += "	   inner join editors e with(nolock) on m.editorId = e.editorid ";
            SQL += " where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient + " and ID_CLIENT_USER = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and ID_RESEARCH = " + DDResearch.SelectedValue.ToString() + " and ID_RESEARCH_TEMA = " + DDResearch_Tema.SelectedValue.ToString();

            DataTable Research = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "getResearchTemaPDF");

            btnBuildPDF.Enabled = false;

            //DIFINIR AS FONTES DO DOCUMENTO

            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            BaseFont bfHelve = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
            BaseFont bfArial = BaseFont.CreateFont("C:\\Windows\\Fonts\\arial.ttf", BaseFont.CP1252, false);

            Font times = new Font(bfTimes, 12, Font.ITALIC, BaseColor.BLACK);
            Font helve = new Font(bfHelve, 18f, Font.NORMAL, BaseColor.BLACK);
            Font ArialSmall = new Font(bfArial, 8f, Font.NORMAL, BaseColor.BLACK);
            Font helveSmall = new Font(bfHelve, 16f, Font.NORMAL, BaseColor.BLACK);
            Font helveVSmall = new Font(bfHelve, 10f, Font.NORMAL, BaseColor.BLACK);

            Font helveVSmallBlue = new Font(bfHelve, 10f, Font.NORMAL, BaseColor.BLUE);
            helveVSmallBlue.SetColor(0, 84, 132);
            Font helveVVSmallBlue = new Font(bfHelve, 8f, Font.NORMAL, BaseColor.BLUE);
            helveVVSmallBlue.SetColor(0, 84, 132);

            Font helveXSmall = new Font(bfHelve, 6f, Font.NORMAL, BaseColor.BLACK);

            Font helveXSmallNota = new Font(bfHelve, 6f, Font.ITALIC, BaseColor.BLACK);

            string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            string varSolutionDir = "G:\\files\\";
            string varCapa = "G:\\pdf\\template_first.pdf";
            string path = Server.MapPath("pdf");

            int vSpace = 5;
            int nPage = 0;

            string NomeBase = "Research";
            string nomeOutFile = NomeBase + "_" + timestamp + ".pdf";
            string varOutputFile = "G:\\pdf\\" + nomeOutFile;

            var doc = new Document();
            PdfWriter pdfw = PdfWriter.GetInstance(doc, new FileStream(varOutputFile, FileMode.Create));
            doc.Open();
            doc.NewPage();

            PlaceText(pdfw.DirectContent, helve, 30, 750, 335, 790, 14, Element.ALIGN_LEFT, "media intelligence");
            PlaceText(pdfw.DirectContent, helveSmall, 30, 710, 335, 750, 14, Element.ALIGN_LEFT, "Research : " + DDResearch.SelectedItem.Text + " - " + DDResearch_Tema.SelectedItem.Text);

            doc.NewPage();

            foreach (DataRow x in Research.Rows)
            {
                String Icon = "";
                switch (x["tipo"].ToString())
                {
                    case "Online": Icon = "globe_1_icon_24.png";
                        break;
                    case "Imprensa": Icon = "doc_export_icon_24.png";
                        break;
                    case "Rádio": Icon = "headphones_icon_16.png";
                        break;
                    case "Televisão": Icon = "movie_icon_24.png";
                        break;
                    default: Icon = "doc_export_icon_24.png";
                        break;
                }

                String Titulo = x["TITLE"].ToString();
                String Nota = x["TXT_NOTA"].ToString();
                String Editor = x["EDITOR"].ToString();
                String Data = Convert.ToDateTime(x["DATE"].ToString()).ToShortDateString();
                String Texto = x["TXT_RESEARCH"].ToString();

                PdfContentByte cb = pdfw.DirectContent;
                ColumnText ct = new ColumnText(cb);
                ct.Alignment = Element.ALIGN_JUSTIFIED;
                ct.YLine = 1f;
                ct.SetLeading(0, 1.2f);

                Paragraph heading = new Paragraph(Titulo, helveVSmallBlue);
                Paragraph editor = new Paragraph(Editor + " - " + Data, helveVVSmallBlue);
                Paragraph interl = new Paragraph(" ", ArialSmall);
                Paragraph texto = new Paragraph(Texto, ArialSmall);
                Paragraph Notas = new Paragraph(Nota, helveXSmallNota);
                
                heading.Leading = 40f;

                doc.Add(heading);
                doc.Add(editor);
                doc.Add(interl);
                doc.Add(texto);
                doc.Add(interl);
                doc.Add(Notas);


            }

            doc.Close();
            doc.Dispose();

            String Capa = "G:\\pdf\\template_first.pdf";
            String PDFFinal = "G:\\pdf\\" + NomeBase + "_" + timestamp + "_" + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + ".pdf";

            string pdfsFinals = Capa + "," + varOutputFile;
            string[] pdfsFim = pdfsFinals.Split(',');
            PdfMerge.MergePdfs(PDFFinal, pdfsFim);
            File.Delete(varOutputFile);

            try
            {
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + NomeBase + "_" + timestamp + "_" + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + ".pdf" + "\"");
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.WriteFile(PDFFinal);
                HttpContext.Current.Response.End();
            }
            catch (Exception teste)
            {
                //lblDataToday.Visible = true;
                //lblDataToday.Text = "Morangos : " + teste.Message.ToString();
            }

            btnBuildPDF.Enabled = true;

        }

        protected static void PlaceText(PdfContentByte pdfContentByte, iTextSharp.text.Font font, float lowerLeftx, float lowerLefty, float upperRightx, float upperRighty, float leading, int alignment, string text)
        {
            ColumnText ct = new ColumnText(pdfContentByte);
            ct.SetSimpleColumn(new Phrase(text, font), lowerLeftx, lowerLefty, upperRightx, upperRighty, leading, alignment);
            ct.Go();
        }
    }
}