﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;
using System;
using INInsigteManager;

namespace Insigte
{
    public class BasePage : System.Web.UI.Page
    {

        protected override void InitializeCulture()
        {
            if (Global.InsigteManager.ContainsKey(Session.SessionID) == false)
            {
                INManager IN = new INManager(Session.SessionID, "", "");
                Global.InsigteManager.Add(Session.SessionID, IN);

            }

            String culture = Global.InsigteManager[Session.SessionID].inUser.CodLanguage;

            //Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
            //Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);

            Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);

            base.InitializeCulture();

        } 
        
    }
}