﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportConfig.aspx.cs" Inherits="Insigte.ReportConfig" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
 <style type="text/css">
        body,a
        {
            font-family:Calibri;
        }
        a
        {
            color:Black;
        }
        a:hover
        {
            text-decoration: underline;
        }
        .scroll_checkboxes
        {
            height: 120px;
            width: 200px;
            padding: 5px;
            overflow: auto;
            border: 1px solid #ccc;
        }
        
        .FormText
        {
            font-size: 11px;
            font-family: tahoma,sans-serif;
        }
        .divSpan
        {
            float: left;
            background-color: #323232;
            border-top-left-radius: 4px;
            border-top-right-radius: 4px;
            color: white;
            width: 100%;
            padding-top: 5px;
            padding-bottom: 5px;
        }
        
        .itemNormal
        {
            background-color:#F0F0F0;
        }
        .itemNeg
        {
            background-color:Red;
        }
        .itemPos
        {
            background-color:Green;
        }
    </style>
    <script language="javascript">

        var color = 'White';

        function changeColor(obj) {
            var rowObject = getParentRow(obj);
            var parentTable =
      document.getElementById("<%=chkList.ClientID%>");

            if (color == '') {
                color = getRowColor();
            }

            if (obj.checked) {
                rowObject.style.backgroundColor = '#A3B1D8';
            }
            else {
                rowObject.style.backgroundColor = color;
                color = 'White';
            }

            // private method
            function getRowColor() {
                if (rowObject.style.backgroundColor == 'White')
                    return parentTable.style.backgroundColor;
                else return rowObject.style.backgroundColor;
            }
        }

        // This method returns the parent row of the object
        function getParentRow(obj) {
            do {
                obj = obj.parentElement;
            }
            while (obj.tagName != "TR")
            return obj;
        }

        function TurnCheckBoixGridView(id) {
            var frm = document.forms[0];

            for (i = 0; i < frm.elements.length; i++) {
                if (frm.elements[i].type == "checkbox" &&
            frm.elements[i].id.indexOf("<%= chkList.ClientID %>") == 0) {
                    frm.elements[i].checked =
              document.getElementById(id).checked;
                }
            }
        }

        function SelectAll(id) {
            var parentTable = document.getElementById("<%=chkList.ClientID%>");
            var color

            if (document.getElementById(id).checked) {
                color = '#A3B1D8'
            }
            else {
                color = 'White'
            }

            for (i = 0; i < parentTable.rows.length; i++) {
                parentTable.rows[i].style.backgroundColor = color;
            }
            TurnCheckBoixGridView(id);
        }


        function escreveTxt(elem) {
            var texto = document.getElementById("<%= txtLtsTemas.ClientID%>").value;
            if (elem.checked) {
                texto += elem.value + ",";
            }
            else {
                texto = texto.replace(elem.value + ',', '');
            }
            document.getElementById("<%= txtLtsTemas.ClientID%>").value = texto;

        }

        function txtTemaClick(txt) {
            if (document.getElementById("divCkBoxs").style.display == "block") {
                document.getElementById("divCkBoxs").style.display = "none";
            } else {
                document.getElementById("divCkBoxs").style.display = "block";
            }
        }

        function targetPos(divid) {
            var res = divid.replace("chk_", "div_");
            document.getElementById(res).className = "itemPos";
        }

        function targetNeg(divid) {
            var res = divid.replace("chk_", "div_");
            document.getElementById(res).className = "itemNeg";
        }

        function targetNeutro(divid) {
            var res = divid.replace("chk_", "div_");
            document.getElementById(res).className = "itemNormal";
        }

        function divOnMouseOver(divid) {
            document.getElementById(divid).style.backgroundColor = "#00add9";
        }

        function divOnMouseOut(divid) {
            document.getElementById(divid).setAttribute("style", "");
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="Geral" style="float: left; padding: 0px; margin: 0px; height: 100%;">
        <div id="infoText" style="float: left; padding: 0px; margin: 2px 0px 2px 0px; height: 35px;
            min-height: 35px; width: 100%">
            <div style="margin-top: 12px">
                <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false" />
            </div>
        </div>
        <div class="form_caja">
            Configuração
        </div>
        <div style="width: 799px;">
            <div style="display: inline;">
                <div style="display: inline; float: left; width: 100%;">
                    <asp:Label runat="server" ID="Label2" Text="Tab" Style="float: left;"></asp:Label>
                    <asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="ddTab_SelectedIndexChanged"
                        AutoPostBack="true" Style="float: left; min-width: 350px;" />
                    <div style="margin-left: 20px; float: left;">
                        <asp:Label runat="server" ID="Label3" Text="Nome Analise: "></asp:Label>
                        <asp:TextBox runat="server" ID="TextBox1" Text="Banca"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div style="display: inline;">
                <div style="display: inline; float: left; width: 100%;">
                    <asp:Label runat="server" ID="lblTab" Text="Tab" Style="float: left;"></asp:Label>
                    <asp:DropDownList ID="ddTab" runat="server" OnSelectedIndexChanged="ddTab_SelectedIndexChanged"
                        AutoPostBack="true" Style="float: left; min-width: 350px;" />
                    <div style="margin-left: 20px; float: left;">
                        <asp:Label runat="server" ID="Label1" Text="Nome Analise: "></asp:Label>
                        <asp:TextBox runat="server" ID="txtNomAnalise" Text="Banca"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div style="width: 100%; float: left; margin-top: 10px;">
                <div style="display: inline; float: left; width: 100%; margin-bottom: 10px;">
                    <asp:Panel runat="server" ID="plTemas" Visible="false">
                        <div style="float: left; width: 380px;">
                            <asp:Label runat="server" ID="lblTemas" Text="Temas"></asp:Label>
                            <asp:TextBox ID="txtLtsTemas" runat="server" Style="cursor: pointer;" Width="314px"></asp:TextBox>
                            <div id="divCkBoxs" class="scroll_checkboxes" style="display: none; float: left;">
                                <asp:CheckBoxList Width="180px" ID="chkList" runat="server" CssClass="FormText" RepeatDirection="Vertical"
                                    RepeatColumns="1" BorderWidth="0" Datafield="description" DataValueField="value">
                                    <%--Lista é carregada no server--%>
                                </asp:CheckBoxList>
                            </div>
                            <%--<asp:DropDownList ID="ddTemas" m runat="server" OnSelectedIndexChanged="ddTemas_SelectedIndexChanged"
                                AutoPostBack="true" />--%>
                        </div>
                        <asp:Button ID="btnTemas" runat="server" Text="Refresh" Style="margin-left: 20px;
                            float: left;" OnClick="btnTemas_Click"></asp:Button>
                        
                    </asp:Panel>
                </div>
            </div>
           
            
        </div>
        
    </div>

</asp:Content>
