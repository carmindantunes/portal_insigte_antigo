﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;

namespace Insigte
{
    public partial class reportview : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                List<ReportParameter> reportParams = new List<ReportParameter>();
                reportParams.Add(new ReportParameter("Cliente", Global.InsigteManager[Session.SessionID].TemaCliente));
                reportParams.Add(new ReportParameter("ID_CLIENT", Global.InsigteManager[Session.SessionID].IdClient));

                RV_test.ServerReport.SetParameters(reportParams);
            }
        }

        protected void RV_test_SubmittingParameters(object sender, ReportParametersEventArgs e)
        {
            //e.Parameters["Cliente"].Values.Add(Global.InsigteManager[Session.SessionID].TemaCliente);
            //e.Parameters["ID_CLIENT"].Values.Add(Global.InsigteManager[Session.SessionID].IdClient);
        }
    }
}