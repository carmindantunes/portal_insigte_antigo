﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using INInsigteManager;
using System.Diagnostics;
using System.Net;

namespace Insigte
{
    public class Global : System.Web.HttpApplication
    {
        //public List<INManager> InsigteManager = new List<INManager>();
        public static SortedList<String, INManager> InsigteManager = new SortedList<string, INManager>();

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup

        }

        void Application_BeginRequest(object sender, EventArgs e)
        {
            //String fullOrigionalpath = Request.Url.ToString();
        

            //if (fullOrigionalpath.Contains(".aspx") )
            //{
            //    string fullexpaspx = fullOrigionalpath.Replace(".aspx", "");
            //    Context.RewritePath(fullexpaspx);
            //}
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

            string sourceName = "INSIGTE";
            string logName = "INSIGTE_MISC_LOG";

            // Code that runs when an unhandled error occurs
            try
            {
                if (!EventLog.SourceExists(sourceName))
                {
                    EventSourceCreationData mySourceData = new EventSourceCreationData(sourceName, logName);

                }

                Exception objErr = Server.GetLastError().GetBaseException();
                string err = "Error Caught in Application_Error event\n" +
                        "Error in: " + Request.Url.ToString() +
                        "\nError Message:" + objErr.Message.ToString() +
                        "\nStack Trace:" + objErr.StackTrace.ToString();

                EventLog.WriteEntry(sourceName, err, EventLogEntryType.Error);
                Server.ClearError();
                //additional actions... 

                //Redirect to errorPage
                //Server.Transfer("ErrorPage.aspx");
            }
            catch (ExecutionEngineException exx)
            {
                Server.Transfer("ErrorPage.aspx");
            }

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

            if (InsigteManager.ContainsKey(Session.SessionID))
                InsigteManager.Remove(Session.SessionID);

            INManager IN = new INManager (Session.SessionID,GetVisitorIpAddress(), GetLanIPAddress());
            InsigteManager.Add(Session.SessionID,IN);

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 

            InsigteManager.Remove(Session.SessionID);

            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.
        }

        public string GetVisitorIpAddress()
        {
            string stringIpAddress;
            stringIpAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (stringIpAddress == null) //may be the HTTP_X_FORWARDED_FOR is null
            {
                stringIpAddress = Request.ServerVariables["REMOTE_ADDR"];//we can use REMOTE_ADDR
            }
            return stringIpAddress;
        }

        //Get Lan Connected IP address method
        public string GetLanIPAddress()
        {
            //Get the Host Name
            string stringHostName = Dns.GetHostName();
            //Get The Ip Host Entry
            IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
            //Get The Ip Address From The Ip Host Entry Address List
            IPAddress[] arrIpAddress = ipHostEntries.AddressList;
            return arrIpAddress[arrIpAddress.Length - 1].ToString();
        }
    }
}
