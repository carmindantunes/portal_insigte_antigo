﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Web.UI.DataVisualization.Charting;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;
using AjaxControlToolkit;

namespace Insigte
{
    public partial class countryTag : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public String TreemapDataAO()
        {
            String QUERY = "select top 20 ent.DES_ENTIDADE,sum(cnt) total from top_word_ANGOLA ta with(nolock) ";
            QUERY += " inner join dbo.BM10D_ENTIDADE ent with(nolock) on COD_ENTIDADE = ta.term ";
                   QUERY += " group by ent.DES_ENTIDADE order by 2 desc ";

            DataTable topAng = Global.InsigteManager[Session.SessionID].getTableQuery(QUERY, "topAng");
            String sData = "";

            int i = 0;
            foreach (DataRow x in topAng.Rows)
            {
                sData += "{ \"name\": \"" + x["DES_ENTIDADE"].ToString() + "\", \"sizes\": " + x["total"].ToString() + ", \"time\": " + i++ + " },";
            }

            return sData.TrimEnd(',');

        }

        public String TreemapDataMZ()
        {
            String QUERY = "select top 20 ent.DES_ENTIDADE,sum(cnt) total from top_word_MOCAMBIQUE ta with(nolock) ";
            QUERY += " inner join dbo.BM10D_ENTIDADE ent with(nolock) on COD_ENTIDADE = ta.term ";
            QUERY += " group by ent.DES_ENTIDADE order by 2 desc ";

            DataTable topAng = Global.InsigteManager[Session.SessionID].getTableQuery(QUERY, "topAng");
            String sData = "";

            int i = 0;
            foreach (DataRow x in topAng.Rows)
            {
                sData += "{ \"name\": \"" + x["DES_ENTIDADE"].ToString() + "\", \"sizes\": " + x["total"].ToString() + ", \"time\": " + i++ + " },";
            }

            return sData.TrimEnd(',');

        }

        public String TreemapDataPT()
        {
            String QUERY = "select top 20 ent.DES_ENTIDADE,sum(cnt) total from top_word_PORTUGAL ta with(nolock) ";
            QUERY += " inner join dbo.BM10D_ENTIDADE ent with(nolock) on COD_ENTIDADE = ta.term ";
            QUERY += " group by ent.DES_ENTIDADE order by 2 desc ";

            DataTable topAng = Global.InsigteManager[Session.SessionID].getTableQuery(QUERY, "topAng");
            String sData = "";

            int i = 0;
            foreach (DataRow x in topAng.Rows)
            {
                sData += "{ \"name\": \"" + x["DES_ENTIDADE"].ToString() + "\", \"sizes\": " + x["total"].ToString() + ", \"time\": " + i++ + " },";
            }

            return sData.TrimEnd(',');

        }

        protected void mudar_tema(object sender, EventArgs e)
        {

        }

    }
}