﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Notas.aspx.cs" Inherits="Insigte.Notas" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Carma | Insigte now is Carma</title>
<link rel="icon" type="ico" href="https://www.carma.com/app/themes/carma/assets/images/favicon.ico" />
    <link href="Styles/StyleSheet.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        a
        {
            text-decoration:none;
            border:0px;
        }
        
        a:hover
        {
            text-decoration:none;
            border:0px;
        }
        
        a:active
        {
            text-decoration:none;
            border:0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <% = "<div style=\"width: 400px; float: left; clear: both; color: #ffffff; height: 67px; background-image: url('Cli/" + global_asax.InsigteManager[Session.SessionID].CodCliente + "/fundo_small.png'); background-repeat:no-repeat; background-color: #003f56;\"> "%>
        </div>
        <div id="divMain" style=" width: auto; height:auto; text-align: justify; clear: both; padding-bottom: 10px; padding-top:10px;">
            <asp:Table ID="Table1" runat="server">
                <asp:TableRow ID="TableRow2" runat="server">
                    <asp:TableCell ID="TableCell3" runat="server" HorizontalAlign="Left">
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell4" runat="server">
                        <asp:Label runat="server" Font-Names="Arial" Font-Size="12px"  ID="Titulo" Text ="Notas"></asp:Label>                        
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" ID="TableRow4">
                    <asp:TableCell ID="TableCell7" runat="server">
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell8" runat="server">        
                        &nbsp;
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow ID="TableRow1" runat="server">
                    <asp:TableCell ID="TableCell1" runat="server" HorizontalAlign="Left">
                        <!--<asp:Label runat="server" ID="lbinfo" Text ="default"></asp:Label>-->
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell2" runat="server">
                        <asp:TextBox runat="server" ID="tbNota" MaxLength="350" Rows="10" Columns="20" TextMode="multiline" Font-Names="Arial" Font-Size="12px" Width="300px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" ID="TableRow3">
                    <asp:TableCell ID="TableCell5" runat="server">
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell6" runat="server">        
                        &nbsp;
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" ID="TableRow5">
                    <asp:TableCell ID="TableCell9" runat="server">
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell10" runat="server">        
                        <asp:LinkButton runat="server" Text="Salvar" Font-Names="Arial" Font-Size="12px" ID="btSalvarNotas" OnClick="btSalvarNota_Click" CssClass="btn-pdfs"></asp:LinkButton>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>
    </form>
</body>
</html>
