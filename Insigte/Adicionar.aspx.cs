﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using APToolkitNET;

namespace Insigte
{
    public partial class Adicionar : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                
                //string qsIDArtigo = Request.QueryString["ID"];


                
                getPastas("-1");
                

            }
        }


        public void getPastas(string pasta)
        {

            ddTo.Visible = true;

            string cmdQueryPastas = "select id, nome from dbo.CL10D_PASTAS with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and id <> " + pasta;
            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(cmdQueryPastas);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                DataSet tempSet;
                SqlDataAdapter iadvisers_ACN_PASTAS;

                iadvisers_ACN_PASTAS = new SqlDataAdapter(cmd);

                tempSet = new DataSet("tempSet");
                iadvisers_ACN_PASTAS.Fill(tempSet);

                ddTo.DataSource = tempSet;
                ddTo.DataBind();
                ddTo.DataTextField = "nome";
                ddTo.DataValueField = "id";
                ddTo.DataBind();
                ddTo.Visible = true;
            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }

        }

        protected void btAddlst(String lst)
        {
            string idVal = "N";

            #region Teste Se not existe na pasta
            string cmdVerifica = "select * from dbo.CL10H_PASTAS_ARTICLES with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and pastaid=" + ddTo.SelectedItem.Value.ToString() + " and articleid=" + lst;
            SqlConnection connv = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                connv.Open();
                SqlCommand cmdv = new SqlCommand(cmdVerifica);
                cmdv.Connection = connv;
                cmdv.CommandType = CommandType.Text;
                SqlDataReader dr = cmdv.ExecuteReader();

                if (dr.HasRows == true)
                {
                    while (dr.Read())
                        idVal += dr["articleid"].ToString() + "|";
                }
                idVal.Substring(0, idVal.Length - 1);
                dr.Close();
            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }
            finally
            {
                connv.Dispose();
                connv.Close();
            }

            #endregion

            #region add not se nao existe
            if (idVal == "N")
            {

                string cmdQueryPastas = " insert into CL10H_PASTAS_ARTICLES ";
                cmdQueryPastas += " (id_client, id_client_user, pastaid, articleid, nota, dat_criacao, cod_dia_criacao) ";
                cmdQueryPastas += " select " + Global.InsigteManager[Session.SessionID].IdClient + ", " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + ", " + ddTo.SelectedItem.Value.ToString() + " ," + lst + ", '', getdate(), convert(varchar,getdate(),112) ";

                SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
                try
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(cmdQueryPastas);
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;

                    cmd.ExecuteNonQuery();

                }
                catch (Exception exp)
                {
                    exp.Message.ToString();
                }
                finally
                {
                    conn.Dispose();
                    conn.Close();
                }

                
            }
            else
            {
                lbInfo.Text = "Este artigo já se encontra nesta pasta!";
            }
            #endregion
        }


        protected void btAdicionar_Click(object sender, EventArgs e)
        {
            string qsIDArtigo = Request.QueryString["ID"];
            if (qsIDArtigo.Contains(";"))
            {
                //add mais que uma
                foreach (String s in qsIDArtigo.Substring(0, (qsIDArtigo.Length - 1)).Split(';'))
                {
                    btAddlst(s);
                }
            }
            else
            {
                //add uma noticia
                btAddlst(qsIDArtigo);
            }

            Response.Write("<script language='javascript'> { self.close() }</script>");
        }  
    }
}