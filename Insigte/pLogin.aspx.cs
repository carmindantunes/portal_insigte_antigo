﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace Insigte
{
    public partial class pLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


        }

        protected void timerAO_Tick(object sender, EventArgs e)
        {
            //lb_tmAo.Text = AoTime();
            //lb_tmMz.Text = MzTime();
            //lb_tmPt.Text = PtTime();

            //lb_tmAS.Text = MzTime();
            //lb_tmNB.Text = MzTime();
        }

        protected String PtTime()
        {

            var remoteTimeZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");
            var remoteTime = TimeZoneInfo.ConvertTime(DateTime.Now, remoteTimeZone);

            string seg = remoteTime.Second.ToString().Length > 1 ? remoteTime.Second.ToString() : "0" + remoteTime.Second.ToString();

            return remoteTime.ToShortTimeString() + ":" + seg;
            //return "Time in " + remoteTimeZone.Id + " is " + remoteTime.TimeOfDay.ToString();
        }

        protected String MzTime()
        {

            var remoteTimeZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");
            var remoteTime = TimeZoneInfo.ConvertTime(DateTime.Now, remoteTimeZone);

            string seg = remoteTime.Second.ToString().Length > 1 ? remoteTime.Second.ToString() : "0" + remoteTime.Second.ToString();

            return remoteTime.AddHours(2).ToShortTimeString() + ":" + seg;
            //return "Time in " + remoteTimeZone.Id + " is " + remoteTime.TimeOfDay.ToString();
        }

        protected String AoTime()
        {

            var remoteTimeZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");
            var remoteTime = TimeZoneInfo.ConvertTime(DateTime.Now, remoteTimeZone);

            string seg = remoteTime.Second.ToString().Length > 1 ? remoteTime.Second.ToString() : "0" + remoteTime.Second.ToString();

            return remoteTime.AddHours(1).ToShortTimeString() + ":" + seg;
            //return "Time in " + remoteTimeZone.Id + " is " + remoteTime.TimeOfDay.ToString();
        }

        protected String getTop5AO()
        {

            String topStr = "";

            //foreach (var pair in Global.InsigteManager)
            //{
            //    topStr += "Session : " + pair.Key.ToString() + "<br>";
            //    topStr += "ID Adr : " + pair.Value.inUser.IPAddress + "<br>";
            //    topStr += "ID Lan Adr : " + pair.Value.inUser.IPLanAddress + "<br>";
            //    topStr += "Nome : " + pair.Value.inUser.Name + "<br>";
            //    topStr += "Cod : " + pair.Value.CodUser + "<br>";
            //}


            String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata  with(nolock) ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            SQLquery += " WHERE clientarticles.clientid='1084' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }

            return topStr;
        }

        protected String getTop5NB()
        {

            String topStr = "";

            //foreach (var pair in Global.InsigteManager)
            //{
            //    topStr += "Session : " + pair.Key.ToString() + "<br>";
            //    topStr += "ID Adr : " + pair.Value.inUser.IPAddress + "<br>";
            //    topStr += "ID Lan Adr : " + pair.Value.inUser.IPLanAddress + "<br>";
            //    topStr += "Nome : " + pair.Value.inUser.Name + "<br>";
            //    topStr += "Cod : " + pair.Value.CodUser + "<br>";
            //}


            String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata with(nolock)  ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            SQLquery += " WHERE clientarticles.clientid='1087' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }

            return topStr;
        }

        protected String getTop5AS()
        {

            String topStr = "";

            String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata with(nolock)  ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            SQLquery += " WHERE clientarticles.clientid='1099' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }

            return topStr;
        }

        protected String getTop5MZ()
        {

            String topStr = "";

            String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata with(nolock)  ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            SQLquery += " WHERE clientarticles.clientid='1085' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }
            return topStr;
        }

        protected String getTop5PT()
        {

            String topStr = "";

            String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata with(nolock)  ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            SQLquery += " WHERE clientarticles.clientid='1014' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }
            return topStr;
        }

        protected String getTop5CV()
        {
            String topStr = "";

            String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata with(nolock)  ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            SQLquery += " WHERE clientarticles.clientid='1089' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }
            return topStr;
        }

        protected String getTop5BW()
        {
            String topStr = "";

            String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata with(nolock)  ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            SQLquery += " WHERE clientarticles.clientid='1101' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }
            return topStr;
        }

        protected String getTop5ZB()
        {
            String topStr = "";

            String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata with(nolock)  ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            SQLquery += " WHERE clientarticles.clientid='1100' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }
            return topStr;
        }

        protected void Ib_login_Click(object sender, EventArgs e)
        {
            Global.InsigteManager[Session.SessionID].Login(txt_user.Value, txt_pass.Value);
            Response.Redirect("Default.aspx");
            //Response.Redirect("login.aspx");
        }

    }
}