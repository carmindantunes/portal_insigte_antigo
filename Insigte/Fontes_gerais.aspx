﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Fontes_gerais.aspx.cs" Inherits="Insigte.Fontes_gerais" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

<script type="text/javascript">

    function lbEditores_DoubleClick(value, vleID) {

        vleID = vleID.replace(vleID, "MainContent_ta_editores_codes");
        //vleID = vleID.replace("LbEditors", "ta_editores_codes");

        if ($('textarea[id^="' + vleID + '"]').tagExist(value)) {
            alert("Editor '" + value + "' já Existe");
        } else {
            if (value == "<%= getResource("geralTagTodos") %>") {
                $('textarea[id^="' + vleID + '"]').importTags('');
                $('textarea[id^="' + vleID + '"]').addTag(value);
            }
            else {
                if ($('textarea[id^="' + vleID + '"]').tagExist("<%= getResource("geralTagTodos") %>")) {
                    $('textarea[id^="' + vleID + '"]').importTags('');
                    $('textarea[id^="' + vleID + '"]').addTag(value);
                } else {
                    $('textarea[id^="' + vleID + '"]').addTag(value);
                }
            }
        } 
    };


    $(function () {
        $('textarea[id^="MainContent_ta_editores_codes"]').tagsInput({
            width: 'auto',
            height: '235px',
            interactive:false
        });
    });

    $(function () {
        $("#tabs").tabs();
    });

    $(function () {
        $("#subTab").tabs();
    });



</script>

<style type="text/css">
    
    .ui-tabs-nav a 
    {
        text-decoration: none;
        font-family: Arial;
        font-size: 12px;
    }
        
    .ui-tabs-nav a:hover 
    {
        text-decoration: none;
        font-family: Arial;
        font-size: 12px;        
    }
    
    #section.sector
    {
        float:left;
        border :1px solid Black;
        width: 100%;
        min-height: 300px;
        min-width:400px;
        width:100%;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
        padding:0px;
        margin:0px;
    }
    
    a.lbk
    {
        color: #000;
        text-decoration: none;
        font-weight:bold;
        font-size: 12px;
        font-family:Arial;
    }
    
    a.lbk:hover
    {
        color: #000;
        text-decoration: underline;
        font-weight: bold;
        font-size: 12px;
        font-family: Arial;
    }
    
    .form_search
    {
        float:left;
        width: 100%;
        height:10px;
        padding-top: 4px;
        padding-bottom: 10px;
        margin: 0 auto 20px auto;
        background-color: #323232;
        color: #FFFFFF;
    }  
    
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="Geral" style="float:left; padding:0px; margin:0px; height:100%; width:800px;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:12px">
            <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false" />
        </div>
    </div>

    <div id="section" class="sector ui-corner-top" style="float:left;width:100%;">
        <div id="titulo" class="form_search headbg ui-corner-top">
            &nbsp;&nbsp;<asp:Label runat="server" ID="LbInfo" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF"><%= getResource("fgTitulo")%></asp:Label>
        </div>
        <div id="sectionBody" style="float:left; margin:5px 20px 10px 20px;float:left; width:94%;">
            <div id="info" style="float:left; width:100%;">
                <p><%= getResource("fgInformacao")%></p>
                <p><%= getResource("fgdescricao")%></p>
                <p><%= getResource("fgduploclick")%></p>
            </div>
            <div id="ListEditors" style="float:left; width:100%;" >
                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-1"><%= getResource("advSMainEditoresT1")%></a></li>
                        <li><a href="#tabs-2"><%= getResource("advSMainEditoresT2")%></a></li>
                        <li><a href="#tabs-3"><%= getResource("advSMainEditoresT3")%></a></li>
                        <li><a href="#tabs-4"><%= getResource("advSMainEditoresT5")%></a></li>
                        <li><a href="#tabs-5"><%= getResource("advSMainEditoresT4")%></a></li>
                    </ul>
                    <div id="tabs-1">
                        <p>
                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="250px" ID="LbEditors" ondblclick="lbEditores_DoubleClick(this.value,this.id)" SelectionMode="Multiple"></asp:ListBox>
       
                        </p>
                    </div>


                    <div id="tabs-2">
                        <p>
                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="250px" ID="LbEditorsAO" ondblclick="lbEditores_DoubleClick(this.value,this.id)" SelectionMode="Multiple"></asp:ListBox>
                        </p>
                    </div>
                    <div id="tabs-3">
                        <p>
                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="250px" ID="LbEditorsMZ" ondblclick="lbEditores_DoubleClick(this.value,this.id)" SelectionMode="Multiple"></asp:ListBox>
                        </p>
                    </div>
                    <div id="tabs-4">
                        <p>
                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="250px" ID="LbEditorsPT" ondblclick="lbEditores_DoubleClick(this.value,this.id)" SelectionMode="Multiple"></asp:ListBox>
                        </p>
                    </div>
                    <div id="tabs-5">
                        <p>
                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="250px" ID="LbEditorsOU" ondblclick="lbEditores_DoubleClick(this.value,this.id)" SelectionMode="Multiple"></asp:ListBox>
                        </p>
                    </div>
                </div>
            </div>
            <div id="SelectedEditors" style="float:left; width:100%; margin-top:10px;">
                <textarea cols="50" rows="6" runat="server" ID="ta_editores_codes" style="width: 354px; height:250px;"></textarea><br />
            </div>
            <div id="action" style="float:left; width:100%;">
                <asp:LinkButton ID="LinkButton1" CssClass="btn-pdfs" runat="server" OnClick="Save_Click"><%= getResource("fgGuardar") %></asp:LinkButton>
            </div>
        </div>
    </div>
</div>
</asp:Content>
