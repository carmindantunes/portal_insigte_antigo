﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Web.UI.DataVisualization.Charting;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;
using AjaxControlToolkit;


namespace Insigte
{
    public partial class bm : BasePage
    {
        private DataTable Meses;
        private DataTable Caracteristicas;
        private DataTable Valores;
        private DataTable Conceitos;
        private DataTable Entidades;
        private DataTable Stakeholders;
        private DataTable Sector;
        private DataTable Geografia;
        private DataTable top10;

        Boolean GRconcorrencia = false;
        String Concorrencia = "";
        Int32 TotalConc = 1;
        String TemaCores = "";
        String[] cores = new String[] { "#00add9", "#00add9", "#00add9", "#00add9", "#00add9" };

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(5, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }

            Titulo.Text = "&nbsp;&nbsp;&nbsp;" + Global.InsigteManager[Session.SessionID].NomCliente + " brand discovery | 2012-2013";
            Lb_ListTemas.Text = "&nbsp;&nbsp;&nbsp;Temas:";
            foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
            {
                if (pair.Value.Name == "Concorrência")
                {
                    GRconcorrencia = true;

                    String[] Conc = pair.Value.SubTemas.Split(';');

                    foreach (String x in Conc)
                    {
                        TotalConc++;
                        Concorrencia += "'" + x.ToString().Substring(4, 3) + "',";
                    }

                    Concorrencia = Concorrencia + "'" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "'";
                    TemaCores = pair.Value.TemaCores;
                    cores = TemaCores.Split(',');
                }
            }


            if (!Page.IsPostBack)
            {
                DDListSubTemas.Visible = false;
                ChKCompare.Visible = false;

                fillDDTemas();

                getData();
                fillData();

                fillGrCaracteristicas();
                fillGrValores();
                fillGrEntidades();
                fillGrSectores();
                fillGrConceitos();
                fillGrStakeholders();
                fillGrGeografia();
                fillGrTop10palavras();
            }

        }

        protected void fillDDTemas()
        {
            String Name = "name";
            String Name_cfg = "name";
            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            {
                Name = "name_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as name";
                Name_cfg = "name_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage;
            }

            

            foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
            {
                if (pair.Value.IndBrandMining == 1)
                {
                    if (pair.Value.IdTema.Substring(4, 3) != "000")
                    {
                        ListItem x = new ListItem(pair.Value.Name, pair.Value.IdTema + pair.Value.TabOrder.ToString());
                        if (x.Value.Substring(0, 7) == Global.InsigteManager[Session.SessionID].TemaCliente)
                            x.Selected = true;
                        DDListTemas.Items.Add(x);
                    }
                    else
                    {

                        if (pair.Value.SubTemas.Substring(4, 3) != "000")
                        {

                            String[] subtemas = pair.Value.SubTemas.Split(';');

                            String SQLTema = "select cast(clienteid as varchar) + cast(subjectid as varchar) as SubTema, " + Name + " from subject with(nolock) where cast(clienteid as varchar) + cast(subjectid as varchar) in ('" + pair.Value.SubTemas.Replace(";", "','") + "') and cast(clienteid as varchar) + cast(subjectid as varchar) in (select distinct cast([COD_TEMA] as varchar) + cast(COD_SUBTEMA as varchar) from [dbo].[BM10F_BRANDMINING] with(nolock)) ";
                            DataTable subTema = new DataTable();
                            subTema = Global.InsigteManager[Session.SessionID].getTableQuery(SQLTema, "subTema");
                            foreach (DataRow row in subTema.Rows)
                            {
                                foreach (String subT in subtemas)
                                {
                                    if (row["SubTema"].ToString() == subT)
                                    {
                                        ListItem x = new ListItem(row["name"].ToString(), subT);
                                        DDListTemas.Items.Add(x);
                                    }
                                }
                            }
                        }

                    }
                }

            }
        }

        protected void DDListTemas_SelectedIndexChanged(object sender, EventArgs e)
        {

            getData();
            fillData();

            fillGrCaracteristicas();
            fillGrValores();
            fillGrEntidades();
            fillGrSectores();
            fillGrConceitos();
            fillGrStakeholders();
            fillGrGeografia();
            fillGrTop10palavras();

        }

        protected void DDListTemasOLD_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ListItem x = new ListItem(pair.Value.Name, pair.Value.IdTema);
            //if (x.Value == Global.InsigteManager[Session.SessionID].TemaCliente)
            //    x.Selected = true;
            //DDListTemas.Items.Add(x);
            String Name = "name";
            String Name_cfg = "name";
            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            {
                Name = "name_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as name";
                Name_cfg = "name_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage;
            }

            DDListSubTemas.Items.Clear();

            foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
            {
                if ((pair.Value.IdTema + pair.Value.TabOrder.ToString()) == DDListTemas.SelectedValue)
                {
                    if (pair.Value.IdTema.Substring(4, 3) == "000")
                    {
                        if (pair.Value.SubTemas.Substring(4, 3) != "000")
                        {
                            String[] subtemas = pair.Value.SubTemas.Split(';');

                            String SQLTema = "select cast(clienteid as varchar) + cast(subjectid as varchar) as SubTema, " + Name + " from subject with(nolock) where cast(clienteid as varchar) + cast(subjectid as varchar) in ('" + pair.Value.SubTemas.Replace(";", "','") + "') ";
                            DataTable subTema = new DataTable();
                            subTema = Global.InsigteManager[Session.SessionID].getTableQuery(SQLTema, "subTema");
                            foreach (DataRow row in subTema.Rows)
                            {
                                foreach (String subT in subtemas)
                                {
                                    if (row["SubTema"].ToString() == subT)
                                    {
                                        ListItem x = new ListItem(row["name"].ToString(), subT);
                                        DDListSubTemas.Items.Add(x);
                                        DDListSubTemas.Visible = true;
                                        ChKCompare.Visible = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            ListItem x = new ListItem(pair.Value.Name, pair.Value.SubTemas);
                            DDListSubTemas.Items.Add(x);
                            DDListSubTemas.Visible = true;
                            ChKCompare.Visible = true;
                        }
                    }
                    else
                    {
                        DDListSubTemas.Visible = false;
                        ChKCompare.Visible = false;
                    }

                }
            }

            getData();
            fillData();

            fillGrCaracteristicas();
            fillGrValores();
            fillGrEntidades();
            fillGrSectores();
            fillGrConceitos();
            fillGrStakeholders();
            fillGrGeografia();
            fillGrTop10palavras();

        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            getData();
            fillData();

            fillGrCaracteristicas();
            fillGrValores();
            fillGrEntidades();
            fillGrSectores();
            fillGrConceitos();
            fillGrStakeholders();
            fillGrGeografia();
            fillGrTop10palavras();
        }

        protected void DDListSubTemas_SelectedIndexChanged(object sender, EventArgs e)
        {
            getData();
            fillData();

            fillGrCaracteristicas();
            fillGrValores();
            fillGrEntidades();
            fillGrSectores();
            fillGrConceitos();
            fillGrStakeholders();
            fillGrGeografia();
            fillGrTop10palavras();
        }

        protected void fillGrTop10palavras()
        {

            Int32[] yValues = new Int32[top10.Rows.Count];
            string[] xValues = new string[top10.Rows.Count];
            string[] sEditor = new string[top10.Rows.Count];

            int counter = 0;
            foreach (DataRow row in top10.Rows)
            {
                yValues[counter] = Convert.ToInt32(row["num"].ToString());
                xValues[counter] = row["des"].ToString();
                counter++;
            }

            ChartTop10Palavras.Series["Series1"].Points.DataBindXY(xValues, yValues);

            counter = 0;
            foreach (DataRow row in top10.Rows)
            {
                ChartTop10Palavras.Series["Series1"].Points[counter].Color = ColorTranslator.FromHtml(cores[0]);
                counter++;
            }

            // Set series chart type
            ChartTop10Palavras.Series["Series1"].ChartType = SeriesChartType.Column;
            // Set series point width
            ChartTop10Palavras.Series["Series1"]["PointWidth"] = "0.5";
            // Show data points labels
            ChartTop10Palavras.Series["Series1"].IsValueShownAsLabel = false;
            // Set data points label style
            ChartTop10Palavras.Series["Series1"]["BarLabelStyle"] = "Center";
            // Show as 3D
            ChartTop10Palavras.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
            // Draw as 3D Cylinder
            ChartTop10Palavras.Series["Series1"]["DrawingStyle"] = "Default";
            //Chart2.Series["Series2"].ToolTip = "#VALX [#VALY]";

            counter = 0;
            foreach (DataRow row in top10.Rows)
            {
                ChartTop10Palavras.Series["Series1"].Points[counter].ToolTip = "#VALX [#VALY]";
                counter++;
            }

            ChartTop10Palavras.Series["Series1"].IsXValueIndexed = false;
            ChartTop10Palavras.Series["Series1"].IsVisibleInLegend = true;
            ChartTop10Palavras.ChartAreas["ChartArea1"].AxisX.LabelStyle.Enabled = true;
            ChartTop10Palavras.ChartAreas["ChartArea1"].AxisX.LabelStyle.Interval = 1;

            //counter = 0;
            //foreach (DataRow row in Caracteristicas.Rows)
            //{
            //    Chart2.Series["Series1"].Points[counter].Url = "~/Subject.aspx?clid=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "&tema=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "&ed=" + row["EDid"].ToString() + "&fon=0&dd=365";
            //    counter++;
            //}

        }

        protected void fillGrGeografia()
        {

            Int32[] yValues = new Int32[Geografia.Rows.Count];
            string[] xValues = new string[Geografia.Rows.Count];
            string[] sEditor = new string[Geografia.Rows.Count];

            int counter = 0;
            foreach (DataRow row in Geografia.Rows)
            {
                yValues[counter] = Convert.ToInt32(row["num"].ToString());
                xValues[counter] = row["des"].ToString();
                counter++;
            }



            ChartGeografia.Series["Series1"].Points.DataBindXY(xValues, yValues);

            counter = 0;
            foreach (DataRow row in Geografia.Rows)
            {
                ChartGeografia.Series["Series1"].Points[counter].Color = ColorTranslator.FromHtml(cores[0]);
                counter++;
            }

            // Set series chart type
            ChartGeografia.Series["Series1"].ChartType = SeriesChartType.Column;
            // Set series point width
            ChartGeografia.Series["Series1"]["PointWidth"] = "0.5";
            // Show data points labels
            ChartGeografia.Series["Series1"].IsValueShownAsLabel = false;
            // Set data points label style
            ChartGeografia.Series["Series1"]["BarLabelStyle"] = "Center";
            // Show as 3D
            ChartGeografia.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
            // Draw as 3D Cylinder
            ChartGeografia.Series["Series1"]["DrawingStyle"] = "Default";
            //Chart2.Series["Series2"].ToolTip = "#VALX [#VALY]";

            counter = 0;
            foreach (DataRow row in Geografia.Rows)
            {
                ChartGeografia.Series["Series1"].Points[counter].ToolTip = "#VALX [#VALY]";
                counter++;
            }

            ChartGeografia.Series["Series1"].IsXValueIndexed = false;
            ChartGeografia.Series["Series1"].IsVisibleInLegend = true;

            ChartGeografia.ChartAreas["ChartArea1"].AxisX.LabelStyle.Enabled = true;
            ChartGeografia.ChartAreas["ChartArea1"].AxisX.LabelStyle.Interval = 1;

        }

        protected void fillGrGeografiaNew()
        {
            Int32[] yValues = new Int32[Geografia.Rows.Count];
            string[] xValues = new string[Geografia.Rows.Count];
            string[] zValues = new string[Geografia.Rows.Count];

            string[] sEditor = new string[Geografia.Rows.Count];


            lblDataToday.Visible = true;

            int counter = 0;
            foreach (DataRow row in Geografia.Rows)
            {
                
                lblDataToday.Text += "<br>Rows : " + row["name"].ToString() + " Value : " + row["des"].ToString() + " ... " + row["num"].ToString();

                yValues[counter] = Convert.ToInt32(row["num"].ToString());
                xValues[counter] = row["des"].ToString();
                zValues[counter] = row["name"].ToString();
                counter++;
            }

            String AuxNew = "";
            foreach (string s in zValues)
            {
                if (s != AuxNew)
                {
                    ChartGeografia.Series.Add(s);
                    ChartGeografia.Series[s].Points.DataBindXY(xValues, yValues);
                    AuxNew = s;
                }
            }

            counter = 0;
            foreach (string s in zValues)
            {
                //lblDataToday.Text += "<br>Point : " + s + " Value : " + xValues[counter].ToString() + " ... " + xValues[counter].ToString();
                //ChartGeografia.Series[s].Points.AddXY(xValues[counter], yValues[counter]);
                //counter++;
                ChartGeografia.Series[s].Points.DataBindXY(xValues, yValues);
            }

            //counter = 0;
            //foreach (DataRow row in Geografia.Rows)
            //{
            //    ChartGeografia.Series["Series1"].Points[counter].Color = ColorTranslator.FromHtml(cores[0]);
            //    counter++;
            //}

            AuxNew = "";
            foreach (string s in zValues)
            {
                if (s != AuxNew)
                {
                    // Set series chart type
                    ChartGeografia.Series[s].ChartType = SeriesChartType.Radar;

                    ChartGeografia.Series[s]["RadarDrawingStyle"] = "Line";  //"Area";
                    ChartGeografia.Series[s]["AreaDrawingStyle"] = "Circle"; //"Polygon";
                    ChartGeografia.Series[s]["CircularLabelsStyle"] = "Circular"; //"Horizontal";

                    if (s == "Accenture")
                        ChartGeografia.Series[s].Color = Color.DarkMagenta;
                    else
                        ChartGeografia.Series[s].Color = Color.DarkGreen;
                    // Show as 3D
                    //ChartGeografia.ChartAreas[s].Area3DStyle.Enable3D = false;

                    AuxNew = s;
                }
            }

            //counter = 0;
            //foreach (DataRow row in Geografia.Rows)
            //{
            //    ChartGeografia.Series[row["name"].ToString()].Points[counter].ToolTip = "#VALX [#VALY]";
            //    counter++;
            //}

            //ChartGeografia.Series["Series1"].IsXValueIndexed = false;
            //ChartGeografia.Series["Series1"].IsVisibleInLegend = true;
            
            //ChartGeografia.ChartAreas["ChartArea1"].AxisX.LabelStyle.Enabled = true;
            //ChartGeografia.ChartAreas["ChartArea1"].AxisX.LabelStyle.Interval = 1;

        }

        protected void fillGrStakeholders()
        {

            Int32[] yValues = new Int32[Stakeholders.Rows.Count];
            string[] xValues = new string[Stakeholders.Rows.Count];
            string[] sEditor = new string[Stakeholders.Rows.Count];

            int counter = 0;
            foreach (DataRow row in Stakeholders.Rows)
            {
                yValues[counter] = Convert.ToInt32(row["num"].ToString());
                xValues[counter] = row["des"].ToString();
                counter++;
            }

            ChartStakeholders.Series["Series1"].Points.DataBindXY(xValues, yValues);

            counter = 0;
            foreach (DataRow row in Stakeholders.Rows)
            {
                ChartStakeholders.Series["Series1"].Points[counter].Color = ColorTranslator.FromHtml(cores[0]);
                counter++;
            }

            // Set series chart type
            ChartStakeholders.Series["Series1"].ChartType = SeriesChartType.Column;
            // Set series point width
            ChartStakeholders.Series["Series1"]["PointWidth"] = "0.5";
            // Show data points labels
            ChartStakeholders.Series["Series1"].IsValueShownAsLabel = false;
            // Set data points label style
            ChartStakeholders.Series["Series1"]["BarLabelStyle"] = "Center";
            // Show as 3D
            ChartStakeholders.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
            // Draw as 3D Cylinder
            ChartStakeholders.Series["Series1"]["DrawingStyle"] = "Default";
            //Chart2.Series["Series2"].ToolTip = "#VALX [#VALY]";

            counter = 0;
            foreach (DataRow row in Stakeholders.Rows)
            {
                ChartStakeholders.Series["Series1"].Points[counter].ToolTip = "#VALX [#VALY]";
                counter++;
            }

            ChartStakeholders.Series["Series1"].IsXValueIndexed = false;
            ChartStakeholders.Series["Series1"].IsVisibleInLegend = true;
            ChartStakeholders.ChartAreas["ChartArea1"].AxisX.LabelStyle.Enabled = true;
            ChartStakeholders.ChartAreas["ChartArea1"].AxisX.LabelStyle.Interval = 1;

            //counter = 0;
            //foreach (DataRow row in Caracteristicas.Rows)
            //{
            //    Chart2.Series["Series1"].Points[counter].Url = "~/Subject.aspx?clid=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "&tema=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "&ed=" + row["EDid"].ToString() + "&fon=0&dd=365";
            //    counter++;
            //}

        }

        protected void fillGrConceitos()
        {

            Int32[] yValues = new Int32[Conceitos.Rows.Count];
            string[] xValues = new string[Conceitos.Rows.Count];
            string[] sEditor = new string[Conceitos.Rows.Count];

            int counter = 0;
            foreach (DataRow row in Conceitos.Rows)
            {
                yValues[counter] = Convert.ToInt32(row["num"].ToString());
                xValues[counter] = row["des"].ToString();
                counter++;
            }

            ChartConceitos.Series["Series1"].Points.DataBindXY(xValues, yValues);

            counter = 0;
            foreach (DataRow row in Conceitos.Rows)
            {
                ChartConceitos.Series["Series1"].Points[counter].Color = ColorTranslator.FromHtml(cores[0]);
                counter++;
            }

            // Set series chart type
            ChartConceitos.Series["Series1"].ChartType = SeriesChartType.Column;
            // Set series point width
            ChartConceitos.Series["Series1"]["PointWidth"] = "0.5";
            // Show data points labels
            ChartConceitos.Series["Series1"].IsValueShownAsLabel = false;
            // Set data points label style
            ChartConceitos.Series["Series1"]["BarLabelStyle"] = "Center";
            // Show as 3D
            ChartConceitos.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
            // Draw as 3D Cylinder
            ChartConceitos.Series["Series1"]["DrawingStyle"] = "Default";
            //Chart2.Series["Series2"].ToolTip = "#VALX [#VALY]";

            counter = 0;
            foreach (DataRow row in Conceitos.Rows)
            {
                ChartConceitos.Series["Series1"].Points[counter].ToolTip = "#VALX [#VALY]";
                counter++;
            }

            ChartConceitos.Series["Series1"].IsXValueIndexed = false;
            ChartConceitos.Series["Series1"].IsVisibleInLegend = true;
            ChartConceitos.ChartAreas["ChartArea1"].AxisX.LabelStyle.Enabled = true;
            ChartConceitos.ChartAreas["ChartArea1"].AxisX.LabelStyle.Interval = 1;

            //counter = 0;
            //foreach (DataRow row in Caracteristicas.Rows)
            //{
            //    Chart2.Series["Series1"].Points[counter].Url = "~/Subject.aspx?clid=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "&tema=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "&ed=" + row["EDid"].ToString() + "&fon=0&dd=365";
            //    counter++;
            //}

        }

        protected void fillGrSectores()
        {

            Int32[] yValues = new Int32[Sector.Rows.Count];
            string[] xValues = new string[Sector.Rows.Count];
            string[] sEditor = new string[Sector.Rows.Count];

            int counter = 0;
            foreach (DataRow row in Sector.Rows)
            {
                yValues[counter] = Convert.ToInt32(row["num"].ToString());
                xValues[counter] = row["des"].ToString();
                counter++;
            }

            ChartSectores.Series["Series1"].Points.DataBindXY(xValues, yValues);

            counter = 0;
            foreach (DataRow row in Sector.Rows)
            {
                ChartSectores.Series["Series1"].Points[counter].Color = ColorTranslator.FromHtml(cores[0]);
                counter++;
            }

            // Set series chart type
            ChartSectores.Series["Series1"].ChartType = SeriesChartType.Column;
            // Set series point width
            ChartSectores.Series["Series1"]["PointWidth"] = "0.5";
            // Show data points labels
            ChartSectores.Series["Series1"].IsValueShownAsLabel = false;
            // Set data points label style
            ChartSectores.Series["Series1"]["BarLabelStyle"] = "Center";
            // Show as 3D
            ChartSectores.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
            // Draw as 3D Cylinder
            ChartSectores.Series["Series1"]["DrawingStyle"] = "Default";
            //Chart2.Series["Series2"].ToolTip = "#VALX [#VALY]";

            counter = 0;
            foreach (DataRow row in Sector.Rows)
            {
                ChartSectores.Series["Series1"].Points[counter].ToolTip = "#VALX [#VALY]";
                counter++;
            }

            ChartSectores.Series["Series1"].IsXValueIndexed = false;
            ChartSectores.Series["Series1"].IsVisibleInLegend = true;
            ChartSectores.ChartAreas["ChartArea1"].AxisX.LabelStyle.Enabled = true;
            ChartSectores.ChartAreas["ChartArea1"].AxisX.LabelStyle.Interval = 1;

            //counter = 0;
            //foreach (DataRow row in Caracteristicas.Rows)
            //{
            //    Chart2.Series["Series1"].Points[counter].Url = "~/Subject.aspx?clid=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "&tema=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "&ed=" + row["EDid"].ToString() + "&fon=0&dd=365";
            //    counter++;
            //}

        }

        protected void fillGrEntidades()
        {

            Int32[] yValues = new Int32[Entidades.Rows.Count];
            string[] xValues = new string[Entidades.Rows.Count];
            string[] sEditor = new string[Entidades.Rows.Count];

            int counter = 0;
            foreach (DataRow row in Entidades.Rows)
            {
                yValues[counter] = Convert.ToInt32(row["num"].ToString());
                xValues[counter] = row["des"].ToString();
                counter++;
            }

            ChartEntidades.Series["Series1"].Points.DataBindXY(xValues, yValues);

            counter = 0;
            foreach (DataRow row in Entidades.Rows)
            {
                ChartEntidades.Series["Series1"].Points[counter].Color = ColorTranslator.FromHtml(cores[0]);
                counter++;
            }

            // Set series chart type
            ChartEntidades.Series["Series1"].ChartType = SeriesChartType.Column;
            // Set series point width
            ChartEntidades.Series["Series1"]["PointWidth"] = "0.5";
            // Show data points labels
            ChartEntidades.Series["Series1"].IsValueShownAsLabel = false;
            // Set data points label style
            ChartEntidades.Series["Series1"]["BarLabelStyle"] = "Center";
            // Show as 3D
            ChartEntidades.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
            // Draw as 3D Cylinder
            ChartEntidades.Series["Series1"]["DrawingStyle"] = "Default";
            //Chart2.Series["Series2"].ToolTip = "#VALX [#VALY]";

            counter = 0;
            foreach (DataRow row in Entidades.Rows)
            {
                ChartEntidades.Series["Series1"].Points[counter].ToolTip = "#VALX [#VALY]";
                counter++;
            }

            ChartEntidades.Series["Series1"].IsXValueIndexed = false;
            ChartEntidades.Series["Series1"].IsVisibleInLegend = true;
            ChartEntidades.ChartAreas["ChartArea1"].AxisX.LabelStyle.Enabled = true;
            ChartEntidades.ChartAreas["ChartArea1"].AxisX.LabelStyle.Interval = 1;

            //counter = 0;
            //foreach (DataRow row in Caracteristicas.Rows)
            //{
            //    Chart2.Series["Series1"].Points[counter].Url = "~/Subject.aspx?clid=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "&tema=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "&ed=" + row["EDid"].ToString() + "&fon=0&dd=365";
            //    counter++;
            //}

        }

        protected void fillGrCaracteristicas()
        {
            Int32[] yValues = new Int32[Caracteristicas.Rows.Count];
            string[] xValues = new string[Caracteristicas.Rows.Count];
            string[] sEditor = new string[Caracteristicas.Rows.Count];

            int counter = 0;
            foreach (DataRow row in Caracteristicas.Rows)
            {
                yValues[counter] = Convert.ToInt32(row["num"].ToString());
                xValues[counter] = row["des"].ToString();
                counter++;
            }

            ChartCaracteristicas.Series["Series1"].Points.DataBindXY(xValues, yValues);

            counter = 0;
            foreach (DataRow row in Caracteristicas.Rows)
            {
                ChartCaracteristicas.Series["Series1"].Points[counter].Color = ColorTranslator.FromHtml(cores[0]);
                counter++;
            }

            // Set series chart type
            ChartCaracteristicas.Series["Series1"].ChartType = SeriesChartType.Column;
            // Set series point width
            ChartCaracteristicas.Series["Series1"]["PointWidth"] = "0.5";
            // Show data points labels
            ChartCaracteristicas.Series["Series1"].IsValueShownAsLabel = false;
            // Set data points label style
            ChartCaracteristicas.Series["Series1"]["BarLabelStyle"] = "Center";
            // Show as 3D
            ChartCaracteristicas.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
            // Draw as 3D Cylinder
            ChartCaracteristicas.Series["Series1"]["DrawingStyle"] = "Default";
            //Chart2.Series["Series2"].ToolTip = "#VALX [#VALY]";

            counter = 0;
            foreach (DataRow row in Caracteristicas.Rows)
            {
                ChartCaracteristicas.Series["Series1"].Points[counter].ToolTip = "#VALX [#VALY]";
                counter++;
            }

            ChartCaracteristicas.Series["Series1"].IsXValueIndexed = false;
            ChartCaracteristicas.Series["Series1"].IsVisibleInLegend = true;
            ChartCaracteristicas.ChartAreas["ChartArea1"].AxisX.LabelStyle.Enabled = true;
            ChartCaracteristicas.ChartAreas["ChartArea1"].AxisX.LabelStyle.Interval = 1;

            //counter = 0;
            //foreach (DataRow row in Caracteristicas.Rows)
            //{
            //    Chart2.Series["Series1"].Points[counter].Url = "~/Subject.aspx?clid=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "&tema=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "&ed=" + row["EDid"].ToString() + "&fon=0&dd=365";
            //    counter++;
            //}

        }

        protected void fillGrValores()
        {

            Int32[] yValues = new Int32[Valores.Rows.Count];
            string[] xValues = new string[Valores.Rows.Count];
            string[] sEditor = new string[Valores.Rows.Count];

            int counter = 0;
            foreach (DataRow row in Valores.Rows)
            {
                yValues[counter] = Convert.ToInt32(row["num"].ToString());
                xValues[counter] = row["des"].ToString();
                counter++;
            }

            ChartValores.Series["Series1"].Points.DataBindXY(xValues, yValues);

            counter = 0;
            foreach (DataRow row in Valores.Rows)
            {
                ChartValores.Series["Series1"].Points[counter].Color = ColorTranslator.FromHtml(cores[0]);
                counter++;
            }

            // Set series chart type
            ChartValores.Series["Series1"].ChartType = SeriesChartType.Column;
            // Set series point width
            ChartValores.Series["Series1"]["PointWidth"] = "0.5";
            // Show data points labels
            ChartValores.Series["Series1"].IsValueShownAsLabel = false;
            // Set data points label style
            ChartValores.Series["Series1"]["BarLabelStyle"] = "Center";
            // Show as 3D
            ChartValores.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;
            // Draw as 3D Cylinder
            ChartValores.Series["Series1"]["DrawingStyle"] = "Default";
            //Chart2.Series["Series2"].ToolTip = "#VALX [#VALY]";

            counter = 0;
            foreach (DataRow row in Valores.Rows)
            {
                ChartValores.Series["Series1"].Points[counter].ToolTip = "#VALX [#VALY]";
                counter++;
            }

            ChartValores.Series["Series1"].IsXValueIndexed = false;
            ChartValores.Series["Series1"].IsVisibleInLegend = true;
            ChartValores.ChartAreas["ChartArea1"].AxisX.LabelStyle.Enabled = true;
            ChartValores.ChartAreas["ChartArea1"].AxisX.LabelStyle.Interval = 1;

            //counter = 0;
            //foreach (DataRow row in Caracteristicas.Rows)
            //{
            //    Chart2.Series["Series1"].Points[counter].Url = "~/Subject.aspx?clid=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) + "&tema=" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "&ed=" + row["EDid"].ToString() + "&fon=0&dd=365";
            //    counter++;
            //}

        }

        protected void getData()
        {
            String SQLquery = "select distinct left(cod_day,6) cod_year_month, DES_YEAR_MONTH_PT from [time_month] with(nolock) where left(cod_day,6) in (select cod_year_month from [dbo].[BM10F_WORD_BY_CLIENT] with(nolock)) order by left(cod_day,6) asc ";
            Meses = new DataTable();
            Meses = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "BMMeses");
            
            String COD_TEMAS = "";
            if (ChKCompare.Checked && ChKCompare.Visible)
            {
                COD_TEMAS = Global.InsigteManager[Session.SessionID].TemaCliente + "','" + DDListSubTemas.SelectedValue.Substring(0, 7);
            }
            else
            {
                if (DDListSubTemas.Visible)
                {
                    COD_TEMAS = DDListSubTemas.SelectedValue.Substring(0, 7);
                    
                }
                else
                {
                    COD_TEMAS = DDListTemas.SelectedValue.Substring(0, 7);
                }
            }

            //lblDataToday.Visible = true;
            //lblDataToday.Text = COD_TEMAS;

            SQLquery = "select top 10 w.VAL_WORD Word ,SUM(w.VAL_COUNT) Num, fl.DES_GEOGRAFIA as des, s.name ";
            SQLquery += "  from [BM10F_BRANDMINING] w with(nolock) ";
            SQLquery += "	   left outer join BM10X_STOP_WORDS sw with(nolock) ";
            SQLquery += " 					on w.VAL_WORD = sw.VAL_WORD ";
            SQLquery += "	   inner join BM10D_GEOGRAFIA fl with(nolock) ";
            SQLquery += "			   on w.VAL_WORD = fl.COD_GEOGRAFIA ";
            SQLquery += "      inner join subject s with(nolock) ";
            SQLquery += "			   on w.[COD_TEMA] = s.[clienteid] ";
            SQLquery += "			  and w.[COD_SUBTEMA] = s.[subjectid] ";
            SQLquery += " where sw.ID_STOP_WORD is null ";
            SQLquery += "   and CAST([COD_TEMA] as Varchar) + CAST([COD_SUBTEMA] as Varchar) in ('" + COD_TEMAS + "')";
            SQLquery += "   and left(COD_DAY,6) >= 201201";
            SQLquery += " group by w.VAL_WORD, fl.DES_GEOGRAFIA, s.name ";
            SQLquery += " order by SUM(w.VAL_COUNT) desc ";

            Geografia = new DataTable();
            Geografia = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "BMGeografia");

            SQLquery = "select top 10 w.VAL_WORD Word ,SUM(w.VAL_COUNT) Num, fl.DES_ENTIDADE as des, s.name ";
            SQLquery += "  from [BM10F_BRANDMINING] w with(nolock) ";
            SQLquery += "	   left outer join BM10X_STOP_WORDS sw with(nolock) ";
            SQLquery += " 					on w.VAL_WORD = sw.VAL_WORD ";
            SQLquery += "	   inner join BM10D_ENTIDADE fl with(nolock) ";
            SQLquery += "			   on w.VAL_WORD = fl.COD_ENTIDADE ";
            SQLquery += "      inner join subject s with(nolock) ";
            SQLquery += "			   on w.[COD_TEMA] = s.[clienteid] ";
            SQLquery += "			  and w.[COD_SUBTEMA] = s.[subjectid] ";
            SQLquery += " where sw.ID_STOP_WORD is null ";
            SQLquery += "   and CAST([COD_TEMA] as Varchar) + CAST([COD_SUBTEMA] as Varchar) in ('" + COD_TEMAS + "')";
            SQLquery += "   and left(COD_DAY,6) >= 201201";
            SQLquery += " group by w.VAL_WORD, fl.DES_ENTIDADE, s.name ";
            SQLquery += " order by SUM(w.VAL_COUNT) desc ";

            Entidades = new DataTable();
            Entidades = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "BMEntidade");

            SQLquery = "select top 10 w.VAL_WORD Word ,SUM(w.VAL_COUNT) Num, fl.DES_CARACTERISTICA as des, s.name ";
            SQLquery += "  from [BM10F_BRANDMINING] w with(nolock) ";
            SQLquery += "	   left outer join BM10X_STOP_WORDS sw with(nolock) ";
            SQLquery += " 					on w.VAL_WORD = sw.VAL_WORD ";
            SQLquery += "	   inner join BM10D_CARACTERISTICA fl with(nolock) ";
            SQLquery += "			   on w.VAL_WORD = fl.COD_CARACTERISTICA ";
            SQLquery += "      inner join subject s with(nolock) ";
            SQLquery += "			   on w.[COD_TEMA] = s.[clienteid] ";
            SQLquery += "			  and w.[COD_SUBTEMA] = s.[subjectid] ";
            SQLquery += " where sw.ID_STOP_WORD is null ";
            SQLquery += "   and CAST([COD_TEMA] as Varchar) + CAST([COD_SUBTEMA] as Varchar) in ('" + COD_TEMAS + "')";
            SQLquery += "   and left(COD_DAY,6) >= 201201";
            SQLquery += " group by w.VAL_WORD, fl.DES_CARACTERISTICA, s.name ";
            SQLquery += " order by SUM(w.VAL_COUNT) desc ";

            
            Caracteristicas = new DataTable();
            Caracteristicas = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "BMCaracteristicas");

            SQLquery = "select top 10 w.VAL_WORD Word ,SUM(w.VAL_COUNT) Num, fl.DES_VALOR as des, s.name ";
            SQLquery += "  from [BM10F_BRANDMINING] w with(nolock) ";
            SQLquery += "	   left outer join BM10X_STOP_WORDS sw with(nolock) ";
            SQLquery += " 					on w.VAL_WORD = sw.VAL_WORD ";
            SQLquery += "	   inner join BM10D_VALOR fl with(nolock) ";
            SQLquery += "			   on w.VAL_WORD = fl.COD_VALOR ";
            SQLquery += "      inner join subject s with(nolock) ";
            SQLquery += "			   on w.[COD_TEMA] = s.[clienteid] ";
            SQLquery += "			  and w.[COD_SUBTEMA] = s.[subjectid] ";
            SQLquery += " where sw.ID_STOP_WORD is null ";
            SQLquery += "   and CAST([COD_TEMA] as Varchar) + CAST([COD_SUBTEMA] as Varchar) in ('" + COD_TEMAS + "')";
            SQLquery += "   and left(COD_DAY,6) >= 201201";
            SQLquery += " group by w.VAL_WORD, fl.DES_VALOR, s.name ";
            SQLquery += " order by SUM(w.VAL_COUNT) desc ";

            Valores = new DataTable();
            Valores = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "BMValores");

            SQLquery = "select top 10 w.VAL_WORD Word ,SUM(w.VAL_COUNT) Num, fl.DES_CONCEITO as des, s.name ";
            SQLquery += "  from [BM10F_BRANDMINING] w with(nolock) ";
            SQLquery += "	   left outer join BM10X_STOP_WORDS sw with(nolock) ";
            SQLquery += " 					on w.VAL_WORD = sw.VAL_WORD ";
            SQLquery += "	   inner join BM10D_CONCEITO fl with(nolock) ";
            SQLquery += "			   on w.VAL_WORD = fl.COD_CONCEITO ";
            SQLquery += "      inner join subject s with(nolock) ";
            SQLquery += "			   on w.[COD_TEMA] = s.[clienteid] ";
            SQLquery += "			  and w.[COD_SUBTEMA] = s.[subjectid] ";
            SQLquery += " where sw.ID_STOP_WORD is null ";
            SQLquery += "   and CAST([COD_TEMA] as Varchar) + CAST([COD_SUBTEMA] as Varchar) in ('" + COD_TEMAS + "')";
            SQLquery += "   and left(COD_DAY,6) >= 201201";
            SQLquery += " group by w.VAL_WORD, fl.DES_CONCEITO, s.name ";
            SQLquery += " order by SUM(w.VAL_COUNT) desc ";

            Conceitos = new DataTable();
            Conceitos = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "BMConceitos");

            SQLquery = "select top 10 w.VAL_WORD Word ,SUM(w.VAL_COUNT) Num, fl.DES_STAKEHOLDER as des, s.name ";
            SQLquery += "  from [BM10F_BRANDMINING] w with(nolock) ";
            SQLquery += "	   left outer join BM10X_STOP_WORDS sw with(nolock) ";
            SQLquery += " 					on w.VAL_WORD = sw.VAL_WORD ";
            SQLquery += "	   inner join BM10D_STAKEHOLDER fl with(nolock) ";
            SQLquery += "			   on w.VAL_WORD = fl.COD_STAKEHOLDER ";
            SQLquery += "      inner join subject s with(nolock) ";
            SQLquery += "			   on w.[COD_TEMA] = s.[clienteid] ";
            SQLquery += "			  and w.[COD_SUBTEMA] = s.[subjectid] ";
            SQLquery += " where sw.ID_STOP_WORD is null ";
            SQLquery += "   and CAST([COD_TEMA] as Varchar) + CAST([COD_SUBTEMA] as Varchar) in ('" + COD_TEMAS + "')";
            SQLquery += "   and left(COD_DAY,6) >= 201201";
            SQLquery += " group by w.VAL_WORD, fl.DES_STAKEHOLDER, s.name ";
            SQLquery += " order by SUM(w.VAL_COUNT) desc ";

            Stakeholders = new DataTable();
            Stakeholders = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "BMStakeholders");

            SQLquery = "select top 10 w.VAL_WORD Word ,SUM(w.VAL_COUNT) Num, fl.DES_SECTOR as des, s.name ";
            SQLquery += "  from [BM10F_BRANDMINING] w with(nolock) ";
            SQLquery += "	   left outer join BM10X_STOP_WORDS sw with(nolock) ";
            SQLquery += " 					on w.VAL_WORD = sw.VAL_WORD ";
            SQLquery += "	   inner join BM10D_SECTOR fl with(nolock) ";
            SQLquery += "			   on w.VAL_WORD = fl.COD_SECTOR ";
            SQLquery += "      inner join subject s with(nolock) ";
            SQLquery += "			   on w.[COD_TEMA] = s.[clienteid] ";
            SQLquery += "			  and w.[COD_SUBTEMA] = s.[subjectid] ";
            SQLquery += " where sw.ID_STOP_WORD is null ";
            SQLquery += "   and CAST([COD_TEMA] as Varchar) + CAST([COD_SUBTEMA] as Varchar) in ('" + COD_TEMAS + "')";
            SQLquery += "   and left(COD_DAY,6) >= 201201";
            SQLquery += " group by w.VAL_WORD, fl.DES_SECTOR, s.name ";
            SQLquery += " order by SUM(w.VAL_COUNT) desc ";

            Sector = new DataTable();
            Sector = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "BMSector");

            //SQLquery = "select top 10 w.VAL_WORD des,SUM(w.VAL_COUNT) num from [dbo].[BM10F_WORD_BY_CLIENT] w left outer join BM10X_STOP_WORDS sw on w.VAL_WORD = sw.VAL_WORD ";
            //SQLquery += " where sw.ID_STOP_WORD is null and w.ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " group by w.VAL_WORD order by SUM(w.VAL_COUNT) desc ";

            SQLquery = "select top 10 coalesce ([DES_CARACTERISTICA],[DES_CONCEITO],[DES_ENTIDADE],[DES_GEOGRAFIA],[DES_SECTOR],[DES_STAKEHOLDER],[DES_VALOR],w.VAL_WORD) des,SUM(w.VAL_COUNT) num, s.name ";
            SQLquery += "  from [dbo].[BM10F_BRANDMINING] w with(nolock)   ";
            SQLquery += "        left outer join BM10X_STOP_WORDS sw with(nolock)   ";
            SQLquery += "					 on w.VAL_WORD = sw.VAL_WORD   ";
            SQLquery += "	    left outer join BM10D_GEOGRAFIA go with(nolock)  ";
            SQLquery += "			         on w.VAL_WORD = go.COD_GEOGRAFIA  ";
            SQLquery += "	    left outer join BM10D_ENTIDADE en with(nolock)  ";
            SQLquery += "			         on w.VAL_WORD = en.COD_ENTIDADE  ";
            SQLquery += "	    left outer join BM10D_CARACTERISTICA ca with(nolock)  ";
            SQLquery += "			         on w.VAL_WORD = ca.COD_CARACTERISTICA  ";
            SQLquery += "	    left outer join BM10D_VALOR vl with(nolock)  ";
            SQLquery += "			         on w.VAL_WORD = vl.COD_VALOR  ";
            SQLquery += "	    left outer join BM10D_CONCEITO co with(nolock)  ";
            SQLquery += "			         on w.VAL_WORD = co.COD_CONCEITO  ";
            SQLquery += "	    left outer join BM10D_STAKEHOLDER st with(nolock)  ";
            SQLquery += "			         on w.VAL_WORD = st.COD_STAKEHOLDER  ";
            SQLquery += "	    left outer join BM10D_SECTOR sc with(nolock)  ";
            SQLquery += "			         on w.VAL_WORD = sc.COD_SECTOR  ";
            SQLquery += "      inner join subject s with(nolock) ";
            SQLquery += "			   on w.[COD_TEMA] = s.[clienteid] ";
            SQLquery += "			  and w.[COD_SUBTEMA] = s.[subjectid] ";
            SQLquery += " where sw.ID_STOP_WORD is null   ";
            SQLquery += "   and CAST([COD_TEMA] as Varchar) + CAST([COD_SUBTEMA] as Varchar) in ('" + COD_TEMAS + "')";
            SQLquery += "   and left(COD_DAY,6) >= 201201";
            SQLquery += "   and ([ID_CARACTERISTICA] is not null OR [ID_CONCEITO] is not null OR [ID_ENTIDADE] is not null or [ID_GEOGRAFIA] is not null or [ID_SECTOR]  is not null or [ID_STAKEHOLDER]  is not null or [ID_VALOR] is not null) ";
            SQLquery += " group by coalesce ([DES_CARACTERISTICA],[DES_CONCEITO],[DES_ENTIDADE],[DES_GEOGRAFIA],[DES_SECTOR],[DES_STAKEHOLDER],[DES_VALOR],w.VAL_WORD), s.name ";
            SQLquery += " order by SUM(w.VAL_COUNT) desc  ";

            top10 = new DataTable();
            top10 = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "BMTop10");


        }
        
        protected void fillData()
        {
            ////Meses
            //foreach (DataRow x in Meses.Rows)
            //{
            //    LBox_TIME.Items.Add(x["DES_YEAR_MONTH_PT"].ToString());
            //}
            LBox_RESULTADOS.Items.Clear();
            foreach (DataRow x in top10.Rows)
            {
                LBox_RESULTADOS.Items.Add(x["des"].ToString());
            }

            //Geografia
            LBox_GEO.Items.Clear();
            foreach (DataRow x in Geografia.Rows)
            {
                LBox_GEO.Items.Add(x["des"].ToString());
            }

            //Entidade
            LBox_ENTIDADE.Items.Clear();
            foreach (DataRow x in Entidades.Rows)
            {
                LBox_ENTIDADE.Items.Add(x["des"].ToString() + "" );
            }

            //Caracteristicas
            LBox_CARAC.Items.Clear();
            foreach (DataRow x in Caracteristicas.Rows)
            {
                LBox_CARAC.Items.Add(x["des"].ToString());
            }

            //Valores
            LBox_VALOR.Items.Clear();
            foreach (DataRow x in Valores.Rows)
            {
                LBox_VALOR.Items.Add(x["des"].ToString());
            }

            //Conceitos
            LBox_CONCEITO.Items.Clear();
            foreach (DataRow x in Conceitos.Rows)
            {
                LBox_CONCEITO.Items.Add(x["des"].ToString());
            }

            //Stakeholders
            LBox_STAKE.Items.Clear();
            foreach (DataRow x in Stakeholders.Rows)
            {
                LBox_STAKE.Items.Add(x["des"].ToString());
            }

            //Sector
            LBox_SECTOR.Items.Clear();
            foreach (DataRow x in Sector.Rows)
            {
                LBox_SECTOR.Items.Add(x["des"].ToString());
            }

        }

        protected void getDataTestes()
        {
            String SQLquery = "";

            String COD_TEMAS = "1017106,1017105";
            
            lblDataToday.Visible = true;
            lblDataToday.Text = COD_TEMAS;

            String[] Temas = COD_TEMAS.Split(',');

            foreach (String tema in Temas)
            {
                SQLquery += " select * from ( ";
                SQLquery += " select top 5 w.VAL_WORD Word ,SUM(w.VAL_COUNT) Num, fl.DES_GEOGRAFIA as des, s.name ";
                SQLquery += "  from [BM10F_BRANDMINING] w with(nolock) ";
                SQLquery += "	   left outer join BM10X_STOP_WORDS sw with(nolock) ";
                SQLquery += " 					on w.VAL_WORD = sw.VAL_WORD ";
                SQLquery += "	   inner join BM10D_GEOGRAFIA fl with(nolock) ";
                SQLquery += "			   on w.VAL_WORD = fl.COD_GEOGRAFIA ";
                SQLquery += "      inner join subject s with(nolock) ";
                SQLquery += "			   on w.[COD_TEMA] = s.[clienteid] ";
                SQLquery += "			  and w.[COD_SUBTEMA] = s.[subjectid] ";
                SQLquery += " where sw.ID_STOP_WORD is null ";
                SQLquery += "   and CAST([COD_TEMA] as Varchar) + CAST([COD_SUBTEMA] as Varchar) = '" + tema + "' ";
                SQLquery += "   and left(COD_DAY,6) >= 201201";
                SQLquery += " group by w.VAL_WORD, fl.DES_GEOGRAFIA, s.name ";
                SQLquery += " order by 2 desc ";
                SQLquery += " ) x ";
                SQLquery += " union all ";
            }

            SQLquery = SQLquery.Substring(0, SQLquery.Length - 10);
            lblDataToday.Text = SQLquery;

            Geografia = new DataTable();
            Geografia = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "BMGeografia");

            SQLquery = "select top 10 w.VAL_WORD Word ,SUM(w.VAL_COUNT) Num, fl.DES_ENTIDADE as des, s.name ";
            SQLquery += "  from [BM10F_BRANDMINING] w with(nolock) ";
            SQLquery += "	   left outer join BM10X_STOP_WORDS sw with(nolock) ";
            SQLquery += " 					on w.VAL_WORD = sw.VAL_WORD ";
            SQLquery += "	   inner join BM10D_ENTIDADE fl with(nolock) ";
            SQLquery += "			   on w.VAL_WORD = fl.COD_ENTIDADE ";
            SQLquery += "      inner join subject s with(nolock) ";
            SQLquery += "			   on w.[COD_TEMA] = s.[clienteid] ";
            SQLquery += "			  and w.[COD_SUBTEMA] = s.[subjectid] ";
            SQLquery += " where sw.ID_STOP_WORD is null ";
            SQLquery += "   and CAST([COD_TEMA] as Varchar) + CAST([COD_SUBTEMA] as Varchar) in ('" + COD_TEMAS + "')";
            SQLquery += "   and left(COD_DAY,6) >= 201201";
            SQLquery += " group by w.VAL_WORD, fl.DES_ENTIDADE, s.name ";
            SQLquery += " order by SUM(w.VAL_COUNT) desc ";

            Entidades = new DataTable();
            Entidades = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "BMEntidade");

            SQLquery = "select top 10 w.VAL_WORD Word ,SUM(w.VAL_COUNT) Num, fl.DES_CARACTERISTICA as des, s.name ";
            SQLquery += "  from [BM10F_BRANDMINING] w with(nolock) ";
            SQLquery += "	   left outer join BM10X_STOP_WORDS sw with(nolock) ";
            SQLquery += " 					on w.VAL_WORD = sw.VAL_WORD ";
            SQLquery += "	   inner join BM10D_CARACTERISTICA fl with(nolock) ";
            SQLquery += "			   on w.VAL_WORD = fl.COD_CARACTERISTICA ";
            SQLquery += "      inner join subject s with(nolock) ";
            SQLquery += "			   on w.[COD_TEMA] = s.[clienteid] ";
            SQLquery += "			  and w.[COD_SUBTEMA] = s.[subjectid] ";
            SQLquery += " where sw.ID_STOP_WORD is null ";
            SQLquery += "   and CAST([COD_TEMA] as Varchar) + CAST([COD_SUBTEMA] as Varchar) in ('" + COD_TEMAS + "')";
            SQLquery += "   and left(COD_DAY,6) >= 201201";
            SQLquery += " group by w.VAL_WORD, fl.DES_CARACTERISTICA, s.name ";
            SQLquery += " order by SUM(w.VAL_COUNT) desc ";


            Caracteristicas = new DataTable();
            Caracteristicas = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "BMCaracteristicas");

            SQLquery = "select top 10 w.VAL_WORD Word ,SUM(w.VAL_COUNT) Num, fl.DES_VALOR as des, s.name ";
            SQLquery += "  from [BM10F_BRANDMINING] w with(nolock) ";
            SQLquery += "	   left outer join BM10X_STOP_WORDS sw with(nolock) ";
            SQLquery += " 					on w.VAL_WORD = sw.VAL_WORD ";
            SQLquery += "	   inner join BM10D_VALOR fl with(nolock) ";
            SQLquery += "			   on w.VAL_WORD = fl.COD_VALOR ";
            SQLquery += "      inner join subject s with(nolock) ";
            SQLquery += "			   on w.[COD_TEMA] = s.[clienteid] ";
            SQLquery += "			  and w.[COD_SUBTEMA] = s.[subjectid] ";
            SQLquery += " where sw.ID_STOP_WORD is null ";
            SQLquery += "   and CAST([COD_TEMA] as Varchar) + CAST([COD_SUBTEMA] as Varchar) in ('" + COD_TEMAS + "')";
            SQLquery += "   and left(COD_DAY,6) >= 201201";
            SQLquery += " group by w.VAL_WORD, fl.DES_VALOR, s.name ";
            SQLquery += " order by SUM(w.VAL_COUNT) desc ";

            Valores = new DataTable();
            Valores = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "BMValores");

            SQLquery = "select top 10 w.VAL_WORD Word ,SUM(w.VAL_COUNT) Num, fl.DES_CONCEITO as des, s.name ";
            SQLquery += "  from [BM10F_BRANDMINING] w with(nolock) ";
            SQLquery += "	   left outer join BM10X_STOP_WORDS sw with(nolock) ";
            SQLquery += " 					on w.VAL_WORD = sw.VAL_WORD ";
            SQLquery += "	   inner join BM10D_CONCEITO fl with(nolock) ";
            SQLquery += "			   on w.VAL_WORD = fl.COD_CONCEITO ";
            SQLquery += "      inner join subject s with(nolock) ";
            SQLquery += "			   on w.[COD_TEMA] = s.[clienteid] ";
            SQLquery += "			  and w.[COD_SUBTEMA] = s.[subjectid] ";
            SQLquery += " where sw.ID_STOP_WORD is null ";
            SQLquery += "   and CAST([COD_TEMA] as Varchar) + CAST([COD_SUBTEMA] as Varchar) in ('" + COD_TEMAS + "')";
            SQLquery += "   and left(COD_DAY,6) >= 201201";
            SQLquery += " group by w.VAL_WORD, fl.DES_CONCEITO, s.name ";
            SQLquery += " order by SUM(w.VAL_COUNT) desc ";

            Conceitos = new DataTable();
            Conceitos = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "BMConceitos");

            SQLquery = "select top 10 w.VAL_WORD Word ,SUM(w.VAL_COUNT) Num, fl.DES_STAKEHOLDER as des, s.name ";
            SQLquery += "  from [BM10F_BRANDMINING] w with(nolock) ";
            SQLquery += "	   left outer join BM10X_STOP_WORDS sw with(nolock) ";
            SQLquery += " 					on w.VAL_WORD = sw.VAL_WORD ";
            SQLquery += "	   inner join BM10D_STAKEHOLDER fl with(nolock) ";
            SQLquery += "			   on w.VAL_WORD = fl.COD_STAKEHOLDER ";
            SQLquery += "      inner join subject s with(nolock) ";
            SQLquery += "			   on w.[COD_TEMA] = s.[clienteid] ";
            SQLquery += "			  and w.[COD_SUBTEMA] = s.[subjectid] ";
            SQLquery += " where sw.ID_STOP_WORD is null ";
            SQLquery += "   and CAST([COD_TEMA] as Varchar) + CAST([COD_SUBTEMA] as Varchar) in ('" + COD_TEMAS + "')";
            SQLquery += "   and left(COD_DAY,6) >= 201201";
            SQLquery += " group by w.VAL_WORD, fl.DES_STAKEHOLDER, s.name ";
            SQLquery += " order by SUM(w.VAL_COUNT) desc ";

            Stakeholders = new DataTable();
            Stakeholders = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "BMStakeholders");

            SQLquery = "select top 10 w.VAL_WORD Word ,SUM(w.VAL_COUNT) Num, fl.DES_SECTOR as des, s.name ";
            SQLquery += "  from [BM10F_BRANDMINING] w with(nolock) ";
            SQLquery += "	   left outer join BM10X_STOP_WORDS sw with(nolock) ";
            SQLquery += " 					on w.VAL_WORD = sw.VAL_WORD ";
            SQLquery += "	   inner join BM10D_SECTOR fl with(nolock) ";
            SQLquery += "			   on w.VAL_WORD = fl.COD_SECTOR ";
            SQLquery += "      inner join subject s with(nolock) ";
            SQLquery += "			   on w.[COD_TEMA] = s.[clienteid] ";
            SQLquery += "			  and w.[COD_SUBTEMA] = s.[subjectid] ";
            SQLquery += " where sw.ID_STOP_WORD is null ";
            SQLquery += "   and CAST([COD_TEMA] as Varchar) + CAST([COD_SUBTEMA] as Varchar) in ('" + COD_TEMAS + "')";
            SQLquery += "   and left(COD_DAY,6) >= 201201";
            SQLquery += " group by w.VAL_WORD, fl.DES_SECTOR, s.name ";
            SQLquery += " order by SUM(w.VAL_COUNT) desc ";

            Sector = new DataTable();
            Sector = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "BMSector");

            //SQLquery = "select top 10 w.VAL_WORD des,SUM(w.VAL_COUNT) num from [dbo].[BM10F_WORD_BY_CLIENT] w left outer join BM10X_STOP_WORDS sw on w.VAL_WORD = sw.VAL_WORD ";
            //SQLquery += " where sw.ID_STOP_WORD is null and w.ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " group by w.VAL_WORD order by SUM(w.VAL_COUNT) desc ";

            SQLquery = "select top 10 coalesce ([DES_CARACTERISTICA],[DES_CONCEITO],[DES_ENTIDADE],[DES_GEOGRAFIA],[DES_SECTOR],[DES_STAKEHOLDER],[DES_VALOR],w.VAL_WORD) des,SUM(w.VAL_COUNT) num, s.name ";
            SQLquery += "  from [dbo].[BM10F_BRANDMINING] w with(nolock)   ";
            SQLquery += "        left outer join BM10X_STOP_WORDS sw with(nolock)   ";
            SQLquery += "					 on w.VAL_WORD = sw.VAL_WORD   ";
            SQLquery += "	    left outer join BM10D_GEOGRAFIA go with(nolock)  ";
            SQLquery += "			         on w.VAL_WORD = go.COD_GEOGRAFIA  ";
            SQLquery += "	    left outer join BM10D_ENTIDADE en with(nolock)  ";
            SQLquery += "			         on w.VAL_WORD = en.COD_ENTIDADE  ";
            SQLquery += "	    left outer join BM10D_CARACTERISTICA ca with(nolock)  ";
            SQLquery += "			         on w.VAL_WORD = ca.COD_CARACTERISTICA  ";
            SQLquery += "	    left outer join BM10D_VALOR vl with(nolock)  ";
            SQLquery += "			         on w.VAL_WORD = vl.COD_VALOR  ";
            SQLquery += "	    left outer join BM10D_CONCEITO co with(nolock)  ";
            SQLquery += "			         on w.VAL_WORD = co.COD_CONCEITO  ";
            SQLquery += "	    left outer join BM10D_STAKEHOLDER st with(nolock)  ";
            SQLquery += "			         on w.VAL_WORD = st.COD_STAKEHOLDER  ";
            SQLquery += "	    left outer join BM10D_SECTOR sc with(nolock)  ";
            SQLquery += "			         on w.VAL_WORD = sc.COD_SECTOR  ";
            SQLquery += "      inner join subject s with(nolock) ";
            SQLquery += "			   on w.[COD_TEMA] = s.[clienteid] ";
            SQLquery += "			  and w.[COD_SUBTEMA] = s.[subjectid] ";
            SQLquery += " where sw.ID_STOP_WORD is null   ";
            SQLquery += "   and CAST([COD_TEMA] as Varchar) + CAST([COD_SUBTEMA] as Varchar) in ('" + COD_TEMAS + "')";
            SQLquery += "   and left(COD_DAY,6) >= 201201";
            SQLquery += "   and ([ID_CARACTERISTICA] is not null OR [ID_CONCEITO] is not null OR [ID_ENTIDADE] is not null or [ID_GEOGRAFIA] is not null or [ID_SECTOR]  is not null or [ID_STAKEHOLDER]  is not null or [ID_VALOR] is not null) ";
            SQLquery += " group by coalesce ([DES_CARACTERISTICA],[DES_CONCEITO],[DES_ENTIDADE],[DES_GEOGRAFIA],[DES_SECTOR],[DES_STAKEHOLDER],[DES_VALOR],w.VAL_WORD), s.name ";
            SQLquery += " order by SUM(w.VAL_COUNT) desc  ";

            top10 = new DataTable();
            top10 = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "BMTop10");


        }

        protected void Limpar_Click(object sender, EventArgs e)
        {

        }

        protected void Calcular_Click(object sender, EventArgs e)
        {

        }

        protected void Exportar_Click(object sender, EventArgs e)
        {
        }
    }
}