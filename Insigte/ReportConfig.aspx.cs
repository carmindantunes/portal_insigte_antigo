﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Insigte
{
    public partial class ReportConfig : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    txtLtsTemas.Attributes.Add("onclick", "txtTemaClick(this);");
                    //txtLtsTemas.Attributes.Add("onmouseover", "this.style='cursor:pointer;'");
                    //txtLtsTemas.Attributes.Add("onmouseout", "this.class='btnOver'");
                    string querychkconf = "";
                    querychkconf += " select ";

                    INInsigteManager.INManager inmanager = Global.InsigteManager[Session.SessionID];

                    string query = "";

                    query += " select CAST(COD_TEMA_ACCESS as varchar(4)) as COD_TEMA_ACCESS,DES_TAB ";
                    query += " from [dbo].[CL10H_CLIENT_SUBJECT] with(nolock) where ID_CLIENT_USER = " + inmanager.inUser.IdClientUser.ToString();
                    query += " and COD_TEMA_ACCESS <> (select COD_CLIENT_TEMA from CL10D_CLIENT with(nolock) where ID_CLIENT = " + inmanager.IdClient.ToString() + "  ) ";
                    query += "order by des_tab ";


                    DataTable dtTabs = inmanager.getTableQuery(query, "TabsForDropDown");

                    preencheDDTemas(dtTabs);
                }
                else
                {
                    lblDataToday.Visible = false;
                }
            }
            catch (Exception er)
            {

                er.Message.ToString();
            }
        }

        private void preencheDDTemas(DataTable tabs)
        {
            try
            {
                if (tabs == null)
                    return;

                ddTab.Items.Add(new System.Web.UI.WebControls.ListItem("selecione uma tab", "-1"));

                foreach (DataRow row in tabs.Rows)
                {
                    ddTab.Items.Add(new System.Web.UI.WebControls.ListItem(row["DES_TAB"].ToString(), row["COD_TEMA_ACCESS"].ToString()));
                }

                ddTab.SelectedValue = "-1";
            }
            catch (Exception er)
            {
                er.Message.ToString();
            }
        }

        protected void ddTab_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                if (ddTab.SelectedValue != "-1")
                {
                    plTemas.Visible = true;

                    INInsigteManager.INManager inmanager = Global.InsigteManager[Session.SessionID];

                    string query = "";

                    query += " select cast(clienteid as varchar) + cast(subjectid as varchar) as TEMA,name ";
                    query += " from subject with(nolock) where cast(clienteid as varchar)  = cast( " + ddTab.SelectedValue.ToString() + " as varchar(4)) order by name ";

                    DataTable dtTemas = inmanager.getTableQuery(query, "TemasDaTab");

                    if (dtTemas == null)
                        return;

                    if (dtTemas.Rows.Count > 0)
                    {
                        chkList.Items.Clear();

                        foreach (DataRow row in dtTemas.Rows)
                        {
                            System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem();
                            item.Value = row["tema"].ToString();
                            item.Text = row["name"].ToString();
                            item.Attributes.Add("onclick", "changeColor(this);escreveTxt(this);");
                            chkList.Items.Add(item);
                        }
                    }

                }
            }
            catch (Exception er)
            {

                er.Message.ToString();
            }
        }

        protected void btnPrev_Click(object sender, EventArgs e)
        {
            //if (ddTemas.SelectedValue == "-2")
            //{
            //    lblDataToday.Text = "necessário escolher tema";
            //    lblDataToday.Visible = true;
            //    return;
            //}

        }

        protected void btnTemas_Click(object sender, EventArgs e)
        {
            //if (txtLtsTemas.Text.Length == 0)
            //{
            //    System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AlertBox", "alert('Texto vazio');", true);
            //    return;
            //}

            //try
            //{
            //    string temas = txtLtsTemas.Text.TrimEnd(',');
            //    string codlg = Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt" ? "_en" : "";
            //    string inner = "";
            //    int i = 0;
            //    if (temas.Contains(','))
            //    {
            //        foreach (string str in txtLtsTemas.Text.TrimEnd(',').Split(','))
            //        {
            //            i++;
            //            inner += trataInnerHtml(str, codlg, i);
            //            inner += "<br />";
            //        }
            //    }
            //    else
            //    {
            //        i++;
            //        inner += trataInnerHtml(temas.TrimEnd(',').Split(',')[0], codlg, i);
            //    }
            //    divtemas.InnerHtml = inner;
            //}
            //catch (Exception er)
            //{
            //    er.Message.ToString();
            //}

        }
       
    }
}