﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Drawing;
using System.IO;
using System.Web.UI.DataVisualization.Charting;

namespace Insigte
{
    public partial class NewsMap : BasePage
    {
        String[] cores = new String[] { "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56", "#00add9", "#515151", "#3e93b2", "#808080", "#002d56" };

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //lblDataToday.Visible = true;
                //lblDataToday.Text = "OMFG TENS DE ESTAR AQUI FDP";
                //getNoticiasRegion();
                contaNotReg();
                carregadivChar1();
            }
        }

        protected void carregadivChar1()
        {
            preencheChart("Cabo Delgado", caCaboDelgado,0);
            preencheChart("Tete", caTete,0);
            preencheChart("Maputo", caMaputo,0);
            preencheChart("Manica", caManica,0);
            preencheChart("Inhambane", caInhambane,0);
            preencheChart("Gaza", caGaza,0);
            preencheChart("Sofala", caSofala,0);
            preencheChart("Zambezia", caZambezia,0);
            preencheChart("Nampula", caNampula,0);
            preencheChart("Niassa", caNiassa,0);

            preencheChart("Cabo Delgado", cabCaboDelgado, 1);
            preencheChart("Tete", cabTete, 1);
            preencheChart("Maputo", cabMaputo, 1);
            preencheChart("Manica", cabManica, 1);
            preencheChart("Inhambane", cabInhambane, 1);
            preencheChart("Gaza", cabGaza, 1);
            preencheChart("Sofala", cabSofala, 1);
            preencheChart("Zambezia", cabZambezia, 1);
            preencheChart("Nampula", cabNampula, 1);
            preencheChart("Niassa", cabNiassa, 1);

            preencheChartc("Cabo Delgado", cacCaboDelgado, 2);
            preencheChartc("Tete", cacTete, 2);
            preencheChartc("Maputo", cacMaputo, 2);
            preencheChartc("Manica", cacManica, 2);
            preencheChartc("Inhambane", cacInhambane, 2);
            preencheChartc("Gaza", cacGaza, 2);
            preencheChartc("Sofala", cacSofala, 2);
            preencheChartc("Zambezia", cacZambezia, 2);
            preencheChartc("Nampula", cacNampula, 2);
            preencheChartc("Niassa", cacNiassa, 2);

        }

        private void preencheChartc(String provNome, Chart _chartNome, int idchart)
        {
            string nome = "";

            if (provNome == "Cabo Delgado")
                nome = "CaboDelgado";
            else
                nome = provNome;

            string _chartSeries = "sr" + nome;
            string _chartAreas = "ca" + nome;
            string _chartTittle = "tt" + nome;

            

            string cmdQuery = "SELECT COUNT(clientarticles.id) AS num, clientarticles.subjectid AS ID, subject.name AS NAME,clientarticles.clientid as CLID, clientarticles.subjectid as SBID ";
            cmdQuery += "FROM clientarticles with(nolock), subject with(nolock), metadata with(nolock) ";
            cmdQuery += "WHERE iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.id ";
            cmdQuery += "AND clientarticles.clientid = 8001 ";
            
            cmdQuery += "AND clientarticles.clientid = subject.clienteid ";
            cmdQuery += "AND clientarticles.subjectid = subject.subjectid ";
            cmdQuery += "AND metadata.date >= getdate()-180 ";
            cmdQuery += "GROUP BY  clientarticles.subjectid, subject.name,clientarticles.clientid, clientarticles.subjectid ";
            cmdQuery += "ORDER BY num DESC, id ";

            lblDataToday.Text = cmdQuery;
            DataTable GrConc = Global.InsigteManager[Session.SessionID].getTableQuery(cmdQuery, "GrConc");

            Int32[] yValues = new Int32[GrConc.Rows.Count];
            string[] xValues = new string[GrConc.Rows.Count];

            int counter = 0;
            foreach (DataRow row in GrConc.Rows)
            {
                yValues[counter] = Convert.ToInt32(row[0].ToString());
                xValues[counter] = row[2].ToString();
                counter++;
            }

            _chartNome.Series[_chartSeries].Points.DataBindXY(xValues, yValues);

            counter = 0;
            foreach (DataRow row in GrConc.Rows)
            {
                _chartNome.Series[_chartSeries].Points[counter].Color = ColorTranslator.FromHtml("#d5d5d5");
                counter++;
            }
            
           _chartNome.Titles[_chartTittle].Text = "Total Notícias | Últimos 180 Dias | Moçambique";
           _chartNome.Titles[_chartTittle].Alignment = ContentAlignment.TopLeft;
            // Set series chart type
            _chartNome.Series[_chartSeries].ChartType = SeriesChartType.Column;
            // Set series point width
            _chartNome.Series[_chartSeries]["PointWidth"] = "0.5";
            // Show data points labels
            _chartNome.Series[_chartSeries].IsValueShownAsLabel = true;
            // Set data points label style
            _chartNome.Series[_chartSeries]["BarLabelStyle"] = "Center";
            // Show as 3D
            //Chart1.ChartAreas[_chartSeries].Area3DStyle.Enable3D = false;
            // Draw as 3D Cylinder
            _chartNome.Series[_chartSeries]["DrawingStyle"] = "Default";
            _chartNome.Series[_chartSeries].ToolTip = "#VALX [#VALY]";
            _chartNome.Series[_chartSeries].IsXValueIndexed = false;
            _chartNome.Series[_chartSeries].IsVisibleInLegend = true;
            _chartNome.ChartAreas[_chartAreas].AxisX.LabelStyle.Enabled = true;
            _chartNome.ChartAreas[_chartAreas].AxisX.Interval = 1;           
            _chartNome.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            _chartNome.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;

            counter = 0;
            foreach (DataRow row in GrConc.Rows)
            {
                _chartNome.Series[_chartSeries].Points[counter].Url = "~/Subject.aspx?clid=" + row["CLID"].ToString() + "&tema=" + row["SBID"].ToString() + "&fon=0&dd=180";
                counter++;
            }
        }

        private void preencheChart(String provNome, Chart _chartNome,int idchart)
        {
            string nome ="";

            if (provNome == "Cabo Delgado")
                nome = "CaboDelgado";
            else
                nome = provNome;

            string _chartSeries = "sr" + nome;
            string _chartAreas = "ca" + nome;
            string _chartTittle = "tt" + nome;

            string query = "";
            if (idchart == 0)//chart a
            {
                query += " select '" + nome + "' as Prov, DES_YEAR_MONTH_ABREV as mes_ano,[COD_MONTH],DES_YEAR_MONTH_ABREV_PT ,count(1) as total ";
                query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-180) inner join clientarticles ca with(nolock) on m.id= ca.id ";
                query += " inner join time_month tm with(nolock) on convert(varchar, m.date,112) = tm.COD_DAY ";
                query += " where CONTAINS (text, '\"" + nome + "\"')  ";
                query += " and clientid = 8001 and subjectid = 101 ";
                query += " group by DES_YEAR_MONTH_ABREV ,[COD_MONTH],DES_YEAR_MONTH_ABREV_PT ";
                query += " order by [COD_MONTH] ";
            }
            if (idchart == 1)//para o chart b
            {
                query += " select '" + nome + "' as Prov, s.name as mes_ano ,count(1) as total ";
                query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-180) inner join clientarticles ca on m.id= ca.id ";
                query += " inner join subject s with(nolock) on s.clienteid = ca.clientid and s.subjectid = ca.subjectid ";
                query += " inner join time_month tm with(nolock) on convert(varchar, m.date,112) = tm.COD_DAY ";
                query += " where CONTAINS (text, '\"" + nome + "\"')  ";
                query += " and ca.clientid = 8001 ";
                query += " group by s.name ";
            }
            

            DataTable _datatable = Global.InsigteManager[Session.SessionID].getTableQuery(query, "getNews6meses");


            if (_datatable.Rows.Count > 0)
            {
                if (idchart == 0)
                {
                    _chartNome.Titles[_chartTittle].Text = "Noticias 180 dias | " + Global.InsigteManager[Session.SessionID].NomCliente.ToString() ;

                }
                if (idchart == 1)
                {
                    _chartNome.Titles[_chartTittle].Text = "Noticias 180 dias | Todos" ;
                }

                Int32[] yValues = new Int32[_datatable.Rows.Count];
                string[] xValues = new string[_datatable.Rows.Count];

                int counter = 0;
                foreach (DataRow r in _datatable.Rows)
                {
                    yValues[counter] = Convert.ToInt32(r["total"].ToString());
                    xValues[counter] = r["mes_ano"].ToString();

                    counter++;
                }
                _chartNome.Series[_chartSeries].Points.DataBindXY(xValues, yValues);

                counter = 0;
                foreach (DataRow r in _datatable.Rows)
                {
                    if (idchart == 0)
                    {
                        _chartNome.Series[_chartSeries].Points[counter].Color = ColorTranslator.FromHtml("#00add9");
                    }
                    else
                    {//Convert.ToInt32(r["total"].ToString())
                        _chartNome.Series[_chartSeries].Points[counter].Color = ColorTranslator.FromHtml("#323232");
                       
 
                    }
                    counter++;
                }

                if (idchart == 0)
                {
                    _chartNome.Series[_chartSeries].ChartType = SeriesChartType.Column;
                }else
                {
                    _chartNome.Series[_chartSeries].ChartType = SeriesChartType.Column;
                }
                _chartNome.Titles[_chartTittle].Alignment = ContentAlignment.TopLeft;
                _chartNome.Series[_chartSeries]["PointWidth"] = "0.5";
                _chartNome.Series[_chartSeries].IsValueShownAsLabel = true;
                _chartNome.Series[_chartSeries]["BarLabelStyle"] = "Center";
                _chartNome.Series[_chartSeries]["PieLabelStyle"] = "Outside";
                _chartNome.Series[_chartSeries]["DrawingStyle"] = "Default";
                _chartNome.Series[_chartSeries].ToolTip = "#VALY [#VALX]";
                _chartNome.Series[_chartSeries].IsXValueIndexed = false;
                _chartNome.Series[_chartSeries].IsVisibleInLegend = true;
                _chartNome.ChartAreas[_chartAreas].AxisX.LabelStyle.Enabled = true;
                _chartNome.ChartAreas[_chartAreas].AxisX.Interval = 1;
                _chartNome.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                _chartNome.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;

                counter = 0;
                foreach (DataRow row in _datatable.Rows)
                {
                    _chartNome.Series[_chartSeries].Points[counter].Url = "~/Subject.aspx?clid=";// +row["CLID"].ToString() + "&tema=" + row["SBID"].ToString() + "&fon=0&dd=90";
                    counter++;
                }
            }

        }

        protected String getTrataFill()
        {
            string QueryTotal = "";
            double Total = 0;
            string Query = " select 'Cabo Delgado' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Cabo Delgado\"')";
            Query += " union all ";
            Query += " select 'Tete' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Tete\"') ";
            Query += " union all ";
            Query += " select 'Maputo' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Maputo\"') ";
            Query += " union all ";
            Query += " select 'Manica' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Manica\"') ";
            Query += " union all ";
            Query += " select 'Inhambane' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m  with(nolock)on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Inhambane\"') ";
            Query += " union all ";
            Query += " select 'Gaza' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Gaza\"') ";
            Query += " union all ";
            Query += " select 'Sofala' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Sofala\"') ";
            Query += " union all ";
            Query += " select 'Zambezia' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Zambezia\"') ";
            Query += " union all ";
            Query += " select 'Nampula' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Nampula\"') ";
            Query += " union all ";
            Query += " select 'Niassa' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Niassa\"') ";

            DataTable news = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "getNotRegion");

            QueryTotal += " select 'Total' as Prov, count(1) as numero from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            QueryTotal += " where CONTAINS (text, '\"Cabo Delgado\"') ";
            QueryTotal += " or CONTAINS (text, '\"Tete\"') ";
            QueryTotal += " or CONTAINS (text, '\"Maputo\"') ";
            QueryTotal += " or CONTAINS (text, '\"Manica\"') ";
            QueryTotal += " or CONTAINS (text, '\"inhambane\"') ";
            QueryTotal += " or CONTAINS (text, '\"Gaza\"') ";
            QueryTotal += " or CONTAINS (text, '\"Sofala\"') ";
            QueryTotal += " or CONTAINS (text, '\"Zambezia\"') ";
            QueryTotal += " or CONTAINS (text, '\"Nampula\"') ";
            QueryTotal += " or CONTAINS (text, '\"Niassa\"') ";

            DataTable newstotal = Global.InsigteManager[Session.SessionID].getTableQuery(QueryTotal, "getNotRegionTotal");

            foreach (DataRow x in newstotal.Rows)
            {
                Total = Convert.ToDouble(x["numero"].ToString());
            }

            String Esreturn = "";
            double Opacity = 0;

            foreach (DataRow y in news.Rows)
            {
                switch (y["Prov"].ToString())
                {
                    case "Cabo Delgado":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            Esreturn += " CaboDelgado.setAttribute(\"fill\", \"#323232\");  ";
                            Esreturn += " CaboDelgado.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",",".") + "\"); ";
                           
                            break;
                        }
                    case "Tete":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            Esreturn += " Tete.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",",".") + "\");  ";
                            Esreturn += " Tete.setAttribute(\"fill\", \"#323232\");  ";
                            break;
                        }

                    case "Maputo":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            Esreturn += " Maputo.setAttribute(\"fill\", \"#323232\");  ";
                            Esreturn += " Maputo.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",",".") + "\");  ";
                            break;
                        }
                    case "Manica":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            Esreturn += " Manica.setAttribute(\"fill\", \"#323232\");  ";
                            Esreturn += " Manica.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",",".") + "\");  ";
                            break;
                        }
                    case "Inhambane":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            Esreturn += " Inhambane.setAttribute(\"fill\", \"#323232\");  ";
                            Esreturn += " Inhambane.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",",".") + "\");  ";
                            break;
                        }
                    case "Gaza":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            Esreturn += " Gaza.setAttribute(\"fill\", \"#323232\");  ";
                            Esreturn += " Gaza.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",",".") + "\");  ";
                            break;
                        }
                    case "Sofala":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            Esreturn += " Sofala.setAttribute(\"fill\", \"#323232\");  ";
                            Esreturn += " Sofala.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",",".") + "\");  ";                            
                            break;
                        }
                    case "Zambezia":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            Esreturn += " Zambezia.setAttribute(\"fill\", \"#323232\");  ";
                            Esreturn += " Zambezia.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",",".") + "\");  ";                         
                            break;
                        }
                    case "Nampula":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            Esreturn += " Nampula.setAttribute(\"fill\", \"#323232\");  ";
                            Esreturn += " Nampula.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",",".") + "\");  ";
                            break;
                        }
                    case "Niassa":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            Esreturn += " Niassa.setAttribute(\"fill\", \"#323232\");  ";
                            Esreturn += " Niassa.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",",".") + "\");  ";                     
                            break;
                        }
                }
            }

            Esreturn += " clicked.setAttribute(\"fill\", \"#00add9\"); ";
            Esreturn += " clicked.setAttribute(\"fill-opacity\", \"1\"); ";

            return Esreturn;
        }

        protected String getRegions()
        {
            string QueryTotal = "";
            double Total = 0;

            string Query = " select 'Cabo Delgado' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Cabo Delgado\"') ";
            Query += " union all ";
            Query += " select 'Tete' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Tete\"') ";
            Query += " union all ";
            Query += " select 'Maputo' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Maputo\"') ";
            Query += " union all ";
            Query += " select 'Manica' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Manica\"') ";
            Query += " union all ";
            Query += " select 'Inhambane' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"inhambane\"') ";
            Query += " union all ";
            Query += " select 'Gaza' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Gaza\"') ";
            Query += " union all ";
            Query += " select 'Sofala' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Sofala\"') ";
            Query += " union all ";
            Query += " select 'Zambezia' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Zambezia\"') ";
            Query += " union all ";
            Query += " select 'Nampula' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Nampula\"') ";
            Query += " union all ";
            Query += " select 'Niassa' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Niassa\"') ";

            DataTable news = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "getNotRegion");

            QueryTotal += " select 'Total' as Prov, count(1) as numero from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            QueryTotal += " where CONTAINS (text, '\"Cabo Delgado\"') ";
            QueryTotal += " or CONTAINS (text, '\"Tete\"') ";
            QueryTotal += " or CONTAINS (text, '\"Maputo\"') ";
            QueryTotal += " or CONTAINS (text, '\"Manica\"') ";
            QueryTotal += " or CONTAINS (text, '\"inhambane\"') ";
            QueryTotal += " or CONTAINS (text, '\"Gaza\"') ";
            QueryTotal += " or CONTAINS (text, '\"Sofala\"') ";
            QueryTotal += " or CONTAINS (text, '\"Zambezia\"') ";
            QueryTotal += " or CONTAINS (text, '\"Nampula\"') ";
            QueryTotal += " or CONTAINS (text, '\"Niassa\"') ";

            DataTable newstotal = Global.InsigteManager[Session.SessionID].getTableQuery(QueryTotal, "getNotRegionTotal");

            foreach (DataRow x in newstotal.Rows)
            {
                Total = Convert.ToDouble( x["numero"].ToString());
            }

            string paths = "";
            double Opacity = 0;

            

            foreach (DataRow y in news.Rows)
            {
                switch (y["Prov"].ToString())
                {
                    case "Cabo Delgado":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            paths += " <path id=\"CaboDelgado\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",",".").Replace(",",".") + "\" onclick=\"MouseClick(id)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(id); \" onmouseout=\"mouseOut(id);\" ";
                            paths += " d=\"M 135,66L 140,66L 141,65L 144,65L 144,64L 145,64L 145,63L 147,63L 147,62L 149,62L 149,61L 151,61L 152,60L 153,60L 154,59L 155,58L 160,58L 160,57L 161,57L 162,57L 162,56L 163,56L 163,55L 164,55L 164,54L 167,54L 167,55L 168,55L 168,56L 169,56L 170,56L 171,57L 171,53L 172,52L 172,48L 171,48L 171,45L 172,45L 172,44L 173,43L 173,41L 173,37L 172,37L 172,36L 171,36L 171,34L 172,34L 172,33L 171,33L 171,32L 170,31L 170,24L 171,24L 171,18L 171,13L 172,13L 172,12L 173,11L 174,10L 173,10L 173,7.00003L 174,7.00003L 174,5.00003L 171,5.00003L 171,6.00003L 170,6.00003L 170,7.00003L 169,7.00003L 169,8.00003L 168,8.00003L 168,9.00003L 167,9.00003L 167,10L 166,10L 166,11L 162,11L 162,12L 161,12L 161,13L 157,13L 157,14L 156,14L 156,15L 154,15L 154,16L 153,16L 153,17L 146,17L 145,18L 142,18L 142,19L 140,19L 138,20L 138,21L 137,22L 137,24L 138,28L 137,29L 137,31L 136,32L 135,33L 133,33L 132,34L 131,34L 131,35L 130,35L 130,37L 129,38L 129,42L 128,43L 128,44L 127,44L 127,45L 126,45L 126,47L 125,48L 125,50L 126,50L 126,49L 127,49L 128,48L 129,47L 132,47L 133,48L 134,48L 134,50L 133,51L 133,52L 132,52L 131,53L 129,53L 129,54L 131,54L 131,55L 132,55L 132,56L 132,59L 133,59L 133,61L 134,61L 134,64L 135,64L 135,66 Z \"/> ";
                            break;
                        }
                    case "Tete":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            paths += " <path id=\"Tete\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",",".") + "\" onclick=\"MouseClick(id)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(id); \" onmouseout=\"mouseOut(id);\" ";
                            paths += " d=\"M 2,78L 30,68L 38,67L 47,63L 50,63L 56,70L 65,70C 65,70 70,69 69,69C 68,69 70,71 70,72C 70,73 70,74 70,74L 72,76L 72,78L 72,83L 68,85L 68,89L 66,90L 66,94L 68,97L 69,101L 78,111L 78,115L 81,116L 81,123L 76,116L 73,111L 67,108L 65,107L 58,102L 55,102L 53,104L 52,107L 50,109L 48,112L 46,112L 45,111L 45,110L 46,107L 46,106L 45,105L 42,104C 42,104 41,103 40,103C 39,103 37,102 37,102L 34,101L 30,100L 28,98L 23,95L 19,94C 19,94 18,94 17,94C 16,94 13,94 13,94L 10,94L 8,93L 7,92C 7,92 6,90 6,89C 6,88 6,85 6,85L 4,83L 2,81L 2,78 Z \"/> ";  
                            break;
                        }

                    case "Maputo":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            paths += " <path id=\"Maputo\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",",".") + "\" onclick=\"MouseClick(id)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(id); \" onmouseout=\"mouseOut(id);\" ";
                            paths += " d=\"M 28,243L 29,243L 30,244L 31,244L 31,245L 31,248L 32,248L 32,250L 33,250L 34,250L 36,250L 36,249L 37,249L 37,248L 38,248L 38,245L 40,245L 40,247L 41,247L 41,248L 42,248L 42,249L 44,249L 44,250L 43,250L 43,251L 42,251L 42,252L 41,252L 41,253L 40,253L 40,254L 39,254L 39,255L 36,255L 36,256L 36,257L 35,257L 35,258L 34,258L 35,265L 36,266L 36,267L 38,267L 38,266L 39,266L 39,265L 40,265L 40,267L 39,267L 39,268L 38,268L 38,269L 38,274L 29,274L 29,270L 28,270L 28,260L 27,260L 27,252L 28,252L 28,243 Z \"/> ";
                            break;
                        }
                    case "Gaza":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            paths += " <path id=\"Gaza\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\" onclick=\"MouseClick(id)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(id); \" onmouseout=\"mouseOut(id);\" ";
                            paths += " d=\"M 19,202L 24,197L 26,195L 29,192L 30,191L 31,190L 32,190L 32,189L 33,189L 33,188L 34,188L 34,187L 36,185L 37,184L 41,188L 44,188L 45,187L 46,187L 46,188L 45,189L 45,194L 46,194L 48,195L 48,201L 47,201L 47,206L 49,208L 52,211L 52,212L 53,212L 53,213L 54,213L 54,214L 55,214L 55,215L 56,215L 56,232L 56,233L 57,234L 57,237L 58,237L 58,238L 60,238L 60,237L 61,237L 61,239L 60,239L 60,243L 61,243L 61,244L 59,244L 59,245L 57,245L 57,246L 55,246L 55,247L 51,247L 51,248L 48,248L 48,249L 45,249L 41,244L 37,244L 37,247L 36,247L 36,248L 35,248L 35,249L 34,249L 34,248L 33,248L 33,245L 32,243L 30,243L 30,242L 29,242L 29,241L 28,241L 28,233L 27,232L 27,227L 25,225L 25,222L 24,221L 24,220L 23,219L 22,219L 22,216L 23,216L 23,213L 22,213L 22,209L 21,208L 20,208L 20,205L 19,205L 19,202 Z \"/> ";
                            break;
                        }
                    case "Inhambane":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            paths += " <path id=\"Inhambane\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",",".") + "\" onclick=\"MouseClick(id)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(id); \" onmouseout=\"mouseOut(id);\" ";
                            paths += " d=\"M 48,205L 57,215L 57,232L 59,234L 59,236L 62,236L 62,240L 61,240L 61,242L 62,242L 62,243L 65,243L 65,242L 70,242L 72,240L 73,240L 76,237L 78,233L 78,232L 79,231L 80,231L 80,228L 77,228L 77,224L 78,223L 79,223L 79,221L 80,220L 80,216L 81,215L 82,214L 82,212L 81,212L 81,208L 82,207L 82,204L 81,200L 79,200L 79,197L 78,196L 77,192L 77,186L 76,184L 75,181L 68,181L 65,184L 65,186L 63,186L 63,185L 52,185L 51,186L 49,186L 49,187L 47,187L 47,189L 46,189L 46,193L 47,193L 48,194L 49,195L 49,202L 48,202L 48,205 Z \"/> ";
                            break;
                        }
                    case "Manica":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            paths += " <path id=\"Manica\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\" onclick=\"MouseClick(id)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(id); \" onmouseout=\"mouseOut(id);\" ";
                            paths += " d=\"M 47,117L 48,117L 48,119L 49,119L 50,119L 50,121L 54,121L 54,120L 56,120L 56,119L 58,119L 58,118L 61,118L 61,119L 63,119L 64,119L 64,122L 63,122L 63,125L 64,126L 65,126L 65,127L 66,127L 66,128L 65,128L 65,129L 64,129L 64,132L 63,132L 63,131L 60,131L 60,132L 59,132L 58,133L 58,135L 59,135L 59,137L 58,137L 58,138L 57,138L 57,139L 56,139L 56,140L 56,142L 57,142L 57,143L 58,143L 59,144L 59,153L 58,153L 57,154L 57,155L 56,155L 56,156L 57,156L 57,157L 57,158L 58,159L 57,160L 56,160L 56,161L 54,161L 54,162L 51,162L 51,164L 52,164L 52,166L 51,166L 51,169L 52,169L 55,171L 55,173L 56,173L 56,174L 57,174L 57,177L 58,177L 58,178L 59,178L 59,179L 60,179L 60,182L 59,182L 59,183L 56,183L 56,184L 50,184L 50,185L 49,185L 46,186L 44,186L 44,187L 42,187L 42,186L 41,186L 41,185L 40,185L 40,184L 39,184L 38,183L 37,183L 37,181L 38,181L 38,174L 39,174L 39,173L 40,173L 42,171L 43,171L 43,167L 43,166L 44,166L 44,165L 45,165L 45,164L 46,164L 46,163L 47,163L 47,162L 48,162L 48,159L 47,159L 47,158L 44,155L 43,155L 43,152L 44,152L 44,144L 45,144L 45,138L 47,138L 48,137L 48,132L 47,132L 47,131L 46,130L 46,123L 47,123L 47,117 Z \" /> ";
                            break;
                        }
                    case "Sofala":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            paths += " <path id=\"Sofala\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",",".") + "\" onclick=\"MouseClick(id)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(id); \" onmouseout=\"mouseOut(id);\" ";
                            paths += " d=\"M 47,113L 48,112L 55,103L 58,103L 61,105L 65,107L 69,110L 74,113L 76,118L 80,123L 83,127L 85,130L 87,131L 89,133L 92,136L 94,139L 96,142L 95,144L 94,144C 93,144 91,146 91,146L 88,148L 77,159L 72,160L 72,164L 71,170L 73,173L 75,174L 75,179L 68,180L 67,180L 64,183L 61,183L 61,178L 58,173L 55,168L 53,168L 53,163L 58,162L 59,161L 59,157L 58,156L 60,153L 61,152L 61,143L 58,140L 60,138L 61,136L 60,135L 60,133L 65,133L 66,132L 66,129L 67,128L 67,126L 66,124L 66,123L 65,119L 64,118L 63,117L 62,117L 61,117L 60,116L 59,116L 58,116L 57,117L 56,117L 55,118L 54,118L 53,118L 52,118L 51,117L 50,116L 49,115L 48,115L 47,113 Z \"/> ";
                            break;
                        }
                    case "Zambezia":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            paths += " <path id=\"Zambezia\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",",".") + "\" onclick=\"MouseClick(id)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(id); \" onmouseout=\"mouseOut(id);\" ";
                            paths += " d=\"M 82,121L 82,125L 83,125L 83,126L 84,126L 84,127L 84,128L 85,128L 85,129L 87,129L 87,130L 88,130L 88,131L 89,131L 90,131L 90,132L 91,132L 91,133L 94,137L 95,139L 96,139L 96,140L 97,140L 97,142L 99,142L 99,140L 104,136L 105,135L 105,131L 107,130L 111,130L 111,128L 116,124L 118,124L 119,123L 125,120L 128,120L 128,119L 131,119L 131,118L 135,118L 135,117L 138,117L 138,116L 141,116L 141,115L 142,115L 144,114L 144,112L 142,110L 142,101L 141,100L 141,95L 140,95L 139,94L 138,94L 138,93L 136,93L 136,92L 135,92L 135,91L 132,89L 132,88L 130,88L 130,87L 128,87L 128,86L 127,86L 126,85L 124,85L 124,84L 120,84L 120,83L 119,82L 118,82L 117,81L 117,80L 107,80L 107,81L 106,81L 100,87L 99,88L 95,88L 94,87L 93,88L 93,95L 92,97L 90,99L 87,99L 87,100L 84,100L 84,105L 83,105L 83,107L 84,107L 84,116L 83,116L 83,117L 82,117L 82,121 Z \"/> ";
                            break;
                        }
                    case "Nampula":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            paths += " <path id=\"Nampula\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",",".") + "\" onclick=\"MouseClick(id)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(id); \" onmouseout=\"mouseOut(id);\" ";
                            paths += " d=\"M 110,77L 109,78L 110,78L 111,78L 111,79L 117,79L 120,81L 121,82L 121,83L 125,83L 125,84L 128,84L 128,85L 130,86L 133,87L 134,88L 135,89L 136,90L 137,91L 137,92L 139,92L 140,93L 141,94L 142,94L 143,95L 143,99L 143,102L 143,109L 144,110L 144,111L 145,111L 145,112L 147,112L 147,111L 148,111L 149,110L 150,110L 151,109L 152,109L 153,108L 154,108L 155,107L 156,106L 159,103L 159,101L 160,100L 160,98L 162,96L 163,96L 167,92L 167,91L 168,91L 168,90L 169,90L 169,89L 170,89L 170,87L 168,87L 168,85L 169,85L 171,83L 171,82L 172,82L 172,81L 173,81L 173,80L 174,80L 174,75L 172,75L 172,71L 173,71L 173,69L 171,69L 171,68L 170,68L 170,65L 171,64L 171,58L 170,58L 169,57L 168,57L 167,56L 166,56L 166,55L 165,55L 165,56L 164,56L 164,57L 163,57L 163,58L 161,58L 161,59L 160,59L 156,59L 155,60L 154,60L 154,61L 153,61L 153,62L 152,62L 151,62L 150,63L 148,63L 148,64L 146,64L 146,66L 144,66L 142,66L 142,67L 133,67L 133,68L 131,68L 131,69L 127,69L 125,69L 124,69L 123,69L 123,70L 121,70L 121,71L 119,71L 119,72L 118,72L 118,73L 116,73L 116,74L 112,74L 112,75L 111,75L 111,76L 110,76L 110,77 Z \"/> ";
                            break;
                        }
                    case "Niassa":
                        {
                            Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total);
                            paths += " <path id=\"Niassa\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",",".") + "\" onclick=\"MouseClick(id)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(id); \" onmouseout=\"mouseOut(id);\" ";
                            paths += " d=\"M 94,86L 96,86L 96,87L 98,87L 99,86L 99,85L 100,85L 102,83L 104,81L 112,73L 115,73L 115,72L 117,72L 117,71L 118,71L 118,70L 119,70L 120,69L 122,69L 122,68L 124,68L 124,67L 125,67L 125,68L 126,68L 130,68L 131,67L 132,66L 133,66L 133,64L 132,63L 132,61L 131,60L 131,58L 131,57L 130,57L 130,56L 130,55L 128,55L 128,52L 129,52L 130,52L 131,51L 132,51L 132,50L 133,50L 133,49L 132,49L 131,48L 130,48L 130,49L 129,49L 128,50L 127,50L 127,51L 124,51L 124,46L 125,45L 125,44L 126,44L 127,42L 128,41L 128,37L 129,37L 129,34L 130,34L 130,33L 131,33L 132,32L 134,32L 135,31L 136,30L 136,28L 136,27L 136,25L 136,21L 137,20L 137,19L 134,19L 134,18L 130,18L 130,20L 129,20L 128,21L 126,23L 125,23L 124,24L 118,24L 117,23L 112,23L 112,24L 109,24L 108,25L 104,25L 102,23L 100,22L 98,21L 96,20L 95,19L 94,19L 94,20L 93,21L 88,21L 87,22L 80,22L 79,23L 79,27L 76,30L 76,33L 75,33L 75,36L 76,36L 77,37L 77,40L 77,51L 82,56L 88,62L 88,65L 95,74L 96,75L 96,79L 95,80L 94,81L 94,86 Z \"/> ";
                            break;
                        }
                }
            }
            return paths;

        }

        protected void contaNotReg()
        {
            string Query = " select 'Cabo Delgado' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Cabo Delgado\"') ";
            Query += " union all ";
            Query += " select 'Tete' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Tete\"') ";
            Query += " union all ";
            Query += " select 'Maputo' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Maputo\"') ";
            Query += " union all ";
            Query += " select 'Manica' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Manica\"') ";
            Query += " union all ";
            Query += " select 'Inhambane' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"inhambane\"') ";
            Query += " union all ";
            Query += " select 'Gaza' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Gaza\"') ";
            Query += " union all ";
            Query += " select 'Sofala' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Sofala\"') ";
            Query += " union all ";
            Query += " select 'Zambezia' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Zambezia\"') ";
            Query += " union all ";
            Query += " select 'Nampula' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Nampula\"') ";
            Query += " union all ";
            Query += " select 'Niassa' as Prov, count(1) as total ";
            Query += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) ";
            Query += " where CONTAINS (text, '\"Niassa\"') ";

            string QueryCli = " select 'Cabo Delgado' as Prov, count(1) as total ";
            QueryCli += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) inner join clientarticles ca on m.id= ca.id ";
            QueryCli += " where CONTAINS (text, '\"Cabo Delgado\"') and cast(clientid as varchar) + cast(subjectid as varchar) = '"+ Global.InsigteManager[Session.SessionID].TemaCliente +"'";
            QueryCli += " union all ";
            QueryCli += " select 'Tete' as Prov, count(1) as total ";
            QueryCli += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) inner join clientarticles ca on m.id= ca.id ";
            QueryCli += " where CONTAINS (text, '\"Tete\"') and cast(clientid as varchar) + cast(subjectid as varchar) = '"+ Global.InsigteManager[Session.SessionID].TemaCliente +"'";
            QueryCli += " union all ";
            QueryCli += " select 'Maputo' as Prov, count(1) as total ";
            QueryCli += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) inner join clientarticles ca on m.id= ca.id ";
            QueryCli += " where CONTAINS (text, '\"Maputo\"') and cast(clientid as varchar) + cast(subjectid as varchar) = '"+ Global.InsigteManager[Session.SessionID].TemaCliente +"'";
            QueryCli += " union all ";
            QueryCli += " select 'Manica' as Prov, count(1) as total ";
            QueryCli += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) inner join clientarticles ca on m.id= ca.id ";
            QueryCli += " where CONTAINS (text, '\"Manica\"') and cast(clientid as varchar) + cast(subjectid as varchar) = '"+ Global.InsigteManager[Session.SessionID].TemaCliente +"'";
            QueryCli += " union all ";
            QueryCli += " select 'Inhambane' as Prov, count(1) as total ";
            QueryCli += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) inner join clientarticles ca on m.id= ca.id ";
            QueryCli += " where CONTAINS (text, '\"inhambane\"') and cast(clientid as varchar) + cast(subjectid as varchar) = '"+ Global.InsigteManager[Session.SessionID].TemaCliente +"'";
            QueryCli += " union all ";
            QueryCli += " select 'Gaza' as Prov, count(1) as total ";
            QueryCli += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) inner join clientarticles ca on m.id= ca.id ";
            QueryCli += " where CONTAINS (text, '\"Gaza\"') and cast(clientid as varchar) + cast(subjectid as varchar) = '"+ Global.InsigteManager[Session.SessionID].TemaCliente +"'";
            QueryCli += " union all ";
            QueryCli += " select 'Sofala' as Prov, count(1) as total ";
            QueryCli += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) inner join clientarticles ca on m.id= ca.id ";
            QueryCli += " where CONTAINS (text, '\"Sofala\"') and cast(clientid as varchar) + cast(subjectid as varchar) = '"+ Global.InsigteManager[Session.SessionID].TemaCliente +"'";
            QueryCli += " union all ";
            QueryCli += " select 'Zambezia' as Prov, count(1) as total ";
            QueryCli += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) inner join clientarticles ca on m.id= ca.id ";
            QueryCli += " where CONTAINS (text, '\"Zambezia\"') and cast(clientid as varchar) + cast(subjectid as varchar) = '"+ Global.InsigteManager[Session.SessionID].TemaCliente +"'";
            QueryCli += " union all ";
            QueryCli += " select 'Nampula' as Prov, count(1) as total ";
            QueryCli += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) inner join clientarticles ca on m.id= ca.id ";
            QueryCli += " where CONTAINS (text, '\"Nampula\"') and cast(clientid as varchar) + cast(subjectid as varchar) = '"+ Global.InsigteManager[Session.SessionID].TemaCliente +"'";
            QueryCli += " union all ";
            QueryCli += " select 'Niassa' as Prov, count(1) as total ";
            QueryCli += " from contents c with(nolock) inner join metadata m with(nolock) on c.id = m.id and m.date > (getdate()-90) inner join clientarticles ca on m.id= ca.id ";
            QueryCli += " where CONTAINS (text, '\"Niassa\"') and cast(clientid as varchar) + cast(subjectid as varchar) = '" + Global.InsigteManager[Session.SessionID].TemaCliente + "'";

 

            try
            {
                DataTable news = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "getNotRegion");

                DataTable newsCliReg = Global.InsigteManager[Session.SessionID].getTableQuery(QueryCli, "getnewsCliReg");
                if (news.Rows.Count > 0)
                {
                    chEvolReg.Titles["ttEvolReg"].Text = "Notícias geral | últimos 90 Dias";
                    Int32[] yValues = new Int32[news.Rows.Count];
                    string[] xValues = new string[news.Rows.Count];
                    int counter = 0;
                    foreach (DataRow r in news.Rows)
                    {
                        yValues[counter] = Convert.ToInt32(r["total"].ToString());
                        xValues[counter] = r["prov"].ToString();
                        counter++;
                    }

                    chEvolReg.Series["srEvolReg"].Points.DataBindXY(xValues, yValues);

                    counter = 0;
                    foreach (DataRow r in news.Rows)
                    {
                        chEvolReg.Series["srEvolReg"].Points[counter].Color = ColorTranslator.FromHtml("#00add9");
                        counter++;
                    }
                    chEvolReg.Titles["ttEvolReg"].Alignment = ContentAlignment.TopLeft;                  
                    chEvolReg.Series["srEvolReg"].ChartType = SeriesChartType.Column;
                    chEvolReg.Series["srEvolReg"]["PointWidth"] = "0.5";
                    chEvolReg.Series["srEvolReg"].IsValueShownAsLabel = true;
                    chEvolReg.Series["srEvolReg"]["BarLabelStyle"] = "Center";
                    chEvolReg.Series["srEvolReg"]["DrawingStyle"] = "Default";
                    chEvolReg.Series["srEvolReg"].ToolTip = "#VALX [#VALY]";
                    chEvolReg.Series["srEvolReg"].IsXValueIndexed = false;
                    chEvolReg.Series["srEvolReg"].IsVisibleInLegend = true;
                    chEvolReg.ChartAreas["caEvolReg"].AxisX.LabelStyle.Enabled = true;
                    chEvolReg.ChartAreas["caEvolReg"].AxisX.Interval = 1;
                    chEvolReg.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                    chEvolReg.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                }

                if (newsCliReg.Rows.Count > 0)
                {
                    chEvolRegCli.Titles["ttEvolRegCli"].Text = "Notícias " + Global.InsigteManager[Session.SessionID].NomCliente.ToString() +" | últimos 90 Dias";
                    Int32[] yValues1 = new Int32[newsCliReg.Rows.Count];
                    string[] xValues1 = new string[newsCliReg.Rows.Count];
                    int counter = 0;
                    foreach (DataRow r in newsCliReg.Rows)
                    {
                        yValues1[counter] = Convert.ToInt32(r["total"].ToString());
                        xValues1[counter] = r["prov"].ToString();
                        counter++;
                    }

                    chEvolRegCli.Series["srEvolRegCli"].Points.DataBindXY(xValues1, yValues1);

                    counter = 0;
                    foreach (DataRow r in newsCliReg.Rows)
                    {
                        chEvolRegCli.Series["srEvolRegCli"].Points[counter].Color = ColorTranslator.FromHtml("#00add9");
                        chEvolRegCli.Series["srEvolRegCli"].Url = "subject.aspx?id="+ r["prov"].ToString()+ "_" + Global.InsigteManager[Session.SessionID].NomCliente.ToString();
                        counter++;
                    }

                    chEvolRegCli.Titles["ttEvolRegCli"].Alignment = ContentAlignment.TopLeft; 
                    chEvolRegCli.Series["srEvolRegCli"].ChartType = SeriesChartType.Column;
                    chEvolRegCli.Series["srEvolRegCli"]["PointWidth"] = "0.5";
                    chEvolRegCli.Series["srEvolRegCli"].IsValueShownAsLabel = true;
                    chEvolRegCli.Series["srEvolRegCli"]["BarLabelStyle"] = "Center";
                    chEvolRegCli.Series["srEvolRegCli"]["DrawingStyle"] = "Default";
                    chEvolRegCli.Series["srEvolRegCli"].ToolTip = "#VALX [#VALY]";
                    chEvolRegCli.Series["srEvolRegCli"].IsXValueIndexed = false;
                    chEvolRegCli.Series["srEvolRegCli"].IsVisibleInLegend = true;
                    chEvolRegCli.ChartAreas["caEvolRegCli"].AxisX.LabelStyle.Enabled = true;
                    chEvolRegCli.ChartAreas["caEvolRegCli"].AxisX.Interval = 1;
                    chEvolRegCli.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                    chEvolRegCli.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                }

            }
            catch (Exception erro)
            {
                //lblDataToday.Visible = true;
                //lblDataToday.Text = "<br>" + erro.Message.ToString() + "<br>deu erro <br>";
            }
        }
    }
}