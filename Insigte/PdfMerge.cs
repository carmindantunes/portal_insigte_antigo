﻿using System;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Insigte
{
    public class PdfMerge
    {
        public static void MergeFiles(string destinationFile, string[] sourceFiles, Boolean AddLogo, String id_client)
        {
            try
            {
                int f = 0;

                PdfReader readerPs = new PdfReader("G:\\pdf\\size.pdf");
                int nPs = readerPs.NumberOfPages;
                Rectangle pageSize = readerPs.GetPageSizeWithRotation(1);
                
                
                Document document = new Document(pageSize);
                FileStream fs = new FileStream(destinationFile, FileMode.Create);
                PdfWriter writer = PdfWriter.GetInstance(document, fs);
                writer.CloseStream = false;
                document.Open();
                PdfContentByte cb = writer.DirectContent;

                PdfImportedPage page;
                int rotation;

                PdfReader[] reader = new PdfReader[sourceFiles.Length];

                int total = 0;
                foreach (string file in sourceFiles)
                {
                    
                    reader[total] = new PdfReader(file);
                    //PdfReader reader = new PdfReader(file);
                    int n = reader[total].NumberOfPages;

                    int i = 0;
                    while (i < n)
                    {
                        i++;
                        document.SetPageSize(reader[total].GetPageSizeWithRotation(i));
                        document.NewPage();
                        page = writer.GetImportedPage(reader[total], i);
                        rotation = reader[total].GetPageRotation(i);
                        if (rotation == 90 || rotation == 270)
                        {
                            cb.AddTemplate(page, 0, -1f, 1f, 0, 0, reader[total].GetPageSizeWithRotation(i).Height);
                        }
                        else
                        {
                            cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                        }

                    }

                    total++;
                }

                document.Close();
                document.Dispose();
                fs.Close();
                fs.Dispose();

                readerPs.Close();
                readerPs.Dispose();

                total = 0;
                foreach (string file in sourceFiles)
                {
                    reader[total].Close();
                    reader[total].Dispose();
                    total++;
                }
                
            }
            catch (Exception e)
            {
                string strOb = e.Message;
            }
        }

        public static void MergePdfs(string destinationFile, string[] sourceFiles)
        {
            try
            {
                Document document = new Document();
                FileStream fs = new FileStream(destinationFile, FileMode.Create);
                PdfCopy copy = new PdfCopy(document, fs);

                document.Open();

                PdfReader[] reader = new PdfReader[sourceFiles.Length];
                int n;
                int total = 0;

                foreach (string file in sourceFiles)
                {
                    reader[total] = new PdfReader(file);
                    //PdfReader reader = new PdfReader(file);
                    n = reader[total].NumberOfPages;

                    for (int page = 0; page < n; page++)
                    {
                        copy.AddPage(copy.GetImportedPage(reader[total], page + 1));
                    }

                    reader[total].Close();
                }

                document.Close();
                document.Dispose();
                fs.Close();
                fs.Dispose();

                total = 0;
                //foreach (string file in sourceFiles)
                //{
                //    reader[total].Close();
                //    reader[total].Dispose();
                //    total++;
                //}
            }
            catch (Exception er)
            {
                er.Message.ToString();
            }
            

        }

        public static int CountPageNo(string strFileName)
        {
            //string pathOriginal = strFileName;
            ////G:\files\2013/04/29/Exame_01052013_p68.pdf 
            //if(!File.Exists(strFileName))
            //{


            //    //int lastindex = strFileName.Replace("\\","/").LastIndexOf("/");
            //    //strFileName = "http://insigte.com/ficheiros/" + strFileName.Substring(lastindex);

            //    strFileName = strFileName.Replace("\\", "/").Replace("G:/files/", "http://insigte.com/ficheiros/");
            //}

            // we create a reader for a certain document
            PdfReader reader = new PdfReader(strFileName);
            
            // we retrieve the total number of pages
            return reader.NumberOfPages;
        }


    }
}