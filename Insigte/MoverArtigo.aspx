﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MoverArtigo.aspx.cs" Inherits="Insigte.MoverArtigo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Carma | Insigte now is Carma</title>
<link rel="icon" type="ico" href="https://www.carma.com/app/themes/carma/assets/images/favicon.ico" />
    <link href="Styles/StyleSheet.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        a
        {
            text-decoration:none;
            border:0px;
        }
        
        a:hover
        {
            text-decoration:none;
            border:0px;
        }
        
        a:active
        {
            text-decoration:none;
            border:0px;
        }
    </style>
</head>
<body onunload='javaScript:window.opener.location=window.opener.location;'>
    <form id="form1" runat="server">
        <% = "<div style=\"width: 400px; float: left; clear: both; color: #ffffff; height: 67px; background-image: url('Cli/" + global_asax.InsigteManager[Session.SessionID].CodCliente + "/fundo_small.png'); background-repeat:no-repeat; background-color: #003f56;\"> "%>
        </div>
        <div id="divMain" style="width: auto; height:auto; text-align: justify; clear: both; padding-bottom: 10px; padding-top: 10px;">
            <asp:Table ID="Table1" runat="server">
                <asp:TableRow ID="TableRow1" runat="server">
                    <asp:TableCell ID="TableCell1" runat="server" HorizontalAlign="Left">
                        &nbsp;&nbsp;&nbsp;<asp:Label ID="lbFrom" runat="server" Font-Names="Arial" Font-Size="12px" Text="Mover para a pasta: "></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell2" runat="server">
                        &nbsp;<asp:DropDownList ID="ddTo" runat="server" AutoPostBack="True"></asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" ID="TableRow2">
                    <asp:TableCell ID="TableCell3" runat="server">
                        
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell4" runat="server">        
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" ID="TableRow5">
                    <asp:TableCell ID="TableCell9" runat="server">
                        &nbsp;&nbsp;&nbsp;<asp:LinkButton runat="server" Text="Mover" Font-Names="Arial" Font-Size="12px" ID="btSalvarMoves" OnClick="btSalvarMove_Click" CssClass="btn-pdfs"></asp:LinkButton>
                    </asp:TableCell>
                    <asp:TableCell ID="TableCell10" runat="server">        
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>
    </form>
</body>
</html>
