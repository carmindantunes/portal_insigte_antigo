﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;


namespace Insigte
{
    public partial class NewsArchive : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            dgNewsArchACN.PagerStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[0].ToString());
            dgNewsArchACN.HeaderStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[1].ToString());
            
            //lblDataToday.Text = "Lisboa, " + DateTime.Now.ToString("dd") + " de " + DateTime.Now.ToString("MMMM", CultureInfo.CreateSpecificCulture("pt-PT")) + " de " + DateTime.Now.ToString("yyyy");

            if (Global.InsigteManager[Session.SessionID].inUser.EmailShareSend != null)
            {
                if (Global.InsigteManager[Session.SessionID].inUser.EmailShareSend == "1")
                {
                    lblDataToday.Visible = true;
                    lblDataToday.Text = "Email enviado.";
                    Global.InsigteManager[Session.SessionID].inUser.EmailShareSend = null;
                }
                else
                {
                    lblDataToday.Visible = true;
                    lblDataToday.Text = "Email não enviado.";

                    Global.InsigteManager[Session.SessionID].inUser.EmailShareSend = null;
                }
            }

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null)
            {
                if (!Page.IsPostBack)
                {
                    getNewsArchive();
                }
            }
            else
            {
               
                lblWarning.Text = Resources.insigte.language.naWarning;
                lblWarning.Visible = true;
            }

            
            //teste aos checkboxs
            //escondeBotton();
            
        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        public void getNewsArchive()
        {
            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];


            if (HttpContext.Current.Request.Cookies["CookieinsigteSys"] != null)
            {
                if (cookie.Values["ID"].ToString().Length > 0)
                {
                    dgNewsArchACN.Visible = true;
                    //HttpCookie cookie = Request.Cookies["Cookieiadvisers_2"];

                    string[] words = cookie.Values["ID"].ToString().Split('|');
                    string idsArt = string.Empty;


                    foreach (string value in words)
                    {
                        idsArt += "'" + value + "',";
                    }

                    idsArt = idsArt.Substring(0, idsArt.LastIndexOf(','));

                    String Title = "TITLE";
                    if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                    {
                        Title = "TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
                    }

                    string cmdQuery = "SELECT " + Title + ", EDITOR, DATE, iadvisers.dbo.metadata.ID as IDMETA, filepath,page_url,tipo ";
                    cmdQuery += " FROM iadvisers.dbo.metadata with(nolock) ";
                    cmdQuery += " where iadvisers.dbo.metadata.ID in (" + idsArt + ") ";

                    
                    if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                    {
                        cmdQuery += " and TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " is not null ";
                    }

                    cmdQuery += " order by iadvisers.dbo.metadata.date desc, IDMETA desc ";

                    //lblQuery.Text = cmdQuery;

                    SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");

                    try
                    {
                        conn.Open();

                        SqlCommand cmd = new SqlCommand(cmdQuery);
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;

                        DataSet tempSet;
                        SqlDataAdapter iadvisers_ACN_NEWS;

                        iadvisers_ACN_NEWS = new SqlDataAdapter(cmd);

                        tempSet = new DataSet("tempSet");
                        iadvisers_ACN_NEWS.Fill(tempSet);

                        dgNewsArchACN.DataSource = tempSet;
                        dgNewsArchACN.DataBind();
                        dgNewsArchACN.Visible = true;
                    }
                    catch (Exception exp)
                    {
                        exp.Message.ToString();
                    }
                }
                else
                {
                    lblWarning.Text = Resources.insigte.language.naWarning;
                    lblWarning.Visible = true;
                }
            }
            else
            {
                lblWarning.Text = Resources.insigte.language.naWarning;
                lblWarning.Visible = true;
            }
        }

        protected string sReturnTitle(string idArticle, string sTitle)
        {
            string sLink = string.Empty;

            //select clientid, subjectid from clientarticles where id='164400'

            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                string cmdQuery = "select clientid, subjectid from clientarticles with(nolock) where id='" + idArticle + "'";

                SqlCommand cmd = new SqlCommand(cmdQuery);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();

                bool isACNArticle = false;

                while (reader.Read())
                {
                    if (reader.GetValue(0).ToString() == Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0,4) && reader.GetValue(1).ToString() == Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4,3))
                    {
                        isACNArticle = true;
                    }
                }

                if (isACNArticle)
                {
                    sLink = "<b>" + sTitle + "</b>";
                }
                else
                {
                    sLink = sTitle;
                }

                cmd.Connection.Close();
                cmd.Connection.Dispose();

                return sLink;


            }
            catch (Exception exp)
            {
                exp.Message.ToString();
                return sTitle;

            }
        }

        protected string sReturnIconTipoPasta(string url, string tipo, string file)
        {
            //Imprensa
            //Insigte
            //Online
            //Rádio
            //Televisão
            switch (tipo)
            {
                case "Imprensa":
                    String FileHelperPath = "ficheiros";
                    if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                    {
                        if (File.Exists(@"G:\cfiles\" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file.Replace("/", "\\")))
                        {
                            FileHelperPath = "cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient;
                        }
                    }
                    return string.Format("<a href=\"http://insigte.com/" + FileHelperPath + "/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);

                case "Online":
                    return string.Format("<a href=\"{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/globe_1_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Link\" style=\"border-width:0px;\" title=\"Link\" /></a>", url);

                case "Rádio":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/headphones_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP3\" style=\"border-width:0px;\" title=\"Download MP3\" /></a>", file);

                case "Televisão":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/movie_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP4\" style=\"border-width:0px;\" title=\"Download MP4\" /></a>", file);

                default:
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);

            }
            return null;
        }

        protected string sReturnImgPDF(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16_sel.png";
                        }
                        else
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16.png";
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16_sel.png";
                        }
                        else
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16.png";
                        }
                    }
                }
            }
            else
            {
                sLink = "Imgs/icons/black/png/book_side_icon_16.png";
            }
            return sLink;
        }

        protected string sReturnIdPDF(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = string.Empty;
                        }
                        else
                        {
                            sLink = strValue;
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = string.Empty;
                        }
                        else
                        {
                            sLink = strValue;
                        }
                    }
                }
            }
            else
            {
                sLink = strValue;
            }
            return sLink;
        }

        protected void addLinkPDF(string qsPDFArtigo)
        {

            string ValCookie = string.Empty;
            string PDFCookie = string.Empty;
            string DelCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

            if (cookie.Values["PDF"] != "")
            {
                PDFCookie = cookie.Values["PDF"] + "|" + qsPDFArtigo;
            }
            else
            {
                PDFCookie = qsPDFArtigo;
            }

            DelCookie = cookie.Values["DELNOT"];
            ValCookie = cookie.Values["ID"];

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = PDFCookie;
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(360);
            Response.Cookies.Add(newCookie);

            //lblArtigoAddPDF.Text = "O artigo de ID: " + qsPDFArtigo + ", foi adicionado com sucesso.";

        }

        protected void bt_AddPDF_Command(Object sender, CommandEventArgs e)
        {
            addLinkPDF(e.CommandArgument.ToString());
            lblDataToday.Text = e.CommandArgument.ToString() + Resources.insigte.language.naAdicionado;

            ImageButton b = sender as ImageButton;
            b.ImageUrl = "Imgs/icons/black/png/book_side_icon_16_sel.png";
        }

        protected string sReturnImgLinkDetalhe(string strValue)
        {
            string sLink = string.Empty;
            sLink = "Imgs/icons/black/png/folder_delete_icon_16.png";
            return sLink;
        }

        protected string sReturnIdLinkDetalhe(string strValue)
        {
            string sLink = string.Empty;
            sLink = strValue;
            return sLink;
        }

        protected void bt_RemArquivo_Command(Object sender, CommandEventArgs e)
        {
            RemArt(e.CommandArgument.ToString());
            lblDataToday.Text = e.CommandArgument.ToString() + Resources.insigte.language.naAdicionado;

            ImageButton b = sender as ImageButton;
            b.ImageUrl = "Imgs/folderplus32_add.png";

            dgNewsArchACN.Items[Convert.ToInt32(e.CommandName)].Visible = false;



        }

        private void RemArt(string qsIDArtigo)
        {
            try
            {

                string ValCookie = string.Empty;
                string DelCookie = string.Empty;
                string PDFCookie = string.Empty;

                HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

                if (cookie.Values["DELNOT"] != "")
                {
                    DelCookie = cookie.Values["DELNOT"] + "|" + qsIDArtigo.TrimEnd('|');
                }
                else
                {
                    DelCookie = qsIDArtigo.TrimEnd('|');
                }

                ValCookie = cookie.Values["ID"].ToString();

                string[] Delete = qsIDArtigo.ToString().TrimEnd('|').Split('|');
                foreach (string value in Delete)
                {
                    ValCookie = ValCookie.Replace(value, "");
                   
                }

                ValCookie = ValCookie.Replace("||", "|");
                ValCookie = ValCookie.TrimEnd('|');
                ValCookie = ValCookie.TrimStart('|');

                //LINQ
                //if (cookie.Values["ID"].ToString().Contains('|'))
                //{
                //    string[] oldValCookie = cookie.Values["ID"].ToString().Split('|');
                //    string numToRemove = qsIDArtigo;
                //    oldValCookie = oldValCookie.Where(val => val != numToRemove).ToArray();

                //    foreach (string value in oldValCookie)
                //    {
                //        ValCookie += value + "|";
                //    }
                //}

                //if (ValCookie != string.Empty)
                //{
                //    ValCookie = ValCookie.Substring(0, ValCookie.LastIndexOf('|'));
                //}
                //else
                //{
                //    ValCookie = "";
                //}

                PDFCookie = cookie.Values["PDF"];

                cookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(cookie);

                HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
                newCookie["Version"] = "JUL2012";
                newCookie["ID"] = ValCookie;
                newCookie["PDF"] = PDFCookie;
                newCookie["DELNOT"] = DelCookie;
                newCookie.Expires = DateTime.Now.AddDays(360);
                Response.Cookies.Add(newCookie);

                //lblArtigoRem.Text = "O artigo de ID: " + qsIDArtigo + ", foi removido com sucesso.";
            }
            catch (Exception exp)
            {
                //lblArtigoRem.Text = exp.Message.ToString() + " - " + exp.StackTrace.ToString() + " - " + exp.Data.ToString();
            }
        }
        
        protected void btnRmvSelected_Click(Object sender, EventArgs e)
        {
            string items = "";
            foreach (DataGridItem drv in dgNewsArchACN.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
                if (chk.Checked)
                {

                    //RemArt(chk.Attributes["NewsId"].ToString());
                    items += chk.Attributes["NewsId"].ToString() + "|";
                    chk.Checked = false;  
                    drv.Visible = false;

                }
            }
            RemArt(items);
            Response.Redirect("NewsArchive.aspx");
        }

        protected Boolean checkDossier(String idArtigo)
        {
            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && idArtigo != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(idArtigo))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (idVal == idArtigo)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }


        }

        protected void chkSelAll_changed(object sender, EventArgs e)
        {
            CheckBox chk2 = (CheckBox)sender;
            int checks = 0;
            foreach (DataGridItem drv in dgNewsArchACN.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
                chk.Checked = chk2.Checked;
                if (chk.Checked)
                    checks++;

                if (checks > 1)
                    btnRemoveClips.Visible = true;
                else
                    btnRemoveClips.Visible = false;
            }

        }

        protected void chkRow_Changed(object sender, EventArgs e)
        {
            CheckBox chk2 = (CheckBox)sender;
            int checks = 0;
            foreach (DataGridItem drv in dgNewsArchACN.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
                if (chk.Checked)
                    checks++;

                if (checks > 1)
                    btnRemoveClips.Visible = true;
                else
                    btnRemoveClips.Visible = false;
            }
        }


    }
}