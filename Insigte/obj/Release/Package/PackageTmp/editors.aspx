﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="editors.aspx.cs" Inherits="Insigte.editors" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>insigte | media intelligence</title>

<style type="text/css">
    
    a.fivenews:link /* unvisited link */
    {
        text-decoration:none;
        color:#000000;
    }
    a.fivenews:visited /* unvisited link */
    {
        text-decoration:none;
        color:#000000;
    }
    a.fivenews:hover /* unvisited link */
    {
        text-decoration:none;
        color:#000000;
    }
    a.fivenews:active /* unvisited link */
    {
        text-decoration:none;
        color:#000000;
    }
    
    div.topnews
    {
        float:inherit;
        border:1px dotted #c9c9c9;
        background-color:#e0e0e0;
        width: 98%;
        margin: 4px 0px 4px 0px;
        padding: 2px 2px 2px 2px;
    }
    
    
    a.ent:link /* unvisited link */
    {
        text-decoration:none;
        color:#000000;
        border-bottom:1px dotted #c9c9c9;
        
    }    
    a.ent:visited /* visited link */
    {
        text-decoration:none;
        color:#000000;
        border-bottom:1px dotted #c9c9c9;
    } 
    a.ent:hover /* mouse over link */
    {
        text-decoration:underline;
        color:#000000;
        border-bottom:1px dotted #c9c9c9;
    }   
    a.ent:active /* selected link */
    {
        text-decoration:underline;
        color:#000000;
        border-bottom:1px dotted #c9c9c9;
    }  
</style>


<style type="text/css">
    .infor
    {
        font-family:Arial;
        font-size:12px;
    }
    
    a{color:#000000;}
</style>

</head>
<body>
<form id="form1" runat="server">
    <% = "<div style=\"width: 400px; float: left; clear: both; color: #ffffff; height: 67px; background-image: url('Cli/" + global_asax.InsigteManager[Session.SessionID].CodCliente + "/fundo_small.png'); background-repeat:no-repeat; background-color: #003f56;\"> "%>
    </div>
    <div style="float:left;width:100%;">
        <div style="margin: 10px 10px 10px 10px;">
            <div style="float:left;width:100%;line-height:1.5em;">
                <div style="float:left;width:12%;">
                    <asp:Literal ID="lt_Editor_photo" runat="server"></asp:Literal>
                </div>
                <div class="infor" style="float:left;width:88%;line-height:1.5em;">
                    <asp:Literal ID="lt_Editor_name" runat="server"></asp:Literal>&nbsp;<asp:Literal ID="lt_Editor_Email" runat="server"></asp:Literal>
                </div>
                <div class="infor" style="float:left;width:88%;line-height:1.5em;">
                    <asp:Literal ID="lt_Editor_funcao" runat="server"></asp:Literal>
                </div>
            </div>
            <div style="float:left;width:100%;height:18px;margin: 10px 0px 0px 0px;">
                <div style="float:left;width:10%;margin: 4px 0px 0px 0px;">
                    <img src='../ficheiros/Editores/infacebook.png' alt="" />
                </div>
                <div class="infor" style="float:left;width:90%;line-height:2em;">
                    <asp:Literal ID="fb_desc" runat="server"></asp:Literal>&nbsp;
                </div>
            </div>
            <div style="float:left;width:100%;height:18px;">
                <div style="float:left;width:10%;margin: 4px 0px 0px 0px;">
                    <img src='../ficheiros/Editores/intwitter.png' alt="" />
                </div>
                <div class="infor" style="float:left;width:90%;line-height:2em;">
                    <asp:Literal ID="tw_desc" runat="server"></asp:Literal>&nbsp;
                </div>
            </div>
            <div style="float:left;width:100%;height:18px;">
                <div style="float:left;width:10%;margin: 4px 0px 0px 0px;">
                    <img src='../ficheiros/Editores/inlinkedin.png' alt="" />
                </div>
                <div class="infor" style="float:left;width:90%;line-height:2em;">
                    <asp:Literal ID="lk_desc" runat="server"></asp:Literal>&nbsp;
                </div>
            </div>

            <div style="float:left;width:100%;height:18px;margin: 18px 0px 0px 0px;">
                <div class="infor" style="float:left;width:18%;">
                    Telefone:
                </div>
                <div class="infor" style="float:left;width:80%;">
                    <asp:Literal ID="lt_telefone" runat="server"></asp:Literal>&nbsp;
                </div>
            </div>
            <div style="float:left;width:100%;height:auto;">
                <div class="infor" style="float:left;width:18%;">
                    Morada:
                </div>
                <div class="infor" style="float:left;width:80%;">
                    <asp:Literal ID="lt_morada" runat="server"></asp:Literal>&nbsp;
                </div>
            </div>
            <div style="float:left;width:100%;height:18px;">
                <div class="infor" style="float:left;width:18%;">
                    &nbsp;
                </div>
            </div>
            <div style="float:left;width:100%; margin-top:5px;">
                <div class="infor" style="float:left;width:100%;">
                    Últimas Notícias
                </div>
                <asp:Repeater ID="rep_last5news" runat="server">
                    <ItemTemplate>
                        <div style="margin: 2px 2px 2px 2px;">
                            <div class="infor" style="float:left;width:100%;">
                                <%# "<div class=\"topnews\"><a class=\"fivenews\" target=\"_blank\" title=\"" + Eval("text").ToString().Replace("\"", "") + "\" href=\"http://www.insigte.com/login/Article.aspx?ID=" + Eval("id").ToString() + "\">" + Eval("editor").ToString() + " | " + Eval("date").ToString().Substring(0, 10) + " <br> " + Eval("title").ToString() + "</a></div>" %>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
