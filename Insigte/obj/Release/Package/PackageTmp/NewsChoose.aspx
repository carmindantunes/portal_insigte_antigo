﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="NewsChoose.aspx.cs" Inherits="Insigte.NewsChoose" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        a.lbk
        {
            color: #000;
            text-decoration: none;
            font-weight: bold;
            font-size: 12px;
            font-family: Arial;
        }
        
        a.lbk:hover
        {
            color: #000;
            text-decoration: underline;
            font-weight: bold;
            font-size: 12px;
            font-family: Arial;
        }
        a
        {
            color: Black;
        }
        
        a:hover
        {
            color: #E39B2F;
        }
        
        
        #dialog-form
        {
            height: auto;
            color: #000;
            text-decoration: none;
            font-weight: bold;
            font-size: 12px;
            font-family: Arial;
        }
        
        #dialog-form p.newDesc
        {
            color: #000;
        }
        
        .ui-widget-header
        {
            border: 1px solid #aaaaaa;
            color: White;
            font-weight: bold;
            font-size: 12px;
            font-family: Arial;
        }
        
        .actionBtn
        {
            width: 17px;
            height: 16px;
            padding: 10px 2px 3px!important;
            padding-top: 1px;
            padding-right: 2px;
            padding-bottom: 3px;
            padding-left: 2px;
            border: medium none;
            background: transparent url(Imgs/icons/white/png/checkbox_unchecked_icon_16.png);
        }
        .actionBtn-snone
        {
            background: transparent url(Imgs/icons/white/png/checkbox_unchecked_icon_16.png);
        }
        
        .actionBtn-snone:hover
        {
            background: transparent url(Imgs/icons/blue/png/checkbox_unchecked_icon_16.png);
        }
        
        .actionBtn-snone:active
        {
            background: transparent url(Imgs/icons/black/png/checkbox_unchecked_icon_16.png);
        }
        
        .actionBtn-sall
        {
            background: transparent url(Imgs/icons/white/png/checkbox_checked_icon_16.png);
        }
        
        .actionBtn-sall:hover
        {
            background: transparent url(Imgs/icons/blue/png/checkbox_checked_icon_16.png);
        }
        
        .actionBtn-sall:active
        {
            background: transparent url(Imgs/icons/black/png/checkbox_checked_icon_16.png);
        }
        
        fieldset
        {
            padding: 0;
            border: 0;
            margin-top: 25px;
        }
        
        #section.sector
        {
            float: left;
            border: 1px solid Black;
            width: 100%;
            min-width: 300px;
            color: #000;
            text-decoration: none;
            font-size: 12px;
            font-family: Arial;
            padding: 0px;
            margin: 0px 0px 5px 0px;
        }
        
        .form_search
        {
            float: left;
            width: 100%;
            height: 10px;
            padding-top: 4px;
            padding-bottom: 10px;
            margin: 0 auto 0px auto;
            background-color: #323232;
            color: #FFFFFF;
        }
        
        .rptnews
        {
            float: left;
            margin: 0 10px 0 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="Geral" style="float: left; padding: 0px; margin: 0px; height: 100%; width: 800px;">
        <div id="infoText" style="float: left; padding: 0px; margin: 2px 0px 2px 0px; height: 35px;
            min-height: 35px; width: 100%">
            <div style="margin-top: 8px">
                <div id="TopInfo">
                    <%--<asp:DropDownList ID="DDLNewsSub" AutoPostBack="true" Font-Names="Arial" Font-Size="12px" ForeColor="#000000" OnSelectedIndexChanged="getSelected" runat="server"></asp:DropDownList>&nbsp;
                <a id="criar-novo" class="btn-pdfs" href="#"><%= getResource("defcolNovo")%></a>&nbsp;
                <asp:LinkButton CssClass="btn-pdfs" ID="lb_delete" Text="<%$Resources:insigte.language,advSMainDelBtn%>" OnClick="Delete_News_Click" OnClientClick='return chkconfirm();' runat="server"></asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnk_guardarTop" CssClass="btn-pdfs" runat="server" OnClick="teste_Click"><%= getResource("advSdlButton")%></asp:LinkButton>&nbsp;&nbsp;
                <asp:Label Font-Names="Arial" Font-Size="12px" ForeColor="#000000" runat="server" ID="lblDataToday" Visible="true" />&nbsp;&nbsp;
                <input type="hidden" id="txt_aux_novo" runat="server" name="name" value="" />--%>
                    <asp:Label Font-Names="Arial" Font-Size="12px" ForeColor="#000000" runat="server"
                        ID="lblDataToday" Visible="false" />&nbsp;&nbsp;
                </div>
            </div>
        </div>
        <div id="Data" style="float: left; width: 100%;">
            <asp:Repeater ID="rp_NewsTemas" runat="server">
                <ItemTemplate>
                    <div id="section" class="sector ui-corner-top">
                        <div id="sectionTitle" style="float: left; width: 100%;" class="form_search headbg ui-corner-top">
                            <div id="titlel" style="float: left; padding-left: 10px">
                                &nbsp;<asp:Label Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" ID="Label1"
                                    runat="server" Text='<%# getname(DataBinder.Eval(Container.DataItem,"name").ToString(),DataBinder.Eval(Container.DataItem,"name_en").ToString(),DataBinder.Eval(Container.DataItem,"tema").ToString()) %>'></asp:Label>
                            </div>
                            <div id="titler" style="float: right; width: 30%;">
                                <div id="btn_action" class="show_on_hover" style="float: right; margin-right: 2px;">
                                    <button type="button" id="btnlimpar" data-name='<%# DataBinder.Eval(Container.DataItem, "tema") %>' class="actionBtn actionBtn-snone">
                                    </button>
                                    <button type="button" id="btnselec" data-name='<%# DataBinder.Eval(Container.DataItem, "tema") %>' class="actionBtn actionBtn-sall">
                                    </button>
                                    <%--<asp:Button ToolTip="limpar todos" ID="Button1" CssClass="actionBtn actionBtn-snone"
                                        runat="server" CommandName="limpar" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"tema").ToString() %>'  />
                                    <asp:Button ToolTip="seleccionar todos" ID="Button2" CssClass="actionBtn actionBtn-sall"
                                        runat="server" CommandName="seleccionar" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"tema").ToString() %>' />--%>
                                </div>
                            </div>
                        </div>
                    <div style="margin-top: 10px; float: left;">
                        <asp:Repeater ID="rp_news1" runat="server" DataSource='<%# FillRp_news(DataBinder.Eval(Container.DataItem,"tema").ToString()) %>'>
                            <ItemTemplate>
                                <div id="sectionBody" name='<%# DataBinder.Eval(Container.DataItem,"tema").ToString() %>[]' style="float: left; margin-left: 8px; width: 100%; margin-bottom: 1px !important;">
                                    <%--<asp:CheckBoxList Font-Names="Arial" Font-Size="12px" ID="Chk_temas" CommandName='<%# DataBinder.Eval(Container.DataItem,"tema").ToString() %>'
                                        runat="server" 
                                        DataTextField="<%# DataBinder.Eval(Container.DataItem,"title").ToString() %>" DataValueField="<%# DataBinder.Eval(Container.DataItem,"id").ToString() %>">
                                    </asp:CheckBoxList>--%>
                                    <asp:CheckBox Font-Names="Arial" Font-Size="12px" ID='Chk_temas' Checked="true" runat="server"
                                        data-id='<%# DataBinder.Eval(Container.DataItem,"id").ToString() %>' data-grupo='<%# DataBinder.Eval(Container.DataItem,"tema").ToString() %>'
                                        AutoPostBack="false" Style="float: left;" value='<%# DataBinder.Eval(Container.DataItem,"id").ToString() %>' />
                                    <a href="<%# DataBinder.Eval(Container.DataItem, "url").ToString()%>" target="_blank"><span class="rptnews" style="width: 450px;">
                                        <%# DataBinder.Eval(Container.DataItem, "title").ToString()%></span></a> <span class="rptnews"
                                            style="width: 135px;">
                                            <%# DataBinder.Eval(Container.DataItem,"editor").ToString() %></span> <span class="rptnews"
                                                style="width: 50px;">
                                                <%# Convert.ToDateTime( DataBinder.Eval(Container.DataItem,"date").ToString()).ToShortDateString() %></span>
                                    <span class="rptnews" style="width: 83; text-align: center;">
                                        <%# DataBinder.Eval(Container.DataItem,"tipo").ToString() %></span>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    </div>
                    <br />
                </ItemTemplate>
            </asp:Repeater>
            <div id="lastbtn" style="float: left; width: 100%; margin: 10px 0px 5px 0px;">

                <asp:LinkButton ID="lnk_guardarBottom" CssClass="btn-pdfs" runat="server" OnClick="teste_Click"><%= getResource("advSdlButton")%></asp:LinkButton>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('.show_on_hover').hide();
            $('.sector').hover(function () {
                $(this).find('.show_on_hover').stop(true, true).fadeIn(400);
            },

        function () {
            $(this).find('.show_on_hover').stop(true, true).fadeOut(400);
        }
        );
        });

    $(document).ready(function () {



            $(document).on('click', '#btnlimpar', function (e) {
                e.preventDefault();
                $("div #sectionBody[name='" + $(this).data('name') + "[]']").children().find('input:checkbox').each(function () {
                    $(this).prop('checked', false);
                });
            });

            $(document).on('click', '#btnselec', function (e) {
                e.preventDefault();

                $("div #sectionBody[name='" + $(this).data('name') + "[]']").children().find('input:checkbox').each(function () {
                        $(this).prop('checked', true);
                });
            });
        });

    </script>
</asp:Content>
