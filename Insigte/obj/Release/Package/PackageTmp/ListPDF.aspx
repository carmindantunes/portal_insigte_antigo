﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" Async="true" AutoEventWireup="true" CodeBehind="ListPDF.aspx.cs" Inherits="Insigte.ListPDF" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    function chkconfirm() {
        return confirm("<%= getResource("ConfApagarClip") %>");
    }

    function Confirm() {
   
            var omfg1 = "<%= getResource("defAskSaveInDoc") %>";
            var filename = "<%= getResource("defAskFileName") %>";
            
            var confirm_value = document.createElement("INPUT");
            document.forms[0].getElementsByTagName('input');
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            confirm_value.value = "No" ;

             var prompt_text = document.createElement("INPUT");
             prompt_text.type = "hidden";
             prompt_text.name = "pdfName_value";
              prompt_text.value = "null";
            if (confirm(omfg1)) {

               
                
                var x = prompt(filename);
                if (x != null)
                {
                    prompt_text.value = x;
                    
                }

                confirm_value.value = "Yes" ;
                
            } else {
                confirm_value.value = "No";
            }
             
             document.forms[0].appendChild(confirm_value);
             document.forms[0].appendChild(prompt_text);
             
             
        }

</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="Geral" style="float:left; padding:0px; margin:0px; height:100%;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:12px">
            <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false" />
        </div>
    </div>
    <div class="form_caja">
     <asp:DataGrid runat="server" ID="dgNewsToPDFACN" AutoGenerateColumns="false" AllowPaging="True"
     GridLines="None" CellPadding="0" CellSpacing="0" BorderWidth="0px" Visible="false" OnPageIndexChanged="dgNewsToPDFACN_PageIndexChanged"
     BorderStyle="None" Width="800px" Font-Names="Arial" Font-Size="11px" PageSize="30" AlternatingItemStyle-BackColor="#EEEFF0" PagerStyle-BackColor="#686C6E" 
     PagerStyle-Font-Size="12px" PagerStyle-ForeColor="#ffffff" PagerStyle-Font-Underline="false" PagerStyle-VerticalAlign="Middle" PagerStyle-HorizontalAlign="Center" PagerStyle-Mode="NumericPages">
        <ItemStyle BackColor="White" Height="24px" />
        <AlternatingItemStyle  BackColor="#EEEFF0" />
        <HeaderStyle BackColor="#8D8D8D" Height="24px" Font-Names="Arial" Font-Size="12px"  CssClass="headbg" />
        <FooterStyle Height="24px" Font-Names="Arial" Font-Size="12px"  CssClass="headbg" />
        <Columns>
            <asp:TemplateColumn HeaderText="&nbsp; <%$Resources:insigte.language,defColTitulo%>" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Justify" ItemStyle-ForeColor="#000000" HeaderStyle-HorizontalAlign="Center">
                <ItemTemplate>&nbsp;
                    <a href="<%# string.Format("Article.aspx?ID={0}", DataBinder.Eval(Container.DataItem, "IDMETA")) %>" target="_self" style="color:#000000; text-decoration:none;" onmouseout="this.style.textDecoration='none';" onmouseover="this.style.textDecoration='underline';"><%# sReturnTitle(DataBinder.Eval(Container.DataItem, "IDMETA").ToString(), DataBinder.Eval(Container.DataItem, "TITLE").ToString())%></a>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="EDITOR" HeaderText="<%$Resources:insigte.language,defColFonte%>" HeaderStyle-HorizontalAlign="Center"
                HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Justify" />
            <asp:BoundColumn DataField="DATE" HeaderText="<%$Resources:insigte.language,defColData%>" DataFormatString="{0:d}" HeaderStyle-HorizontalAlign="Center"
                HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Center"  ItemStyle-Width="100px" />
            <asp:TemplateColumn HeaderText="<%$Resources:insigte.language,defColAccoes%>" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" >
                <ItemTemplate>
                    <asp:Table ID="Table1" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell ID="TableCell3" runat="server">
                                <%# sReturnIconTipoPasta(DataBinder.Eval(Container.DataItem, "page_url").ToString(), DataBinder.Eval(Container.DataItem, "tipo").ToString(), DataBinder.Eval(Container.DataItem, "filepath").ToString())%>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell1" runat="server">
                                <a href="<%# string.Format("Article.aspx?ID={0}", DataBinder.Eval(Container.DataItem, "IDMETA")) %>" target="_self" style="color:#000000; text-decoration:none;">
                                    <img src="Imgs/icons/black/png/doc_lines_stright_icon_16.png" width="16px" height="16px" alt="<%= getResource("defColAccoesTexto") %>" title="<%= getResource("defColAccoesTexto") %>" style="border-width:0px;"/>
                                </a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell2" runat="server">
                                <%--<a href="javascript:window.open('Email.aspx?ID=<%# DataBinder.Eval(Container.DataItem, "IDMETA")%>','CustomPopUp', 'width=400, height=400, menubar=no, resizeble=no, location=no'); void(0)" style="border-width:0px;text-decoration:none;">
                                    <img src="Imgs/icons/black/png/share_icon_16.png" width="16px" height="16px" alt="<%= getResource("defColAccoesShare") %>" title="<%= getResource("defColAccoesShare") %>" style="border-width:0px;"/>
                                </a>--%>
                                <a id="share-news" onclick="shareTheNews(<%# "'" + DataBinder.Eval(Container.DataItem, "IDMETA").ToString() +  "','" +  DataBinder.Eval(Container.DataItem, "TITLE").ToString().Replace("'"," ").Replace("\""," ") + "'" %>)" title='<%# DataBinder.Eval(Container.DataItem, "TITLE").ToString() %>' style="color:#000000; text-decoration:none;" href="#">
                                        <img src="Imgs/icons/black/png/share_icon_16.png" width="16px" height="16px" alt="<%= getResource("defColAccoesShare") %>" title="<%= getResource("defColAccoesShare") %>" style="border-width:0px;"/>
                                    </a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell4" runat="server">
                                    <asp:ImageButton ID="bt_AddArquivo" runat="server" 
                                        OnCommand="bt_AddArquivo_Command" 
                                        CommandArgument='<%# sReturnIdLinkDetalhe(DataBinder.Eval(Container.DataItem, "IDMETA").ToString())%>'
                                        CausesValidation="True" 
                                        ImageUrl='<%# sReturnImgLinkDetalhe(DataBinder.Eval(Container.DataItem, "IDMETA").ToString()) %>' width='16px' height='16px' alt='<%# getResource("defColAccoesClips") %>' title='<%# getResource("defColAccoesClips") %>' style='border-width:0px;' >
                                    </asp:ImageButton >
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell6" runat="server">
                                    <asp:ImageButton ID="bt_RemPDF" runat="server" 
                                        OnCommand="bt_RemPDF_Command"
                                        CommandArgument='<%# sReturnIdPDF(DataBinder.Eval(Container.DataItem, "IDMETA").ToString())%>'
                                        CommandName='<%# Container.ItemIndex %>'
                                        CausesValidation="True" 
                                        OnClientClick='return chkconfirm();'
                                        ImageUrl='<%# sReturnImgPDF(DataBinder.Eval(Container.DataItem, "IDMETA").ToString()) %>' width='16px' height='16px' alt='Remover PDF' title='Remover PDF' style='border-width:0px;' >
                                    </asp:ImageButton >
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
    </div>
    <div style="float:right; padding-right:7px; margin-right:10px;">
        <asp:Table ID="Table1" runat="server">
            <asp:TableRow ID="TableRow1" runat="server">
                <asp:TableCell ID="TableCell5" runat="server">
                    <asp:CheckBox ID="chk_InText" Font-Names="Arial" Font-Size="12px" Height="17px" runat="server" Text ="Apenas Texto" Checked="false" />
                </asp:TableCell>
                <asp:TableCell ID="TableCell1" runat="server">
                    <asp:Button runat="server" ID="btnBuildPDF" Text="<%$Resources:insigte.language,dossierPDF%>" CssClass="btn-pdfs" OnClick="btnBuildPDF_Click" OnClientClick = "Confirm()" />
                </asp:TableCell>
                <asp:TableCell ID="TableCell2" runat="server">
                    <asp:Button runat="server" ID="btnClearPDFList" Text="<%$Resources:insigte.language,dossieeLimpar%>" CssClass="btn-pdfs" OnClick="btnClearPDFList_Click" /> 
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
    <div style="float:left; padding-right:7px;">
        <asp:Label runat="server" ID="lblWarning" Font-Names="Arial" Font-Size="12px" Visible="false" Font-Bold="true" ForeColor="#000000"/>
    </div>


</div>
</asp:Content>
