﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdvSearch.aspx.cs" Inherits="Insigte.AdvSearch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">

    $(function () {
        $("#MainContent_tbDateDe").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            onClose: function (selectedDate) {
                $("#MainContent_tbDateAte").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#MainContent_tbDateAte").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            onClose: function (selectedDate) {
                $("#MainContent_tbDateDe").datepicker("option", "maxDate", selectedDate);
            }
        });
    });

    function chkconfirm() {
        return confirm("<%= getResource("advSMainConfDel") %>");
    }

    function clearSearch(value) {
        if (value=="<%= getResource("advStxtPesquisa") %>")
        {
            $("#MainContent_tbSearchAdvance").val("");
        }
    }

    function lbEditores_DoubleClick(value, vleID) {

        vleID = vleID.replace(vleID, "MainContent_ta_editores_codes");
        //vleID = vleID.replace("LbEditors", "ta_editores_codes");

        if ($('textarea[id^="' + vleID + '"]').tagExist(value)) {
            alert("Editor '" + value + "' já Existe");
        } else {
            if (value == "<%= getResource("geralTagTodos") %>") {
                $('textarea[id^="' + vleID + '"]').importTags('');
                $('textarea[id^="' + vleID + '"]').addTag(value);
            }
            else {
                if ($('textarea[id^="' + vleID + '"]').tagExist("<%= getResource("geralTagTodos") %>")) {
                    $('textarea[id^="' + vleID + '"]').importTags('');
                    $('textarea[id^="' + vleID + '"]').addTag(value);
                } else {
                    $('textarea[id^="' + vleID + '"]').addTag(value);
                }
            }
        }
    };

    function lbJornalists_DoubleClick(value, vleID) {

        vleID = vleID.replace(vleID, "MainContent_ta_jornalists_codes");
        //vleID = vleID.replace("LbEditors", "ta_editores_codes");

        if ($('textarea[id^="' + vleID + '"]').tagExist(value)) {
            alert("Editor '" + value + "' já Existe");
        } else {
            if (value == "<%= getResource("geralTagTodos") %>") {
                $('textarea[id^="' + vleID + '"]').importTags('');
                $('textarea[id^="' + vleID + '"]').addTag(value);
            }
            else {
                if ($('textarea[id^="' + vleID + '"]').tagExist("<%= getResource("geralTagTodos") %>")) {
                    $('textarea[id^="' + vleID + '"]').importTags('');
                    $('textarea[id^="' + vleID + '"]').addTag(value);
                } else {
                    $('textarea[id^="' + vleID + '"]').addTag(value);
                }
            }
        }
    };

    $(function () {
        $('textarea[id^="MainContent_ta_jornalists_codes"]').tagsInput({
            width: 'auto',
            height: '50px',
            interactive: false
        });

        $('textarea[id^="MainContent_ta_editores_codes"]').tagsInput({
            width: 'auto',
            height: '50px',
            interactive: false
        });

        $("#tabs").tabs();

        $("#tabs2").tabs();

        $("#dialog-form").dialog({
            autoOpen: false,
            height: 200,
            width: 350,
            modal: true,
            beforeClose: function (event, ui) {
                $("#MainContent_txt_aux_novo").val($("#MainContent_name").val());
            }
        });

        $("#criar-novo").click(function () {
            $("#dialog-form").dialog("open");
        });

        $("#criar-novo-bottom").click(function () {
            $("#dialog-form").dialog("open");
        });

    });

    $(function () {
        $("#dialog-form-help").dialog({
            autoOpen: false,
            height: 'auto',
            width: 450,
            modal: true
        });
    });

    $(function () {
        $("#ajuda-adv").click(function () {
            $("#dialog-form-help").dialog("open");
            });
    });

</script>
<style type="text/css">
    #section.sector
    {
        float:left;
        border :1px solid Black;
        width: 100%;
        min-height: 300px;
        min-width:400px;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
        padding:0px;
        margin:0px;
    }
    
    .ui-datepicker
    {
        text-decoration: none;
        font-family: Arial;
        font-size: 12px;
    }
    
    .ui-tabs-nav a 
    {
        text-decoration: none;
        font-family: Arial;
        font-size: 12px;
    }
        
    .ui-tabs-nav a:hover 
    {
        text-decoration: none;
        font-family: Arial;
        font-size: 12px;        
    }
    
    a.lbk
    {
        color: #000;
        text-decoration: none;
        font-weight:bold;
        font-size: 12px;
        font-family:Arial;
    }
    
    a.lbk:hover
    {
        color: #000;
        text-decoration: underline;
        font-weight: bold;
        font-size: 12px;
        font-family: Arial;
    }
    
    .form_search
    {
        float:left;
        width: 100%;
        height:10px;
        padding-top: 4px;
        padding-bottom: 10px;
        margin: 0 auto 20px auto;
        background-color: #323232;
        color: #FFFFFF;
    }    
    
    fieldset { padding:0; border:0; margin-top:25px; }
    
    #dialog-form
    {
        display:none;
        height:auto;
        color: #000;
        text-decoration: none;
        font-weight: bold;
        font-size: 12px;
        font-family: Arial;
    }
    
    #dialog-form p.newDesc { color: #000; }
    
    .ui-widget-header  
    {
        border: 1px solid #aaaaaa; 
        color:White;
        font-weight: bold;
        font-size: 12px;
        font-family: Arial;                     
    }
    
    #dialog-form-help
    {
        display:none;
        height:auto;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family: Arial;
    }
    
    #dialog-form-help p.newDesc { color: #000; }
    
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div id="dialog-form-help" title="<%= getResource("advSMainTitulo") %>">
	<p class="newDesc"><%= getResource("advSdiHelp1")%></p>
    <p class="newDesc"><%= getResource("advSdiHelp2")%></p>
    <p class="newDesc">&nbsp;</p>
    <p class="newDesc"><%= getResource("advSdiHelp3")%></p>
    <p class="newDesc"><%= getResource("advSdiHelp4")%></p>
    <p class="newDesc"><%= getResource("advSdiHelp5")%></p>
    <p class="newDesc"><%= getResource("advSdiHelp6")%></p>
    <p class="newDesc"><%= getResource("advSdiHelp7")%></p>
    <p class="newDesc"><%= getResource("advSdiHelp8")%></p>
    <p class="newDesc">&nbsp;</p>
    <p class="newDesc"><%= getResource("advSdiHelp9")%></p>
    <p class="newDesc">&nbsp;</p>
    <p class="newDesc"><%= getResource("advSdiHelp10")%></p>
    <p class="newDesc"><%= getResource("advSdiHelp11")%></p>
    <p class="newDesc">&nbsp;</p>
    <p class="newDesc"><%= getResource("advSdiHelp12")%></p>
    <p class="newDesc"><%= getResource("advSdiHelp13")%></p>
</div>

<div id="dialog-form" title="<%= getResource("advSdlTitulo") %>">
	<p class="newDesc"><%= getResource("advSdlDesc")%></p>
		<fieldset>
			<label for="name"><%= getResource("advSdlNome")%></label>
            <input type="text" runat="server" name="name" id="name" value="" style="width:90%;"/><br />
		</fieldset>
    <p>
        <asp:LinkButton ID="lb_guardar" CssClass="btn-pdfs" ForeColor="White" runat="server" OnClientClick='$("#dialog-form").dialog("close");' OnClick="Guardar_Click"><%= getResource("advSdlButton")%></asp:LinkButton>
    </p>
</div>

<div id="Geral" style="float:left; padding:0px; margin:0px; height:100%; width:800px;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:12px">
            <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false" />
            <input type="hidden" id="txt_aux_novo" runat="server" name="name" value="" />
        </div>
    </div>
    <div id="section" class="sector ui-corner-top" style="float:left;width:100%;">
        <div id="titulo" class="form_search headbg ui-corner-top" style="float:left;width:100%;">
            <div id="caixa" style="float:left;width:80%;">
                &nbsp;<asp:Label runat="server" ID="lblTitleAdvSearch" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" Text=" <%$Resources:insigte.language,advSMainTitulo%>" />
            </div>
            <div id="help" style="float:left;width:20%; ">
                <div style="float:right;padding-right:5px;">
                    <a id="ajuda-adv" href="#"><img alt="Ajuda" src="Imgs/icons/white/png/info_icon_16.png" /></a>
                </div>
            </div>
        </div>
        <div id="Buttao" style="float:left;background-color:#ffffff; margin:5px 0px 0px 10px; width:94%;">
            <div id="Div2" style="float:left; margin:0px 0px 0px 0px;">
                <asp:DropDownList runat="server" ID="DDSearchs" OnSelectedIndexChanged="getSelected" AutoPostBack="true" >
                    <asp:ListItem selected="True" Text="<%$Resources:insigte.language,advSMainNew%>" Value="-1"></asp:ListItem>
                </asp:DropDownList>&nbsp;&nbsp;
                <a id="criar-novo" class="btn-pdfs" href="#"><%= getResource("advSdlButton")%></a>&nbsp;&nbsp;
                <asp:LinkButton ID="lnkApagar" CssClass="btn-pdfs" runat="server" OnClick="Apagar_Click" OnClientClick='return chkconfirm();'><%= getResource("advSMainDelBtn")%></asp:LinkButton>
            </div>
            <div id="Div1" style="float:right; margin:0px 0px 0px 0px;">
                <asp:LinkButton ID="lnkPesquisa" CssClass="btn-pdfs" runat="server" OnClick="Pesquisar_Click"><%= getResource("advSMainBtnSearch")%></asp:LinkButton>
            </div>
        </div>
        <div id="formSearch" style="float:left;background-color:#ffffff; margin:5px 0px 0px 10px; width:94%;">
            <div id="texto" class="ui-corner-all in-cli-small-search" style="margin-top:10px; height:22px; width:100%;">
                <div style=" margin:1px; vertical-align:middle; height:100%; width:100%;">
                    <asp:TextBox runat="server" BorderColor="#ffffff"  BorderWidth="0px"  Font-Names="Arial" Font-Size="12px" onkeydown="return (event.keyCode!=13);" ID="tbSearchAdvance" Text="" Font-Italic="true" Width="100%"/>
                </div>
            </div>
            <div id="temas" style="background-color:#ffffff;margin:20px 0px 10px 0px;">
                <div style="float:left; width:25%;background-color:#ffffff;">
                    <asp:Label ID="Label1" runat="server" Text="<%$Resources:insigte.language,advSMainTemasTitulo%>"></asp:Label>
                    <asp:DropDownList runat="server" ID="DDL_Temas">
                        <asp:ListItem selected="True" Text="<%$Resources:insigte.language,advSMainTemasItem1%>" Value="1"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div style="float:left; width:25%;background-color:#ffffff;">
                    <asp:Label ID="Label2" runat="server" Text="<%$Resources:insigte.language,advSMainPeriodoTitulo%>"></asp:Label>
                    <asp:DropDownList runat="server" ID="DDL_Data">
                        <asp:ListItem Text="<%$Resources:insigte.language,advSMainPeriodoItem1%>" Value="1" Selected="true"></asp:ListItem>
                        <asp:ListItem Text="<%$Resources:insigte.language,advSMainPeriodoItem2%>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$Resources:insigte.language,advSMainPeriodoItem3%>" Value="31"></asp:ListItem>
                        <asp:ListItem Text="<%$Resources:insigte.language,advSMainPeriodoItem4%>" Value="91"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div style="float:right; width:45%;background-color:#ffffff;">
                    <asp:Label ID="Label3" runat="server" Text="<%$Resources:insigte.language,advSMainDataDe%>"></asp:Label>
                    <asp:TextBox runat="server" ID="tbDateDe" Font-Names="Arial" Font-Size="12px" Width="80px" />
                    <asp:Label ID="Label4" runat="server" Text="<%$Resources:insigte.language,advSMainDataAte%>"></asp:Label>
                    <asp:TextBox runat="server" ID="tbDateAte" Font-Names="Arial" Font-Size="12px" Width="80px" />
                </div>
            </div>
            <div id="ListEditors" style="background-color:#ffffff;float:left; width:100%;margin:20px 0px 10px 0px;" >
                <div><asp:Label ID="Label5" runat="server" Text="<%$Resources:insigte.language,advSMainEditores%>"></asp:Label></div>
                <div id="tabs">
                    <ul>
                        <li><a href="#tabs-1"><%= getResource("advSMainEditoresT1")%></a></li>
                        <li><a href="#tabs-2"><%= getResource("advSMainEditoresT2")%></a></li>
                        <li><a href="#tabs-3"><%= getResource("advSMainEditoresT3")%></a></li>
                        <li><a href="#tabs-4"><%= getResource("advSMainEditoresT5")%></a></li>
                        <li><a href="#tabs-5"><%= getResource("advSMainEditoresT4")%></a></li>
                    </ul>
                    <div id="tabs-1">
                        <p>
                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="100px" ID="LbEditors" ondblclick="lbEditores_DoubleClick(this.value,this.id)" SelectionMode="Multiple"></asp:ListBox>
                        </p>
                    </div>
                    <div id="tabs-2">
                        <p>
                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="100px" ID="LbEditorsAO" ondblclick="lbEditores_DoubleClick(this.value,this.id)" SelectionMode="Multiple"></asp:ListBox>
                        </p>
                    </div>
                    <div id="tabs-3">
                        <p>
                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="100px" ID="LbEditorsMZ" ondblclick="lbEditores_DoubleClick(this.value,this.id)" SelectionMode="Multiple"></asp:ListBox>
                        </p>
                    </div>
                    <div id="tabs-4">
                        <p>
                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="100px" ID="LbEditorsPT" ondblclick="lbEditores_DoubleClick(this.value,this.id)" SelectionMode="Multiple"></asp:ListBox>
                        </p>
                    </div>
                    <div id="tabs-5">
                        <p>
                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="100px" ID="LbEditorsOU" ondblclick="lbEditores_DoubleClick(this.value,this.id)" SelectionMode="Multiple"></asp:ListBox>
                        </p>
                    </div>
                </div>
                <div id="SelectedEditors" style="float:left; width:100%; margin-top:10px;">
                    <textarea cols="50" rows="2" runat="server" ID="ta_editores_codes" style="width: 354px; height:50px;"><%# getResource("geralTagTodos") %></textarea><br />
                </div>

            </div>
            <div id="ListJornalistas" style="background-color:#ffffff;float:left; width:100%;margin:10px 0px 10px 0px;" >
                <div><asp:Label ID="Label6" runat="server" Text="<%$Resources:insigte.language,advSMainJornalistas%>"></asp:Label></div>
                <div id="tabs2">
                    <ul>
                        <li><a href="#tabs2-1">A-D</a></li>
                        <li><a href="#tabs2-2">E-H</a></li>
                        <li><a href="#tabs2-3">I-L</a></li>
                        <li><a href="#tabs2-4">M-P</a></li>
                        <li><a href="#tabs2-5">K-T</a></li>
                        <li><a href="#tabs2-6">U-X</a></li>
                        <li><a href="#tabs2-7">Y-#</a></li>
                    </ul>
                    <div id="tabs2-1">
                        <p>
                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="100px" ID="LbJornalistAD" ondblclick="lbJornalists_DoubleClick(this.value,this.id)" SelectionMode="Multiple"></asp:ListBox>
                        </p>
                    </div>
                    <div id="tabs2-2">
                        <p>
                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="100px" ID="LbJornalistEH" ondblclick="lbJornalists_DoubleClick(this.value,this.id)" SelectionMode="Multiple"></asp:ListBox>
                        </p>
                    </div>
                    <div id="tabs2-3">
                        <p>
                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="100px" ID="LbJornalistIL" ondblclick="lbJornalists_DoubleClick(this.value,this.id)" SelectionMode="Multiple"></asp:ListBox>
                        </p>
                    </div>
                    <div id="tabs2-4">
                        <p>
                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="100px" ID="LbJornalistMP" ondblclick="lbJornalists_DoubleClick(this.value,this.id)" SelectionMode="Multiple"></asp:ListBox>
                        </p>
                    </div>
                    <div id="tabs2-5">
                        <p>
                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="100px" ID="LbJornalistQT" ondblclick="lbJornalists_DoubleClick(this.value,this.id)" SelectionMode="Multiple"></asp:ListBox>
                        </p>
                    </div>
                    <div id="tabs2-6">
                        <p>
                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="100px" ID="LbJornalistUX" ondblclick="lbJornalists_DoubleClick(this.value,this.id)" SelectionMode="Multiple"></asp:ListBox>
                        </p>
                    </div>
                    <div id="tabs2-7">
                        <p>
                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="100px" ID="LbJornalistYN" ondblclick="lbJornalists_DoubleClick(this.value,this.id)" SelectionMode="Multiple"></asp:ListBox>
                        </p>
                    </div>
                </div>
                <div id="SelectedJornalists" style="float:left; width:100%; margin-top:10px;">
                    <textarea cols="50" rows="2" runat="server" ID="ta_jornalists_codes" style="width: 354px; height:50px;"></textarea><br />
                </div>
            </div> 
            <div id="Outros" style="background-color:#ffffff;float:left; width:100%;margin:20px 0px 10px 0px;">
                <div style="float:left; width:25%;">
                    <asp:Label ID="Label7" runat="server" Text="<%$Resources:insigte.language,advSMainTipoTitulo%>"></asp:Label>
                    <asp:DropDownList ID="DropDownMeio" runat="server">
                        <asp:ListItem Text="<%$Resources:insigte.language,advSMainTipoTitulo%>" Value="1" Selected="true"></asp:ListItem>
                        <asp:ListItem Text="<%$Resources:insigte.language,advSMainTipoItem2%>" Value="Imprensa"></asp:ListItem>
                        <asp:ListItem Text="<%$Resources:insigte.language,advSMainTipoItem3%>" Value="Online"></asp:ListItem>
                        <asp:ListItem Text="<%$Resources:insigte.language,advSMainTipoItem4%>" Value="Rádio"></asp:ListItem>
                        <asp:ListItem Text="<%$Resources:insigte.language,advSMainTipoItem5%>" Value="Televisão"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div style="float:left; width:35%;">
                    <asp:Label ID="Label9" runat="server" Text="<%$Resources:insigte.language,advSMainClassTitulo%>"></asp:Label>
                    <asp:DropDownList ID="DropDownSubject" runat="server">
                        <asp:ListItem Text="<%$Resources:insigte.language,advSMainClassItem1%>" Value="1" Selected="true"></asp:ListItem>
                    </asp:DropDownList> 
                </div>
                <div style="float:right; width:35%;">
                <asp:Label ID="LbCountry" runat="server" Text="<%$Resources:insigte.language,advSMainPaisTitulo%>"></asp:Label>
                    <asp:DropDownList ID="DropDownCountry" runat="server">
                        <asp:ListItem Text="<%$Resources:insigte.language,advSMainPaisItem1%>" Value="1" Selected="true"></asp:ListItem>
                        <asp:ListItem Text="<%$Resources:insigte.language,advSMainPaisItem2%>" Value="Angola"></asp:ListItem>
                        <asp:ListItem Text="<%$Resources:insigte.language,advSMainPaisItem3%>" Value="Moçambique"></asp:ListItem>
                        <asp:ListItem Text="<%$Resources:insigte.language,advSMainPaisItem4%>" Value="Portugal"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div id="SubmitBtn" style="background-color:#ffffff;float:right; width:100%;margin:20px 0px 20px 0px;">
                <asp:LinkButton ID="Lb_Pesquisa" CssClass="btn-pdfs" runat="server" OnClick="Pesquisar_Click"><%= getResource("advSMainBtnSearch")%></asp:LinkButton>&nbsp;&nbsp;<a id="criar-novo-bottom" class="btn-pdfs" href="#"><%= getResource("advSdlButton")%></a>
            </div>
        </div>
    </div>
</div>
</asp:Content>
