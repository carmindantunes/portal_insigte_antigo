﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Lists.ascx.cs" Inherits="Insigte.UserControls.Lists" %>

<div class="ui-corner-top in-cli-usercontrols" style="height: 25px; width: 180px;">
    <div style="padding-top: 4px; padding-left: 5px;float:left;">
        <asp:Label runat="server" Text="<%$Resources:insigte.language,ucListTitulo%>" ID="Lb_Titulo" ForeColor="white" Font-Names="Arial" Font-Size="12px" />
    </div>
    <div style="padding-top: 4px; padding-left: 5px; padding-right: 5px; float:right">
        <asp:Label runat="server" ForeColor="#00add9" Text="" Font-Bold="true" ID="Label1"  Font-Names="Arial" Font-Size="12px" />
    </div>
</div>
<div class="ui-corner-bottom" style="width: 180px; padding-top: 5px; background-color: #F0F0F0;">
    <asp:Table ID="tblArchives" runat="server">
        <asp:TableRow ID="TableRow1" runat="server">
            <asp:TableCell ID="TableCell2" runat="server" VerticalAlign="Middle" >
            </asp:TableCell>
            <asp:TableCell ID="TableCell3" runat="server">
                <a href="pr.aspx" style="color:#000000; font-size:12px; font-family:Arial;" target="_self"><asp:Literal ID="Literal1" runat="server" Text="<%$Resources:insigte.language,topMnSearch%>" />
                </a></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow10" runat="server">
            <asp:TableCell ID="TableCell0" runat="server" VerticalAlign="Middle" >
            </asp:TableCell>
            <asp:TableCell ID="TableCell1" runat="server">
                <a href="listas.aspx" style="color:#000000; font-size:12px; font-family:Arial;" target="_self"><asp:Literal ID="LtLink1" runat="server" Text="<%$Resources:insigte.language,ucPastasLink1%>" />
                </a></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow2" runat="server" Visible="true">
            <asp:TableCell ID="TableCell4" runat="server" VerticalAlign="Middle" >
            </asp:TableCell>
            <asp:TableCell ID="TableCell5" runat="server">
                <asp:LinkButton ID="LinkButton1" runat="server" Text="<%$Resources:insigte.language,ucPastasLink2%>" OnClientClick="openWindow('listscfg.aspx')" Font-Names="Arial" Font-Size="12px" ForeColor="#000000" Font-Underline="false"></asp:LinkButton><br />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</div>