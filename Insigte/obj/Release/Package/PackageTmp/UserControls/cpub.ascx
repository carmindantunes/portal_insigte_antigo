﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="cpub.ascx.cs" Inherits="Insigte.UserControls.cpub" %>
<script type="text/javascript">
    function openWindow(url) {
        var w = window.open(url, '', 'width=400,height=400,toolbar=0,status=0,location=0,menubar=0,directories=0,resizable=1,scrollbars=1');
        w.focus();
    }
</script>
<div class="ui-corner-top in-cli-usercontrols" style="height: 25px; width: 180px;">
    <div style="padding-top: 4px; padding-left: 5px; float:left;">
        <asp:Label runat="server" Text="<%$Resources:insigte.language,ucCpubTitulo%>" ID="Lb_Titulo" ForeColor="white" Font-Names="Arial" Font-Size="12px" />
    </div>
    <div style="padding-top: 4px; padding-left: 5px; padding-right: 5px; float:right">
        <asp:Label runat="server" ForeColor="#00add9" Text="" ID="Label1" Font-Bold="true" Font-Names="Arial" Font-Size="12px" />
    </div>
</div>

<div class="ui-corner-bottom" style="width: 180px; padding-top:5px; background-color: #F0F0F0;">
    <table>
        <tr>
            <td></td>
            <td><a href="cpnews.aspx" style="color:#000000; font-size:12px; font-family:Arial;" target="_self"><asp:Literal ID="LtLink1" runat="server" Text="<%$Resources:insigte.language,ucCpNl4%>" /></a>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:LinkButton ID="Lb_NlCp" runat="server" ForeColor="Black" Font-Names="Arial" Font-Size="12px" Text="<%$Resources:insigte.language,ucCpNl3%>" OnClick="NewsL_Click"></asp:LinkButton>
            </td>
        </tr>
        <asp:Repeater ID="cpubRep" runat="server">
            <ItemTemplate>
                <tr>
                    <td></td>
                    <td><%# "<a href=cpnews.aspx?tp=" + Eval("ID_TEMA").ToString() + " style='color:#000000; font-size:12px; font-family:Arial;' target='_self'>" + Eval("DES_TEMA") + "</a><br>" %>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
</div>