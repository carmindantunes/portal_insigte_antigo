﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PastRmv.aspx.cs" Inherits="Insigte.PastRmv" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Carma | Insigte now is Carma</title>
<link rel="icon" type="ico" href="https://www.carma.com/app/themes/carma/assets/images/favicon.ico" />
    <link href="Styles/StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body onunload='javaScript:window.opener.location=window.opener.location;'>
    <form id="form1" runat="server">
        <% = "<div style=\"width: 400px; float: left; clear: both; color: #ffffff; height: 67px; background-image: url('Cli/" + global_asax.InsigteManager[Session.SessionID].CodCliente + "/fundo_small.png'); background-repeat:no-repeat; background-color: #003f56;\"> "%>
        </div>
        <div id="divMain" style="width: auto; height:auto; text-align: justify; clear: both; padding-bottom: 10px;">
            <asp:Table ID="Table1" runat="server">
                <asp:TableRow ID="TableRow2" runat="server">
                    <asp:TableCell ID="TableCell3" runat="server" HorizontalAlign="Left">
                        <asp:Label ID="lbPasta" Text="" runat="server" style="color:#000000; font-size:12px; font-family:Arial;"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow runat="server" ID="TableRow5">
                    <asp:TableCell ID="TableCell10" runat="server">        
                        <div style="float:left;width:50%">
                            <asp:Button runat="server" Text="Apagar" Font-Names="Arial" Font-Size="12px" ID="btApagar" OnClick="btApagar_Click" CssClass="btn-pdfs"/>
                        </div>
                        <div style="float:right;width:50%">
                            <asp:Button runat="server" Text="<%$Resources:insigte.language,DefCancelar%>" Font-Names="Arial" Font-Size="12px" ID="btCancelar" OnClick="cancelar_click" CssClass="btn-pdfs"/>
                        </div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>
    </form>
</body>
</html>