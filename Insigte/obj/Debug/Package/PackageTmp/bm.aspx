﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="bm.aspx.cs" Inherits="Insigte.bm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    function lbBoxes_DoubleClick(value, vleID) {
        vleID = vleID.replace(vleID, "MainContent_ta_words_codes");
        //vleID = vleID.replace("LbEditors", "ta_words_codes");

        if ($('textarea[id^="' + vleID + '"]').tagExist(value)) {
            alert("Palavra '" + value + "' já Existe");
        } else {
            if (value == "Todos") {
                $('textarea[id^="' + vleID + '"]').importTags('');
                $('textarea[id^="' + vleID + '"]').addTag(value);
            }
            else {
                if ($('textarea[id^="' + vleID + '"]').tagExist("Todos")) {
                    $('textarea[id^="' + vleID + '"]').importTags('');
                    $('textarea[id^="' + vleID + '"]').addTag(value);
                } else {
                    $('textarea[id^="' + vleID + '"]').addTag(value);
                }
            }
        }
    };

    $(function () {
        $('textarea[id^="MainContent_ta_words_codes"]').tagsInput({
            width: 'auto',
            height: '288px',
            interactive: false
        });
    });

    $(function () {
        $("#dialog-form-help").dialog({
            autoOpen: false,
            height: 'auto',
            width: 450,
            modal: true
        });
    });

    $(function () {
        $("#ajuda-bm").click(function () {
            $("#dialog-form-help").dialog("open");
            });
    });

</script>

<style type="text/css">
    #section.sector
    {
        float:left;
        border :1px solid Black;
        width: 100%;
        min-height: 300px;
        min-width:400px;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
        padding:0px;
        margin:0px;
    }
    .form_search
    {
        float:left;
        width: 100%;
        height:10px;
        padding-top: 4px;
        padding-bottom: 10px;
        margin: 0 auto 20px auto;
        background-color: #323232;
        color: #FFFFFF;
    }    
    
    #dialog-form-help
    {
        height:auto;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family: Arial;
    }
    
    #dialog-form-help p.newDesc { color: #000; }
    
    .ui-widget-header  
    {
        border: 1px solid #aaaaaa; 
        color:White;
        font-weight: bold;
        font-size: 12px;
        font-family: Arial;                     
    }
    
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div id="dialog-form-help" title="Brandmining">
	    <p class="newDesc">"Organizations are like human beings, in that we all have some similar characteristics, but each is also very unique. Understanding the distinguishing characteristics of an organization often determines the success or failure"
    Michael W. Howe</p>
        <p class="newDesc">O brand mining é uma aplicação tecnológica - desenvolvida pela insigte – que através de algoritmos linguístico-computacionais [text mining] afere o impacto e a percepção que a informação provocou junto dos stakeholders da organização. Desta forma, permite ter o conhecimento e a dimensão: dos conceitos associados à marca; dos valores organizacionais; das entidades relacionadas; das características da personalidade organizacional e dos públicos-alvo, sectores e geografias mais relevantes.</p>
        <p class="newDesc">Este projecto é desenvolvido pelo Núcleu de Investigação da insigte. Está em constante aperfeiçoamento e irá extender-se às àreas de reputation management e competitive intelligence.</p>
    </div>

<div id="Geral" style="float:left; padding:0px; margin:0px; height:100%; width:800px;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:12px">
            <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false" />
        </div>
    </div>
    <div id="section" class="sector ui-corner-top" style="float:left;width:100%;">
        <div id="titulo" class="form_search headbg ui-corner-top" style="float:left;width:100%;">
            <div id="caixa" style="float:left;width:80%;">
                &nbsp;<asp:Label runat="server" ID="lblTitleAdvSearch" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" Text=" Brandmining" />
            </div>
            <div id="help" style="float:left;width:20%; ">
                <div style="float:right;padding-right:5px;">
                    <a id="ajuda-bm" href="#"><img alt="Ajuda" src="Imgs/icons/white/png/info_icon_16.png" /></a>
                </div>
            </div>
        </div>
        <div id="sectionBody" style="float:left;width:100%;">
            <div id="SubMitHeader" style="float:left;width:100%;min-height:50px; vertical-align:middle;">
                <div id="Information" style="line-height:4em;float:left;width:40%;">
                    <asp:Label ID="Titulo" runat="server" Font-Names="Arial" Font-Size="12px"></asp:Label>
                </div>
                <div id="CBTemas" style="line-height:4em;float:right;width:60%; text-align:right;"> 
                    <div style="float:right;width:100%">
                        <asp:Label ID="Lb_ListTemas" runat="server" Font-Names="Arial" Font-Size="12px"></asp:Label>
                        <asp:DropDownList ID="DDListTemas" runat="server" AutoPostBack="true" onselectedindexchanged="DDListTemas_SelectedIndexChanged"></asp:DropDownList>
                        <asp:Label ID="Lb_ListSubTemas" runat="server" Font-Names="Arial" Font-Size="12px" Visible="false"></asp:Label>
                        <asp:DropDownList ID="DDListSubTemas" runat="server" AutoPostBack="true" onselectedindexchanged="DDListSubTemas_SelectedIndexChanged"></asp:DropDownList>
                        &nbsp;&nbsp;<asp:CheckBox ID="ChKCompare" runat="server" Text="Comparar?" />
                        &nbsp;&nbsp;<asp:LinkButton ID="Submit" CssClass="btn-pdfs" runat="server" OnClick="Submit_Click" Visible="false">Submit</asp:LinkButton>&nbsp;&nbsp;
                    </div>
                </div>
            </div>
            <div id="Boxes" style="float:left;width:100%;">
                <div id="BoxLL" style="float:left;width:25%;">
                    <div style="margin:10px 10px 10px 10px;">
                        <p>
                            <asp:Label ID="LCARAC" runat="server">Características</asp:Label>
                        </p>
                        <asp:ListBox ID="LBox_CARAC" runat="server" Font-Names="Arial" Font-Size="12px" Width="100%" Height="100px" SelectionMode="Multiple"></asp:ListBox>
                    </div>
                    <div style="margin:10px 10px 10px 10px;">
                        <p>
                            <asp:Label ID="Label1" runat="server">Conceitos</asp:Label>
                        </p>
                        <asp:ListBox ID="LBox_CONCEITO" runat="server" Font-Names="Arial" Font-Size="12px" Width="100%" Height="100px" SelectionMode="Multiple"></asp:ListBox>
                    </div>
                </div>
                <div id="BoxLR" style="float:left;width:25%;">
                    <div style="margin:10px 10px 10px 10px;">
                        <p>
                            <asp:Label ID="Label2" runat="server">Entidades</asp:Label>
                        </p>
                        <asp:ListBox ID="LBox_ENTIDADE" runat="server" Font-Names="Arial" Font-Size="12px" Width="100%" Height="100px" SelectionMode="Multiple"></asp:ListBox>
                    </div>
                    <div style="margin:10px 10px 10px 10px;">
                        <p>
                            <asp:Label ID="Label3" runat="server">Geografia</asp:Label>
                        </p>
                        <asp:ListBox ID="LBox_GEO" runat="server" Font-Names="Arial" Font-Size="12px" Width="100%" Height="100px" SelectionMode="Multiple"></asp:ListBox>
                    </div>
                </div>
                <div id="BoxRL" style="float:left;width:25%;">
                    <div style="margin:10px 10px 10px 10px;">
                        <p>
                            <asp:Label ID="Label4" runat="server">Valores</asp:Label>
                        </p>
                        <asp:ListBox ID="LBox_VALOR" runat="server" Font-Names="Arial" Font-Size="12px" Width="100%" Height="100px" SelectionMode="Multiple"></asp:ListBox>
                    </div>
                    <div style="margin:10px 10px 10px 10px;">
                        <p>
                            <asp:Label ID="Label5" runat="server">Stakeholders</asp:Label>
                        </p>
                        <asp:ListBox ID="LBox_STAKE" runat="server" Font-Names="Arial" Font-Size="12px" Width="100%" Height="100px" SelectionMode="Multiple"></asp:ListBox>
                    </div>
                </div>
                <div id="BoxRR" style="float:left;width:25%;">
                    <div style="margin:10px 10px 10px 10px;">
                        <p>
                            <asp:Label ID="Label6" runat="server">Sectores</asp:Label>
                        </p>
                        <asp:ListBox ID="LBox_SECTOR" runat="server" Font-Names="Arial" Font-Size="12px" Width="100%" Height="100px" SelectionMode="Multiple"></asp:ListBox>
                    </div>
                    <div style="margin:10px 10px 10px 10px;">
                        <p>
                            <asp:Label ID="Label7" runat="server">Top 10</asp:Label>
                        </p>
                        <asp:ListBox ID="LBox_RESULTADOS" runat="server" Font-Names="Arial" Font-Size="12px" Width="100%" Height="100px" SelectionMode="Multiple"></asp:ListBox>
                    </div>
                </div>
            </div>
            <div id="Div1" style="float:left;width:100%;display:none;">
                <div id="Div2" style="float:left;width:100%;">
                    <div style="margin:10px 10px 10px 10px; display:none;">
                        <p>
                            <asp:Label ID="Label16" runat="server">Geografia</asp:Label>
                        </p>
                        <asp:Chart runat="server" ID="ChartGeografiaBCK" Width="650px" Height="400px" Palette="Fire">
                            <Series><asp:Series Name="Series1"></asp:Series></Series>
                            <ChartAreas><asp:ChartArea Name="ChartArea1"></asp:ChartArea></ChartAreas>
                        </asp:Chart>
                    </div>
                </div>
            </div>
            <div id="Results" style="float:left;width:100%;">
                <div id="LbActions" style="float:left;width:50%;">
                    <div style="margin:10px 10px 10px 10px;">
                        <p>
                            <asp:Label ID="Label8" runat="server">Características</asp:Label>
                        </p>
                        <asp:Chart runat="server" ID="ChartCaracteristicas" Width="350px" Height="200px" Palette="Fire">
                            <Series><asp:Series Name="Series1"></asp:Series></Series>
                            <ChartAreas><asp:ChartArea Name="ChartArea1"></asp:ChartArea></ChartAreas>
                        </asp:Chart>
                    </div>
                    <div style="margin:10px 10px 10px 10px;">
                        <p>
                            <asp:Label ID="Label9" runat="server">Entidades</asp:Label>
                        </p>
                        <asp:Chart runat="server" ID="ChartEntidades" Width="350px" Height="200px" Palette="Fire">
                            <Series><asp:Series Name="Series1"></asp:Series></Series>
                            <ChartAreas><asp:ChartArea Name="ChartArea1"></asp:ChartArea></ChartAreas>
                        </asp:Chart>
                    </div>
                    <div style="margin:10px 10px 10px 10px;">
                        <p>
                            <asp:Label ID="Label10" runat="server">Conceitos</asp:Label>
                        </p>
                        <asp:Chart runat="server" ID="ChartConceitos" Width="350px" Height="200px" Palette="Fire">
                            <Series><asp:Series Name="Series1"></asp:Series></Series>
                            <ChartAreas><asp:ChartArea Name="ChartArea1"></asp:ChartArea></ChartAreas>
                        </asp:Chart>
                    </div>
                    <div style="margin:10px 10px 10px 10px;">
                        <p>
                            <asp:Label ID="Label11" runat="server">Geografia</asp:Label>
                        </p>
                        <asp:Chart runat="server" ID="ChartGeografia" Width="350px" Height="200px" Palette="Fire">
                            <Series><asp:Series Name="Series1"></asp:Series></Series>
                            <ChartAreas><asp:ChartArea Name="ChartArea1"></asp:ChartArea></ChartAreas>
                        </asp:Chart>
                    </div>
                </div>
                <div id="LbResults" style="float:left;width:50%;">
                    <div style="margin:10px 10px 10px 10px;">
                        <p>
                            <asp:Label ID="Label12" runat="server">Valores</asp:Label>
                        </p>
                        <asp:Chart runat="server" ID="ChartValores" Width="350px" Height="200px" Palette="Fire">
                            <Series><asp:Series Name="Series1"></asp:Series></Series>
                            <ChartAreas><asp:ChartArea Name="ChartArea1"></asp:ChartArea></ChartAreas>
                        </asp:Chart>
                    </div>
                    <div style="margin:10px 10px 10px 10px;">
                        <p>
                            <asp:Label ID="Label13" runat="server">Sectores</asp:Label>
                        </p>
                        <asp:Chart runat="server" ID="ChartSectores" Width="350px" Height="200px" Palette="Fire">
                            <Series><asp:Series Name="Series1"></asp:Series></Series>
                            <ChartAreas><asp:ChartArea Name="ChartArea1"></asp:ChartArea></ChartAreas>
                        </asp:Chart>
                    </div>
                    <div style="margin:10px 10px 10px 10px;">
                        <p>
                            <asp:Label ID="Label14" runat="server">Stakeholders</asp:Label>
                        </p>
                        <asp:Chart runat="server" ID="ChartStakeholders" Width="350px" Height="200px" Palette="Fire">
                            <Series><asp:Series Name="Series1"></asp:Series></Series>
                            <ChartAreas><asp:ChartArea Name="ChartArea1"></asp:ChartArea></ChartAreas>
                        </asp:Chart>
                    </div>
                    <div style="margin:10px 10px 10px 10px;">
                        <p>
                            <asp:Label ID="Label15" runat="server">Top 10</asp:Label>
                        </p>
                        <asp:Chart runat="server" ID="ChartTop10Palavras" Width="350px" Height="200px" Palette="Fire">
                            <Series><asp:Series Name="Series1"></asp:Series></Series>
                            <ChartAreas><asp:ChartArea Name="ChartArea1"></asp:ChartArea></ChartAreas>
                        </asp:Chart>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</asp:Content>
