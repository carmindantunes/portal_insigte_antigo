﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SubscribeLstAl.aspx.cs" Inherits="Insigte.SubscribeLstAl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="Geral" style="float:left; padding:0px; margin:0px; height:100%;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:12px">
            <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false" />
        </div>
    </div>
    <div class="form_caja">
        <asp:DataGrid runat="server" ID="rp_AlertsDisp" AutoGenerateColumns="false" AllowPaging="false"
         GridLines="None" CellPadding="0" CellSpacing="0" BorderWidth="0px" Visible="true"
         BorderStyle="None" Width="800px" Font-Names="Arial" Font-Size="11px" PageSize="30" AlternatingItemStyle-BackColor="#EEEFF0" PagerStyle-BackColor="#686C6E" 
         PagerStyle-Font-Size="12px" PagerStyle-ForeColor="#ffffff" PagerStyle-Font-Underline="false" PagerStyle-VerticalAlign="Middle" PagerStyle-HorizontalAlign="Center" PagerStyle-Mode="NumericPages">
            <ItemStyle Width="24px" BackColor="White" Height="24px"  />
            <AlternatingItemStyle  BackColor="#EEEFF0" />
            <HeaderStyle BackColor="#8D8D8D" Height="24px" Font-Names="Arial" Font-Size="12px"  CssClass="headbg" />
            <FooterStyle  Height="24px" Font-Names="Arial" Font-Size="12px"  CssClass="headbg" />
          
            <Columns>
                <asp:TemplateColumn ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="24" >
                    <HeaderTemplate  >
                        <div>
                            <asp:CheckBox ID="chkSelAll" runat="server" OnCheckedChanged="chkSelAll_changed" AutoPostBack="true" />
                        </div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox Checked='false' Font-Names="Arial" NewsId='<%# DataBinder.Eval(Container.DataItem, "COD_ALERT_TEMAS").ToString() %>' NewsName='<%# DataBinder.Eval(Container.DataItem, "name").ToString() %>'  Font-Size="12px" runat="server" ToolTip="" ID="chka_AddPDF" Visible='true' AutoPostBack="false" /> 
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="<%$Resources:insigte.language,ucNwlLink3%>" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="left" ItemStyle-ForeColor="#000000" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <span><%# DataBinder.Eval(Container.DataItem, "name")%></span>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
    </div>
    <div style="float: left; padding: 5px,0px,5px,0px; width:98%">
        <div style="float:left; width:100%"> 
            
            <div style="float:Right; width:60%; vertical-align:middle">
                <div style="float:right;">
                    <div style="margin-left:5px; float: right;">
                        <asp:LinkButton  runat="server" Visible="true" ID="btnGuardar" OnClick="btnGuardar_Click" CssClass="btn-pdfs" Text="<%$Resources:insigte.language,advSdlButton%>" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false"  />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</asp:Content>
