﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="sExecutivo.aspx.cs" Inherits="Insigte.sExecutivo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <script type="text/javascript">
        function openReportWindow(url) {
            var w = window.open(url, '', 'width=1200,height=800,toolbar=0,status=0,location=0,menubar=0,directories=0,resizable=1,scrollbars=1');
            w.focus();
        }
    </script>

<style type="text/css">
      
    
    .ui-tabs-nav a 
    {
        text-decoration: none;
        font-family: Arial;
        font-size: 12px;
    }
        
    .ui-tabs-nav a:hover 
    {
        text-decoration: none;
        font-family: Arial;
        font-size: 12px;        
    }
    
    #section.sector
    {
        float:left;
        border :1px solid Black;
        width: 100%;
        min-height: 300px;
        min-width:400px;
        width:100%;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
        padding:0px;
        margin:0px;
    }
    
    a.lbk
    {
        color: #000;
        text-decoration: none;
        font-weight:bold;
        font-size: 12px;
        font-family:Arial;
    }
    
    a.lbk:hover
    {
        color: #000;
        text-decoration: underline;
        font-weight: bold;
        font-size: 12px;
        font-family: Arial;
    }
    
    .form_search
    {
        float:left;
        width: 100%;
        height:10px;
        padding-top: 4px;
        padding-bottom: 10px;
        margin: 0 auto 20px auto;
        background-color: #323232;
        color: #FFFFFF;
    }
     
    .linhaX
    {
        float:left;
        width:100%;
        height:46px;
        vertical-align:middle;
        background-color:#F0F0F0;
    }
    
    .linhaY
    {
        float:left;
        width:100%;
        height:46px;
        vertical-align:middle;
        background-color:#FFFFFF;
    }
    
    .linhaX span { padding:5px; }
    .linhaY span { padding:5px; }
    
    
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div id="Geral" style="float:left; padding:0px; margin:0px; height:100%; width:800px;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:12px">
            <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false" />
        </div>
    </div>

    <div id="section" class="sector ui-corner-top" style="float:left;width:100%;">
        <div id="titulo" class="form_search headbg ui-corner-top">
            &nbsp;&nbsp;<asp:Label runat="server" ID="LbInfo" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF">Summario Executivo</asp:Label>
        </div>
        <div id="sectionBody" style="float:left; margin: 5px 0px 5px 0px;float:left; width:100%;">
            <div id="omfg" style="float:left; width:100%; padding: 5px; margin: 5px;">
                Clientes: <asp:DropDownList ID="ddCliente" runat="server" AutoPostBack="True" onselectedindexchanged="ddCliente_SelectedIndexChanged"></asp:DropDownList>&nbsp;
                Temas: <asp:DropDownList ID="ddTemas" runat="server" AutoPostBack="True" onselectedindexchanged="ddTemas_SelectedIndexChanged"></asp:DropDownList>&nbsp;
            </div>
            <div id="info" style="float:left; width:100%;">
                <asp:Repeater ID="rep_newsToday" runat="server">
                    <ItemTemplate>
                        <div class="<%= getLinha() %>">
                            <div style="padding:3px;">

                                <div id="LinhaTitulo" style="float:left; width:100%;">
                                    <div style="float:left; width:3%; vertical-align:middle; text-align:center; "><asp:CheckBox ID="chk_news" runat="server" /></div>
                                    <div style="float:left; width:80%; line-height:20px; text-align:left;"><%# Eval("TITLE").ToString() %></div>
                                    <div style="float:left; width:10%; text-align:center;"><%# Convert.ToDateTime(Eval("DATE").ToString()).ToString("dd-MM-yyyy") %></div>
                                    <div style="float:left; width:7%; text-align:center;"><%# Convert.ToDateTime(Eval("DATE").ToString()).ToString("HH:mm:ss") %></div>
                                </div>

                                
                                <div id="LinhaInfo" style="float:left; width:100%;">
                                    <div style="float:left; width:3%; vertical-align:middle; text-align:center; ">&nbsp;</div>
                                    <div style="float:left; width:97%; line-height:20px; text-align:left;">
                                        <%# getMainLink(Eval("tipo").ToString(), Eval("filepath").ToString(), Eval("page_url").ToString()) + " " + Eval("EDITOR").ToString() + getRelacao(Eval("IDMETA").ToString())%>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>

                
            </div>
        </div>
        

    </div>
</div>



</asp:Content>
