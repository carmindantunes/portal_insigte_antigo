﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Reports.ascx.cs" Inherits="Insigte.UserControls.Reports" %>
<div class="ui-corner-top in-cli-usercontrols" style="height: 25px; width: 180px;">
    <div style="padding-top: 4px; padding-left: 5px; float: left;">
        <asp:Label runat="server" Text="<%$Resources:insigte.language,ucRpLabelTitulo%>"
            ID="Lb_Titulo" ForeColor="white" Font-Names="Arial" Font-Size="12px" />
    </div>
    <div style="padding-top: 4px; padding-left: 5px; padding-right: 5px; float: right">
        <asp:Label runat="server" ForeColor="#f89c3e" Text="" Font-Bold="true" ID="Label1"
            Font-Names="Arial" Font-Size="12px" />
    </div>
</div>
<div class="ui-corner-bottom" style="width: 180px; padding-top: 5px; background-color: #d7d7d7;">
    <asp:Table ID="Table1" runat="server">
        <asp:TableRow ID="TableRow1" runat="server">
            <asp:TableCell ID="TableCell1" runat="server">
                <asp:LinkButton ID="LinkButton1" runat="server" ForeColor="Black" Font-Names="Arial"
                    Font-Size="12px" Text="<%$Resources:insigte.language,ucRpLLink1%>" OnClick="btnReport_Click"></asp:LinkButton>
            </asp:TableCell>
         </asp:TableRow>
         <%--<asp:TableRow ID="TableRow2" runat="server">    
            <asp:TableCell ID="TableCell2" runat="server">
                <asp:LinkButton ID="lnkConfig" runat="server" ForeColor="Black" Font-Names="Arial"
                    Font-Size="12px" Text="Configuração" PostBackUrl="~/ReportConfig.aspx">
                </asp:LinkButton>
            </asp:TableCell>
        </asp:TableRow>--%>
    </asp:Table>
</div>
