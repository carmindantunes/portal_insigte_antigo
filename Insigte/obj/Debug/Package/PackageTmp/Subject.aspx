﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Subject.aspx.cs" Inherits="Insigte.Subject" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        var _gaq = _gaq || [];

        _gaq.push(['_setAccount', 'UA-33483716-1']);

        _gaq.push(['_trackPageview']);

        (function () {

            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

        })();
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="Geral" style="float:left; padding:0px; margin:0px;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%">
        <div style="margin-top:12px">
            <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false"/><asp:Label runat="server" ID="lblTema" Font-Names="Arial" Font-Size="12px" />
        </div>
    </div>
    <div class="form_caja">
    <asp:DataGrid runat="server" ID="dgSubjectACN" AutoGenerateColumns="false" AllowPaging="True"
     GridLines="None" CellPadding="0" CellSpacing="0" BorderWidth="0px" 
     BorderStyle="None" Width="800px" Font-Names="Arial" Font-Size="11px" PageSize="40" AlternatingItemStyle-BackColor="#EEEFF0" PagerStyle-BackColor="#686C6E" 
     PagerStyle-Font-Size="12px" PagerStyle-ForeColor="#ffffff" OnPageIndexChanged="dgSubjectACN_PageIndexChanged" PagerStyle-Font-Underline="false" PagerStyle-VerticalAlign="Middle" PagerStyle-HorizontalAlign="Center" PagerStyle-Mode="NumericPages">
        <ItemStyle BackColor="White" Height="24px" />
        <AlternatingItemStyle  BackColor="#EEEFF0" />
        <HeaderStyle BackColor="#8D8D8D" Height="24px" Font-Names="Arial" Font-Size="12px"  CssClass="headbg" />
        <Columns>
            <asp:TemplateColumn>
                <HeaderTemplate>
                    <div>
                        <asp:CheckBox ID="chkSelAll" runat="server" OnCheckedChanged="chkSelAll_changed" AutoPostBack="true" Checked="false" />
                    </div>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox Checked='false' Font-Names="Arial" NewsId='<%# DataBinder.Eval(Container.DataItem, "IDMETA").ToString() %>' Font-Size="12px" runat="server" ToolTip="" ID="chka_AddPDF" Visible='True' /> 
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="&nbsp; <%$Resources:insigte.language,defColTitulo%>" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Justify" ItemStyle-ForeColor="#000000" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>&nbsp;
                    <%# sReturnImgAttach(Eval("attach").ToString())%>
                    <a href="<%# string.Format("Article.aspx?ID={0}", DataBinder.Eval(Container.DataItem, "IDMETA")) %>" target="_self" style="color:#000000; text-decoration:none;" onmouseout="this.style.textDecoration='none';" onmouseover="this.style.textDecoration='underline';"><%# DataBinder.Eval(Container.DataItem, "TITLE") %></a>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="EDITOR" HeaderText="<%$Resources:insigte.language,defColFonte%>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Left" />

            <asp:BoundColumn DataField="DATE" HeaderText="<%$Resources:insigte.language,defColData%>" DataFormatString="{0:dd-MM-yyyy}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="60px" />
            <asp:BoundColumn DataField="DATE" HeaderText="Hora" DataFormatString="{0:t}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="60px"  Visible="false" />
            <asp:TemplateColumn HeaderText="<%$Resources:insigte.language,defColAccoes%>" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="150px">
                <ItemTemplate>
                    <asp:Table ID="Table1" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell ID="TableCell9" runat="server" HorizontalAlign="Left">
                                &nbsp;
                            </asp:TableCell>

                            <asp:TableCell ID="TableCell3" runat="server">
                                <%# sReturnIconTipoPasta(DataBinder.Eval(Container.DataItem, "page_url").ToString(), DataBinder.Eval(Container.DataItem, "tipo").ToString(), DataBinder.Eval(Container.DataItem, "filepath").ToString())%>
                            </asp:TableCell>

                            <asp:TableCell ID="TableCell1" runat="server">
                                <a href="<%# string.Format("Article.aspx?ID={0}", DataBinder.Eval(Container.DataItem, "IDMETA")) %>" target="_self" style="color:#000000; text-decoration:none;">
                                    <img src="Imgs/icons/black/png/doc_lines_stright_icon_16.png" width="16px" height="16px" alt="<%= getResource("defColAccoesTexto") %>" title="<%= getResource("defColAccoesTexto") %>" style="border-width:0px;"/>
                                </a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell2" runat="server">
                                <a id="share-news" onclick="shareTheNews(<%# "'" + DataBinder.Eval(Container.DataItem, "IDMETA").ToString() +  "','" +  DataBinder.Eval(Container.DataItem, "TITLE").ToString().Replace("'"," ").Replace("\""," ") + "'" %>)" title='<%# DataBinder.Eval(Container.DataItem, "TITLE").ToString() %>' style="color:#000000; text-decoration:none;" href="#">
                                        <img src="Imgs/icons/black/png/share_icon_16.png" width="16px" height="16px" alt="<%= getResource("defColAccoesShare") %>" title="<%= getResource("defColAccoesShare") %>" style="border-width:0px;"/>
                                    </a>
                            </asp:TableCell>
                            
                            <asp:TableCell ID="TableCell4" runat="server">
                                    <asp:ImageButton ID="bt_AddArquivo" runat="server" 
                                        OnCommand="bt_AddArquivo_Command" 
                                        CommandArgument='<%# sReturnIdLinkDetalhe(DataBinder.Eval(Container.DataItem, "IDMETA").ToString())%>'
                                        CausesValidation="True" 
                                        ImageUrl='<%# sReturnImgLinkDetalhe(DataBinder.Eval(Container.DataItem, "IDMETA").ToString()) %>' width='16px' height='16px' alt='<%# getResource("defColAccoesClips") %>' title='<%# getResource("defColAccoesClips") %>' style='border-width:0px;' >
                                    </asp:ImageButton >
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell5" runat="server" Visible='<%# pdfIsVisable() %>'>
                                    <asp:ImageButton ID="bt_AddPDF" runat="server" 
                                        OnCommand="bt_AddPDF_Command" 
                                        CommandArgument='<%# sReturnIdPDF(DataBinder.Eval(Container.DataItem, "IDMETA").ToString())%>'
                                        CausesValidation="True" 
                                        Visible='<%# pdfIsVisable() %>'
                                        ImageUrl='<%# sReturnImgPDF(DataBinder.Eval(Container.DataItem, "IDMETA").ToString()) %>' width='16px' height='16px' alt='<%# getResource("defColAccoesDossier") %>' title='<%# getResource("defColAccoesDossier") %>' style='border-width:0px;' >
                                    </asp:ImageButton >
                            </asp:TableCell>
                            <asp:TableCell ID="Dossier" runat="server">
                                    <asp:ImageButton ID="ImageButton1" runat="server" 
                                        OnCommand="bt_AddPDF_Command" 
                                        CommandArgument='<%# sReturnIdPDF(DataBinder.Eval(Container.DataItem, "IDMETA").ToString())%>'
                                        CausesValidation="True" 
                                        ImageUrl='<%# sReturnImgPDF(DataBinder.Eval(Container.DataItem, "IDMETA").ToString()) %>' width='16px' height='16px' alt='<%# getResource("defColAccoesDossier") %>' title='<%# getResource("defColAccoesDossier") %>' style='border-width:0px;' >
                                    </asp:ImageButton >
                            </asp:TableCell>
                            <%--<asp:TableCell ID="TCChkBox" runat="server">
                                    <asp:CheckBox Checked='<%# checkDossier(DataBinder.Eval(Container.DataItem, "IDMETA").ToString()) %>' Font-Names="Arial" NewsId='<%# DataBinder.Eval(Container.DataItem, "IDMETA").ToString() %>' Font-Size="12px" runat="server" ToolTip="" ID="chka_AddPDF" Visible='<%# chkIsVisable() %>' /> 
                            </asp:TableCell>--%>
                            <asp:TableCell ID="TableCell8" runat="server">
                                <%# sReturnLinkPasta(DataBinder.Eval(Container.DataItem, "IDMETA").ToString())%>                            
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell7" runat="server">
                                    <asp:ImageButton ID="bt_AddComposta" runat="server" 
                                        OnCommand="bt_AddComposta_Command" 
                                        CommandArgument='<%# sReturnIdComposed(DataBinder.Eval(Container.DataItem, "IDMETA").ToString())%>'
                                        CausesValidation="True" 
                                        Visible='<%# sReturnVisComposed() %>'
                                        ImageUrl='<%# sReturnImgComposed(DataBinder.Eval(Container.DataItem, "IDMETA").ToString()) %>' width='16px' height='16px' alt='Adicionar ao Email' title='Adicionar ao Email' style='border-width:0px;' >
                                    </asp:ImageButton >
                            </asp:TableCell>
                           <%-- <asp:TableCell ID="TableCell6" runat="server"  Visible='<%# FbIsVisable() %>'>
                                    <asp:LinkButton ID="btn_face" runat="server" 
                                        PostBackUrl='<%# "FbNews.aspx?fb=" + DataBinder.Eval(Container.DataItem, "IDMETA").ToString() %>' 
                                        Visible='<%# sReturnFace(DataBinder.Eval(Container.DataItem, "IDMETA").ToString()) %>' >
                                        <img src="Imgs/icons/black/png/facebook_icon_16.png" alt="" style='border-width:0px; width:16px; height:16px' />
                                    </asp:LinkButton>
                            </asp:TableCell>--%>
                        </asp:TableRow>
                    </asp:Table>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
    </div>
    <div style="float: left; padding: 5px,0px,5px,0px; width:98%">
        <div style="float:left; width:100%"> 
            <div style="float:left;width:50%; vertical-align:middle">
                <asp:ImageButton runat="server" ID="ImageButton1" Text="Exportar Excel" OnClick="btnExportXML_Click" Enabled="true" Width="16px" Height="16px" ImageUrl="~/Imgs/pesquisa_button.png" />
                <asp:LinkButton runat="server" ID="LinkButton1" OnClick="btnExportXML_Click" Text="Exportar Excel" Font-Names="Arial" Font-Size="12px" Font-Underline="false" ForeColor="#000000"/>

            </div>
            <div style="float:left; width:50%;vertical-align:middle">
                <div style="float:right;">
                    <div style="margin-left:5px; float: right;">
                        <asp:LinkButton runat="server" Visible="true" ID="btnAddtoFolders" CssClass="btn-pdfs" OnClick="btnAddtoFolders_Click" Text="<%$Resources:insigte.language,addToFolders%>" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false" />
                    </div>
                     <div style="margin-left:5px; float: right;">
                        <asp:LinkButton runat="server" Visible="false" ID="btnDossier" CssClass="btn-pdfs" OnClick="btnDossier_Click" Text="<%$Resources:insigte.language,defColAccoesDossier%>" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false" />
                        <asp:LinkButton runat="server" Visible="false" ID="btnDossierEA" CssClass="btn-pdfs" OnClick="btnDossier_Click" Text="<%$Resources:insigte.language,defColAccoesDossierEA%>" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false" />
                    </div>
                    <div style="margin-left:5px; float: right;">
                        <asp:LinkButton runat="server" Visible="true" ID="btnAddtoClip" CssClass="btn-pdfs" OnClick="btnAddtoClip_Click" Text="<%$Resources:insigte.language,AddToClip%>" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="False" />
                    </div>
                    <div style="margin-left:5px; float: right;">
                        <asp:LinkButton runat="server" Visible="false" ID="btnNewsLetterCp" CssClass="btn-pdfs" OnClick="btnNewsLetterCp_Click" Text="<%$Resources:insigte.language,addToNewsComposta%>" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false" />
                    </div>
                    <div style="margin-left:5px; float: right;">
                        <asp:LinkButton runat="server" Visible="false" ID="btnSelAll" CssClass="btn-pdfs" OnClick="btnSelAll_Click" Text="<%$Resources:insigte.language,defColAccoesDossierSell%>" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</asp:Content>
