﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GeoMKT.aspx.cs" Inherits="Insigte.GeoMKT" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   
<script type="text/javascript">
    var posx = 0; var posy = 0;
    var clicked;

    $(document).mousemove(function (e) {
        var $div = $("#mapMorld");

       
        posx = e.pageX - $div.offset().left,
        posy = e.pageY - $div.offset().top
        // alert(posx +" - " + posy );
    
    });

    function mouseOver(Contry){
        Contry.setAttribute("class", "Contry_Over");
       
    }

    function mouseOut(Contry) {
        Contry.setAttribute("class", "Contry");
        var t = document.getElementById("info");
        t.setAttribute("style", "display:none;");
        restart(); 
    }

    function mouseClick(Contry) {
        document.body.style.cursor = 'wait';
        //window.document.location.href = 'Geo' + Contry.getAttribute('id').toString() + '.aspx';
        window.onload = $("#dialog_form_help_blog").dialog("open");
//        $('#status').fadeOut(); // will first fade out the loading animation
        //$('#mapMorld').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
        //$('#mapMorld').delay(350).css({ 'overflow': 'hidden' });
        window.setTimeout(function () { window.document.location.href = 'Geo' + Contry.getAttribute('id').toString() + '.aspx' }, 3000);
    }

    function restart(){
        FlagAngola.setAttribute("style", "display:none;");
        FlagMozambique.setAttribute("style", "display:none;");
        FlagPortugal.setAttribute("style", "display:none;");
    }

    function SetValues(Contry) {
        restart();
        
        var _info = document.getElementById('info');

        _info.setAttribute("style", " top: " + (posy + 225) + "px; left: " + (posx + 15) + "px; ");
        var _infoNome = document.getElementById('infoNome');

        var divFragNome = "Flag" + Contry.getAttribute('nome').toString();
       
        var flag = document.getElementById(divFragNome);
        flag.setAttribute("style", "display:block;");
        //flag.innerHTML = Contry.getAttribute('nome').toString();
        
        var countryName = document.getElementById('infoNomeCountry');
       countryName.innerHTML = Contry.getAttribute('nome').toString();
      
        _infoNome.setAttribute("class", "infoNome");
        //_infoNome.innerHTML = Contry.getAttribute('nome').toString();

        

        var _infoText = document.getElementById('infoDesc');

        _infoText.setAttribute("class", "infoDesc");
        _infoText.innerHTML = Contry.getAttribute('value').toString() + " Article(s)"; // <br> concorrencia : " + Contry.getAttribute('concorrencia').toString();
    }

    $(function () {
        $("#dialog_form_help_blog").dialog({
            resizable: false,
            autoOpen: false,
            height: 'auto',
            width: 'auto',
            modal: false,
            draggable: false,
            //position: 'center',
            dialogClass: "no-close",
            beforeClose: 'destroy'

        });
        
    });

    $(window).resize(function () { $("#dialog_form_help_blog").dialog({position: 'center'}); });

    $(window).scroll(function () { $("#dialog_form_help_blog").dialog({ position: 'center'}); });

</script>
<style type="text/css">

.no-close .ui-dialog-titlebar-close {
    display: none;
}
    #section.sector
    {
        float:left;
        border :1px solid Black;
        width: 100%;
        min-height: 300px;
        min-width:400px;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
        padding:0px;
        margin:0px;
    }
    
    .form_search
    {
        float:left;
        width: 100%;
        height:10px;
        padding-top: 4px;
        padding-bottom: 10px;
        margin: 0 auto 5px auto;
        background-color: #323232;
        color: #FFFFFF;
        bottom:
    }
    
    .mapWorld
    {
       float:left;
       /*border:1px solid black;*/
       margin:15px 5px 10px 15px;
       width:360px;
       height:556px;
    }
    
    .info
    {
        /*border:1px solid black;*/
        display:block; 
        position:absolute;               
        border-radius:5px; 
        font-family:Arial;
        font-size:12px;
    }

    .infoNome
    {    
        color:white;        
        background-color:#323232;
        padding:2px;
        border-top-left-radius:5px;
        border-top-right-radius:5px; 
                
    }
    
    .infoDesc
    {
        padding:2px;
        background-color:#f0f0f0; 
        border-bottom-left-radius:5px;
        border-bottom-right-radius:5px;            
    }
    
    .Contry
    {
        fill:#323232;
    }
    .Contry_Over
    {
        fill:#f89c3e;
        cursor: pointer;
    }
    .waitload
    {
        cursor:wait;
    }
    #dialog_form_help_blog
    {
        display:none;
        height:auto;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family: Arial;
    }
    
</style>   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="dialog_form_help_blog" title="Please Wait...">
<div style="background:url(Imgs/sprites/WorldMap.gif); width:833px; height:417px; background-size: 100% 100%; fill-opacity=0.6;"></div>
    <%--<img src="Imgs/sprites/WorldMap.gif" />--%>
</div>
    <div id="Geral" style="float:left; padding:0px; margin:0px; height:100%; width:800px;">
        <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%" > 
        <div style="margin-top:12px">
        
            <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false" />
        </div>
    </div>
    <div id="section" class="sector ui-corner-top" style="float:left;width:100%;">
        <div id="titulo" class="form_search headbg ui-corner-top" style="float:left;width:100%;">
            <div id="caixa" style="float:left;width:80%;">
              <a href="#"><div ID="lblTitleAdvSearch" style="font-family:Arial; float:left; font-size:12px; color:#FFFFFF;" >&nbsp;Geo-MKT | Beta 1.0</div></a>
            </div>
            <div id="help" style="float:left;width:20%; ">
                <div style="float:right;padding-right:5px; display: none;">
                    <img alt="Ajuda" src="Imgs/icons/white/png/info_icon_16.png" />
                </div>
            </div>
        </div>    
        <div id="mapMorld" class="mapMorld">
            <div style="text-align:left; color:#00add9; font-size:12px; font-family:Arial; display:none;">WorldMap</div>
            
            
            
            <div style="float:left;margin-top:20px;"> 
           
           <%-- <div id="mydiv" class="currently-loading" style="display:none;">&nbsp;</div> --%>
                <svg id="map" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" baseProfile="full" width="800px" height="402px" viewBox="0 0 1250.00 626.00" enable-background="new 0 0 1250.00 626.00" xml:space="preserve">
	                <image x="0" y="0" width="1250" height="626" transform="matrix(1.00,0.00,0.00,1.00,-0.00,-0.00)" xlink:href="Imgs/WorldMap.png"/>
	                <g id="Africa" fill="#323232" fill-opacity="1" stroke-width="0.2" stroke-linejoin="round" >
                        <%= getPaths() %>
                    </g>
                </svg>
            </div>
            <div id="info" class="info" style="display:none;">
	            <div id="infoNome" class="infoNome">
                <div style="width:16px; height:16px; float:left;">
                    <img alt="" id="FlagAngola" src="Imgs/icons/flags/16/Angola.png" style="display:none; "/>
                    <img alt="" id="FlagMozambique" src ="Imgs/icons/flags/16/Mozambique.png" style="display:none;"/> 
                    <img alt="" id="FlagPortugal" src ="Imgs/icons/flags/16/Portugal.png" style="display:none;"/>           
                </div>
                <span id="infoNomeCountry" style="margin-left:5px;"></span>
                </div>
                <div id="infoDesc" class="infoDesc"></div>
            </div>
            <%--<div style="border: 0px solid black; margin-top:10px; float:left; width:100%;">
                <asp:Chart ID="chCountryCli" runat="server" Width="800px" Height="230px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttCountryCli" >
                    </asp:Title>
                    </Titles>                  
                    <ChartAreas>
                        <asp:ChartArea Name="caCountryCli">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div> 
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div id="divChart1" runat="server" visible="false" style="margin-top:10px; float:left;width:100%;" >
            
                    <asp:Chart ID="Chart1" runat="server" Width="800px" Height="300px" Palette="Chocolate">
                        <Titles>
                        <asp:Title Name="ttChart1" >
                        </asp:Title>
                        </Titles>                  
                        <ChartAreas>
                            <asp:ChartArea Name="caChart1">
                            </asp:ChartArea>
                        </ChartAreas>
                        <Series>
                            <asp:Series Name="srChart1">
                            </asp:Series>
                        </Series>
                    </asp:Chart>
                </div>
                
            </ContentTemplate> 
            <Triggers>
            <asp:AsyncPostBackTrigger ControlID="chCountryCli" EventName="click" />
            </Triggers>
            </asp:UpdatePanel>
             
            <div style="margin-top:10px; float:left; width:100%;">
                <asp:Chart ID="chCountryGeral" runat="server" Width="800px" Height="230px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttCountryGeral" >
                    </asp:Title>
                    </Titles>                  
                    <ChartAreas>
                        <asp:ChartArea Name="caCountryGeral">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>            
            </div>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
            <div id="divChart2" runat="server" visible="false" style="margin-top:10px; float:left; width:100%;" >
                <asp:Chart ID="Chart2" runat="server" Width="800px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttChart2" >
                    </asp:Title>
                    </Titles>                  
                    <ChartAreas>
                        <asp:ChartArea Name="caChart2">
                        </asp:ChartArea>
                    </ChartAreas>
                    <Series>
                        <asp:Series Name="srChart2">
                        </asp:Series>
                    </Series>
                </asp:Chart>
            </div> 
             </ContentTemplate> 
            <Triggers>
            <asp:AsyncPostBackTrigger ControlID="chCountryGeral" EventName="click" />
            </Triggers>
            </asp:UpdatePanel> --%>
        </div> 
    </div>
</div>

</asp:Content>
