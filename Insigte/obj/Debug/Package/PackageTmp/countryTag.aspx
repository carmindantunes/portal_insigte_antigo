﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="countryTag.aspx.cs" Inherits="Insigte.countryTag" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
    #section.sector
    {
        float:left;
        border :1px solid Black;
        width: 100%;
        min-height: 300px;
        min-width:400px;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
        padding:0px;
        margin:0px;
    }
    .form_search
    {
        float:left;
        width: 100%;
        height:10px;
        padding-top: 4px;
        padding-bottom: 10px;
        margin: 0 auto 20px auto;
        background-color: #323232;
        color: #FFFFFF;
    }    
</style>

<script type="text/javascript" src="http://d3js.org/d3.v3.min.js"></script>


<style type="text/css">
    
.cell {
  color: white;
  border: solid 1px white;
  overflow: hidden;
  position: absolute;
}


 
</style>
<script type="text/javascript">

    function drawTreemap(height, width, elementSelector, data, childrenFunction, nameFunction, sizeFunction, colorFunction, colorScale) {

        var treemap = d3.layout.treemap()
        .children(childrenFunction)
        .size([width, height])
        .value(sizeFunction);

        var div = d3.select(elementSelector)
        .append("div")
        .style("position", "relative")
        .style("width", width + "px")
        .style("height", height + "px");

        console.log(treemap.nodes(data));

        div.data(data).selectAll("div")
        .data(function (d) { return treemap.nodes(d); })
        .enter()
        .append("div")
        .attr("class", "cell")
        .style("background", function (d) { return colorScale(colorFunction(d)); })
        .call(cell)
        .text(nameFunction);
    }

    function cell() {
        this
        .style("right", function (d) { return d.x + "px"; }) //.style("left", function (d) { return d.x + "px"; })
        .style("bottom", function (d) { return d.y + "px"; }) //.style("top", function (d) { return d.y + "px"; })
        .style("width", function (d) { return d.dx - 1 + "px"; })
        .style("height", function (d) { return d.dy - 1 + "px"; });
    }

$(function () {

    var dataAO = [{ "name": "levelone", "elements": 
        [
        <%= TreemapDataAO() %>
        ]
    }];

    var dataMZ = [{ "name": "levelone", "elements": 
        [
        <%= TreemapDataMZ() %>
        ]
    }];

    var dataPT = [{ "name": "levelone", "elements": 
        [
        <%= TreemapDataPT() %>
        ]
    }];

    var childrenFunction = function (d) { return d.elements };
    var sizeFunction = function (d) { return d.sizes; };
    var colorFunction = function (d) { return d.time; };
    var nameFunction = function (d) { return d.name; };

    var color = d3.scale.linear()
                .domain([0, 20, 35, 50])
                .range(["#00add9", "#", "#696969", "pink"]);

    drawTreemap(380, 750, '#chartAO', dataAO, childrenFunction, nameFunction, sizeFunction, colorFunction, color);
    drawTreemap(380, 750, '#chartMZ', dataMZ, childrenFunction, nameFunction, sizeFunction, colorFunction, color);
    drawTreemap(380, 750, '#chartPT', dataPT, childrenFunction, nameFunction, sizeFunction, colorFunction, color);

});

</script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="Geral" style="float:left; padding:0px; margin:0px; height:100%; width:800px;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:12px">
            <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false" />
        </div>
    </div>
    <div id="section" class="sector ui-corner-top" style="float:left;width:100%;">
        <div id="titulo" class="form_search headbg ui-corner-top" style="float:left;width:100%;">
            <div id="caixa" style="float:left;width:80%;">
                &nbsp;<asp:Label runat="server" ID="lblTitleAdvSearch" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" Text=" Countrytag" />
            </div>
            <div id="help" style="float:left;width:20%; ">
                <div style="float:right;padding-right:5px;">
                    <img alt="Ajuda" src="Imgs/icons/white/png/info_icon_16.png" />
                </div>
            </div>
        </div>

        <div id="TagCloud" style="float:left;width:100%;">
            <div style="margin-left:20px">
                Tema : <asp:DropDownList ID="tema" runat="server" AutoPostBack="true" OnSelectedIndexChanged="mudar_tema"></asp:DropDownList>
            </div>
            <div style="margin-left:20px">
                <p>Angola</p>
                <div id="chartAO"></div>
            </div>
            <div style="margin-left:20px">
                <p>Moçambique</p>
                <div id="chartMZ"></div>
            </div>
            <div style="margin-left:20px">
                <p>Portugal</p>
                <div id="chartPT"></div>
            </div>
        </div>

        <div id="bottom" style="float:left;width:100%;margin:10px 0px 10px 0px">
        </div>

    </div>
</div>
</asp:Content>