﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;

namespace Insigte
{
    public partial class Subject : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dgSubjectACN.Columns[4].Visible = chkifinsigte();

            if (!IsPostBack)
            {
                if (Global.InsigteManager[Session.SessionID].IdClient == "3003")
                {
                    btnDossierEA.Visible = true;
                }
                else
                {
                    btnDossier.Visible = true;
                }
            }

            if (Global.InsigteManager[Session.SessionID].inUser.EmailShareSend != null)
            {
                if (Global.InsigteManager[Session.SessionID].inUser.EmailShareSend == "1")
                {
                    lblDataToday.Visible = true;
                    lblDataToday.Text = "Email enviado.";
                    Global.InsigteManager[Session.SessionID].inUser.EmailShareSend = null;
                }
                else
                {
                    lblDataToday.Visible = true;
                    lblDataToday.Text = "Email não enviado.";

                    Global.InsigteManager[Session.SessionID].inUser.EmailShareSend = null;
                }
            }

            if (Global.InsigteManager[Session.SessionID].inUser.HasPastas.ToString() == "1")
            {


                if (Global.InsigteManager[Session.SessionID].IdClient == "3003")
                {

                    btnDossierEA.Visible = true;
                    btnSelAll.Visible = false;
                }
                else
                {
                    btnDossier.Visible = true;
                    btnSelAll.Visible = false;
                }

            }
            else
            {
                btnDossier.Visible = false;
                btnDossierEA.Visible = false;
                btnSelAll.Visible = false;
            }

            //lblDataToday.Text = "Lisboa, " + DateTime.Now.ToString("dd") + " de " + DateTime.Now.ToString("MMMM", CultureInfo.CreateSpecificCulture("pt-PT")) + " de " + DateTime.Now.ToString("yyyy");
            dgSubjectACN.PagerStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[0].ToString());
            dgSubjectACN.HeaderStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[1].ToString());
            string qsTema = Request.QueryString["tema"];
            string qsClid = Request.QueryString["clid"];
            string qsAtid = Request.QueryString["at"];
            string qsFLon = Request.QueryString["fon"];
            string qsNddb = Request.QueryString["dd"];
            string qsYmdd = Request.QueryString["ym"];
            string qsEdid = Request.QueryString["ed"];
            string qsTpnw = Request.QueryString["tp"];

            if (!Page.IsPostBack)
            {
                getPublicacoes(qsTema, qsClid, qsAtid, qsFLon, qsNddb, qsYmdd, qsEdid, qsTpnw);
                getTema(qsTema, qsClid, qsTpnw);
            }
            else
            {
                //getPublicacoes(qsTema, qsClid, qsAtid, qsFLon, qsNddb, qsYmdd, qsEdid, qsTpnw);
                //getTema(qsTema, qsClid, qsTpnw);
            }

        }

        protected Boolean chkifinsigte()
        {
            if (Global.InsigteManager[Session.SessionID].IdClient == "1000" || Global.InsigteManager[Session.SessionID].inUser.IdClientUser == "178")
                return true;
            else
                return false;
        }

        protected Boolean FbIsVisable()
        {
            if (Global.InsigteManager[Session.SessionID].inUser.HasAlerts.ToString() == "0")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //Facebook
        protected bool sReturnFace(string strValue)
        {
            //FaceBook
            DataTable find = Global.InsigteManager[Session.SessionID].getTableQuery("SELECT * FROM [iadvisers].[dbo].[FB10F_NEWS_POST] with(nolock) where  id_noticia = " + strValue, "getFaceNews");

            if (find.Rows.Count > 0)
                return true;

            return false;
        }

        protected Boolean chkIsVisable()
        {
            if (Global.InsigteManager[Session.SessionID].inUser.HasPastas.ToString() == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected Boolean pdfIsVisable()
        {
            if (Global.InsigteManager[Session.SessionID].inUser.HasPastas.ToString() == "1")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected Boolean checkDossier(String idArtigo)
        {
            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && idArtigo != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(idArtigo))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (idVal == idArtigo)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }


        }

        protected void btnSelAll_Click(object sender, EventArgs e)
        {
            foreach (DataGridItem drv in dgSubjectACN.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
                chk.Checked = true;
            }
        }



        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        protected string sReturnIdComposed(string strValue)
        {
            string sLink = string.Empty;

            DataTable find = Global.InsigteManager[Session.SessionID].getTableQuery("select * from ML10F_CLIENT_USER_CONTROL_COMPOSED_ADD with(nolock) where convert(varchar,DAT_CREATE,112)=convert(varchar,getdate(),112) ", "findIdComposta");

            Boolean foundit = false;
            if (find.Rows.Count > 0)
            {
                foreach (DataRow row in find.Rows)
                {
                    if (row["ID_NEWS"].ToString() == strValue)
                        foundit = true;
                }
            }

            if (!foundit)
                sLink = strValue;

            return sLink;
        }

        protected string sReturnImgComposed(string strValue)
        {
            string sLink = "Imgs/icons/black/png/push_pin_icon_16_sell.png";

            DataTable find = Global.InsigteManager[Session.SessionID].getTableQuery("select * from ML10F_CLIENT_USER_CONTROL_COMPOSED_ADD with(nolock) where convert(varchar,DAT_CREATE,112)=convert(varchar,getdate(),112) ", "findIdComposta");

            Boolean foundit = false;
            if (find.Rows.Count > 0)
            {
                foreach (DataRow row in find.Rows)
                {
                    if (row["ID_NEWS"].ToString() == strValue)
                        foundit = true;
                }
            }

            if (foundit)
                sLink = "Imgs/icons/black/png/push_pin_icon_16.png";

            return sLink;
        }

        protected bool sReturnVisComposed()
        {
            //Newsletter Composta
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(7, 1) == "0")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void bt_AddComposta_Command(Object sender, CommandEventArgs e)
        {
            if (e.CommandArgument.ToString().Length > 1)
            {
                addLinkComposta(e.CommandArgument.ToString());
                lblDataToday.Visible = true;
                //lblDataToday.Text = e.CommandArgument.ToString() + " : Adicionado com sucesso!";
                lblDataToday.Text = "Adicionado ao email com sucesso!";

                ImageButton b = sender as ImageButton;
                b.ImageUrl = "Imgs/icons/black/png/push_pin_icon_16.png";
            }
            else
            {
                lblDataToday.Visible = true;
                //lblDataToday.Text = e.CommandArgument.ToString() + " : Adicionado com sucesso!";
                lblDataToday.Text = "Já se encontra adicionada email!";
            }
        }

        protected void addLinkComposta(string qsPDFArtigo)
        {
            String Query = "insert into ML10F_CLIENT_USER_CONTROL_COMPOSED_ADD ";
            Query += " (ID_CLIENT, COD_NEWSLETTER_COMPOSE, ID_NEWS, DAT_CREATE) ";
            Query += " Select " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + ", 0, " + qsPDFArtigo + ", getdate() ";
            DataTable insertComp = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "InsertComp");
        }

        protected string sReturnIconTipoPasta(string url, string tipo, string file)
        {
            //Imprensa
            //Insigte
            //Online
            //Rádio
            //Televisão
            switch (tipo)
            {
                case "Imprensa":
                    String FileHelperPath = "ficheiros";
                    if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                    {
                        if (File.Exists(@"G:\cfiles\" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file.Replace("/", "\\")))
                        {
                            FileHelperPath = "cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient;
                        }
                    }
                    return string.Format("<a href=\"http://insigte.com/" + FileHelperPath + "/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    //return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    break;
                case "Online":
                    return string.Format("<a href=\"{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/globe_1_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Link\" style=\"border-width:0px;\" title=\"Link\" /></a>", url);
                    break;
                case "Rádio":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/headphones_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP3\" style=\"border-width:0px;\" title=\"Download MP3\" /></a>", file);
                    break;
                case "Televisão":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/movie_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP4\" style=\"border-width:0px;\" title=\"Download MP4\" /></a>", file);
                    break;
                default:
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    break;
            }
            return null;
        }

        private void getTema(string qsTema, string qsClid, string qsTpnw)
        {
            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");

            if (qsTpnw != null)
            {
                conn.Open();
                string cmdQuery = string.Empty;

                String TipoN = "des_tipo_noticia";

                if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                    TipoN += "_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage;

                cmdQuery = "select id_tipo_noticia, " + TipoN + " from tipo_noticia with(nolock) where id_tipo_noticia = " + qsTpnw;
                SqlCommand cmd = new SqlCommand(cmdQuery);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    lblTema.Text = Resources.insigte.language.subDescricao2 + " " + reader.GetValue(1).ToString();
                }

                cmd.Connection.Close();
                cmd.Connection.Dispose();
            }
            else
            {
                try
                {
                    conn.Open();
                    string cmdQuery = string.Empty;
                    if (qsTema != "000")
                    {
                        String subject = "name";

                        if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                            subject += "_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage;

                        cmdQuery = "select subjectid, " + subject + " from subject with(nolock) where subjectid = '" + qsTema + "' and clienteid  = '" + qsClid + "' ";

                        SqlCommand cmd = new SqlCommand(cmdQuery);
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;

                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            lblTema.Text = Resources.insigte.language.subDescricao + " " + reader.GetValue(1).ToString();
                        }

                        cmd.Connection.Close();
                        cmd.Connection.Dispose();
                    }
                    else
                    {
                        foreach (var t in Global.InsigteManager[Session.SessionID].inUser.Temas)
                        {
                            if (t.Value.IdTema == (qsClid + qsTema))
                                lblTema.Text = Resources.insigte.language.subDescricao2 + " " + t.Value.Name;

                        }
                    }

                }
                catch (Exception exp)
                {
                    exp.Message.ToString();
                }
            }
        }

        protected string sReturnLinkPasta(string strValue)
        {
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(0, 1) == "0")
            {
                return null;
            }
            string sLink = string.Empty;
            string idVal = string.Empty;

            string cmdQueryPastas = "select * from dbo.CL10H_PASTAS_ARTICLES with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and articleid=" + strValue;
            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(cmdQueryPastas);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows == true)
                {
                    while (dr.Read())
                        idVal += dr["articleid"].ToString() + "|";
                }

                idVal.Substring(0, idVal.Length - 1);

                dr.Close();

            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }
            finally
            {
                conn.Dispose();
                conn.Close();
            }


            if (idVal.Contains('|'))
            {
                string[] words = idVal.Split('|');
                if (words.Contains(strValue))
                {

                    sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('Adicionar.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/icons/black/png/folder_open_icon_16_sel.png' width='16px' height='16px' alt='" + Resources.insigte.language.defColAccoesPastas + "' title='" + Resources.insigte.language.defColAccoesPastas + "' style='border-width:0px;'/></a>";
                }
                else
                {
                    sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('Adicionar.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/icons/black/png/folder_open_icon_16.png' width='16px' height='16px' alt='" + Resources.insigte.language.defColAccoesPastas + "' title='" + Resources.insigte.language.defColAccoesPastas + "' style='border-width:0px;'/></a>";
                }
            }
            else
            {
                if (idVal == strValue)
                {
                    sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('Adicionar.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/icons/black/png/folder_open_icon_16_sel.png' width='16px' height='16px' alt='" + Resources.insigte.language.defColAccoesPastas + "' title='" + Resources.insigte.language.defColAccoesPastas + "' style='border-width:0px;'/></a>";
                }
                else
                {
                    sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('Adicionar.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/icons/black/png/folder_open_icon_16.png' width='16px' height='16px' alt='" + Resources.insigte.language.defColAccoesPastas + "' title='" + Resources.insigte.language.defColAccoesPastas + "' style='border-width:0px;'/></a>";
                }
            }

            return sLink;
        }

        private void getPublicacoes(string qsTema, string qsClid, string qsAtid, string qsFLon, string qsNddb, string qsYmdd, string qsEdid, string qsTpnw)
        {

            String Title = "TITLE";
            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            {
                Title = "TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
            }

            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");

            String FilterEditors = "*";
            foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
            {
                Boolean Found = false;
                if (pair.Value.IdTema.ToString() == (qsClid + qsTema))
                {
                    FilterEditors = pair.Value.FILTER_EDITORS_TEMA == "*" ? Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter : pair.Value.FILTER_EDITORS_TEMA;
                    Found = true;
                }

                if (pair.Value.IdTema.ToString() == (qsClid + "000") && !Found)
                {
                    FilterEditors = pair.Value.FILTER_EDITORS_TEMA == "*" ? Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter : pair.Value.FILTER_EDITORS_TEMA;
                    Found = true;
                }
            }

            try
            {
                conn.Open();
                string cmdQuery = string.Empty;

                if (qsTpnw != null)
                {

                    String TemasFiltro = "( ";

                    foreach (var t in Global.InsigteManager[Session.SessionID].inUser.Temas)
                    {
                        TemasFiltro += "( ";

                        if (t.Value.IdTema.Substring(4, 3) != "000")
                        {
                            TemasFiltro += " cast(clientarticles.clientid as varchar ) + cast(clientarticles.subjectid as varchar) = '" + t.Value.IdTema + "'";
                        }
                        else
                        {
                            if (t.Value.SubTemas.Substring(4, 3) != "000")
                            {
                                TemasFiltro += " cast(clientarticles.clientid as varchar ) + cast(clientarticles.subjectid as varchar) in ('" + t.Value.SubTemas.Replace(";", "','") + "')";
                            }
                            else
                            {
                                TemasFiltro += "clientarticles.clientid = '" + t.Value.SubTemas.Substring(0, 4) + "' ";
                            }
                        }

                        TemasFiltro += " ) OR ";
                    }

                    TemasFiltro = TemasFiltro.Substring(0, TemasFiltro.Length - 3) + ")";

                    cmdQuery = "SELECT DISTINCT TOP 200 m.ID as IDMETA, " + Title + ", EDITOR, DATE, filepath, page_url, tipo, ( case  isnull( ma.id_news, 0) when  0 then '0' else '1' end ) as [attach] "; 
                    cmdQuery += " FROM iadvisers.dbo.metadata m with(nolock) ";
                    cmdQuery += " left join metadata_attach ma with(nolock) on ma.id_news = m.id ";
                    cmdQuery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ";
                    cmdQuery += " ON iadvisers.dbo.clientarticles.id = m.ID ";
                    cmdQuery += " WHERE " + TemasFiltro + " ";
                    cmdQuery += " AND m.id_tipo_noticia = " + qsTpnw + " ";
                    //if (FilterEditors != "*" && qsFLon == "1")
                    //    cmdQuery += " and m.editorid in (" + FilterEditors + ") ";

                    //FILTRA OS EDITORES
                    if (FilterEditors != "*")
                        cmdQuery += " and m.editorid in (" + FilterEditors + ") ";


                    //if (qsAtid != null && qsAtid.Length > 0)
                    //    cmdQuery += " and m.authorid = '" + qsAtid.Replace("'", "") + "' ";
                    //if (qsNddb != null && qsNddb.Length > 0)
                    //    cmdQuery += " and m.date >= getdate()-" + qsNddb.Replace("'", "");
                    //if (qsYmdd != null && qsYmdd.Length > 0)
                    //    cmdQuery += " and left(convert(varchar,m.date,112),6) = '" + qsYmdd.Replace("'", "") + "' ";
                    //if (qsEdid != null && qsEdid.Length > 0)
                    //    cmdQuery += " and m.editorid = " + qsEdid.Replace("'", "") + " ";
                    string lstusers = "941,861,943";
                    if (lstusers.Contains(Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString()))
                        cmdQuery += " order by m.insert_date desc, IDMETA desc ";
                    else
                        cmdQuery += " order by m.date desc, IDMETA desc ";

                    //cmdQuery += " order by m.date desc, IDMETA desc ";

                    //lblDataToday.Visible = true;
                    //lblDataToday.Text = cmdQuery;

                }
                else
                {

                    if (qsTema != "000")
                    {
                        //cmdQuery = "SELECT TOP 200 " + Title + ", EDITOR, DATE, m.ID as IDMETA, filepath, page_url, tipo ";
                        cmdQuery = " SELECT TOP 200 " + Title + ", EDITOR, DATE, m.ID as IDMETA, filepath, page_url, tipo , ( case  isnull( ma.id_news, 0) when  0 then '0' else '1' end ) as [attach] ";
                        cmdQuery += " FROM iadvisers.dbo.metadata m with(nolock)";
                        cmdQuery += " left join metadata_attach ma with(nolock) on ma.id_news = m.id ";
                        cmdQuery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ";
                        cmdQuery += " ON iadvisers.dbo.clientarticles.id = m.ID ";
                        cmdQuery += " where clientarticles.clientid='" + qsClid.Replace("'", "") + "' ";
                        cmdQuery += " and clientarticles.subjectid='" + qsTema.Replace("'", "") + "' ";
                        if (qsAtid != null && qsAtid.Length > 0)
                            cmdQuery += " and m.authorid = '" + qsAtid.Replace("'", "") + "' ";
                        //if (FilterEditors != "*" && qsFLon == "1")
                        //    cmdQuery += " and m.editorid in (" + FilterEditors + ") ";

                        //FILTRA OS EDITORES
                        if (FilterEditors != "*")
                            cmdQuery += " and m.editorid in (" + FilterEditors + ") ";

                        if (qsNddb != null && qsNddb.Length > 0)
                            cmdQuery += " and m.date >= getdate()-" + qsNddb.Replace("'", "");
                        if (qsYmdd != null && qsYmdd.Length > 0)
                            cmdQuery += " and left(convert(varchar,m.date,112),6) = '" + qsYmdd.Replace("'", "") + "' ";
                        if (qsEdid != null && qsEdid.Length > 0)
                            cmdQuery += " and m.editorid = " + qsEdid.Replace("'", "") + " ";

                        //fabio 20150116
                        //OLD - cmdQuery += " order by m.date desc, IDMETA desc";
                        /****new****/
                        string lstusers = "941,861,943";
                        if (lstusers.Contains(Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString()))
                            cmdQuery += " order by m.insert_date desc, IDMETA desc ";
                        else
                            cmdQuery += " order by m.date desc, IDMETA desc ";
                        /***********/
                        
                    }
                    else
                    {
                        foreach (var t in Global.InsigteManager[Session.SessionID].inUser.Temas)
                        {
                            if (t.Value.IdTema == (qsClid.Replace("'", "") + qsTema.Replace("'", "")))
                            {
                                if (t.Value.SubTemas.Substring(4, 3) != "000")
                                {
                                    cmdQuery = "SELECT DISTINCT TOP 200 m.ID as IDMETA, " + Title + ", EDITOR, DATE, filepath, page_url, tipo , ( case  isnull( ma.id_news, 0) when  0 then '0' else '1' end ) as [attach] ";
                                    cmdQuery += " FROM iadvisers.dbo.metadata m with(nolock) ";
                                    cmdQuery += " left join metadata_attach ma with(nolock) on ma.id_news = m.id ";
                                    cmdQuery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ";
                                    cmdQuery += " ON iadvisers.dbo.clientarticles.id = m.ID ";
                                    cmdQuery += " WHERE cast(clientarticles.clientid as varchar ) + cast(clientarticles.subjectid as varchar) IN ( '" + t.Value.SubTemas.Replace(";", "','") + "' ) ";
                                    //if (FilterEditors != "*" && qsFLon == "1")
                                    //    cmdQuery += " and m.editorid in (" + FilterEditors + ") ";

                                    //FILTRA OS EDITORES
                                    if (FilterEditors != "*")
                                        cmdQuery += " and m.editorid in (" + FilterEditors + ") ";


                                    if (qsAtid != null && qsAtid.Length > 0)
                                        cmdQuery += " and m.authorid = '" + qsAtid.Replace("'", "") + "' ";
                                    if (qsNddb != null && qsNddb.Length > 0)
                                        cmdQuery += " and m.date >= getdate()-" + qsNddb.Replace("'", "");
                                    if (qsYmdd != null && qsYmdd.Length > 0)
                                        cmdQuery += " and left(convert(varchar,m.metadata.date,112),6) = '" + qsYmdd.Replace("'", "") + "' ";
                                    if (qsEdid != null && qsEdid.Length > 0)
                                        cmdQuery += " and m.editorid = " + qsEdid.Replace("'", "") + " ";
                                    cmdQuery += " order by m.date desc, IDMETA desc ";
                                }
                                else
                                {
                                    cmdQuery = "SELECT DISTINCT TOP 200 m.ID as IDMETA, " + Title + ", EDITOR, DATE, filepath, page_url, tipo , ( case  isnull( ma.id_news, 0) when  0 then '0' else '1' end ) as [attach] ";
                                    cmdQuery += " FROM iadvisers.dbo.metadata m with(nolock) ";
                                    cmdQuery += " left join metadata_attach ma with(nolock) on ma.id_news = m.id ";
                                    cmdQuery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ";
                                    cmdQuery += " ON iadvisers.dbo.clientarticles.id = m.ID ";
                                    cmdQuery += " WHERE clientarticles.clientid = '" + t.Value.SubTemas.Substring(0, 4) + "' ";
                                    //if (FilterEditors != "*" && qsFLon == "1")
                                    //    cmdQuery += " and m.editorid in (" + FilterEditors + ") ";

                                    //FILTRA OS EDITORES
                                    if (FilterEditors != "*")
                                        cmdQuery += " and m.editorid in (" + FilterEditors + ") ";

                                    if (qsAtid != null && qsAtid.Length > 0)
                                        cmdQuery += " and m.authorid = '" + qsAtid.Replace("'", "") + "' ";
                                    if (qsNddb != null && qsNddb.Length > 0)
                                        cmdQuery += " and m.date >= getdate()-" + qsNddb.Replace("'", "");
                                    if (qsYmdd != null && qsYmdd.Length > 0)
                                        cmdQuery += " and left(convert(varchar,m.date,112),6) = '" + qsYmdd.Replace("'", "") + "' ";
                                    if (qsEdid != null && qsEdid.Length > 0)
                                        cmdQuery += " and m.editorid = " + qsEdid.Replace("'", "") + " ";
                                    cmdQuery += " order by m.date desc, IDMETA desc ";
                                }
                            }
                        }

                    }
                }

                SqlCommand cmd = new SqlCommand(cmdQuery);

                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                DataSet tempSet;
                SqlDataAdapter iadvisers_ACN_SUBJECT;

                iadvisers_ACN_SUBJECT = new SqlDataAdapter(cmd);

                tempSet = new DataSet("tempSet");
                iadvisers_ACN_SUBJECT.Fill(tempSet);

                dgSubjectACN.DataSource = tempSet;
                dgSubjectACN.DataBind();

            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }
        }

        private void getPublicacoes(string qsTema, string qsClid)
        {
            String Title = "TITLE";
            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            {
                Title = "TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
            }

            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");

            String FilterEditors = "";
            foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
            {
                Boolean Found = false;
                if (pair.Value.IdTema.ToString() == (qsClid + qsTema))
                {
                    FilterEditors = pair.Value.FILTER_EDITORS_TEMA == "*" ? Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter : pair.Value.FILTER_EDITORS_TEMA;
                    Found = true;
                }

                if (pair.Value.IdTema.ToString() == (qsClid + "000") && !Found)
                {
                    FilterEditors = pair.Value.FILTER_EDITORS_TEMA == "*" ? Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter : pair.Value.FILTER_EDITORS_TEMA;
                    Found = true;
                }
            }

            try
            {
                conn.Open();
                string cmdQuery = string.Empty;
                if (qsTema != "000")
                {
                    cmdQuery = "SELECT TOP 80 " + Title + ", EDITOR, DATE, m.ID as IDMETA, filepath, page_url, tipo , ( case  isnull( ma.id_news, 0) when  0 then '0' else '1' end ) as [attach]  ";
                    cmdQuery += " FROM iadvisers.dbo.metadata  m with(nolock)";
                    cmdQuery += " left join metadata_attach ma with(nolock) on ma.id_news = m.id ";
                    cmdQuery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ";
                    cmdQuery += " ON iadvisers.dbo.clientarticles.id = m.ID ";
                    cmdQuery += " where clientarticles.clientid='" + qsClid + "' ";
                    cmdQuery += " and clientarticles.subjectid='" + qsTema + "' ";
                    if (FilterEditors != "*")
                        cmdQuery += " and m.editorid in (" + FilterEditors + ") ";
                    cmdQuery += " order by m.date desc, IDMETA desc";
                }
                else
                {
                    foreach (var t in Global.InsigteManager[Session.SessionID].inUser.Temas)
                    {
                        if (t.Value.IdTema == (qsClid + qsTema))
                        {
                            if (t.Value.SubTemas.Substring(4, 3) != "000")
                            {
                                cmdQuery = "SELECT DISTINCT TOP 80 m.ID as IDMETA, " + Title + ", EDITOR, DATE, filepath, page_url , ( case  isnull( ma.id_news, 0) when  0 then '0' else '1' end ) as [attach]  ";
                                cmdQuery += " FROM iadvisers.dbo.metadata m with(nolock) ";
                                cmdQuery += " left join metadata_attach ma with(nolock) on ma.id_news = m.id ";
                                cmdQuery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ";
                                cmdQuery += " ON iadvisers.dbo.clientarticles.id = m.ID ";
                                cmdQuery += " WHERE cast(clientarticles.clientid as varchar ) + cast(clientarticles.subjectid as varchar) IN ( '" + t.Value.SubTemas.Replace(";", "','") + "' ) ";
                                if (FilterEditors != "*")
                                    cmdQuery += " and m.editorid in (" + FilterEditors + ") ";
                                cmdQuery += " order by m.date desc, IDMETA desc ";
                            }
                            else
                            {
                                cmdQuery = "SELECT DISTINCT TOP 80 m.ID as IDMETA, " + Title + ", EDITOR, DATE, filepath, page_url , ( case  isnull( ma.id_news, 0) when  0 then '0' else '1' end ) as [attach]  ";
                                cmdQuery += " FROM iadvisers.dbo.metadata m with(nolock) ";
                                cmdQuery += " left join metadata_attach ma with(nolock) on ma.id_news = m.id ";
                                cmdQuery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ";
                                cmdQuery += " ON iadvisers.dbo.clientarticles.id = m.ID ";
                                cmdQuery += " WHERE clientarticles.clientid = '" + t.Value.SubTemas.Substring(0, 4) + "' ";
                                if (FilterEditors != "*")
                                    cmdQuery += " and m.editorid in (" + FilterEditors + ") ";
                                cmdQuery += " order by m.date desc, IDMETA desc ";
                            }
                        }
                    }

                }

                SqlCommand cmd = new SqlCommand(cmdQuery);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                DataSet tempSet;
                SqlDataAdapter iadvisers_ACN_SUBJECT;

                iadvisers_ACN_SUBJECT = new SqlDataAdapter(cmd);

                tempSet = new DataSet("tempSet");
                iadvisers_ACN_SUBJECT.Fill(tempSet);

                dgSubjectACN.DataSource = tempSet;
                dgSubjectACN.DataBind();

            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }
        }
        protected string sReturnImgAttach(string attach)
        {
            return attach.Equals("1") ? "<img id=\"imagemt\" src=\"Imgs/icons/black/png/clip_icon_16.png\" alt=\"\" runat=\"server\" />" : "";
        }

        protected void dgSubjectACN_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgSubjectACN.CurrentPageIndex = e.NewPageIndex;

            string qsTema = Request.QueryString["tema"];
            string qsClid = Request.QueryString["clid"];
            string qsAtid = Request.QueryString["at"];
            string qsFLon = Request.QueryString["fon"];
            string qsNddb = Request.QueryString["dd"];
            string qsYmdd = Request.QueryString["ym"];
            string qsEdid = Request.QueryString["ed"];
            string qsTpnw = Request.QueryString["tp"];

            getPublicacoes(qsTema, qsClid, qsAtid, qsFLon, qsNddb, qsYmdd, qsEdid, qsTpnw);
            getTema(qsTema, qsClid, qsTpnw);

            //getPublicacoes(Request.QueryString["tema"], Request.QueryString["clid"]);
        }

        private DataTable GetData(SqlCommand cmd)
        {
            DataTable dt = new DataTable();
            SqlConnection con = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.CommandType = CommandType.Text;
            cmd.Connection = con;

            try
            {
                con.Open();
                sda.SelectCommand = cmd;
                sda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                sda.Dispose();
                con.Dispose();
            }

        }

        protected void btnExportXML_Click(object sender, EventArgs e)
        {
            String Title = "TITLE";
            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            {
                Title = "TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
            }

            string qsTema = Request.QueryString["tema"];
            string qsClid = Request.QueryString["clid"];

            string cmdQuery = string.Empty;
            if (qsTema != "000")
            {
                cmdQuery = "SELECT TOP 80 " + Title + ", EDITOR, DATE, m.ID as IDMETA, filepath, page_url, m.tipo as tipo, cast(c.text as nvarchar(max)) as text ";
                if (Global.InsigteManager[Session.SessionID].IdClient == "1096")
                {
                    cmdQuery += " , (select AVE_USD from [dbo].[fnCalMediaValue] (m.id) ) as AVE ";
                    cmdQuery += " , (select OTS from [dbo].[fnCalMediaValue] (m.id) ) as OTS "; 
                }
                cmdQuery += " FROM iadvisers.dbo.metadata m with(nolock)";
                cmdQuery += " JOIN iadvisers.dbo.clientarticles with(nolock)";
                cmdQuery += " ON iadvisers.dbo.clientarticles.id = m.ID ";
                cmdQuery += " JOIN iadvisers.dbo.contents c with(nolock) ON c.id = m.ID";
                cmdQuery += " where clientarticles.clientid='" + qsClid + "' ";
                cmdQuery += " and clientarticles.subjectid='" + qsTema + "' ";
                cmdQuery += " order by m.date desc, IDMETA desc";
            }
            else
            {
                foreach (var t in Global.InsigteManager[Session.SessionID].inUser.Temas)
                {
                    if (t.Value.IdTema == (qsClid + qsTema))
                    {
                        if (t.Value.SubTemas.Substring(4, 3) != "000")
                        {
                            cmdQuery = "SELECT DISTINCT TOP 80 m.ID as IDMETA, " + Title + ", EDITOR, DATE, filepath, page_url, m.tipo as tipo, cast(c.text as nvarchar(max)) as text ";
                            if (Global.InsigteManager[Session.SessionID].IdClient == "1096")
                            {
                                cmdQuery += " , (select AVE_USD from [dbo].[fnCalMediaValue] (m.id) ) as AVE ";
                                cmdQuery += " , (select OTS from [dbo].[fnCalMediaValue] (m.id) ) as OTS ";
                            }
                            cmdQuery += " FROM iadvisers.dbo.metadata m with(nolock)";
                            cmdQuery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock)";
                            cmdQuery += " ON iadvisers.dbo.clientarticles.id = m.ID ";
                            cmdQuery += " JOIN iadvisers.dbo.contents c with(nolock) ON c.id = m.ID";
                            cmdQuery += " WHERE cast(clientarticles.clientid as varchar ) + cast(clientarticles.subjectid as varchar) IN ( '" + t.Value.SubTemas.Replace(";", "','") + "' ) ";
                            cmdQuery += " order by m.date desc, IDMETA desc ";
                        }
                        else
                        {
                            cmdQuery = "SELECT DISTINCT TOP 80 m.ID as IDMETA, " + Title + ", EDITOR, DATE, filepath, page_url, m.tipo as tipo, cast(c.text as nvarchar(max)) as text ";
                            if (Global.InsigteManager[Session.SessionID].IdClient == "1096")
                            {
                                cmdQuery += " , (select AVE_USD from [dbo].[fnCalMediaValue] (m.id) ) as AVE ";
                                cmdQuery += " , (select OTS from [dbo].[fnCalMediaValue] (m.id) ) as OTS ";
                            }
                            cmdQuery += " FROM iadvisers.dbo.metadata m with(nolock)";
                            cmdQuery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock)";
                            cmdQuery += " ON iadvisers.dbo.clientarticles.id = m.ID ";
                            cmdQuery += " JOIN iadvisers.dbo.contents c with(nolock) ON c.id = m.ID";
                            cmdQuery += " WHERE clientarticles.clientid = '" + t.Value.SubTemas.Substring(0, 4) + "' ";
                            cmdQuery += " order by m.date desc, IDMETA desc ";
                        }
                    }
                }

            }

            SqlCommand cmd = new SqlCommand(cmdQuery);
            DataTable dt = GetData(cmd);

            //Create a dummy GridView
            GridView GridView1 = new GridView();
            GridView1.AllowPaging = false;
            GridView1.DataSource = dt;
            GridView1.DataBind();

            string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=" + qsClid + "_" + qsTema + "_" + timestamp + ".xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";

            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                //Apply text style to each Row
                GridView1.Rows[i].Attributes.Add("class", "textmode");
            }
            GridView1.RenderControl(hw);
            //style to format numbers to string

            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }

        protected string sReturnImgLinkDetalhe(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["ID"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["ID"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = "Imgs/icons/black/png/clipboard_copy_icon_16_sel.png";
                        }
                        else
                        {
                            sLink = "Imgs/icons/black/png/clipboard_copy_icon_16.png";
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = "Imgs/icons/black/png/clipboard_copy_icon_16_sel.png";
                        }
                        else
                        {
                            sLink = "Imgs/icons/black/png/clipboard_copy_icon_16.png";
                        }
                    }
                }
            }
            else
            {
                sLink = "Imgs/icons/black/png/clipboard_copy_icon_16.png";
            }
            return sLink;
        }

        protected string sReturnIdLinkDetalhe(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["ID"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["ID"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = string.Empty;
                        }
                        else
                        {
                            sLink = strValue;
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = string.Empty;
                        }
                        else
                        {
                            sLink = strValue;
                        }
                    }
                }
            }
            else
            {
                sLink = strValue;
            }
            return sLink;
        }

        protected string sReturnImgPDF(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16_sel.png";
                        }
                        else
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16.png";
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16_sel.png";
                        }
                        else
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16.png";
                        }
                    }
                }
            }
            else
            {
                sLink = "Imgs/icons/black/png/book_side_icon_16.png";
            }
            return sLink;
        }

        protected string sReturnIdPDF(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = string.Empty;
                        }
                        else
                        {
                            sLink = strValue;
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = string.Empty;
                        }
                        else
                        {
                            sLink = strValue;
                        }
                    }
                }
            }
            else
            {
                sLink = strValue;
            }
            return sLink;
        }

        protected void addLinkPDF(string qsPDFArtigo)
        {

            string ValCookie = string.Empty;
            string PDFCookie = string.Empty;
            string DelCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

            if (cookie.Values["PDF"] != "")
            {
                PDFCookie = cookie.Values["PDF"] + "|" + qsPDFArtigo;
            }
            else
            {
                PDFCookie = qsPDFArtigo;
            }

            DelCookie = cookie.Values["DELNOT"];
            ValCookie = cookie.Values["ID"];

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = PDFCookie;
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(360);
            Response.Cookies.Add(newCookie);

            //lblArtigoAddPDF.Text = "O artigo de ID: " + qsPDFArtigo + ", foi adicionado com sucesso.";

        }

        protected void addNews(string qsIDArtigo)
        {
            string ValCookie = string.Empty;
            string PDFCookie = string.Empty;
            string DelCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

            if (cookie.Values["ID"] != "")
            {
                ValCookie = cookie.Values["ID"] + "|" + qsIDArtigo;
            }
            else
            {
                ValCookie = qsIDArtigo;
            }

            DelCookie = cookie.Values["DELNOT"];
            PDFCookie = cookie.Values["PDF"];

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = PDFCookie;
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(360);
            Response.Cookies.Add(newCookie);

            //lblArtigoAdd.Text = "O artigo de ID: " + qsIDArtigo + ", foi adicionado com sucesso.";
        }

        protected void bt_AddPDF_Command(Object sender, CommandEventArgs e)
        {
            addLinkPDF(e.CommandArgument.ToString());
            lblDataToday.Text = e.CommandArgument.ToString() + " : Adicionado com sucesso!";

            ImageButton b = sender as ImageButton;
            b.ImageUrl = "Imgs/icons/black/png/book_side_icon_16_sel.png";
        }

        protected void bt_AddArquivo_Command(Object sender, CommandEventArgs e)
        {
            addNews(e.CommandArgument.ToString());
            lblDataToday.Text = e.CommandArgument.ToString() + " : Adicionado com sucesso!";

            ImageButton b = sender as ImageButton;
            b.ImageUrl = "Imgs/icons/black/png/clipboard_copy_icon_16_sel.png";
        }

        protected void btnDossier_Click(object sender, EventArgs e)
        {
            lblDataToday.Visible = true;

            string ValCookie = string.Empty;
            string PDFCookie = string.Empty;
            string DelCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
            String Artigos = "";

            foreach (DataGridItem drv in dgSubjectACN.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
                ImageButton btn = (ImageButton)drv.FindControl("ImageButton1");
                if (chk.Checked)
                {
                    Artigos += chk.Attributes["NewsId"].ToString() + "|";
                    btn.ImageUrl = "Imgs/icons/black/png/book_side_icon_16_sel.png";
                    chk.Checked = false;
                }
            }

            Artigos = Artigos.TrimEnd('|');

            if (cookie.Values["PDF"] != "")
            {
                PDFCookie = cookie.Values["PDF"] + "|" + Artigos;
            }
            else
            {
                PDFCookie = Artigos;
            }


            DelCookie = cookie.Values["DELNOT"];
            ValCookie = cookie.Values["ID"];

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = PDFCookie;
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(360);
            Response.Cookies.Add(newCookie);

            //drv.Cells[0].Text;

            lblDataToday.Text += " Artigos adicionados com sucesso!";

        }

        protected void btnNewsLetterCp_Click(object sender, EventArgs e)
        {
            lblDataToday.Visible = true;

            string query = "";

            foreach (DataGridItem drv in dgSubjectACN.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
                if (chk.Checked)
                {
                    query = " insert into ML10F_CLIENT_NEWSLETTER_CHOICE ";
                    query += " (ID_CLIENT_USER,ID_CLIENT,ID_NEWS) ";
                    query += " values(" + Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString() + "," + Global.InsigteManager[Session.SessionID].IdClient.ToString() + "," + chk.Attributes["NewsId"].ToString() + "); ";

                    DataTable NewsLetterChoice = Global.InsigteManager[Session.SessionID].getTableQuery(query, "InsNewsLetterChoice");
                }
            }
            //string ValCookie = string.Empty;
            //string PDFCookie = string.Empty;
            //string DelCookie = string.Empty;

            //HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
            //String Artigos = "";
            //foreach (DataGridItem drv in dgNewsACN.Items)
            //{
            //    CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
            //    if (chk.Checked)
            //    {
            //        Artigos += chk.Attributes["NewsId"].ToString() + "|";
            //    }
            //}

            //Artigos = Artigos.TrimEnd('|');

            //if (cookie.Values["PDF"] != "")
            //{
            //    PDFCookie = cookie.Values["PDF"] + "|" + Artigos;
            //}
            //else
            //{
            //    PDFCookie = Artigos;
            //}

            //DelCookie = cookie.Values["DELNOT"];
            //ValCookie = cookie.Values["ID"];

            //cookie.Expires = DateTime.Now.AddDays(-1d);
            //Response.Cookies.Add(cookie);

            //HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            //newCookie["Version"] = "JUL2012";
            //newCookie["ID"] = ValCookie;
            //newCookie["PDF"] = PDFCookie;
            //newCookie["DELNOT"] = DelCookie;
            //newCookie.Expires = DateTime.Now.AddDays(360);
            //Response.Cookies.Add(newCookie);

            ////drv.Cells[0].Text;

            //lblDataToday.Text += " Artigos adicionados com sucesso!";
        }

        protected void btnAddtoFolders_Click(object sender, EventArgs e)
        {

            string chkbox = "";
            foreach (DataGridItem drv in dgSubjectACN.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
                if (chk.Checked)
                {
                    chkbox += chk.Attributes["NewsId"].ToString() + ";";

                }
            }

            ScriptManager.RegisterStartupScript(Page, typeof(Page), "OpenWindow", "window.open('Adicionar.aspx?ID=" + chkbox.ToString() + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no');", true);

            //lblDataToday.Visible = true;
            //lblDataToday.Text = chkbox;
        }

        protected void btnAddtoClip_Click(object sender, EventArgs e)
        {
            string items = "";
            foreach (DataGridItem drv in dgSubjectACN.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
                ImageButton btn = (ImageButton)drv.FindControl("bt_AddArquivo");
                if (chk.Checked)
                {

                    //RemArt(chk.Attributes["NewsId"].ToString());
                    items += chk.Attributes["NewsId"].ToString() + "|";
                    btn.ImageUrl = "Imgs/icons/black/png/clipboard_copy_icon_16_sel.png";
                    chk.Checked = false;
                    //drv.Visible = false;

                }
            }
            addNews(items.TrimEnd('|'));

        }

        protected void chkSelAll_changed(object sender, EventArgs e)
        {
            CheckBox chk2 = (CheckBox)sender;
            //CheckBox chk1 = (CheckBox)dgPasta.FindControl("chkSelAll");
            foreach (DataGridItem drv in dgSubjectACN.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");

                chk.Checked = chk2.Checked;
            }
        }

    }
}