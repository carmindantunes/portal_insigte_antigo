﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using Microsoft.Reporting.WebForms;
using System.Collections;
using System.Net;

namespace Insigte.Media_Measurement
{
    public partial class PrevMediaReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            if (!Page.IsPostBack)
            {


                String SReport = Global.InsigteManager[Session.SessionID].inUser.RSVLastReport; // Session["Report"].ToString();
                String qsReport = Request.QueryString["rld"];


                if (qsReport != null && qsReport.Length > 0)
                    SReport = "/MediaReports/" + qsReport;// Global.InsigteManager[Session.SessionID].getDecrypt(qsReport);

                List<ReportParameter> reportParams = new List<ReportParameter>();
                reportParams.Add(new ReportParameter("COD_TEMA_CLI", Global.InsigteManager[Session.SessionID].TemaCliente));
                reportParams.Add(new ReportParameter("ID_CLIENT", Global.InsigteManager[Session.SessionID].IdClient));
                reportParams.Add(new ReportParameter("ID_CLIENT_USER", Global.InsigteManager[Session.SessionID].inUser.IdClientUser));
                reportParams.Add(new ReportParameter("Language", Global.InsigteManager[Session.SessionID].inUser.CodLanguage));

                //Session["Report"] = "/";
                // NetworkCredential myCred = new NetworkCredential("in_app", "in.app.2013", "insigte.com");
                
                //RV_MediaMeasurement.ServerReport.ReportServerCredentials.NetworkCredentials = myCred;
                RV_MediaMeasurement.ServerReport.ReportPath = SReport;
                RV_MediaMeasurement.ServerReport.SetParameters(reportParams);
                
            }
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
           DatePickers.Value = string.Join(",", (GetDateParameters().ToList().ToArray()));
        }
        private IEnumerable<string> GetDateParameters()
        {
            // I'm assuming report view control id as reportViewer
            foreach (ReportParameterInfo info in RV_MediaMeasurement.ServerReport.GetParameters())
            {
                if (info.DataType == ParameterDataType.DateTime)
                {
                    yield return string.Format("[{0}]", info.Prompt);
                }
            }
        }


        protected void RV_test_SubmittingParameters(object sender, ReportParametersEventArgs e)
        {
            //e.Parameters["Cliente"].Values.Add(Global.InsigteManager[Session.SessionID].TemaCliente);
            //e.Parameters["ID_CLIENT"].Values.Add(Global.InsigteManager[Session.SessionID].IdClient);
        }
    }
}