﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Drawing;
using System.IO;
using System.Web.UI.DataVisualization.Charting;

namespace Insigte
{
    public partial class GeoMZ : BasePage
    {
        Boolean GRconcorrencia = false;
        String Concorrencia = "";
        Int32 TotalConc = 1;
        String TemaCores = "";
        String[] cores = new String[] { "#f89c3e", "#515151", "#3e93b2", "#808080", "#002d56" };

        private String idCountry = "1";
        protected double Total = 0;
        protected String Filtro = "";
        
        //protected DataTable ConcorrenciaCount = new DataTable();
        //protected DataTable newstotal = new DataTable();
        //protected DataTable news = new DataTable();
        //protected DataTable newsCliReg = new DataTable();

        

        protected void Page_Load(object sender, EventArgs e)
        {

            this.upcont1.Load += new EventHandler(upcont1_Load);
            this.UpdatePanel1.Load += new EventHandler(UpdatePanel1_Load);

            if (!Page.IsPostBack)
            {
                if (!Global.InsigteManager[Session.SessionID].IsLogged)
                    return;


                getCores();
                DivChartCon6M.Visible = false;
                //DivChartCon6M1.Visible = false;
                porvNome.Visible = false;
                getPaths();
            }
            else
            {
                getCores();
            }

        }

        protected void UpdatePanel1_Load(object sender, EventArgs e)
        {//Request["__EVENTARGUMENT"].Split(';').First().Equals("ChartZoom")


            if (IsPostBack)
            {

                if (!Request["__EVENTARGUMENT"].Split(';').First().Equals("ChartZoom"))
                    return;
                //lblDataToday.Visible = true;
                //lblDataToday.Text = Request["__EVENTARGUMENT"].ToString();
                ////imgtest.Attributes["Scr"] =
                ////imgtest.Src = "Imgs/sprites/WorldMap.gif";
                string[] arg = Request["__EVENTARGUMENT"].Split(';');

                if (arg.Length > 2)//chart 1b e 2b , 3
                {

                    chartZoom(arg[1].ToString(), arg[2].ToString());
                }

                if (arg.Length == 2)//chart 1a, 2a 
                {
                    chartZoom(arg[1].ToString(), "");
                }
            }
        }

        protected void chartZoom(String idchart, String provnome)
        {
            if (idchart == "1a")
                preencheChart1a(chZoom, "Zoom");
            if (idchart == "2a")
                preencheChart2a(chZoom, "Zoom");
            if (idchart == "1b")
                preencheChart1b(provnome, chZoom, "Zoom");
            if (idchart == "2b")
                preencheChart2b(provnome, chZoom, "Zoom");
            if (idchart == "3")
                preencheChart3(provnome, chZoom, "Zoom");

        }

        protected void getCores()
        {
            foreach (KeyValuePair<int, INinsigteManager.UserTemas> pair in Global.InsigteManager[Session.SessionID].inUser.Temas)
            {
                if (pair.Value.Name == "Concorrência" || pair.Value.Name == "Competitors")
                {
                    GRconcorrencia = true;

                    String[] Conc = pair.Value.SubTemas.Split(';');

                    foreach (String x in Conc)
                    {
                        TotalConc++;
                        Concorrencia += "'" + x.ToString().Substring(4, 3) + "',";
                    }

                    Concorrencia = Concorrencia + "'" + Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3) + "'";
                    TemaCores = pair.Value.TemaCores;
                    cores = TemaCores.Split(',');
                }
            }
        }

        //cria o grafico 1a e chama 1b
        protected void upcont1_Load(object sender, EventArgs e)
        {
            getCores();
            //try
            //{
            //}catch(Exception){};
            //se for postBack é porque foi selecionada uma regiao entao é necessario carregar os 4 graficos
            if (IsPostBack)
            {
                if (Request["__EVENTARGUMENT"].Split(';').First().Equals("ChartZoom"))
                    return;

                if (Request["__EVENTARGUMENT"].Contains(';'))
                {
                    string[] arg = Request["__EVENTARGUMENT"].Split(';');
                    porvNome.Visible = true;
                    porvNome.InnerText = arg[0].ToString();
                    //teste.Visible = false;

                    preencheChart1b(arg[0].ToString(), chEvolReg, "");//grafico 1achEvolReg.Visible = false;               
                    preencheChart2b(arg[0].ToString(), chEvolRegCli, "");//grafico 2a               
                    preencheChart3(arg[0].ToString(), chcGeral, "");//grafico 3
                    //preencheChart4(arg[0].ToString(), chdGeral);//grafico 4
                }
                else
                {
                    porvNome.Visible = false;
                    DivChartCon6M.Visible = false;
                    preencheChart1a(chEvolReg, "");
                    preencheChart2a(chEvolRegCli, "");
                }
            }
            else//primeira pagina mostra os 2 graficos do top-right
            {
                preencheChart1a(chEvolReg, "");
                preencheChart2a(chEvolRegCli, "");
            }
        }

        protected void preencheChart4(String provNome, Chart _chartNome)
        {
            //string QueryMktTipoGeralGroup = " select s.name as Mercado,count(1) as Total,m.tipo ";
            //QueryMktTipoGeralGroup += " from contents c ";
            //QueryMktTipoGeralGroup += " inner join metadata m ";
            //QueryMktTipoGeralGroup += " on c.id = m.id ";
            //QueryMktTipoGeralGroup += " and m.date > (getdate()-90) ";
            //QueryMktTipoGeralGroup += " and m.tipo <> 'Insigte' ";
            //QueryMktTipoGeralGroup += " inner join clientarticles ca ";
            //QueryMktTipoGeralGroup += " on m.id = ca.id ";
            //QueryMktTipoGeralGroup += " inner join subject s ";
            //QueryMktTipoGeralGroup += " on ca.clientid = s.clienteid ";
            //QueryMktTipoGeralGroup += " and ca.subjectid = s.subjectid ";
            //QueryMktTipoGeralGroup += " and ca.clientid = 1084 ";
            //QueryMktTipoGeralGroup += " where CONTAINS (text, '\"" + provNome + "\"') and m.id in(select id from clientarticles WHERE cast(clientid as varchar) + cast(subjectid as varchar) = '" + Global.InsigteManager[Session.SessionID].TemaCliente + "')";
            //QueryMktTipoGeralGroup += " group by s.name,m.tipo ";

            //string _chartAreas = "cadGeral" ;
            //string _chartTittle = "ttdGeral";
            //try
            //{
            //    DataTable GrMktTipoGeralGroup = Global.InsigteManager[Session.SessionID].getTableQuery(QueryMktTipoGeralGroup, "GetMktTipoGeralGroup");
            //    if (GrMktTipoGeralGroup.Rows.Count > 0)
            //    {
            //        //DivChartCon6M1.Visible = true;
            //        Int32[] yValues = new Int32[GrMktTipoGeralGroup.Rows.Count];
            //        string[] xValues = new string[GrMktTipoGeralGroup.Rows.Count];
            //        string[] zValues = new string[GrMktTipoGeralGroup.Rows.Count];

            //        int counter = 0;
            //        foreach (DataRow row in GrMktTipoGeralGroup.Rows)
            //        {
            //            yValues[counter] = Convert.ToInt32(row["Total"].ToString());
            //            xValues[counter] = row["Mercado"].ToString();
            //            zValues[counter] = row["tipo"].ToString();
            //            counter++;
            //        }

            //        string AuxNew = "";
            //        int i = 0;
            //        foreach (string c in zValues)
            //        {
            //            if (!AuxNew.Contains(c))
            //            {
            //                _chartNome.Series.Add(c);
            //                //por cor
            //                _chartNome.Legends.Add(c);
            //                _chartNome.Legends[c].ItemColumnSpacing = 1;
            //                //_chartNome.Legends[c].IsDockedInsideChartArea = true;
            //                _chartNome.Legends[c].Docking = Docking.Right;
            //                AuxNew += c + " , ";
            //                i++;
            //            }
            //        }

            //        counter = 0;
            //        foreach (string c in zValues)
            //        {
            //            _chartNome.Series[c].Points.AddXY(xValues[counter], yValues[counter]);
            //            counter++;
            //        }
            //        AuxNew = "";
            //        foreach (string c in zValues)
            //        {
            //            if (c != AuxNew)
            //            {
            //                //_chartNome.Series[c].PostBackValue = c.ToString() + "-" + "#VALX";
            //                // Set series chart type
            //                _chartNome.Series[c].ChartType = SeriesChartType.Column;
            //                // Set series point width
            //                _chartNome.Series[c]["PointWidth"] = "0.5";
            //                // Show data points labels
            //                _chartNome.Series[c].IsValueShownAsLabel = false;
            //                //_chartNome.Series[c].LabelAngle = 45;
            //                // Set data points label style
            //                _chartNome.Series[c]["BarLabelStyle"] = "Center";
            //                // Show as 3D
            //                //Chart1.ChartAreas[_chartSeries].Area3DStyle.Enable3D = false;
            //                // Draw as 3D Cylinder
            //                _chartNome.Series[c]["DrawingStyle"] = "Default";
            //                _chartNome.Series[c].ToolTip = "#VALX [#VALY]";

            //                _chartNome.Series[c].IsXValueIndexed = false;
            //                _chartNome.Series[c].IsVisibleInLegend = true;
            //            }
            //            AuxNew = c;
            //        }

            //        _chartNome.Titles[_chartTittle].Text = Global.InsigteManager[Session.SessionID].NomCliente.ToString() + " | 90 Dias | Group Tipo | Por Mercados | " + provNome;
            //        _chartNome.Titles[_chartTittle].Alignment = ContentAlignment.TopLeft;

            //        _chartNome.ChartAreas[_chartAreas].AxisX.Interval = 0.5;
            //        _chartNome.ChartAreas[_chartAreas].AxisX.IntervalAutoMode = IntervalAutoMode.FixedCount;

            //        _chartNome.ChartAreas[_chartAreas].AxisX.IsLabelAutoFit = true;
            //        //_chartNome.ChartAreas[_chartAreas].AxisX.LabelStyle.Angle = -35;
            //        //_chartNome.ChartAreas[_chartAreas].AxisX.LabelAutoFitStyle = LabelAutoFitStyles.LabelsAngleStep30;
            //        _chartNome.ChartAreas[_chartAreas].AxisX.LabelStyle.Enabled = true;

            //        _chartNome.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            //        _chartNome.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            //        //DivChartCon6M1.Visible = true;
            //    }
            //    else
            //    {
            //        //DivChartCon6M1.Visible = false;
            //    }//fim if


            //}
            //catch (Exception e)
            //{
            //    lblDataToday.Visible = true;
            //    lblDataToday.Text += "<br> GetMktTipoGeralGroup:" + e.Message.ToString();
            //    lblDataToday.Text += "<br>" + QueryMktTipoGeralGroup;
            //}
        }

        protected void preencheChart3(String provNome, Chart _chartNome, String nome)
        {
            string QueryMktTipoGeralGroup = "";
            QueryMktTipoGeralGroup += " select ID_CLIENT,COD_NOM as cliente,VAl_VALOR as Total from GM10F_TEMA_COUNTRY with(nolock) ";
            QueryMktTipoGeralGroup += " where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " AND COD_QUERY = 'REGCLIVSCONTOTGROUP' AND ID_COUNTRY = " + idCountry + " AND COD_REGION = '" + provNome + "' ";
            QueryMktTipoGeralGroup += " order by VAl_VALOR  desc ";

            if (nome == "")
            {
                _chartNome.Attributes.Add("value", "3");
                _chartNome.Attributes.Add("onclick", "teste(this);");
                _chartNome.Attributes.Add("onmouseover", "chartmouseover(this);");
                _chartNome.Attributes.Add("onmouseout", "chartmouseout(this);");
                nome = "cGeral";
            }
            string _chartSeries = "sr" + nome;
            string _chartAreas = "ca" + nome;
            string _chartTittle = "tt" + nome;

            try
            {

                DataTable GrMktTipoGeralGroup = Global.InsigteManager[Session.SessionID].getTableQuery(QueryMktTipoGeralGroup, "GetMktTipoGeralGroup");
                if (GrMktTipoGeralGroup.Rows.Count > 0)
                {

                    Int32[] yValues = new Int32[GrMktTipoGeralGroup.Rows.Count];
                    string[] xValues = new string[GrMktTipoGeralGroup.Rows.Count];
                    string[] idcliente = new string[GrMktTipoGeralGroup.Rows.Count];
                    int counter = 0;
                    foreach (DataRow row in GrMktTipoGeralGroup.Rows)
                    {
                        yValues[counter] = Convert.ToInt32(row["Total"].ToString());
                        xValues[counter] = row["cliente"].ToString();
                        idcliente[counter] = row["ID_CLIENT"].ToString();
                        counter++;
                    }

                    _chartNome.Series[_chartSeries].Points.DataBindXY(xValues, yValues);

                    counter = 0;
                    foreach (DataRow row in GrMktTipoGeralGroup.Rows)
                    {
                        if (row["cliente"].ToString().ToLower() == Global.InsigteManager[Session.SessionID].NomCliente.ToString().ToLower())
                        {
                            if (Global.InsigteManager[Session.SessionID].IdClient.ToString().ToLower().Equals(row["ID_CLIENT"].ToString().ToLower()))
                            {
                                if (Global.InsigteManager[Session.SessionID].CorCliente == null)
                                {
                                    _chartNome.Series[_chartSeries].Points[counter].Color = ColorTranslator.FromHtml("#f89c3e");
                                }
                                else
                                {
                                    _chartNome.Series[_chartSeries].Points[counter].Color = ColorTranslator.FromHtml(Global.InsigteManager[Session.SessionID].CorCliente.ToString());
                                }
                            }
                            else
                            {
                                _chartNome.Series[_chartSeries].Points[counter].Color = ColorTranslator.FromHtml("#f89c3e");
                            }
                        }
                        else
                        {
                            _chartNome.Series[_chartSeries].Points[counter].Color = ColorTranslator.FromHtml("#d5d5d5");
                        }
                        //_chartNome.Series["srEvolRegCli"].Points[counter].Url = "~/Subject.aspx?clid=" + row["CLID"].ToString() + "&tema=" + row["SBID"].ToString() + "&rg=" + row["Prov"].ToString() + "&fon=0&dd=90";
                        counter++;
                    }
                    string text = Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt" ? " VS Competitors | 180 days | " : " VS Concorrência | 180 dias | ";
                    _chartNome.Titles[_chartTittle].Text = Global.InsigteManager[Session.SessionID].NomCliente.ToString() + text + provNome;
                    _chartNome.Titles[_chartTittle].Alignment = ContentAlignment.TopLeft;
                    // Set series chart type
                    _chartNome.Series[_chartSeries].ChartType = SeriesChartType.Column;
                    // Set series point width
                    _chartNome.Series[_chartSeries]["PointWidth"] = "0.5";
                    // Show data points labels
                    _chartNome.Series[_chartSeries].IsValueShownAsLabel = true;
                    // Set data points label style
                    _chartNome.Series[_chartSeries]["BarLabelStyle"] = "Center";
                    // Show as 3D
                    //Chart1.ChartAreas[_chartSeries].Area3DStyle.Enable3D = false;
                    // Draw as 3D Cylinder
                    _chartNome.Series[_chartSeries]["DrawingStyle"] = "Default";
                    _chartNome.Series[_chartSeries].ToolTip = "#VALX [#VALY]";
                    _chartNome.Series[_chartSeries].IsXValueIndexed = false;
                    _chartNome.Series[_chartSeries].IsVisibleInLegend = true;
                    _chartNome.ChartAreas[_chartAreas].AxisX.LabelStyle.Enabled = true;
                    _chartNome.ChartAreas[_chartAreas].AxisX.Interval = 1;
                    _chartNome.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                    _chartNome.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                    DivChartCon6M.Visible = true;
                }
            }
            catch (Exception)
            {
            }
        }

        protected void preencheChart2b(String provNome, Chart _chartNome, String nome)
        {
            string QueryMktTipoCli = "";

            QueryMktTipoCli += " select  top 5 COD_REGION as Prov, VAL_VALOR as total,COD_NOM as nome  from GM10F_TEMA_COUNTRY with(nolock) ";
            QueryMktTipoCli += " where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " AND COD_QUERY = 'REGCLICONTOTGROUP' AND ID_COUNTRY = " + idCountry + " AND COD_REGION = '" + provNome + "' ";
            QueryMktTipoCli += " order by VAL_VALOR desc ";

            if (nome == "")
            {
                _chartNome.Attributes.Add("value", "2b");
                _chartNome.Attributes.Add("onclick", "teste(this);");
                _chartNome.Attributes.Add("onmouseover", "chartmouseover(this);");
                _chartNome.Attributes.Add("onmouseout", "chartmouseout(this);");
                nome = "EvolRegCli";
            }

            string _chartSeries = "sr" + nome;
            string _chartAreas = "ca" + nome;
            string _chartTittle = "tt" + nome;

            try
            {

                DataTable GrMktTipoCliGroup = Global.InsigteManager[Session.SessionID].getTableQuery(QueryMktTipoCli, "GetGrMktTipoCliGroup");

                if (GrMktTipoCliGroup.Rows.Count > 0)
                {
                    Int32[] yValues = new Int32[GrMktTipoCliGroup.Rows.Count];
                    string[] xValues = new string[GrMktTipoCliGroup.Rows.Count];

                    int counter = 0;
                    foreach (DataRow row in GrMktTipoCliGroup.Rows)
                    {
                        yValues[counter] = Convert.ToInt32(row["total"].ToString());
                        xValues[counter] = row["nome"].ToString();
                        counter++;
                    }

                    _chartNome.Series[_chartSeries].Points.DataBindXY(xValues, yValues);


                    counter = 0;
                    foreach (DataRow row in GrMktTipoCliGroup.Rows)
                    {

                        _chartNome.Series[_chartSeries].Points[counter].Color = ColorTranslator.FromHtml(cores[counter]);
                        counter++;
                    }
                    _chartNome.Legends.Add(_chartSeries);
                    string text = Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt" ? "Top 5 | 180 days | " : "Top 5 | 180 Dias";
                    _chartNome.Titles[_chartTittle].Text = text;
                    _chartNome.Titles[_chartTittle].Alignment = ContentAlignment.TopLeft;
                    // Set series chart type
                    _chartNome.Series[_chartSeries].ChartType = SeriesChartType.Pie;
                    // Set series point width
                    _chartNome.Series[_chartSeries]["PointWidth"] = "0.5";
                    // Show data points labels
                    _chartNome.Series[_chartSeries].IsValueShownAsLabel = true;
                    // Set data points label style
                    _chartNome.Series[_chartSeries]["BarLabelStyle"] = "Center";
                    // Show as 3D
                    //Chart1.ChartAreas[_chartSeries].Area3DStyle.Enable3D = false;
                    // Draw as 3D Cylinder
                    _chartNome.Series[_chartSeries]["DrawingStyle"] = "Default";
                    _chartNome.Series[_chartSeries].ToolTip = "#VALX [#VALY]";
                    _chartNome.Series[_chartSeries].IsXValueIndexed = false;
                    _chartNome.Series[_chartSeries].IsVisibleInLegend = true;
                    _chartNome.ChartAreas[_chartAreas].AxisX.LabelStyle.Enabled = true;
                    _chartNome.ChartAreas[_chartAreas].AxisX.Interval = 1;
                    _chartNome.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                    _chartNome.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                }

            }
            catch (Exception) { }
        }

        protected void preencheChart2a(Chart _chartNome, String nome)
        {
            string QueryCli = "";
            QueryCli += " select COD_VALOR as Prov, VAL_VALOR as total from GM10f_TEMA_COUNTRY with(nolock) ";
            QueryCli += " where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " and COD_QUERY = 'REGCLITOT' and id_COUNTRY = " + idCountry + " ORDER BY VAL_VALOR Desc ";

            if (nome == "")
            {
                _chartNome.Attributes.Add("value", "2a");
                _chartNome.Attributes.Add("onclick", "teste(this);");
                _chartNome.Attributes.Add("onmouseover", "chartmouseover(this);");
                _chartNome.Attributes.Add("onmouseout", "chartmouseout(this);");
                nome = "EvolRegCli";
            }

            string _chartSeries = "sr" + nome;
            string _chartAreas = "ca" + nome;
            string _chartTittle = "tt" + nome;
            try
            {
                DataTable newsCliReg = Global.InsigteManager[Session.SessionID].getTableQuery(QueryCli, "getNotRegion");
                if (newsCliReg.Rows.Count > 0)
                {

                    Int32[] yValues = new Int32[newsCliReg.Rows.Count];
                    string[] xValues = new string[newsCliReg.Rows.Count];
                    int counter = 0;
                    foreach (DataRow r in newsCliReg.Rows)
                    {
                        yValues[counter] = Convert.ToInt32(r["total"].ToString());
                        xValues[counter] = r["prov"].ToString();
                        counter++;
                    }

                    _chartNome.Series[_chartSeries].Points.DataBindXY(xValues, yValues);

                    counter = 0;
                    foreach (DataRow r in newsCliReg.Rows)
                    {
                        _chartNome.Series[_chartSeries].Points[counter].Color = ColorTranslator.FromHtml("#f89c3e");
                        //chEvolReg.Series[_chartSeries].Points[counter].Url = "~/Subject.aspx?clid=" + r["CLID"].ToString() + "&tema=" + r["SBID"].ToString() + "&rg="+r["Prov"].ToString() + "&fon=0&dd=180";
                        counter++;
                    }
                    string text = Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt" ? "180 days | " : "180 Dias";
                    _chartNome.Titles[_chartTittle].Text = Global.InsigteManager[Session.SessionID].NomCliente.ToString() + " | " + text;
                    _chartNome.Titles[_chartTittle].Alignment = ContentAlignment.TopLeft;
                    _chartNome.Series[_chartSeries].ChartType = SeriesChartType.Column;
                    _chartNome.Series[_chartSeries]["PointWidth"] = "0.5";
                    _chartNome.Series[_chartSeries].IsValueShownAsLabel = true;
                    _chartNome.Series[_chartSeries]["BarLabelStyle"] = "Center";
                    _chartNome.Series[_chartSeries]["DrawingStyle"] = "Default";
                    _chartNome.Series[_chartSeries].ToolTip = "#VALX [#VALY]";
                    _chartNome.Series[_chartSeries].IsXValueIndexed = false;
                    _chartNome.Series[_chartSeries].IsVisibleInLegend = true;
                    _chartNome.ChartAreas[_chartAreas].AxisX.LabelStyle.Enabled = true;
                    _chartNome.ChartAreas[_chartAreas].AxisX.Interval = 1;
                    _chartNome.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                    _chartNome.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                }
            }
            catch (Exception e) { e.Message.ToString(); }
        }

        protected void preencheChart1b(String provNome, Chart _chartNome, String nome)
        {


            string QueryMktTipoGeralGroup = "";
            QueryMktTipoGeralGroup += " select VAL_VALOR,COD_VALOR,COD_NOM ";
            QueryMktTipoGeralGroup += " from GM10F_TEMA_COUNTRY TC with(nolock) ";
            QueryMktTipoGeralGroup += " inner join ( select distinct left(COD_DAY,6) as COD_MONTH_ORDER, DES_YEAR_MONTH_ABREV_PT from time_month with(nolock) ) tm on TC.COD_VALOR = tm.DES_YEAR_MONTH_ABREV_PT ";
            QueryMktTipoGeralGroup += " where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " and TC.COD_QUERY = 'REGMESCLICONGROUP' ";
            QueryMktTipoGeralGroup += " and ID_COUNTRY = " + idCountry ;
            QueryMktTipoGeralGroup += " and TC.COD_REGION = '" + provNome + "' ";
            QueryMktTipoGeralGroup += " and Cod_NOM in (select cod_nom from ( select top 5 Cod_NOM,sum(Val_valor) as total from GM10F_TEMA_COUNTRY TC with(nolock) ";
            QueryMktTipoGeralGroup += " where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " and TC.COD_QUERY = 'REGMESCLICONGROUP' ";
            QueryMktTipoGeralGroup += " and ID_COUNTRY = " + idCountry ;
            QueryMktTipoGeralGroup += " and TC.COD_REGION = '" + provNome + "' ";
            QueryMktTipoGeralGroup += " group by  cod_nom order by 2 desc) x ) order by COD_MONTH_ORDER ";

            if (nome == "")
            {
                _chartNome.Attributes.Add("value", "1b");
                _chartNome.Attributes.Add("onclick", "teste(this);");
                _chartNome.Attributes.Add("onmouseover", "chartmouseover(this);");
                _chartNome.Attributes.Add("onmouseout", "chartmouseout(this);");
                nome = "EvolReg";
            }
            string _chartAreas = "ca" + nome;
            string _chartTittle = "tt" + nome;
            //string _chartSeries = "sr" + nome;

            try
            {

                DataTable GrMktTipoGeralGroup = Global.InsigteManager[Session.SessionID].getTableQuery(QueryMktTipoGeralGroup, "GetMktTipoGeralGroup");

                if (GrMktTipoGeralGroup.Rows.Count > 0)
                {

                    Int32[] yValues = new Int32[GrMktTipoGeralGroup.Rows.Count];
                    string[] xValues = new string[GrMktTipoGeralGroup.Rows.Count];
                    string[] zValues = new string[GrMktTipoGeralGroup.Rows.Count];

                    int counter = 0;
                    foreach (DataRow row in GrMktTipoGeralGroup.Rows)
                    {

                        yValues[counter] = Convert.ToInt32(row["VAL_VALOR"].ToString());
                        xValues[counter] = row["COD_VALOR"].ToString();
                        zValues[counter] = row["COD_NOM"].ToString();
                        counter++;
                    }

                    //remover chart serie
                    _chartNome.Series.RemoveAt(0);

                    string AuxNew = "";
                    int i = 0;
                    foreach (string c in zValues)
                    {

                        if (!AuxNew.Contains(c))
                        {
                            _chartNome.Series.Add(c);
                            //por cor
                            _chartNome.Legends.Add(c);
                            _chartNome.Legends[c].ItemColumnSpacing = 1;
                            _chartNome.Legends[c].IsDockedInsideChartArea = true;
                            _chartNome.Legends[c].Docking = Docking.Bottom;
                            _chartNome.Series[c].Color = ColorTranslator.FromHtml(cores[i]);
                            AuxNew += c + " , ";
                            i++;

                        }

                    }

                    counter = 0;
                    foreach (string c in zValues)
                    {

                        _chartNome.Series[c].Points.AddXY(xValues[counter], yValues[counter]);
                        counter++;
                    }
                    AuxNew = "";
                    foreach (string c in zValues)
                    {
                        if (c != AuxNew)
                        {
                            //_chartNome.Series[c].PostBackValue = c.ToString() + "-" + "#VALX";
                            // Set series chart type
                            _chartNome.Series[c].ChartType = SeriesChartType.Line;
                            // Set series point width
                            _chartNome.Series[c]["PointWidth"] = "1";
                            // Show data points labels
                            _chartNome.Series[c].IsValueShownAsLabel = false;
                            //_chartNome.Series[c].LabelAngle = 45;
                            // Set data points label style
                            _chartNome.Series[c]["BarLabelStyle"] = "Center";
                            // Show as 3D
                            //Chart1.ChartAreas[_chartSeries].Area3DStyle.Enable3D = false;
                            // Draw as 3D Cylinder
                            _chartNome.Series[c]["DrawingStyle"] = "Default";
                            _chartNome.Series[c].ToolTip = "#VALX [#VALY]";

                            _chartNome.Series[c].IsXValueIndexed = false;
                            _chartNome.Series[c].IsVisibleInLegend = true;
                        }
                        AuxNew = c;
                    }
                    //for (int j = 0; j < 5; i++)
                    //{
                    //    _chartNome.Series[j].Points[counter].Color = ColorTranslator.FromHtml(cores[counter]);
                    //}
                    string text = Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt" ? "Top 5 | 180 days | " : "Top 5 | 180 Dias";
                    _chartNome.Titles[_chartTittle].Text = text; //Global.InsigteManager[Session.SessionID].NomCliente.ToString() + " | 90 Dias | Group Tipo | Por Mercados | " + provNome;//+ Global.InsigteManager[Session.SessionID].NomCliente.ToString();
                    _chartNome.Titles[_chartTittle].Alignment = ContentAlignment.TopLeft;

                    _chartNome.ChartAreas[_chartAreas].AxisX.Interval = 0.5;
                    _chartNome.ChartAreas[_chartAreas].AxisX.IntervalAutoMode = IntervalAutoMode.FixedCount;

                    _chartNome.ChartAreas[_chartAreas].AxisX.IsLabelAutoFit = true;
                    //_chartNome.ChartAreas[_chartAreas].AxisX.LabelStyle.Angle = -35;
                    //_chartNome.ChartAreas[_chartAreas].AxisX.LabelAutoFitStyle = LabelAutoFitStyles.LabelsAngleStep30;
                    _chartNome.ChartAreas[_chartAreas].AxisX.LabelStyle.Enabled = true;

                    _chartNome.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                    _chartNome.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                }
            }
            catch (Exception) { }
        }

        protected void preencheChart1a(Chart _chartNome, String nome)
        {
            string Query = "";
            Query += " select COD_VALOR as Prov, sum(VAL_VALOR) as total from GM10F_TEMA_COUNTRY with(nolock) ";
            Query += " where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " and id_country = " + idCountry + " and COD_QUERY in ('REGCONTOT','REGCLITOT') ";
            Query += " group by id_client, COD_VALOR ";
            Query += " order by total Desc";

            if (nome == "")
            {
                _chartNome.Attributes.Add("value", "1a");
                _chartNome.Attributes.Add("onclick", "teste(this);");
                _chartNome.Attributes.Add("onmouseover", "chartmouseover(this);");
                _chartNome.Attributes.Add("onmouseout", "chartmouseout(this);");
                nome = "EvolReg";
            }

            string _chartSeries = "sr" + nome;
            string _chartAreas = "ca" + nome;
            string _chartTittle = "tt" + nome;

            try
            {
                DataTable news = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "getNotRegion");
                if (news.Rows.Count > 0)
                {
                    string text = Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt" ? "Competitors | 180 days" : "Concorrência | 180 Dias";
                    _chartNome.Titles[_chartTittle].Text = text;
                    Int32[] yValues = new Int32[news.Rows.Count];
                    string[] xValues = new string[news.Rows.Count];
                    int counter = 0;
                    foreach (DataRow r in news.Rows)
                    {
                        yValues[counter] = Convert.ToInt32(r["total"].ToString());
                        xValues[counter] = r["Prov"].ToString();
                        counter++;
                    }

                    _chartNome.Series[_chartSeries].Points.DataBindXY(xValues, yValues);

                    counter = 0;
                    foreach (DataRow r in news.Rows)
                    {
                        _chartNome.Series[_chartSeries].Points[counter].Color = ColorTranslator.FromHtml("#f89c3e");
                        //chEvolReg.Series[_chartSeries].Points[counter].Url = "~/Subject.aspx?clid=" + r["CLID"].ToString() + "&tema=" + r["SBID"].ToString() + "&rg="+r["Prov"].ToString() + "&fon=0&dd=180";
                        counter++;
                    }
                    _chartNome.Titles[_chartTittle].Alignment = ContentAlignment.TopLeft;
                    _chartNome.Series[_chartSeries].ChartType = SeriesChartType.Column;
                    _chartNome.Series[_chartSeries]["PointWidth"] = "0.5";
                    _chartNome.Series[_chartSeries].IsValueShownAsLabel = true;
                    _chartNome.Series[_chartSeries]["BarLabelStyle"] = "Center";
                    _chartNome.Series[_chartSeries]["DrawingStyle"] = "Default";
                    _chartNome.Series[_chartSeries].ToolTip = "#VALX [#VALY]";
                    _chartNome.Series[_chartSeries].IsXValueIndexed = false;
                    _chartNome.Series[_chartSeries].IsVisibleInLegend = true;
                    _chartNome.ChartAreas[_chartAreas].AxisX.LabelStyle.Enabled = true;
                    _chartNome.ChartAreas[_chartAreas].AxisX.Interval = 1;
                    _chartNome.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
                    _chartNome.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
                }
            }
            catch (Exception e) { e.Message.ToString(); }
        }

        protected String getTrataFill()
        {
            String Esreturn = "";
            try
            {

                string Query = "";
                Query += " select COD_VALOR as Prov, sum(VAL_VALOR) as total from GM10F_TEMA_COUNTRY with(nolock) ";
                Query += " where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " and id_country = " + idCountry + " and COD_QUERY in ('REGCONTOT','REGCLITOT') ";
                Query += " group by id_client, COD_VALOR ";
                Query += " order by total Desc";
                DataTable news = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "getNotRegion");
                string QueryTotal = "";
                QueryTotal += " select COD_VALOR as Prov, VAL_VALOR as numero from GM10F_TEMA_COUNTRY with(nolock) ";
                QueryTotal += " where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " and COD_QUERY = 'MKTTOTAL' and id_COUNTRY = " + idCountry + " ";
                DataTable newstotal = Global.InsigteManager[Session.SessionID].getTableQuery(QueryTotal, "getNotRegionTotal");
                Total = Convert.ToDouble(newstotal.Rows[0]["numero"].ToString());
                if (news.Rows.Count == 0 || news.Rows == null)
                    return "";


                double Opacity = 0;

                foreach (DataRow y in news.Rows)
                {
                    switch (y["Prov"].ToString())
                    {
                        case "Cabo Delgado":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                Esreturn += " CaboDelgado.setAttribute(\"fill\", \"#323232\");  ";
                                Esreturn += " CaboDelgado.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\"); ";

                                break;
                            }
                        case "Tete":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                Esreturn += " Tete.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\");  ";
                                Esreturn += " Tete.setAttribute(\"fill\", \"#323232\");  ";
                                break;
                            }

                        case "Maputo":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                Esreturn += " Maputo.setAttribute(\"fill\", \"#323232\");  ";
                                Esreturn += " Maputo.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\");  ";
                                break;
                            }
                        case "Manica":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                Esreturn += " Manica.setAttribute(\"fill\", \"#323232\");  ";
                                Esreturn += " Manica.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\");  ";
                                break;
                            }
                        case "Inhambane":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                Esreturn += " Inhambane.setAttribute(\"fill\", \"#323232\");  ";
                                Esreturn += " Inhambane.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\");  ";
                                break;
                            }
                        case "Gaza":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                Esreturn += " Gaza.setAttribute(\"fill\", \"#323232\");  ";
                                Esreturn += " Gaza.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\");  ";
                                break;
                            }
                        case "Sofala":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                Esreturn += " Sofala.setAttribute(\"fill\", \"#323232\");  ";
                                Esreturn += " Sofala.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\");  ";
                                break;
                            }
                        case "Zambezia":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                Esreturn += " Zambezia.setAttribute(\"fill\", \"#323232\");  ";
                                Esreturn += " Zambezia.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\");  ";
                                break;
                            }
                        case "Nampula":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                Esreturn += " Nampula.setAttribute(\"fill\", \"#323232\");  ";
                                Esreturn += " Nampula.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\");  ";
                                break;
                            }
                        case "Niassa":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                Esreturn += " Niassa.setAttribute(\"fill\", \"#323232\");  ";
                                Esreturn += " Niassa.setAttribute(\"fill-opacity\", \"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\");  ";
                                break;
                            }
                    }
                }
            }
            catch (Exception)
            {
            }
            Esreturn += " clicked.setAttribute(\"fill\", \"#f89c3e\"); ";
            Esreturn += " clicked.setAttribute(\"fill-opacity\", \"1\"); ";

            return Esreturn;
        }

        protected String getPaths()
        {
            try
            {
                string paths = "";
                double Opacity = 0;

                string Query = "";
                Query += " select COD_VALOR as Prov, sum(VAL_VALOR) as total from GM10F_TEMA_COUNTRY with(nolock) ";
                Query += " where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " and id_country = " + idCountry + " and COD_QUERY in ('REGCONTOT','REGCLITOT') ";
                Query += " group by id_client, COD_VALOR ";
                Query += " order by total Desc";

                DataTable news = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "getNotRegion");

                string QueryTotal = "";
                QueryTotal += " select COD_VALOR as Prov, VAL_VALOR as numero from GM10F_TEMA_COUNTRY with(nolock) ";
                QueryTotal += " where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " and COD_QUERY = 'MKTTOTAL' and id_COUNTRY = " + idCountry + " ";

                DataTable newstotal = Global.InsigteManager[Session.SessionID].getTableQuery(QueryTotal, "getNotRegionTotal");

                Total = Convert.ToDouble(newstotal.Rows[0]["numero"].ToString());

                foreach (DataRow y in news.Rows)
                {
                    switch (y["Prov"].ToString())
                    {
                        case "Cabo Delgado":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                paths += " <path id=\"CaboDelgado\" nome=\"Cabo Delgado\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",", ".").Replace(",", ".") + "\" onclick=\"MouseClick(this)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(this); \" onmouseout=\"mouseOut(this);\" ";
                                paths += " d=\"M 135,66L 140,66L 141,65L 144,65L 144,64L 145,64L 145,63L 147,63L 147,62L 149,62L 149,61L 151,61L 152,60L 153,60L 154,59L 155,58L 160,58L 160,57L 161,57L 162,57L 162,56L 163,56L 163,55L 164,55L 164,54L 167,54L 167,55L 168,55L 168,56L 169,56L 170,56L 171,57L 171,53L 172,52L 172,48L 171,48L 171,45L 172,45L 172,44L 173,43L 173,41L 173,37L 172,37L 172,36L 171,36L 171,34L 172,34L 172,33L 171,33L 171,32L 170,31L 170,24L 171,24L 171,18L 171,13L 172,13L 172,12L 173,11L 174,10L 173,10L 173,7.00003L 174,7.00003L 174,5.00003L 171,5.00003L 171,6.00003L 170,6.00003L 170,7.00003L 169,7.00003L 169,8.00003L 168,8.00003L 168,9.00003L 167,9.00003L 167,10L 166,10L 166,11L 162,11L 162,12L 161,12L 161,13L 157,13L 157,14L 156,14L 156,15L 154,15L 154,16L 153,16L 153,17L 146,17L 145,18L 142,18L 142,19L 140,19L 138,20L 138,21L 137,22L 137,24L 138,28L 137,29L 137,31L 136,32L 135,33L 133,33L 132,34L 131,34L 131,35L 130,35L 130,37L 129,38L 129,42L 128,43L 128,44L 127,44L 127,45L 126,45L 126,47L 125,48L 125,50L 126,50L 126,49L 127,49L 128,48L 129,47L 132,47L 133,48L 134,48L 134,50L 133,51L 133,52L 132,52L 131,53L 129,53L 129,54L 131,54L 131,55L 132,55L 132,56L 132,59L 133,59L 133,61L 134,61L 134,64L 135,64L 135,66 Z \"/> ";
                                break;
                            }
                        case "Tete":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                paths += " <path id=\"Tete\" nome=\"Tete\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\" onclick=\"MouseClick(this)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(this); \" onmouseout=\"mouseOut(this);\" ";
                                paths += " d=\"M 2,78L 30,68L 38,67L 47,63L 50,63L 56,70L 65,70C 65,70 70,69 69,69C 68,69 70,71 70,72C 70,73 70,74 70,74L 72,76L 72,78L 72,83L 68,85L 68,89L 66,90L 66,94L 68,97L 69,101L 78,111L 78,115L 81,116L 81,123L 76,116L 73,111L 67,108L 65,107L 58,102L 55,102L 53,104L 52,107L 50,109L 48,112L 46,112L 45,111L 45,110L 46,107L 46,106L 45,105L 42,104C 42,104 41,103 40,103C 39,103 37,102 37,102L 34,101L 30,100L 28,98L 23,95L 19,94C 19,94 18,94 17,94C 16,94 13,94 13,94L 10,94L 8,93L 7,92C 7,92 6,90 6,89C 6,88 6,85 6,85L 4,83L 2,81L 2,78 Z \"/> ";
                                break;
                            }

                        case "Maputo":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                paths += " <path id=\"Maputo\" nome=\"Maputo\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\" onclick=\"MouseClick(this)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(this); \" onmouseout=\"mouseOut(this);\" ";
                                paths += " d=\"M 28,243L 29,243L 30,244L 31,244L 31,245L 31,248L 32,248L 32,250L 33,250L 34,250L 36,250L 36,249L 37,249L 37,248L 38,248L 38,245L 40,245L 40,247L 41,247L 41,248L 42,248L 42,249L 44,249L 44,250L 43,250L 43,251L 42,251L 42,252L 41,252L 41,253L 40,253L 40,254L 39,254L 39,255L 36,255L 36,256L 36,257L 35,257L 35,258L 34,258L 35,265L 36,266L 36,267L 38,267L 38,266L 39,266L 39,265L 40,265L 40,267L 39,267L 39,268L 38,268L 38,269L 38,274L 29,274L 29,270L 28,270L 28,260L 27,260L 27,252L 28,252L 28,243 Z \"/> ";
                                break;
                            }
                        case "Gaza":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                paths += " <path id=\"Gaza\" nome=\"Gaza\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\" onclick=\"MouseClick(this)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(this); \" onmouseout=\"mouseOut(this);\" ";
                                paths += " d=\"M 19,202L 24,197L 26,195L 29,192L 30,191L 31,190L 32,190L 32,189L 33,189L 33,188L 34,188L 34,187L 36,185L 37,184L 41,188L 44,188L 45,187L 46,187L 46,188L 45,189L 45,194L 46,194L 48,195L 48,201L 47,201L 47,206L 49,208L 52,211L 52,212L 53,212L 53,213L 54,213L 54,214L 55,214L 55,215L 56,215L 56,232L 56,233L 57,234L 57,237L 58,237L 58,238L 60,238L 60,237L 61,237L 61,239L 60,239L 60,243L 61,243L 61,244L 59,244L 59,245L 57,245L 57,246L 55,246L 55,247L 51,247L 51,248L 48,248L 48,249L 45,249L 41,244L 37,244L 37,247L 36,247L 36,248L 35,248L 35,249L 34,249L 34,248L 33,248L 33,245L 32,243L 30,243L 30,242L 29,242L 29,241L 28,241L 28,233L 27,232L 27,227L 25,225L 25,222L 24,221L 24,220L 23,219L 22,219L 22,216L 23,216L 23,213L 22,213L 22,209L 21,208L 20,208L 20,205L 19,205L 19,202 Z \"/> ";
                                break;
                            }
                        case "Inhambane":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                paths += " <path id=\"Inhambane\" nome=\"Inhambane\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\" onclick=\"MouseClick(this)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(this); \" onmouseout=\"mouseOut(this);\" ";
                                paths += " d=\"M 48,205L 57,215L 57,232L 59,234L 59,236L 62,236L 62,240L 61,240L 61,242L 62,242L 62,243L 65,243L 65,242L 70,242L 72,240L 73,240L 76,237L 78,233L 78,232L 79,231L 80,231L 80,228L 77,228L 77,224L 78,223L 79,223L 79,221L 80,220L 80,216L 81,215L 82,214L 82,212L 81,212L 81,208L 82,207L 82,204L 81,200L 79,200L 79,197L 78,196L 77,192L 77,186L 76,184L 75,181L 68,181L 65,184L 65,186L 63,186L 63,185L 52,185L 51,186L 49,186L 49,187L 47,187L 47,189L 46,189L 46,193L 47,193L 48,194L 49,195L 49,202L 48,202L 48,205 Z \"/> ";
                                break;
                            }
                        case "Manica":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                paths += " <path id=\"Manica\" nome=\"Manica\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\" onclick=\"MouseClick(this)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(this); \" onmouseout=\"mouseOut(this);\" ";
                                paths += " d=\"M 47,117L 48,117L 48,119L 49,119L 50,119L 50,121L 54,121L 54,120L 56,120L 56,119L 58,119L 58,118L 61,118L 61,119L 63,119L 64,119L 64,122L 63,122L 63,125L 64,126L 65,126L 65,127L 66,127L 66,128L 65,128L 65,129L 64,129L 64,132L 63,132L 63,131L 60,131L 60,132L 59,132L 58,133L 58,135L 59,135L 59,137L 58,137L 58,138L 57,138L 57,139L 56,139L 56,140L 56,142L 57,142L 57,143L 58,143L 59,144L 59,153L 58,153L 57,154L 57,155L 56,155L 56,156L 57,156L 57,157L 57,158L 58,159L 57,160L 56,160L 56,161L 54,161L 54,162L 51,162L 51,164L 52,164L 52,166L 51,166L 51,169L 52,169L 55,171L 55,173L 56,173L 56,174L 57,174L 57,177L 58,177L 58,178L 59,178L 59,179L 60,179L 60,182L 59,182L 59,183L 56,183L 56,184L 50,184L 50,185L 49,185L 46,186L 44,186L 44,187L 42,187L 42,186L 41,186L 41,185L 40,185L 40,184L 39,184L 38,183L 37,183L 37,181L 38,181L 38,174L 39,174L 39,173L 40,173L 42,171L 43,171L 43,167L 43,166L 44,166L 44,165L 45,165L 45,164L 46,164L 46,163L 47,163L 47,162L 48,162L 48,159L 47,159L 47,158L 44,155L 43,155L 43,152L 44,152L 44,144L 45,144L 45,138L 47,138L 48,137L 48,132L 47,132L 47,131L 46,130L 46,123L 47,123L 47,117 Z \" /> ";
                                break;
                            }
                        case "Sofala":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                paths += " <path id=\"Sofala\" nome=\"Sofala\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\" onclick=\"MouseClick(this)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(this); \" onmouseout=\"mouseOut(this);\" ";
                                paths += " d=\"M 47,113L 48,112L 55,103L 58,103L 61,105L 65,107L 69,110L 74,113L 76,118L 80,123L 83,127L 85,130L 87,131L 89,133L 92,136L 94,139L 96,142L 95,144L 94,144C 93,144 91,146 91,146L 88,148L 77,159L 72,160L 72,164L 71,170L 73,173L 75,174L 75,179L 68,180L 67,180L 64,183L 61,183L 61,178L 58,173L 55,168L 53,168L 53,163L 58,162L 59,161L 59,157L 58,156L 60,153L 61,152L 61,143L 58,140L 60,138L 61,136L 60,135L 60,133L 65,133L 66,132L 66,129L 67,128L 67,126L 66,124L 66,123L 65,119L 64,118L 63,117L 62,117L 61,117L 60,116L 59,116L 58,116L 57,117L 56,117L 55,118L 54,118L 53,118L 52,118L 51,117L 50,116L 49,115L 48,115L 47,113 Z \"/> ";
                                break;
                            }
                        case "Zambezia":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                paths += " <path id=\"Zambezia\" nome=\"Zambezia\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\" onclick=\"MouseClick(this)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(this); \" onmouseout=\"mouseOut(this);\" ";
                                paths += " d=\"M 82,121L 82,125L 83,125L 83,126L 84,126L 84,127L 84,128L 85,128L 85,129L 87,129L 87,130L 88,130L 88,131L 89,131L 90,131L 90,132L 91,132L 91,133L 94,137L 95,139L 96,139L 96,140L 97,140L 97,142L 99,142L 99,140L 104,136L 105,135L 105,131L 107,130L 111,130L 111,128L 116,124L 118,124L 119,123L 125,120L 128,120L 128,119L 131,119L 131,118L 135,118L 135,117L 138,117L 138,116L 141,116L 141,115L 142,115L 144,114L 144,112L 142,110L 142,101L 141,100L 141,95L 140,95L 139,94L 138,94L 138,93L 136,93L 136,92L 135,92L 135,91L 132,89L 132,88L 130,88L 130,87L 128,87L 128,86L 127,86L 126,85L 124,85L 124,84L 120,84L 120,83L 119,82L 118,82L 117,81L 117,80L 107,80L 107,81L 106,81L 100,87L 99,88L 95,88L 94,87L 93,88L 93,95L 92,97L 90,99L 87,99L 87,100L 84,100L 84,105L 83,105L 83,107L 84,107L 84,116L 83,116L 83,117L 82,117L 82,121 Z \"/> ";
                                break;
                            }
                        case "Nampula":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                paths += " <path id=\"Nampula\" nome=\"Nampula\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\" onclick=\"MouseClick(this)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(this); \" onmouseout=\"mouseOut(this);\" ";
                                paths += " d=\"M 110,77L 109,78L 110,78L 111,78L 111,79L 117,79L 120,81L 121,82L 121,83L 125,83L 125,84L 128,84L 128,85L 130,86L 133,87L 134,88L 135,89L 136,90L 137,91L 137,92L 139,92L 140,93L 141,94L 142,94L 143,95L 143,99L 143,102L 143,109L 144,110L 144,111L 145,111L 145,112L 147,112L 147,111L 148,111L 149,110L 150,110L 151,109L 152,109L 153,108L 154,108L 155,107L 156,106L 159,103L 159,101L 160,100L 160,98L 162,96L 163,96L 167,92L 167,91L 168,91L 168,90L 169,90L 169,89L 170,89L 170,87L 168,87L 168,85L 169,85L 171,83L 171,82L 172,82L 172,81L 173,81L 173,80L 174,80L 174,75L 172,75L 172,71L 173,71L 173,69L 171,69L 171,68L 170,68L 170,65L 171,64L 171,58L 170,58L 169,57L 168,57L 167,56L 166,56L 166,55L 165,55L 165,56L 164,56L 164,57L 163,57L 163,58L 161,58L 161,59L 160,59L 156,59L 155,60L 154,60L 154,61L 153,61L 153,62L 152,62L 151,62L 150,63L 148,63L 148,64L 146,64L 146,66L 144,66L 142,66L 142,67L 133,67L 133,68L 131,68L 131,69L 127,69L 125,69L 124,69L 123,69L 123,70L 121,70L 121,71L 119,71L 119,72L 118,72L 118,73L 116,73L 116,74L 112,74L 112,75L 111,75L 111,76L 110,76L 110,77 Z \"/> ";
                                break;
                            }
                        case "Niassa":
                            {
                                Opacity = (double)(Convert.ToDouble(y["total"].ToString()) / Total) + 0.1;
                                paths += " <path id=\"Niassa\" nome=\"Niassa\" tipo=\"" + Total.ToString() + "\" fill=\"#323232\" value=\"" + y["total"].ToString() + "\" fill-opacity=\"" + String.Format("{0:0.00}", Opacity).Replace(",", ".") + "\" onclick=\"MouseClick(this)\" onmousemove=\"SetValues(this); \" onmouseover=\"mouseOver(this); \" onmouseout=\"mouseOut(this);\" ";
                                paths += " d=\"M 94,86L 96,86L 96,87L 98,87L 99,86L 99,85L 100,85L 102,83L 104,81L 112,73L 115,73L 115,72L 117,72L 117,71L 118,71L 118,70L 119,70L 120,69L 122,69L 122,68L 124,68L 124,67L 125,67L 125,68L 126,68L 130,68L 131,67L 132,66L 133,66L 133,64L 132,63L 132,61L 131,60L 131,58L 131,57L 130,57L 130,56L 130,55L 128,55L 128,52L 129,52L 130,52L 131,51L 132,51L 132,50L 133,50L 133,49L 132,49L 131,48L 130,48L 130,49L 129,49L 128,50L 127,50L 127,51L 124,51L 124,46L 125,45L 125,44L 126,44L 127,42L 128,41L 128,37L 129,37L 129,34L 130,34L 130,33L 131,33L 132,32L 134,32L 135,31L 136,30L 136,28L 136,27L 136,25L 136,21L 137,20L 137,19L 134,19L 134,18L 130,18L 130,20L 129,20L 128,21L 126,23L 125,23L 124,24L 118,24L 117,23L 112,23L 112,24L 109,24L 108,25L 104,25L 102,23L 100,22L 98,21L 96,20L 95,19L 94,19L 94,20L 93,21L 88,21L 87,22L 80,22L 79,23L 79,27L 76,30L 76,33L 75,33L 75,36L 76,36L 77,37L 77,40L 77,51L 82,56L 88,62L 88,65L 95,74L 96,75L 96,79L 95,80L 94,81L 94,86 Z \"/> ";
                                break;
                            }
                    }
                }
                return paths;

            }
            catch (Exception e)
            {
                //return "<Script>Alert(" + e.Message.ToString() +");</script>";
                return "";
            }
        }
       
    }
}