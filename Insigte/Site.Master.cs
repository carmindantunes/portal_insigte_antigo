﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mail;
using System.Globalization;
using System.Threading;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using AjaxControlToolkit;
using INInsigteManager;
using System.Net;
using System.Xml.Linq;

using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Insigte
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            //this.InitializeCulture();
            //if (Global.InsigteManager[Session.SessionID].CodCliente == "bfa")
            //{
            //    Info.Visible = true;
            //    Info.Text = "Layout em desenvolvimento!"; //Mensagens Globais.
            //}

            //ddIdioma.Visible = false;

            //String culture = Global.InsigteManager[Session.SessionID].inUser.CodLanguage;
            //Info.Visible = true;
            //Info.Text = "Layout em desenvolvimento! : " + culture; //Mensagens Globais.
            //Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
            //Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
            
            //Thread.CurrentThread.CurrentCulture = new CultureInfo(culture);
            //Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);



            LB_Menu_Info.Text = Global.InsigteManager[Session.SessionID].inUser.Name + " @ media intelligence center";
            //LB_Menu_Info.Text = "Angola <font color=\"#00add9\" ><b>.</b></font> Moçambique <font color=\"#00add9\"><b>.</b></font> Portugal ";
            //LB_Menu_Info.Text = DateTime.Now.ToString("dd") + "-" + DateTime.Now.ToString("MM") + "-" + DateTime.Now.ToString("yyyy") + " | collecting small pieces to create the big picture";
            lbl_bottom.Text = " © Carma 2015 | Delivering what matters";


            if (Global.InsigteManager.ContainsKey(Session.SessionID) == false)
            {
                INManager IN = new INManager (Session.SessionID,GetVisitorIpAddress(), GetLanIPAddress());
                Global.InsigteManager.Add(Session.SessionID, IN);

            }

            //Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("pt-PT");
            //Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("pt-PT");

            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] == null)
            {
                Response.Cookies["CookieinsigteInfo"]["SiteName"] = "http://www.insigte.com";
                Response.Cookies["CookieinsigteInfo"]["Creator"] = "Insigte";
                Response.Cookies["CookieinsigteInfo"].Expires = DateTime.Now.AddDays(360);
            }

            if (HttpContext.Current.Request.Cookies["CookieinsigteSys"] == null)
            {
                //*** Instance of the HttpCookies ***//
                HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
                newCookie["Version"] = "Abr2013";
                newCookie["ID"] = "";
                newCookie["PDF"] = "";
                newCookie["DELNOT"] = "";
                newCookie.Expires = DateTime.Now.AddDays(360);
                Response.Cookies.Add(newCookie);
            }

            if (!Page.IsPostBack)
            {
                
                clicss.Href = "Cli/" + Global.InsigteManager[Session.SessionID].CodCliente + "/cli.css";

                tbSearchSimple.Attributes.Add("onclick", "this.value='';this.style.fontStyle='normal';");
                //tbSearchSimple.Attributes.Add("onfocus", "this.select();");
                //tbSearchSimple.Attributes.Add("onblur", "this.value=!this.value?'Pesquisa simples':this.value; this.value!='Pesquisa simples'?this.style.fontStyle='normal':this.style.fontStyle='italic';:this.style.color='#00add9';");
                
            }

            ChKVisible();

            Global.InsigteManager[Session.SessionID].updateUserState();

            string user = Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString();
            if (user == "2")
                tc_mm.Visible = true;

            if (user == "1")
                tcReports.Visible = true;
            //Response.Write("<script language='javascript'> { alert('Message Send'); } " + "<" + "/script>");

            //if (Global.InsigteManager[Session.SessionID].inUser.EmailShareSend != null)
            //{
            //    if (Global.InsigteManager[Session.SessionID].inUser.EmailShareSend == "1")
            //    {
            //        Response.Write("<script language='javascript'> { alert('Message Send'); } " + "<" + "/script>");
            //        Response.Write("<script language='javascript'> $(document).ready(function(){ alert('Mensagem enviada com sucesso!');}); " + "<" + "/script>");

            //        Global.InsigteManager[Session.SessionID].inUser.EmailShareSend = null;
            //    }
            //    else
            //    {
            //        Response.Write("<script language='javascript'> { alert('Message Not Send'); } " + "<" + "/script>");
            //        Response.Write("<script language='javascript'> $(document).ready(function(){ alert('Ocurreu um problema com ao enviar a menssagem!');}); " + "<" + "/script>");

            //        Global.InsigteManager[Session.SessionID].inUser.EmailShareSend = null;
            //    }
            //}

        }

        protected void lkb_mm_click(object sender, EventArgs e)
        {
            Global.InsigteManager[Session.SessionID].inUser.RSVLastReport = "/RL_Research";

            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallopenReportWindowA4", "openReportWindowA4('Media Measurement/RptMediaMeasurement.aspx')", true);

            //Session["Report"] = "/RL_Research";

        }

        protected void ddIdioma_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (ddIdioma.SelectedValue)
            {
                case "0": Global.InsigteManager[Session.SessionID].inUser.CodLanguage = "pt";
                    Global.InsigteManager[Session.SessionID].Refresh();
                    Response.Redirect(Request.Url.ToString());
                    ddIdioma.SelectedValue = "0";
                    break;
                case "1": Global.InsigteManager[Session.SessionID].inUser.CodLanguage = "en";
                    Global.InsigteManager[Session.SessionID].Refresh();
                    Response.Redirect(Request.Url.ToString());
                    ddIdioma.SelectedValue = "1";
                    break;
            }

            
            //LB_Menu_Info.Text = Global.InsigteManager[Session.SessionID].inUser.CodLanguage;
        }

        protected String getFormTitle()
        {
            return Resources.insigte.language.shareFormTitulo;
        }

        protected String getTemas()
        {
            String topStr = "";

            String SQLqueryCli = " select * from  CL10H_CLIENT_SUBJECT with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString() + " order by COD_TAB ";
            DataTable CliTemas = new DataTable();
            CliTemas = Global.InsigteManager[Session.SessionID].getTableQuery(SQLqueryCli, "CliTemas");

            foreach (DataRow row in CliTemas.Rows)
            {
                String TabName;

                if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                {
                    TabName = row["DES_TAB" + "_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage.ToUpper()].ToString(); 
                }
                else
                {
                    TabName = row["DES_TAB"].ToString();
                }

                //if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                //    TabName = row["DES_TAB"].ToString();
                //else
                //    TabName = row["DES_TAB"].ToString();

                topStr += " <li class=\"main-nav-item ui-corner-all\"><a href=\"Subject.aspx?clid=" + row["COD_TEMA_ACCESS"].ToString().Substring(0, 4).ToString() + "&tema=" + row["COD_TEMA_ACCESS"].ToString().Substring(4, 3).ToString() + "\" class=\"main-nav-tab ui-corner-all\" title=\"" + TabName + "\">" + TabName + "</a>\r";
                String[] SubTemas = row["COD_SUBTEMAS"].ToString().Split(';');

                
                if (SubTemas[0].Length > 0)
                {
                    String sql = "";

                    sql = "select * from subject with(nolock) where ";

                    int z = 0;
                    foreach (String subtema in SubTemas)
                    {
                        if (subtema.Substring(4, 3).ToString() == "000")
                        {
                            if (z > 0)
                                sql += " OR ";
                            sql += " clienteid = '" + subtema.Substring(0, 4).ToString() + "'";
                        }
                        else
                        {
                            if (z > 0)
                                sql += " OR ";
                            sql += " cast(clienteid as varchar) + cast(subjectid as varchar) = '"+ subtema +"' ";
                        }
                        z++;
                    }

                    sql += " order by name ";

                    DataTable Temas = new DataTable();
                    Temas = Global.InsigteManager[Session.SessionID].getTableQuery(sql, "Temas");

                    int Mod = Convert.ToInt32(Temas.Rows.Count / 3);
                    Mod++;

                    if (Temas.Rows.Count > 48)
                        topStr += "<div class=\"main-nav-dd ui-corner-all\"> <div class=\"main-nav-dd-column ui-corner-all\"><ul style=\"height: 710px;\">\r";
                    else
                    {
                        if (Temas.Rows.Count > 30)
                            topStr += "<div class=\"main-nav-dd ui-corner-all\"> <div class=\"main-nav-dd-column ui-corner-all\"><ul style=\"height: 495px;\">\r";
                        else
                            topStr += "<div class=\"main-nav-dd ui-corner-all\"> <div class=\"main-nav-dd-column ui-corner-all\"><ul>\r";
                    }

                    int i = 1;
                    foreach (DataRow rowTema in Temas.Rows)
                    {
                        String name = "";

                        if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt")
                            name = rowTema["name"].ToString();
                        else
                            name = rowTema["name_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage].ToString();
                        

                        topStr += "<li class\"ui-corner-all\"><img src=\"Cli/" + Global.InsigteManager[Session.SessionID].CodCliente + "/tab-arrow.gif\" alt=\"" + name + "\" title=\"" + name + "\" /> <a href=\"Subject.aspx?clid=" + rowTema["clienteid"].ToString() + "&tema=" + rowTema["subjectid"].ToString() + "\" title=\"" + name + "\">" + name + "</a></li> \r";
                        if (i % Mod == 0)
                        {
                            topStr += "</ul></div><div class=\"main-nav-dd-column ui-corner-all\"><ul>\r";
                        }
                        i++;
                    }

                    topStr += "</ul></div></div>\r";
                }

                topStr += "</li>\r";

            }
            return topStr;
        }

        protected String getTemasGroup()
        {
            String topStr = "";

            String SQL = "select * from CL10H_CLIENT_SUBJECT_GROUP with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString() + " order by COD_TAB_GROUP ";
            DataTable CliTemasGroup = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "CliTemasGroup");

            //Verifica se tem group de temas, snão executa codigo sem grupos.
            if (CliTemasGroup.Rows.Count == 0)
                return getTemas();


            foreach (DataRow group in CliTemasGroup.Rows)
            {
                String SQLqueryCli = " select * from  CL10H_CLIENT_SUBJECT with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString() + " and COD_TAB in (" + group["COD_TABS"].ToString() + ") order by COD_TAB ";
                DataTable CliTemas = CliTemas = Global.InsigteManager[Session.SessionID].getTableQuery(SQLqueryCli, "CliTemas");

                String TabName;

                if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                    TabName = group["DES_TAB_GROUP" + "_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage.ToUpper()].ToString();
                else
                    TabName = group["DES_TAB_GROUP"].ToString();

                if (group["COD_TEMA_LINK"].ToString().Length > 0)
                    topStr += " <li class=\"main-nav-item ui-corner-all\"><a href=\"Subject.aspx?clid=" + group["COD_TEMA_LINK"].ToString().Substring(0, 4).ToString() + "&tema=" + group["COD_TEMA_LINK"].ToString().Substring(4, 3).ToString() + "\" class=\"main-nav-tab ui-corner-all\" title=\"" + TabName + "\">" + TabName + "</a>\r";
                else
                    topStr += " <li class=\"main-nav-item ui-corner-all\"><a class=\"main-nav-tab ui-corner-all\" title=\"" + TabName + "\">" + TabName + "</a>\r";

                if (group["COD_TABS"].ToString().Contains(','))
                {
                    topStr += "<div class=\"main-nav-dd ui-corner-all\">";

                    foreach (DataRow row in CliTemas.Rows)
                    {
                        String SepName;

                        if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                            SepName = row["DES_TAB" + "_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage.ToUpper()].ToString();
                        else
                            SepName = row["DES_TAB"].ToString();

                        String[] SubTemas = row["COD_SUBTEMAS"].ToString().Split(';');

                        topStr += "<div class=\"main-nav-title\"><a href=\"Subject.aspx?clid=" + row["COD_TEMA_ACCESS"].ToString().Substring(0, 4).ToString() + "&tema=" + row["COD_TEMA_ACCESS"].ToString().Substring(4, 3).ToString() + "\">" + SepName + "</a><br><hr></div>";

                        if (SubTemas[0].Length > 0)
                        {
                            String sql = "";

                            sql = "select * from subject with(nolock) where ";

                            int z = 0;
                            foreach (String subtema in SubTemas)
                            {
                                if (subtema.Substring(4, 3).ToString() == "000")
                                {
                                    if (z > 0)
                                        sql += " OR ";
                                    sql += " clienteid = '" + subtema.Substring(0, 4).ToString() + "'";
                                }
                                else
                                {
                                    if (z > 0)
                                        sql += " OR ";
                                    sql += " cast(clienteid as varchar) + cast(subjectid as varchar) = '" + subtema + "' ";
                                }
                                z++;
                            }

                            sql += " order by clienteid, subjectid ";

                            DataTable Temas = new DataTable();
                            Temas = Global.InsigteManager[Session.SessionID].getTableQuery(sql, "Temas");

                            int Mod = Convert.ToInt32(Temas.Rows.Count / 3);
                            Mod++;

                            topStr += " <div class=\"main-nav-dd-columnXL ui-corner-all\"><ul style=\"height: auto;min-height: 50px;\">\r";

                            int i = 1;
                            foreach (DataRow rowTema in Temas.Rows)
                            {
                                String name = "";

                                if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt")
                                    name = rowTema["name"].ToString();
                                else
                                    name = rowTema["name_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage].ToString();

                                topStr += "<li class\"ui-corner-all\"><img src=\"Cli/" + Global.InsigteManager[Session.SessionID].CodCliente + "/tab-arrow.gif\" alt=\"" + name + "\" title=\"" + name + "\" /> <a href=\"Subject.aspx?clid=" + rowTema["clienteid"].ToString() + "&tema=" + rowTema["subjectid"].ToString() + "\" title=\"" + name + "\">" + name + "</a></li> \r";
                                if (i % Mod == 0)
                                {
                                    topStr += "</ul></div><div class=\"main-nav-dd-columnXL ui-corner-all\"><ul style=\"height: auto;min-height: 50px;\">\r";
                                }
                                i++;
                            }

                            topStr += "</ul></div>\r";
                        }

                    }
                }

                else
                {
                    foreach (DataRow row in CliTemas.Rows)
                    {
                        String[] SubTemas = row["COD_SUBTEMAS"].ToString().Split(';');

                        if (SubTemas[0].Length > 0)
                        {
                            String sql = "";

                            sql = "select * from subject with(nolock) where ";

                            int z = 0;
                            foreach (String subtema in SubTemas)
                            {
                                if (subtema.Substring(4, 3).ToString() == "000")
                                {
                                    if (z > 0)
                                        sql += " OR ";
                                    sql += " clienteid = '" + subtema.Substring(0, 4).ToString() + "'";
                                }
                                else
                                {
                                    if (z > 0)
                                        sql += " OR ";
                                    sql += " cast(clienteid as varchar) + cast(subjectid as varchar) = '" + subtema + "' ";
                                }
                                z++;
                            }

                            sql += " order by name ";

                            DataTable Temas = new DataTable();
                            Temas = Global.InsigteManager[Session.SessionID].getTableQuery(sql, "Temas");

                            int Mod = Convert.ToInt32(Temas.Rows.Count / 3);

                            if (Mod < 1)
                                Mod++;

                            if (Temas.Rows.Count > 48)
                                topStr += "<div class=\"main-nav-dd ui-corner-all\"> <div class=\"main-nav-dd-column ui-corner-all\"><ul style=\"height: 710px;\">\r";
                            else
                            {
                                if (Temas.Rows.Count > 30)
                                    topStr += "<div class=\"main-nav-dd ui-corner-all\"> <div class=\"main-nav-dd-column ui-corner-all\"><ul style=\"height: 495px;\">\r";
                                else
                                    topStr += "<div class=\"main-nav-dd ui-corner-all\"> <div class=\"main-nav-dd-column ui-corner-all\"><ul>\r";
                            }

                            int i = 1;
                            foreach (DataRow rowTema in Temas.Rows)
                            {
                                String name = "";

                                if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt")
                                    name = rowTema["name"].ToString();
                                else
                                    name = rowTema["name_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage].ToString();


                                topStr += "<li class\"ui-corner-all\"><img src=\"Cli/" + Global.InsigteManager[Session.SessionID].CodCliente + "/tab-arrow.gif\" alt=\"" + name + "\" title=\"" + name + "\" /> <a href=\"Subject.aspx?clid=" + rowTema["clienteid"].ToString() + "&tema=" + rowTema["subjectid"].ToString() + "\" title=\"" + name + "\">" + name + "</a></li> \r";
                                if (i % Mod == 0)
                                {
                                    topStr += "</ul></div><div class=\"main-nav-dd-column ui-corner-all\"><ul>\r";
                                }
                                i++;
                            }

                            topStr += "</ul></div></div>\r";
                        }
                    }
                }

                topStr += "</li>\r";
            }

            return topStr;
        }

        protected void ChKVisible()
        {
            
            ibSearchSimple.ImageUrl = "Imgs/pesquisa_button.png";
            ibSearchSimple.BorderColor = System.Drawing.ColorTranslator.FromHtml("#002d56");
            ibSearchSimple.Width = new System.Web.UI.WebControls.Unit("16px");
            ibSearchSimple.Height = new System.Web.UI.WebControls.Unit("16px");

            //RedesSociais
            //if (Global.InsigteManager[Session.SessionID].inUser.HasAlerts == 0)
            //{
            //    GetACNRS.Visible = false;
            //    dvGetACNRS.Visible = false;
            //}

            #region Visiabilidades
            //Pastas
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(0, 1) == "0")
            {
                GetACNPastas1.Visible = false;
                dvGetACNPastas1.Visible = false;
            }

            //Newsletters
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(1, 1) == "0")
            {
                GetACNNewsletters.Visible = false;
                dvGetACNNewsletters.Visible = false;
            }

            //Advanced Search
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(2, 1) == "0")
            {
                lbSearch.Visible = false;
                TC_Search.Visible = false;
                TC_SearchSep.Visible = false;
            }

            //DashBoard
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(3, 1) == "0")
            {
                lbDashboard.Visible = false;
                TC_Dashboard.Visible = false;
                TC_DashboardSep.Visible = false;

                GetACNgrafx.Visible = false;
            }

            //Documents
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(4, 1) == "0")
            {
                LblReports.Visible = false;
                LbReports.Visible = false;
                TC_Reports.Visible = false;
                TC_ReportsSep.Visible = false;
            }

            //BrandMining
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(5, 1) == "0")
            {
                Lblbrandm.Visible = false;
                Lbbrandm.Visible = false;
                TC_Brandm.Visible = false;
                TC_BrandmSep.Visible = false;
            }

            //Fontes
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(6, 1) == "0")
            {
                GetACNCondig.Visible = false;
                dvGetACNCondig.Visible = false;
            }

            //Newsletter Composta
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(7, 1) == "0")
            {
                // Não necessita de acção apenas para informação.
            }

            //Language Change
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(8, 1) == "0")
            {
                ddIdioma.Visible = false;
            }

            //Listas
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(9, 1) == "0")
            {
                GetACNLists1.Visible = false;
                dvGetACNLists1.Visible = false;
            }

            //GeoMKT
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(10, 1) == "0")
            {
                LnkGeoMKT.Visible = false;
                LnkGeoMKTSep.Visible = false;
                TC_GeoMKT.Visible = false;
                TC_GeoMKTSep.Visible = false;
                
            }

            //Research
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(11, 1) == "0")
            {
                GetACNResearch1.Visible = false;
                dvGetACNResearch1.Visible = false;
            }

            //Concursos Publicos
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(12, 1) == "0")
            {
                GetCpub1.Visible = false;
                dvGetCpub1.Visible = false;
            }

            //Publicidade
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(13, 1) == "0")
            {
                TC_PubList.Visible = false;
                TC_Sep_Publist.Visible = false;
                Link_Publist.Visible = false;
                Link_Sep_Publist.Visible = false;
            }

            //BLOGS
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(14, 1) == "0")
            {
                TC_Blogs.Visible = false;
                TC_BlogsSep.Visible = false;
                lnkBlogs.Visible = false;
                lnkBlogsSep.Visible = false;
            }

            try
            {
                //Reports
                if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(15, 1) == "0")
                {
                    dvGetReports.Visible = false;
                }
            }
            catch (Exception er)
            {
                dvGetReports.Visible = false;
                er.Message.ToString();
              
            }
            


            #endregion
            //Publicidade
            //if (Global.InsigteManager[Session.SessionID].inUser.IdClientUser != "135" 
            //    && Global.InsigteManager[Session.SessionID].inUser.IdClientUser != "179"
            //    && Global.InsigteManager[Session.SessionID].inUser.IdClientUser != "117"
            //    && Global.InsigteManager[Session.SessionID].inUser.IdClientUser != "34"
            //    && Global.InsigteManager[Session.SessionID].inUser.IdClientUser != "2")
            //{
            //    TC_PubList.Visible = false;
            //    TC_Sep_Publist.Visible = false;
            //    Link_Publist.Visible = false;
            //    Link_Sep_Publist.Visible = false;

            //}

    
        }

        protected void lbLogoff_Click(object sender, EventArgs e)
        {
            Global.InsigteManager[Session.SessionID].IsLogged = false;
            Session.RemoveAll();
            
            //Global.InsigteManager[Session.SessionID] = null;
            Response.Redirect("login.aspx", true);

        }

        protected void ibSearchSimple_Click(object sender, ImageClickEventArgs e)
        {
            if (tbSearchSimple.Text != string.Empty && tbSearchSimple.Text != "" && tbSearchSimple.Text != "Pesquisa livre")
            {
                Session["ADVSearchBool"] = "true";
                Session["ADVSearchTxt"] = tbSearchSimple.Text.Replace("'", "''");
                Session["ADVSearchTema"] = "1";
                Session["ADVSearchPeriodo"] = "90";
                Session["ADVSearchDatIni"] = "";
                Session["ADVSearchDatFim"] = "";
                Session["ADVSearchEditor"] = Resources.insigte.language.geralTagTodos;
                Session["ADVSearchJornalistas"] = Resources.insigte.language.geralTagTodos;
                Session["ADVSearchTipo"] = "1";
                Session["ADVSearchPais"] = "1";
                Session["ADVSearchClasse"] = "1";

                Response.Redirect("Search.aspx");
            }
        }

        public string GetVisitorIpAddress()
        {
            string stringIpAddress;
            stringIpAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (stringIpAddress == null) //may be the HTTP_X_FORWARDED_FOR is null
            {
                stringIpAddress = Request.ServerVariables["REMOTE_ADDR"];//we can use REMOTE_ADDR
            }
            return stringIpAddress;
        }

        //Get Lan Connected IP address method
        public string GetLanIPAddress()
        {
            //Get the Host Name
            string stringHostName = Dns.GetHostName();
            //Get The Ip Host Entry
            IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
            //Get The Ip Address From The Ip Host Entry Address List
            IPAddress[] arrIpAddress = ipHostEntries.AddressList;
            return arrIpAddress[arrIpAddress.Length - 1].ToString();
        }

        public void Share_News_Click(object sender, EventArgs e)
        {
            string MailHTMLBody = string.Empty;
            string MailEnviadoPor_Comment = string.Empty;
            string MailSubject = string.Empty;

            if (txt_aux_share_sendby.Value != "" || txt_aux_share_sendby.Value != string.Empty)
            {
                MailEnviadoPor_Comment += Resources.insigte.language.defEmailSend + " " + txt_aux_share_sendby.Value + "<br />" + Environment.NewLine;
            }

            if (txt_aux_share_comment.Value != "" || txt_aux_share_comment.Value != string.Empty)
            {
                MailEnviadoPor_Comment += Resources.insigte.language.defEmailComment + " " + txt_aux_share_comment.Value + "<br />" + Environment.NewLine;
            }

            #region Send Link?
            // -------------- SEND LINK -------------- 
            if (txt_aux_share_rblShare.Value == "2")
            {
                MailHTMLBody += "<body style='font-family:Arial;'>";


                String Title = "TITLE";
                String texto = "TEXT as text";
                if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                {
                    Title = "TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
                    texto = "TEXT_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as text";
                }

                String cmdQuery = "SELECT " + texto + ", " + Title + ", EDITOR, DATE, m.ID FROM iadvisers.dbo.metadata m with(nolock) ";
                cmdQuery += " INNER JOIN iadvisers.dbo.contents c with(nolock) ON m.ID=c.ID where m.ID='" + txt_aux_share_id.Value + "' ";

                DataTable ShareQ = Global.InsigteManager[Session.SessionID].getTableQuery(cmdQuery, "ShareQ");

                foreach (DataRow x in ShareQ.Rows)
                {
                    MailSubject = "insigte | media intelligence - " + x["TITLE"].ToString();
                    MailHTMLBody = "<br />" + x["EDITOR"].ToString() + " | " + x["DATE"].ToString().Substring(0, 10) + "<br />" + Environment.NewLine;
                    MailHTMLBody += "<br />" + Resources.insigte.language.defEmailLinkFollow + " <a href='http://www.insigte.com/article_email.asp?ID=" + x["ID"].ToString() + "' target='_blank' >Link</a> .";
                }

                MailHTMLBody += "</body>";

                MailMessage mail = new MailMessage();
                mail.To = txt_aux_share_email.Value;
                mail.From = "insigte <info@insigte.com>";
                mail.Bcc = "";
                mail.Subject = MailSubject;
                mail.BodyFormat = MailFormat.Html;
                mail.Body = MailEnviadoPor_Comment + MailHTMLBody;

                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", 25);
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", 2);
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", 1);
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "info@insigte.com");
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "jQY4qhQk");
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", "false");

                try
                {
                    SmtpMail.SmtpServer = "mail.insigte.com";
                    SmtpMail.Send(mail);
                    
                    //Response.Write("<script language='javascript'> { Alert('Mensagem enviada com sucesso!'); }</script>");

                }
                catch (Exception ex)
                {
                    //Response.Write("<script language='javascript'> { Alert(\"Ocurreu um problema muito Agudo, PKP. tenta mais tarde.\"); }</script>");

                }

            }

            #endregion

            #region Send PDF
            // -------------- SEND PDF -------------- 
            if (txt_aux_share_rblShare.Value == "0")
            {
                #region Cria pdf
                //DIFINIR AS FONTES DO DOCUMENTO

                BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
                BaseFont bfHelve = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
                BaseFont bfArial = BaseFont.CreateFont("C:\\Windows\\Fonts\\arial.ttf", BaseFont.CP1252, false);

                Font times = new Font(bfTimes, 12, Font.ITALIC, BaseColor.BLACK);
                Font helve = new Font(bfHelve, 18f, Font.NORMAL, BaseColor.BLACK);
                Font ArialSmall = new Font(bfArial, 8f, Font.NORMAL, BaseColor.BLACK);
                Font helveSmall = new Font(bfHelve, 16f, Font.NORMAL, BaseColor.BLACK);
                Font helveVSmall = new Font(bfHelve, 10f, Font.NORMAL, BaseColor.BLACK);

                Font helveVSmallBlue = new Font(bfHelve, 10f, Font.NORMAL, BaseColor.BLUE);
                helveVSmallBlue.SetColor(0, 84, 132);
                Font helveVVSmallBlue = new Font(bfHelve, 8f, Font.NORMAL, BaseColor.BLUE);
                helveVVSmallBlue.SetColor(0, 84, 132);

                Font helveXSmall = new Font(bfHelve, 6f, Font.NORMAL, BaseColor.BLACK);


                //ABRE O INDICE E PREPARA OD DOCUMENTOS

                string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                string varSolutionDir = "G:\\files\\";

                string varIndice = "G:\\pdf\\findiceMISC" + timestamp + ".pdf";
                string varCapa = "G:\\pdf\\template_first.pdf";

                var docIndice = new Document();
                string path = Server.MapPath("pdf");
                PdfWriter pdfw = PdfWriter.GetInstance(docIndice, new FileStream(varIndice, FileMode.Create));

                docIndice.Open();
                docIndice.NewPage();

                //INICIALIZA O INDICE

                PlaceText(pdfw.DirectContent, helve, 30, 750, 335, 790, 14, Element.ALIGN_LEFT, "media intelligence");
                PlaceText(pdfw.DirectContent, helveSmall, 30, 710, 335, 750, 14, Element.ALIGN_LEFT, Resources.insigte.language.defPDFDossierImp);
                PlaceText(pdfw.DirectContent, helveVSmall, 30, 680, 335, 710, 14, Element.ALIGN_LEFT, Resources.insigte.language.defPDFDossierImpIndice);

                
                iTextSharp.text.Image myImage = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\logo_insigte_trans.png");
                myImage.SetAbsolutePosition(500f, 760f);
                //myImage.ScalePercent(36f); 100%
                myImage.ScalePercent(10f);
                //myImage.ScaleToFit(16, 16);
                docIndice.Add(myImage);

                String Title = "TITLE";
                String texto = "TEXT as text";
                if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                {
                    Title = "TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
                    texto = "TEXT_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as text";
                }

                string cmdQuery = " SELECT " + texto + ", " + Title + ", EDITOR, DATE, m.ID, filepath, page_url, tipo, convert(varchar,DATE,105) as data FROM iadvisers.dbo.metadata m with(nolock) ";
                cmdQuery += " INNER JOIN iadvisers.dbo.contents c with(nolock) ON m.ID=c.ID where m.ID='" + txt_aux_share_id.Value + "' ";

                DataTable News = Global.InsigteManager[Session.SessionID].getTableQuery(cmdQuery, "getDadosPdf");

                int vSpace = 5;
                int nPage = 0;

                //----------------------------------------NOTICIAS -------------------
                string NomeBase = Resources.insigte.language.defPDFDossierImp;

                string nomeOutFile = NomeBase + "_" + timestamp + "_0.pdf";
                string AuxNomeOutFile = "";

                string varOutputFile = "G:\\pdf\\" + nomeOutFile;
                string AuxVarOutputFile = "";

                string varOutputTxtFile = "G:\\pdf\\" + "tmp_" + nomeOutFile;

                var docNoticias = new Document();
                FileStream fsNot = new FileStream(varOutputFile, FileMode.Create);
                PdfWriter pdfwNews = PdfWriter.GetInstance(docNoticias, fsNot);

                docNoticias.Open();
                docNoticias.NewPage();

                PlaceText(pdfwNews.DirectContent, helve, 30, 750, 335, 790, 14, Element.ALIGN_LEFT, "media intelligence");
                PlaceText(pdfwNews.DirectContent, helveSmall, 30, 710, 335, 750, 14, Element.ALIGN_LEFT, Resources.insigte.language.defPDFDossierImp);

                docNoticias.Close();
                docNoticias.Dispose();
                fsNot.Close();

                int xCounter = 0;
                int PagNum = 3;
                String PDF_OMFG = "";

                foreach (DataRow x in News.Rows)
                {
                    //ADICIONA AO INDICE
                    float NumPagIndice = News.Rows.Count / 42;
                    PagNum = PagNum + Convert.ToInt32(NumPagIndice);

                    String Icon = "";

                    switch (x["tipo"].ToString())
                    {
                        case "Online": Icon = "globe_1_icon_24.png";
                            break;
                        case "Imprensa": Icon = "doc_export_icon_24.png";
                            break;
                        case "Rádio": Icon = "headphones_icon_16.png";
                            break;
                        case "Televisão": Icon = "movie_icon_24.png";
                            break;
                        default: Icon = "doc_export_icon_24.png";
                            break;
                    }

                    if (nPage > 42)
                    {
                        docIndice.NewPage();
                        PlaceText(pdfw.DirectContent, helveVSmall, 30, 680, 335, 790, 14, Element.ALIGN_LEFT, Resources.insigte.language.defPDFDossierImpIndice);
                        vSpace = -60;
                        nPage = 0;
                    }

                    vSpace += 15;

                    String Titulo = x["TITLE"].ToString();
                    String Editor = x["EDITOR"].ToString();
                    String Data = Convert.ToDateTime(x["DATE"].ToString()).ToShortDateString();

                    if (Titulo.Length > 125)
                    {
                        iTextSharp.text.Image newsIcon = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\icons\black\png\" + Icon);
                        newsIcon.SetAbsolutePosition(30f, 695f - vSpace);
                        newsIcon.ScaleAbsolute(6f, 6f);
                        docIndice.Add(newsIcon);

                        //PlaceText(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 435, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo.Substring(0, Titulo.IndexOf(' ', 100)));
                        //PlaceText(pdfw.DirectContent, helveXSmall, 435, 660 - vSpace, 535, 710 - vSpace, 14, Element.ALIGN_LEFT, Editor);
                        //PlaceText(pdfw.DirectContent, helveXSmall, 535, 660 - vSpace, 635, 710 - vSpace, 14, Element.ALIGN_LEFT, Data.ToString());

                        PlaceText(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo.Substring(0, Titulo.IndexOf(' ', 100)));
                        PlaceText(pdfw.DirectContent, helveXSmall, 358, 660 - vSpace, 458, 710 - vSpace, 14, Element.ALIGN_LEFT, Editor);
                        PlaceText(pdfw.DirectContent, helveXSmall, 458, 660 - vSpace, 558, 710 - vSpace, 14, Element.ALIGN_LEFT, Data.ToString());
                        PlaceText(pdfw.DirectContent, helveXSmall, 558, 660 - vSpace, 635, 710 - vSpace, 14, Element.ALIGN_LEFT, PagNum.ToString());

                        vSpace += 15;

                        PlaceText(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo.Substring(Titulo.IndexOf(' ', 100), Titulo.Length - (Titulo.IndexOf(' ', 100))));
                        PlaceText(pdfw.DirectContent, helveXSmall, 358, 660 - vSpace, 458, 710 - vSpace, 14, Element.ALIGN_LEFT, " ");
                        PlaceText(pdfw.DirectContent, helveXSmall, 458, 660 - vSpace, 558, 710 - vSpace, 14, Element.ALIGN_LEFT, " ");
                        PlaceText(pdfw.DirectContent, helveXSmall, 558, 660 - vSpace, 635, 710 - vSpace, 14, Element.ALIGN_LEFT, PagNum.ToString());

                        nPage++;
                    }
                    else
                    {
                        iTextSharp.text.Image newsIcon = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\icons\black\png\" + Icon);
                        newsIcon.SetAbsolutePosition(30f, 695f - vSpace);
                        newsIcon.ScaleAbsolute(6f, 6f);
                        docIndice.Add(newsIcon);

                        PlaceText(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo);
                        PlaceText(pdfw.DirectContent, helveXSmall, 358, 660 - vSpace, 458, 710 - vSpace, 14, Element.ALIGN_LEFT, Editor);
                        PlaceText(pdfw.DirectContent, helveXSmall, 458, 660 - vSpace, 558, 710 - vSpace, 14, Element.ALIGN_LEFT, Data.ToString());
                        PlaceText(pdfw.DirectContent, helveXSmall, 558, 660 - vSpace, 635, 710 - vSpace, 14, Element.ALIGN_LEFT, PagNum.ToString());
                    }

                    nPage++;

                    //TRATA NOTICIAS

                    xCounter++;
                    varOutputTxtFile = "G:\\pdf\\" + "tmp_" + nomeOutFile;

                    String Texto = x["text"].ToString();

                    

                    if (x["filepath"].ToString().Length > 0 && Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt")
                    {
                        String NewsPDF = varSolutionDir + x["filepath"].ToString();

                        PDF_OMFG = NewsPDF;
                        // INSERT CLIENT LOGO

                        if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                        {
                            PDF_OMFG = "G:\\pdf\\Share_" + timestamp + "_" + xCounter.ToString() + ".pdf";

                            PdfReader readerPs = new PdfReader("G:\\pdf\\size.pdf");
                            int nPs = readerPs.NumberOfPages;
                            Rectangle pageSize = readerPs.GetPageSizeWithRotation(1);
                            Document document = new Document(pageSize);
                            FileStream fs = new FileStream(PDF_OMFG, FileMode.Create);
                            PdfWriter writer = PdfWriter.GetInstance(document, fs);
                            writer.CloseStream = false;
                            document.Open();
                            PdfContentByte cb = writer.DirectContent;
                            PdfImportedPage page;
                            int rotation;
                            PdfReader InsLogo = new PdfReader(NewsPDF);
                            int n = InsLogo.NumberOfPages;
                            int i = 0;
                            while (i < n)
                            {
                                i++;
                                document.SetPageSize(InsLogo.GetPageSizeWithRotation(i));
                                document.NewPage();
                                page = writer.GetImportedPage(InsLogo, i);
                                rotation = InsLogo.GetPageRotation(i);
                                if (rotation == 90 || rotation == 270)
                                {
                                    cb.AddTemplate(page, 0, -1f, 1f, 0, 0, InsLogo.GetPageSizeWithRotation(i).Height);
                                }
                                else
                                {
                                    cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                                }

                                iTextSharp.text.Image ClearLogoImage = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\logo_clear.png");
                                ClearLogoImage.SetAbsolutePosition(518f, 775f);
                                ClearLogoImage.ScalePercent(15f);
                                writer.DirectContent.AddImage(ClearLogoImage);


                                iTextSharp.text.Image ClienteLogoImage = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\logo_" + Global.InsigteManager[Session.SessionID].IdClient.ToString() + ".png");
                                ClienteLogoImage.SetAbsolutePosition(526f, 775f);
                                ClienteLogoImage.ScalePercent(10f);
                                writer.DirectContent.AddImage(ClienteLogoImage);

                                PlaceText(writer.DirectContent, helveXSmall, 528, 20 - vSpace, 605, 60 - vSpace, 14, Element.ALIGN_LEFT, "insigte ™ | 2014");

                                //document.Add(ClienteLogoImage);
                            }
                            document.Close();
                            document.Dispose();
                            fs.Close();
                            fs.Dispose();
                            readerPs.Close();
                            readerPs.Dispose();
                        }

                        //*********************

                        string pdfss = varOutputFile + "," + NewsPDF;
                        string[] pdfs = pdfss.Split(',');

                        AuxNomeOutFile = NomeBase + "_" + timestamp + "_" + xCounter.ToString() + ".pdf";
                        AuxVarOutputFile = "G:\\pdf\\" + AuxNomeOutFile;

                        //PdfReader readerNumPages = new PdfReader(NewsPDF);

                        PagNum++;

                        //try
                        //{
                        //    PagNum += readerNumPages.NumberOfPages;
                        //    //PagNum += PdfMerge.CountPageNo(NewsPDF);
                        //}
                        //catch (Exception ePagnum)
                        //{
                        //    PagNum++;
                        //}
                        //finally
                        //{
                        //    readerNumPages.Close();
                        //}

                        PdfMerge.MergePdfs(AuxVarOutputFile, pdfs);

                        File.Delete(varOutputFile);

                        nomeOutFile = AuxNomeOutFile;
                        varOutputFile = AuxVarOutputFile;

                    }
                    else
                    {

                        //Cria PDF temporario para depois fazer o merge.
                        var docAdd = new Document();
                        FileStream fs = new FileStream(varOutputTxtFile, FileMode.Create);

                        PdfWriter pdfwAdd = PdfWriter.GetInstance(docAdd, fs);
                        docAdd.Open();
                        docAdd.NewPage();

                        //PlaceText(pdfwAdd.DirectContent, helveVSmallBlue, 30, 690, 435, 710, 14, Element.ALIGN_LEFT, Titulo);

                        if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                        {
                            iTextSharp.text.Image ClienteLogoImage = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\logo_" + Global.InsigteManager[Session.SessionID].IdClient.ToString() + ".png");
                            ClienteLogoImage.SetAbsolutePosition(500f, 760f);
                            //myImage.ScalePercent(36f); 100%
                            ClienteLogoImage.ScalePercent(10f);
                            //myImage.ScaleToFit(16, 16);
                            docAdd.Add(ClienteLogoImage);
                        }


                        PdfContentByte cb = pdfwAdd.DirectContent;
                        ColumnText ct = new ColumnText(cb);
                        ct.Alignment = Element.ALIGN_JUSTIFIED;
                        ct.YLine = 1f;
                        ct.SetLeading(0, 1.2f);

                        Paragraph heading = new Paragraph(Titulo, helveVSmallBlue);
                        Paragraph editor = new Paragraph(Editor + " - " + Data, helveVVSmallBlue);
                        heading.Leading = 40f;
                        docAdd.Add(heading);
                        docAdd.Add(editor);

                        Phrase phr = new Phrase(Texto, ArialSmall);
                        phr.Leading = 0;
                        ct.AddText(phr);

                        float gutter = 15f;
                        float colwidth = (docAdd.Right - docAdd.Left - gutter) / 2;
                        //float[] left = { docAdd.Left + 90f, docAdd.Top - 80f, docAdd.Left + 90f, docAdd.Top - 170f, docAdd.Left, docAdd.Top - 170f, docAdd.Left, docAdd.Bottom };
                        float[] left = { docAdd.Left, docAdd.Top - 80f, docAdd.Left, docAdd.Bottom };
                        float[] right = { docAdd.Left + colwidth, docAdd.Top - 80f, docAdd.Left + colwidth, docAdd.Bottom };
                        float[] left2 = { docAdd.Right - colwidth, docAdd.Top - 80f, docAdd.Right - colwidth, docAdd.Bottom };
                        float[] right2 = { docAdd.Right, docAdd.Top - 80f, docAdd.Right, docAdd.Bottom };

                        int status = 0;
                        int i = 0;
                        bool nextPage = false;

                        while (ColumnText.HasMoreText(status))
                        {
                            if (nextPage)
                            {
                                i = 0;
                                docAdd.NewPage();
                                PagNum++;
                                nextPage = false;
                            }


                            if (Texto.Length > 6000)

                                if (i == 0)
                                {
                                    //Writing the first column
                                    ct.SetColumns(left, right);
                                    i++;
                                }
                                else
                                {
                                    //write the second column
                                    ct.SetColumns(left2, right2);
                                    nextPage = true;
                                }

                            else
                            {
                                ct.SetColumns(left, right2);
                                nextPage = true;
                            }

                            //Needs to be here to prevent app from hanging
                            // NUNCA MEXER NESTA MERDA PKP LIXOU O IIS
                            ct.YLine = docAdd.Top - 80f;
                            //Commit the content of the ColumnText to the document
                            //ColumnText.Go() returns NO_MORE_TEXT (1) and/or NO_MORE_COLUMN (2)
                            //In other words, it fills the column until it has either run out of column, or text, or both
                            status = ct.Go();

                        }

                        docAdd.Close();
                        docAdd.Dispose();
                        fs.Close();

                        //NEW FOR SHARE
                        PDF_OMFG = varOutputTxtFile.Replace("tmp_","Share_");
                        File.Copy(varOutputTxtFile, PDF_OMFG);
                        //*************

                        string pdfss = varOutputFile + "," + varOutputTxtFile;
                        string[] pdfs = pdfss.Split(',');

                        AuxNomeOutFile = NomeBase + "_" + timestamp + "_" + xCounter.ToString() + ".pdf";
                        AuxVarOutputFile = "G:\\pdf\\" + AuxNomeOutFile;

                        PdfMerge.MergePdfs(AuxVarOutputFile, pdfs);

                        File.Delete(varOutputFile);
                        File.Delete(varOutputTxtFile);

                        nomeOutFile = AuxNomeOutFile;
                        varOutputFile = AuxVarOutputFile;

                        PagNum++;
                    }
                }

                docIndice.Close();

                //Finaliza o PDF

                String Capa = "G:\\pdf\\template_first.pdf";
                String PDFFinal = "G:\\pdf\\" + NomeBase + "_" + timestamp + "_" + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + ".pdf";

                string pdfsFinals = Capa + "," + varIndice + "," + varOutputFile;
                string[] pdfsFim = pdfsFinals.Split(',');

                PdfMerge.MergePdfs(PDFFinal, pdfsFim);

                File.Delete(varIndice);
                File.Delete(varOutputFile);

                string DownloadPath = PDFFinal;

                #endregion

                #region EmailBody

               
                

                try
                {
                    String sqlCli = "select * from CL10D_CLIENT with(nolock) where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient.ToString();
                    DataTable dtCliente = Global.InsigteManager[Session.SessionID].getTableQuery(sqlCli, "Cliente");

                    String TextColor = "#000000,#00add9,#000000,#000000";
                    String CorClienteTop = "#525252";
                    String CorClienteBot = "#323232";
                    String CodCliente = "Insigte";


                    foreach (DataRow cli in dtCliente.Rows)
                    {
                        CorClienteTop = cli["COD_MAIL_HEAD"].ToString();
                        CorClienteBot = cli["COD_MAIL_BOTTOM"].ToString();
                        CodCliente = cli["COD_CLIENT"].ToString();
                        TextColor = cli["COD_MAIL_TEXT"].ToString();
                    }

                    //MailHTMLBody += "<body style=\"font-family:Calibri;\">";
                    //MailHTMLBody += getHeader(CodCliente, CorClienteTop);





                    foreach (DataRow sh in News.Rows)
                    {
                        MailHTMLBody = getBodyHtml(sh["TITLE"].ToString(), (sh["EDITOR"].ToString() + " | " + sh["data"].ToString().Substring(0, 10)),
                            txt_aux_share_sendby.Value, txt_aux_share_comment.Value, "");

                        MailSubject = "insigte | media intelligence - " + sh["TITLE"].ToString();
                        //MailHTMLBody += "<br />" + sh["TITLE"].ToString() + "<br />" + Environment.NewLine;
                        //MailHTMLBody += "<br />" + sh["EDITOR"].ToString() + " | " + sh["DATE"].ToString().Substring(0, 10) + "<br />" + Environment.NewLine;
                    }

                    //MailHTMLBody += getBottom(CodCliente, CorClienteBot);

                    MailMessage mail = new MailMessage();
                    mail.To = txt_aux_share_email.Value;
                    mail.From = "insigte <info@insigte.com>";
                    mail.Bcc = "";
                    mail.Subject = MailSubject;
                    mail.BodyFormat = MailFormat.Html;
                    mail.Body = MailHTMLBody;

                    //mail.Attachments.Add(new MailAttachment(DownloadPath));
                    mail.Attachments.Add(new MailAttachment(PDF_OMFG));


                    mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", 25);
                    mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", 2);
                    mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", 1);
                    mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "info@insigte.com");
                    mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "jQY4qhQk");
                    mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", "false");


                    SmtpMail.SmtpServer = "mail.insigte.com";
                    SmtpMail.Send(mail);

                    Global.InsigteManager[Session.SessionID].inUser.EmailShareSend = "1";
                    //Session["EmailSend"] = "1";
                    //lbDashboard.Text = "OMFG " + Session["EmailSend"].ToString();
                }
                catch (Exception ex)
                {
                    Global.InsigteManager[Session.SessionID].inUser.EmailShareSend = "0";
                }

                Response.Redirect(Request.RawUrl);

                #endregion
            }

            #endregion

            #region Send Text
            // -------------- SEND TEXT -------------- 
            if (txt_aux_share_rblShare.Value == "1")
            {
                //MailHTMLBody += "<body style='font-family:Arial;'>";

                String Title = "TITLE";
                String texto = "TEXT as text";
                if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                {
                    Title = "TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
                    texto = "TEXT_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as text";
                }

                string cmdQuery = " SELECT " + texto + ", " + Title + ", EDITOR, DATE, m.ID, filepath, page_url, convert(varchar,DATE,105) as data FROM iadvisers.dbo.metadata m with(nolock) ";
                cmdQuery += " INNER JOIN iadvisers.dbo.contents c with(nolock) ON m.ID=c.ID where m.ID='" + txt_aux_share_id.Value + "' ";

                DataTable shareQ = Global.InsigteManager[Session.SessionID].getTableQuery(cmdQuery, "shareQ");

                foreach (DataRow sh in shareQ.Rows)
                {
                    MailSubject = "insigte | media intelligence - " + sh["TITLE"].ToString();
                    //MailHTMLBody = "<br />" + sh["EDITOR"].ToString() + " | " + sh["DATE"].ToString().Substring(0, 10) + "<br />" + Environment.NewLine;
                    //MailHTMLBody += "<br />" + sh["text"].ToString().Replace(Environment.NewLine, "<br />" + Environment.NewLine);
                    MailHTMLBody = getBodyHtml(sh["TITLE"].ToString(), (sh["EDITOR"].ToString() + " | " + sh["data"].ToString().Substring(0, 10)),
                       txt_aux_share_sendby.Value, txt_aux_share_comment.Value, sh["text"].ToString().Replace("\r\n", "<br />"));
                }
              

                try
                {



                    //MailHTMLBody += "</body>";

                    MailMessage mail = new MailMessage();
                    mail.To = txt_aux_share_email.Value;
                    mail.From = "insigte <info@insigte.com>";
                    mail.Bcc = "";
                    mail.Subject = MailSubject;
                    mail.BodyFormat = MailFormat.Html;
                    //mail.Body = MailEnviadoPor_Comment + MailHTMLBody;
                    mail.Body = MailHTMLBody;

                    mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", 25);
                    mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", 2);
                    mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", 1);
                    mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "info@insigte.com");
                    mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "jQY4qhQk");
                    mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", "false");

                    SmtpMail.SmtpServer = "mail.insigte.com";
                    SmtpMail.Send(mail);

                    Session["EmailSend"] = "1";
                }
                catch (Exception ex)
                {
                    Session["EmailSend"] = "0";
                }

            }
            #endregion

        }

        public string getBodyHtml(String _titulo, String _editDate, String _sendBy, String _commnent, String _texto)
        {

            string textotop = "Carma Delivery Services | " + Resources.insigte.language.shareArticle.ToString();
            string textoBot = "© Carma 2015 | Delivering what matters ";

            string rtn = "";

            rtn += "<body bgcolor='#ffffff' leftMargin='0px' link='#000000' vlink='#000000' alink='#000000' style='margin-top: 0px; margin-bottom:0px; font-family:Arial; font-size:26px; '>";
            rtn += "</br>";
            rtn += "<table border='0px' width='100%' style='font-size:26px; '>";
            rtn += "<tr>";
            rtn += "<td colspan='7' style='font-family:Arial; '>" + textotop + "</td>";
            rtn += "</tr>";
            rtn += "<tr><td colspan='7' height='10px' ></td></tr>";
            rtn += "<tr>";
            rtn += "<td colspan='7' height='50px' bgcolor='#f89c3e' style='color:white; padding-left:10px; font-family:Arial;'>" + Resources.insigte.language.sharedArticle.ToString() + ": " + _titulo + "</td>";
            rtn += "</tr>";
            rtn += "<tr><td colspan='7' height='10px' ></td></tr>";
            rtn += "<tr>";
            rtn += "<td  width='28%' bgcolor='#3b3d3c' >";
            rtn += "<table>";
            rtn += "<tr><td style='color:white; padding-left:10px;font-size:26px; font-family:Arial;'>" + Resources.insigte.language.shareSource.ToString() + "</td></tr>";
            rtn += "<tr><td style='color:white; padding-left:10px;font-size:18px; font-family:Arial;'>" + _editDate + "</td></tr>";
            rtn += "</table>";
            rtn += "</td>";
            rtn += "<td width='10px'></td>";
            rtn += "<td width='28%' bgcolor='#3b3d3c' >";
            rtn += "<table>";
            rtn += "<tr>";
            rtn += "<td style='color:white; padding-left:10px; font-size:26px; font-family:Arial;' >" + Resources.insigte.language.shareSent.ToString() + "</td></tr>";
            
            rtn += "<tr><td style='color:white; padding-left:10px; font-size:18px; font-family:Arial;'>" + _sendBy + "</td></tr>";
            rtn += "</table>";
            rtn += "</td>";
            rtn += "<td width='10px'></td>";
            rtn += "<td width='28%' bgcolor='#3b3d3c' >";
            rtn += "<table>";
            rtn += "<tr>";
            rtn += "<td style='color:white; padding-left:10px; font-size:26px; font-family:Arial;'>" + Resources.insigte.language.shareComment.ToString() + "</td>";
            rtn += "</tr>";
            rtn += "<tr><td style='color:white; padding-left:10px; font-size:18px; font-family:Arial;'>" + _commnent + "</td></tr>";
            rtn += "</table>";
            rtn += "</td>";
            rtn += "<td width='10px'></td>";
            rtn += "<td width='10%'><img src='http://insigte.com/login/Imgs/logo_carma_trans_small.png'/></td>";
            rtn += "<tr><td colspan='7' height='10px'  ></td></tr>";

            if (_texto.Equals(""))
            {
                rtn += "<tr><td colspan='7' style='padding-left:10px; font-family:Arial; font-size:18px;'><i>" + Resources.insigte.language.shareAnxPdf.ToString().Trim() + "</i></td></tr>";
                rtn += "<tr><td colspan='7' height='10px' ></td></tr>";
            }
            else
            {
                rtn += "<tr>";
                rtn += "<td colspan='7' style='color:#002d56; padding-left:10px; font-family:Arial;font-size:20px;'>" + _texto.Trim() + "</td>";
                rtn += "</tr>";
                rtn += "<tr><td colspan='7' height='10px' ></td></tr>";
            }
            //rtn += "<tr>";
            //rtn += "<td colspan='5'>";
            //rtn += "<table width='100%'>";
            ////rtn += "<td width='10px'></td>";
            //rtn += "<td colspan='5' style='font-size:14px; font-family:Arial;'>" + textoBot + "</td>";
            //rtn += "</table>";
            //rtn += "</td>";
            //rtn += "</tr>";
            rtn += "<td colspan='7' style='font-family:Arial;font-size:14px;'>" + textoBot.Trim() + "</td>";
            //rtn += "<td colspan='7' style='padding-left:10; font-family:Arial;font-size:14;'>" + textoBot + "</td>";
            rtn += "</tr>";
            rtn += "</table>";
            rtn += "</body>";

            return rtn;
        }

         //public String getHeader(String nome, String desc, String Cod_cli, String Color)
        public String getHeader(String CodCliente, String CorClienteTop)
        {
 
            String Head = "";
            Head += "<body bgcolor='#ffffff' leftMargin='0' link='#000000' vlink='#000000' alink='#000000' style='margin-top: 0; margin-bottom: 0'>       ";
            Head += "<table border='0' cellPadding='0' cellSpacing='0' width='100%'><tr><td align='center'><table border='0' cellPadding='0' cellSpacing='0' width='100%'><tr><td align='center'>  ";
            Head += "<table border='0' cellPadding='0' cellSpacing='0' width='100%'>  ";
            Head += "	<tr>  ";
            Head += "    	<td height='95px' width='190px'>  ";
            Head += "    		<img alt='' src='http://insigte.com/login/cli/" + CodCliente + "/fundo_email_newsletter_top.png'>  ";
            Head += "    	</td>  ";
            Head += "    	<td height='95px' width='100%' bgcolor='" + CorClienteTop + "'>  ";
            Head += "    		&nbsp;  ";
            Head += "    	</td>  ";
            Head += "    	<td height='95px' width='6px'>  ";
            Head += "    		<img alt='' src='http://insigte.com/login/cli/" + CodCliente + "/fundo_email_newsletter_top_r.png'>  ";
            Head += "    	</td>  ";
            Head += " </tr>  ";
            Head += "	<tr>  ";
            Head += "    	<td width='190px' bgcolor='#FFFFFF' style='height:1px;'>  ";
            Head += "    	</td>  ";
            Head += "    	<td width='100%' bgcolor='#FFFFFF' style='height:1px;'>  ";
            Head += "    	</td>  ";
            Head += "    	<td width='6px' bgcolor='#FFFFFF' style='height:1px;'>  ";
            Head += "    	</td>  ";
            Head += " </tr>  ";
            Head += "	<tr>  ";
            Head += "    	<td height='38px' width='190px'>  ";
            Head += "    		<img alt='' src='http://insigte.com/login/cli/" + CodCliente + "/fundo_email_newsletter_bottom.png'>  ";
            Head += "    	</td>  ";
            Head += "    	<td width='100%' bgcolor='#323232'>  ";
            Head += "    		&nbsp;  ";
            Head += "    	</td>  ";
            Head += "    	<td width='auto'>  ";
            Head += "    		<img alt='' src='http://insigte.com/login/cli/" + CodCliente + "/fundo_email_newsletter_bottom_r.png'>  ";
            Head += "    	</td>  ";
            Head += " </tr>  ";
            Head += "</table>  ";
            Head += "<table border='0' cellPadding='0' cellSpacing='0' width='100%'>  ";
            Head += "	<tr>  ";
            Head += "		<td vAlign='top'>  ";
            Head += "			<div align='center'>  ";
            Head += "				<table border='0' cellpadding='0' cellspacing='0' width='100%'>  ";
            Head += "					<tr>  ";
            Head += "						<td valign='top'>  ";
            Head += "							<div align='center'>  ";
            Head += "								<table border='0' cellpadding='0' cellspacing='0' width='100%' height='200'>     ";
            Head += "									<tr>  ";
            Head += "										<td valign='top' height='20' style='padding-left: 5'>  ";
            Head += "										</td>  ";
            Head += "										<td height='20'></td>  ";
            Head += "									</tr>  ";
            Head += "									<tr>  ";
            Head += "										<td valign='top' style='border-right: 1 dotted #C0C0C0; padding-left: 12; padding-right: 22' height='25'>  ";
            Head += "											<div align='center'>   ";

            return Head;
        }

        public String getBottom(String CodCliente, String CorClienteBot)
        {
            String bottom = "											</div> ";
            bottom += "										</td> ";
            bottom += "										<td valign='bottom'></td> ";
            bottom += "									</tr> ";
            bottom += "									<tr> ";
            bottom += "										<td valign='top' style='padding-left: 12; padding-right: 22' height='25'></td> ";
            bottom += "										<td valign='bottom'></td>    ";
            bottom += "									</tr> ";
            bottom += "								</table> ";
            bottom += "							</div> ";
            bottom += "						</td> ";
            bottom += "					</tr> ";
            bottom += "				</table> ";
            bottom += "			</div> ";
            bottom += "		</td> ";
            bottom += "	</tr> ";
            bottom += "</table> ";
            bottom += "<table border='0' cellpadding='0' cellspacing='0' width='100%'> ";
            bottom += "	<tr>       ";
            bottom += "	<td align='center' width='190px'> ";
            bottom += "	 <img alt='' src='http://insigte.com/login/cli/" + CodCliente + "/fundo_email_bottom.png'> </td><td width='100%' bgcolor='" + CorClienteBot + "'></td><td width='auto'><img alt='' src='http://insigte.com/login/cli/" + CodCliente + "/fundo_email_bottom_r.png'> </td></tr></table> ";
            bottom += " </td></tr></table></td></tr></table>  </body>";

            return bottom;
        }

        protected static void PlaceText(PdfContentByte pdfContentByte, iTextSharp.text.Font font, float lowerLeftx, float lowerLefty, float upperRightx, float upperRighty, float leading, int alignment, string text)
        {
            ColumnText ct = new ColumnText(pdfContentByte);
            ct.SetSimpleColumn(new Phrase(text, font), lowerLeftx, lowerLefty, upperRightx, upperRighty, leading, alignment);
            ct.Go();
        }

        public static void ErrorHandler(string strMethod, object rtnCode)
        {
            System.Diagnostics.Debug.WriteLine(strMethod + " error:  " + rtnCode.ToString());
        }

        public String Translate(String texto, String From, String To)
        {
            if (From != To)
            {

                string clientID = "In2013sigteTrans";
                string clientSecret = "BWZ7K5uc0+61tuLmOS9vzAQ03NGeuRmA9KwhiFBzEp8";

                String strTranslatorAccessURI = "https://datamarket.accesscontrol.windows.net/v2/OAuth2-13";
                String strRequestDetails = string.Format("grant_type=client_credentials&client_id={0}&client_secret={1}&scope=http://api.microsofttranslator.com", HttpUtility.UrlEncode(clientID), HttpUtility.UrlEncode(clientSecret));
                System.Net.WebRequest webRequest = System.Net.WebRequest.Create(strTranslatorAccessURI);

                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.Method = "POST";
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(strRequestDetails);
                webRequest.ContentLength = bytes.Length;

                using (System.IO.Stream outputStream = webRequest.GetRequestStream())
                {
                    outputStream.Write(bytes, 0, bytes.Length);
                }

                System.Net.WebResponse webResponse = webRequest.GetResponse();
                System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(AdmAccessToken));

                //Get deserialized object from JSON stream 
                AdmAccessToken token = (AdmAccessToken)serializer.ReadObject(webResponse.GetResponseStream());
                string headerValue = "Bearer " + token.access_token;


                string txtToTranslate = texto;
                string uri = "http://api.microsofttranslator.com/v2/Http.svc/Translate?text=" + System.Web.HttpUtility.UrlEncode(txtToTranslate) + "&from=" + From + "&to=" + To + "";
                System.Net.WebRequest translationWebRequest = System.Net.WebRequest.Create(uri);
                translationWebRequest.Headers.Add("Authorization", headerValue);
                System.Net.WebResponse response = null;
                response = translationWebRequest.GetResponse();
                System.IO.Stream stream = response.GetResponseStream();
                System.Text.Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
                System.IO.StreamReader translatedStream = new System.IO.StreamReader(stream, encode);
                System.Xml.XmlDocument xTranslation = new System.Xml.XmlDocument();
                xTranslation.LoadXml(translatedStream.ReadToEnd());
                return xTranslation.InnerText;
            }

            else
            {
                return texto;
            }

        }


        public object ShortedList { get; set; }
    }
}
