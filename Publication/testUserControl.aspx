﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="testUserControl.aspx.cs" Inherits="Insigte.testUserControl" %>

<%@ Register Src="~/UserControls/TestUserControl.ascx" TagPrefix="uc1" TagName="TestUserControl" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
        <div>
            <asp:Chart runat="server" ID="Chart1" Width="175px" Height="175px" Palette="Fire" Enabled="false">
            <Series>
                <asp:Series Name="Series1">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1">
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
            <br/>
            <br/>
            <br/>
            Below is the Chart within the User Control:
            <br/>
            <uc1:TestUserControl runat="server" id="ChartControlUC" />
        </div>

</body>
</html>
