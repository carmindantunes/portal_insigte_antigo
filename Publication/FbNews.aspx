﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FbNews.aspx.cs" Inherits="Insigte.FbNews" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

<style type="text/css"> 

    .Messagem
    {
    border:2px solid #a1a1a1;
    background:#dddddd;
    width:769px;
    border-radius:5px;
    border-bottom-color:rgb(161, 161, 161); 
    padding:7px ; 
    margin:5px;
    float:left;
    height:100%;  
    }

    .MessagemComents
    {
    border:2px solid #a1a1a1;
    background-color: rgb(240, 240, 240);
    width:758px;
    border-radius:5px;
    border-bottom-color:rgb(161, 161, 161); 
    padding:7px ; 
    margin:5px;
    }

    .ui-widget-contentrpt2 
    {
     border: 1px solid #aaaaaa/*{borderColorContent}*/;
	background: #ffffff/*{bgColorContent}*/ url(images/ui-bg_flat_75_ffffff_40x100.png)/*{bgImgUrlContent}*/ 50%/*{bgContentXPos}*/ 50%/*{bgContentYPos}*/ repeat-x/*{bgContentRepeat}*/;
	color: #222222/*{fcContent}*/;  
    border-radius:5px; 
    float:left;
    margin-top:5px;
    }

    .ui-widget-contentrpt1 
    {
    border: 1px solid #aaaaaa/*{borderColorContent}*/;
	background: #ffffff/*{bgColorContent}*/ url(images/ui-bg_flat_75_ffffff_40x100.png)/*{bgImgUrlContent}*/ 50%/*{bgContentXPos}*/ 50%/*{bgContentYPos}*/ repeat-x/*{bgContentRepeat}*/;
	color: #222222/*{fcContent}*/;  
    border-radius:5px;       
    }

    .teste
    {
    margin:0px 0px 10px 10px;
    font-family:Arial;
    color:Black;    
    }

    .hiddendiv {display:none;}
    .visiblediv {display:block;}

    .links
    {
    color: rgb(0,0,0);
    font-size:12px;
    }
</style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div id="Geral" style="float:left; padding:0px; margin:0px; height:100%; width:800px;">
        <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%;"> 
            <div style="margin-top:12px">
                <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false"  />
            </div>
        </div>
        <asp:Label ID="lbl_titulo"  CssClass="teste"  runat="server" Visible= "true" Font-Bold="true" ><%# Eval("title").ToString() %></asp:Label> 
    </div>
    
    <div style="font-family:Arial; font-size:12px; margin-top:20px">
    <asp:Repeater id="rpt_Fb_posts"  runat="server"  > <%--OnItemDataBound="rpt_Fb_posts_OnItemDataBound">--%>
        <itemtemplate>
            <div>
                <asp:Label ID="lbl_fb_id" runat="server" Visible="false" ><%# Eval("fb_id_news_posts") %></asp:Label>
                <div class="ui-widget-contentrpt1" id="<%# Eval("fb_id_news_posts") %> " style="  float:left;  padding:0px;   margin:15px 5px 5px 0px; width:800px; "  >
                    <div style="margin-left:10px; margin-top:10px; margin-bottom:10px; font-size:14px;" >
                        <%# Eval("fb_id_post_user_name").ToString() %><br />
                        <%# Eval("fb_created_time").ToString() %><br />
                    </div>
                    <div class="Messagem" >
                        <%# Eval("fb_post_msg").ToString()%>
                    </div>
                    <div style="float:right; margin:5px 12px 5px 0px; color: #000000; text-decoration: none;">
                   
                         <asp:LinkButton CssClass="links" id="btn_comentarios" runat="server" Visible="true" OnClick="btn_comentarios_click"  CommandArgument='<%# Eval("fb_id_news_posts") %>'>               
                                  <img src="Imgs/icons/custom/png/comment.jpg" alt="" style='border-width:0px; width:16px; height:16px' />
                                  <%# Eval("fb_coments_val").ToString()%>
                         </asp:LinkButton> 
                                
                         <asp:LinkButton CssClass="links" id="btnlikes"  runat="server" Visible="true" >
                                  <img src="Imgs/icons/custom/png/likes.jpg" alt="" style='border-width:0px; width:16px; height:16px' />
                                  <%# Eval("fb_likes_val").ToString()%>
                         </asp:LinkButton>   

                         <asp:LinkButton CssClass="links" id="LinkButton2" runat="server" Visible="true">
                                  <img src="Imgs/icons/custom/png/share.jpg" alt="" style='border-width:0px; width:16px; height:16px' />
                                  <%# Eval("fb_shares_val").ToString()%>
                         </asp:LinkButton>

                         <a href="<%# Eval("fb_post_url") %>"
                                target="blank" onmouseout="this.style.textDecoration='none';">
                                <img  src="Imgs/icons/custom/png/link.png" alt="Link" style='border-width:0px; width:16px; height:16px' />                    
                         </a>
                     </div>
                    <div id="_divComents" postid='<%# Eval("fb_id_news_posts") %>' runat="server" visible="false" ><%# mostraComent() %> </div>
                 </div>
            </div>
        </itemtemplate>
    </asp:Repeater>
    </div>
</asp:Content>

