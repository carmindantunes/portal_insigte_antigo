﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pLogin.aspx.cs" Inherits="Insigte.pLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.0.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>   
<meta content="pt" http-equiv="Content-Language" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>insigte | media intelligence</title>

<script type="text/javascript">    !function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = "//platform.twitter.com/widgets.js "; fjs.parentNode.insertBefore(js, fjs); } } (document, "script", "twitter-wjs");</script>

<link rel="icon" type="ico" href="Imgs/insigte_icon.ico" />

<script  type="text/javascript">
    var imgArray = new Array();
    var macOver = false;
    for (i = 1; i <= 2; i++) {
        //alert("Imgs/pLogin/SlideMac/img" + i + ".png");
        imgArray[i] = "Imgs/pLogin/SlideMac/img" + i + ".png";

    }

    $(document).ready(function () {

        window.onscroll = function () {
            var p = $("#pTabCol");
            var offset = p.offset();
            var scrollTop = $(window).scrollTop();
//            steste.innerHTML = scrollTop;
            if (scrollTop > 50) {

                pTopDiv.setAttribute("style", "background-color:White;");

                if (scrollTop > 420) {
                    pTabColTD.setAttribute("bgcolor", "#00ADD9");
                    pNewTecTB.setAttribute("style", "width: 1000px; background-color:white; margin-top:58px;");
                    pTabCol.setAttribute("style", " position:fixed; width:1000px; left:50%; top:82px; margin-left:-500px; border-style:none; border-width:0px; height:58px;");
                } else {
                    pTabCol.setAttribute("style", "width:1000px; height:58px;");
                    pTabColTD.setAttribute("bgcolor", "#333333");
                    pNewTecTB.setAttribute("style", "width: 1000px; background-color:white; margin-top:0px;");
                }

            } else {

                pTopDiv.setAttribute("style", "background-color:none;");
                if (scrollTop > 420) {

                    pTabColTD.setAttribute("bgcolor", "#00ADD9");
                    pNewTecTB.setAttribute("style", "width: 1000px; background-color:white; margin-top:58px;");
                    pTabCol.setAttribute("style", " position:fixed; width:1000px; left:50%; top:82px; margin-left:-500px; border-style:none; border-width:0px; height:58px;");
                } else {
                    pTabCol.setAttribute("style", "width:1000px; height:58px;");
                    pTabColTD.setAttribute("bgcolor", "#333333");
                    pNewTecTB.setAttribute("style", "width: 1000px; background-color:white; margin-top:0px;");
                }
            }
        }
    });
</script>

<style type="text/css">
    .auto-style1 {
	    text-align: left;
	    background-image: url('Imgs/pLogin/fundo_hands_mac_trans.png');
	    background-size:100% 100%;
    }
    .auto-style2 {
	    text-align: left;
	    margin-left: 40px;
	    width:143px;
    }
    .auto-style3 {
	    text-align: left;
	    font-family: Arial, Helvetica, sans-serif;
	    font-size: x-large;
	    color: #FFFFFF;
	    margin-left: 40px;
    }
    .auto-style5 {
	    text-align: left;
	    font-family: Arial, Helvetica, sans-serif;
	    font-size: medium;
	    width: 441px;
    }
    .auto-style6 {
	    text-align: left;
	    font-family:Arial, Helvetica, sans-serif;
	    font-size: medium;
	    color: #FFFFFF;
    }
    .auto-style7 {
	    font-family: Arial, Helvetica, sans-serif;
	    font-size: medium;
	    color: #FFFFFF;
    }
    .auto-style8 {
	    font-family: Arial, Helvetica, sans-serif;
	    font-size: medium;
    }
    .auto-style9 {
	    color: #00ADD9;
	    font-size: 24pt;
	    font-family: Verdana;
    }
    .auto-style11 {
	    background-color: #333333;
    }
    .auto-style13 {
	    text-align: right;
    }
    .auto-style14 {
	    font-family: Arial, Helvetica, sans-serif;
	    color: #FFFFFF;
	    font-size: 11pt;
    }
    .auto-style22 {
	    color: #00ADD9;
	    font-size: 24pt;
	    font-family: Calibri;
    }
    .auto-style23 {
	    font-size: 28pt;
	    font-family: Calibri;
    }
    .auto-style24 {
	    border-width: 0px;
    }
    a {
	    color: #00ADD9;
    }
    .auto-style25 {
	    text-align: left;
    }
    .auto-style26 {
	    text-align: left;
	    color: #FFFFFF;
	    font-family: calibri;
	    font-size: 16pt;
    }
    .auto-style27 {
	    color: #FFFFFF;
	    font-family: calibri;
    }
    .auto-style28 {
	    border-width: 0px;
	    margin-left: 0px;
    }
    .auto-style30 {
	    font-family: calibri;
	    font-size: 26pt;
	    color: #FFFFFF;
    }
    .auto-style31 {
	    color: #FFFFFF;
	    font-family: calibri;
	    font-size: 10pt;
    }
    .auto-style32 {
	    background-color: #808080;
    }
    .auto-style33 {
	    background-color: #505050;
    }
    .auto-style35 {
	    font-size: 12pt;
    }
    .auto-style38 {
	    text-align: left;
	    font-family: calibri;
	    font-size: 14pt;
    }
    .auto-style40 {
	    background-color: #333333;
    }
    .auto-style41 {
	    text-align: right;
	    font-size: large;
    }
    .auto-style42 {
	    color: #FFFFFF;
	    font-size: small;
	    font-family: calibri;
    }
    .auto-style43 {
	    font-family: calibri;
	    font-size: xx-large;
	    color: #FFFFFF;
    }
    .auto-style44 {
	    font-family: calibri;
	    font-size: 39pt;
	    color: #FFFFFF;
    }
        
    .styleB
    {
        padding:0px;
    }
    .styleTec
    {
        width:100%; 
        height:150px; 
        font-family:Calibri; 
        text-align:center;
    }
    .styleTec1
    {
        width:100%; 
        height:185px; 
        font-family:Calibri; 
        text-align:center;
    }
    .styleTec1t
    {
        width:100%; 
        height:180px; 
        font-family:Calibri; 
        text-align:center;
    }
    .styleTec-p1
    {
        color:#00ADD9; font-size:18pt; margin-bottom:0px; font-weight: bold;
    }
    
    .ptopDivCla
    {
        position:fixed; width:1000px; left:50%; top:0px; margin-left:-500px; border-style:none; border-width:0px; z-index: 2;
    }

    .style1
    {
        width: 561px;
    }

    .style2
    {
        height: 500px;
        width: 143px;
    }
    
    .slideMac
    {
        height: 216px;
        width: 336px;
        position: relative;
        top: -80px;
        right: -49px;
        z-index: 1;
        background-size: 100% 100%;  
    }

    </style>

</head>
<body style="margin-top: 0;">
<a href="https://plus.google.com/107453152500726127422" rel="publisher"></a>
<form id="login" runat="server">

<div id="pTopDiv" class="ptopDivCla" align="center" style="">
    <table cellpadding="0" cellspacing="0" style="border-style: none; border-color: inherit; border-width: 0px; width: 100%; " >
        <tr>
            <td class="style1" >
                <img src="Imgs/insigte_logo.png" alt="" height="66px" width="119px" style="margin-left:20px; margin-top:8px; margin-bottom:8px; float:left;" />
                <%--<span id="steste" style="color: Red;">dfsfs</span>--%>
            </td>
            <td align="right"  >
	            <div style="margin-right:20px;">
                    <a href="https://www.linkedin.com/company/insigte" target="_blank"><img alt="" height="32" src="Imgs/linkedinwhite.png" width="32" style="border:0px none white;"  /></a>
                    <a href="https://plus.google.com/107453152500726127422" target="_blank"><img alt="" height="32" src="Imgs/googlepluswhite.png" width="32" style="border:0px none white;"  /></a>
                </div>
            </td>
        </tr>
    </table>
</div>
<table align="center" border="0" cellpadding="0" cellspacing="0" style="width: 1000px; height: 500px;">
	<tr>
		<td class="auto-style1" valign="top">
		<table cellpadding="0" cellspacing="4" style="width: 100%; height: 500px;">
			<tr>
				<td style="width: 369px; height: 100%; ">
				    <div style="margin:0px 70px 20px 50px;">
                        <div style="margin-bottom:20px;">
				            <strong>
                                <span class="auto-style22">Clientes</span>
                            </strong>
                        </div>                       
					    <table border="0" width="100%">
						    <tr>
							    <td class="auto-style6">
                                    <div style="text-align:right; padding-right: 4px;">utilizador</div>
                                </td>
                                <td style="width: 100%"><input type="text" id="txt_user" runat="server" name="logas" style="width: 100%" /></td>
						    </tr>
						    <tr>
							    <td class="auto-style6"><div style="text-align: right; padding-right: 4px;">código</div></td>
							    <td style="width: 100%"><input type="password" id="txt_pass" runat="server" name="passas" style="width: 100%" /></td>
							    <td>
                                    <div style="margin-left:5px; text-align: right;">
                                        <asp:ImageButton runat="server" ID="Ib_login" ImageUrl="Imgs/site_ok.png" OnClick="Ib_login_Click"  />
							        </div>
                                </td>
						    </tr>						
					    </table>
				    </div>
				</td>
				<td class="style2" style="height: 100%;" >
                    <div></div>
                </td>
				<td valign="bottom"  style="height: 100%;">
                    <div id="slide" class="slideMac" style="" onmouseover="slideit(true);" onmouseout="slideit(false);">
                        <script type="text/javascript">
                            //variable that will increment through the images

//                            function mouseOuverMac(var ) {
//                                macOver = true;
//                                
//                            }

//                            function mouseLeaveMac() {
//                                macOver = false;
//                                slideit();
//                            }

                            var step = 1
                            function slideit(teste) {
                                
                                if (teste == true) 
                                    return;
                                    //document.getElementById('slide')
                                    //if browser does not support the image object, exit.
                                    if (!document.images)
                                        return;

                                    slide.setAttribute("style", " background-image:url( " + imgArray[step] + ");");
                                    if (step < 2)
                                        step++;
                                    else
                                        step = 1;
                                    //call function "slideit()" every 2.5 seconds
                                    setTimeout("slideit()", 2500); 
                                }
                                
                            slideit(false);
                        </script>
                    </div>
                    <div style="text-align: center; font-size: 22pt; font-family: calibri; color: #000000; margin-bottom: 50px; margin-right: 35px; ">
				        <span>media intelligence company</span>
                    </div>
                </td>
			</tr>
			</table>
		</td>
	</tr>
</table>
<table id="pTabCol" align="center" class="auto-style11" cellpadding="0" cellspacing="0" style="width: 1000px; height:58px; ">
	<tr>
		<td id="pTabColTD" bgcolor="#333333" >
        <div class="auto-style30" style=" margin-left: 50px; font-size: 17pt; ">collecting small pieces to deliver the Big Picture
        </div></td>
	</tr>
</table>
<table id="pNewTecTB" align="center" cellpadding="0" cellspacing="0" style="width: 1000px; background-color: white;">    
    <tr>
        <td valign="middle"  style="height: 40px;">
            <p style="text-align: center; font-family: Calibri; font-size: 28pt; margin-top: 30px; margin-bottom: 30px;">Nova tecnologia, novos INSIGTES</p>
        </td>
    </tr>
    <tr >
        <td >
            <table align="center" style="width: 90%;" >
                <tr style="text-align: center;" valign="middle" >
                    <td class="styleB">
                        <img src="Imgs/pLogin/newTec/RealTime.png" alt="" style="height: 120px; " />
                        <div class="styleTec">
                            <p class="styleTec-p1">REAL TIME</p>
                            <div style="color: black; margin-top: 20px;">Recepção de informação em tempo real de forma a permitir uma atitude proactiva, eficaz e assertiva</div>
                        </div>
                    </td>
                    <td style="width: 2.5%;"></td>
                    <td class="styleB">
                        <img src="Imgs/pLogin/newTec/EarlyWarning.png" alt="" style="height: 120px; " />
                        <div class="styleTec">
                            <p class="styleTec-p1">EARLY WARNING</p>
                            <div style="color: black; margin-top: 20px;">Entender as tendências, antecipar as necessidades e monitorizar o ambiente competitivo</div>
                        </div>
                    </td>
                    <td style="width: 2.5%;"></td>
                    <td class="styleB">
                        <img src="Imgs/pLogin/newTec/Performance.png" alt="" style="height: 120px; " />
                        <div class="styleTec" style="width:100%;">
                            <p class="styleTec-p1">PERFORMANCE</p>
                            <div style="color: black; margin-top: 20px;">Partilhar o retorno financeiro das acções de comunicação, as pessoas atingidas e o share mediático</div>
                        </div>
                    </td>
                    <td style="width: 2.5%;"></td>
                    <td class="styleB">
                        <img src="Imgs/pLogin/newTec/BenchMark.png" alt="" style="height: 120px; " />
                        <div class="styleTec">
                            <p class="styleTec-p1">BENCHMARK</p>
                            <div style="color: black; margin-top: 20px;">Conhecer a performance mediática de cada concorrente e identificar as acções</div>
                        </div>
                    </td>
                </tr>
                <tr">
                    <td style="height: 15px;"></td>
                    <td style="width: 2.5%;"></td>
                    <td style="height: 15px;"></td>
                    <td style="width: 2.5%;"></td>
                    <td style="height: 15px;"></td>
                    <td style="width: 2.5%;"></td>
                    <td style="height: 15px;"></td>
                    
                </tr>
                <tr style="text-align: center" valign="middle" >
                    <td class="styleB">
                        <img src="Imgs/pLogin/newTec/BrandMining.png" alt="" style="height: 120px; " />
                        <div class="styleTec">
                            <p class="styleTec-p1">BRANDMINING</p>
                            <div style="color: black; margin-top: 20px;">Analisar o comportamento da marca, defender a reputação e realinhar o planeamento estratégico</div>
                        </div>
                    </td>
                    <td style="width: 2.5%;"></td>
                    <td class="styleB">
                        <img src="Imgs/pLogin/newTec/Geo-Mkt.png" alt="" style="height: 120px; " />
                        <div class="styleTec">
                            <p class="styleTec-p1">GEO-MKT</p>
                           <div style="color: black; margin-top: 20px;">Conhecer a performance geográfica da organização, em cada província e o respectivo benchmark</div>
                        </div>
                    </td>
                    <td style="width:2.5%;"></td>
                    <td class="styleB">
                        <img src="Imgs/pLogin/newTec/Multi-Format.png" alt="" style="height:120px; " />
                        <div class="styleTec"">
                            <p class="styleTec-p1">MULTI-FORMAT</p>
                            <div style="color: black; margin-top: 20px;">Plataforma acessível através de qualquer dispositivo, seja o computador, o tablet ou o telefone</div>
                        </div>
                    </td>
                    <td style="width:2.5%;"></td>
                    <td class="styleB">
                        <img src="Imgs/pLogin/newTec/Share.png" alt="" style="height:120px; " />
                        <div class="styleTec" >
                            <p class="styleTec-p1">SHARE</p>
                           <div style="color: black; margin-top: 20px;">Partilhar os factos relevantes por toda a organização de forma a gerar valor</div>
                        </div>
                    </td>
                </tr>
                <tr >
                    <td style="height:15px;"></td>
                </tr>   
            </table>
        </td>
    </tr>
    <%--<tr style="background-color:#E5E5E5;">
        <td valign="top"  style=" text-align:center; color:#505050; font-family:Calibri;" >
            <div style=" margin:20px 1px 20px 1px;  ">
                <span  style="font-size:18pt;">análise de impacto | performance da marca | gestão da reputação</span>
            </div>
        </td>
    </tr>--%>
</table>

<table id="Table1" align="center" cellpadding="0" cellspacing="0" style="width: 1000px; background-color: #d7d7d7;">    
    <tr>
        <td valign="middle"  style="height: 40px;">
            <p style="text-align: center; font-family: Calibri; font-size: 28pt; margin-top: 30px; margin-bottom: 30px;">OUTPUTS que geram INSIGTES</p>
        </td>
    </tr>
    <tr >
        <td >
            <table align="center" style="width: 90%;" >
                <tr style="text-align: center;" valign="middle" >
                    <td class="styleB">
                        <img src="Imgs/pLogin/outPuts/Management.png" alt="" style="height: 120px; " />
                        <div class="styleTec1">
                            <p class="styleTec-p1">MANAGEMENT</p>
                            <div style="color: black; margin-top: 20px;">Sumário Executivo<br />Diagnóstico reputacional<br />Relatório de media<br />Relatório BI<br /></div>
                        </div>
                    </td>
                    <td style="width: 2.5%;"></td>
                    <td class="styleB">
                        <img src="Imgs/pLogin/outPuts/MktComm.png" alt="" style="height: 120px; " />
                        <div class="styleTec1">
                            <p class="styleTec-p1">MKT & COMM</p>
                            <div style="color: black; margin-top: 20px;">Sumário Executivo<br />Diagnóstico reputacional<br />Relatório de media<br />Relatório BI<br />Newsletter & Alertas<br />Portal</div>
                        </div>
                    </td>
                    <td style="width: 2.5%;"></td>
                    <td class="styleB">
                        <img src="Imgs/pLogin/outPuts/NewBusiness.png" alt="" style="height: 120px; " />
                        <div class="styleTec1" style="width:100%;">
                            <p class="styleTec-p1">NEW BUSINESS</p>
                            <div style="color: black; margin-top: 20px;">Sumário Executivo<br />Newsletter<br />Alertas | Prospects</div>
                        </div>
                    </td>
                    <td style="width: 2.5%;"></td>
                    <td class="styleB">
                        <img src="Imgs/pLogin/outPuts/ClientCare.png" alt="" style="height: 120px; " />
                        <div class="styleTec1">
                            <p class="styleTec-p1">CLIENT CARE</p>
                            <div style="color: black; margin-top: 20px;">Sumário Executivo<br />Newsletter<br />Alertas | Clientes</div>
                        </div>
                    </td>
                </tr>
                <tr">
                    <td style="height: 15px;"></td>
                    <td style="width: 2.5%;"></td>
                    <td style="height: 15px;"></td>
                    <td style="width: 2.5%;"></td>
                    <td style="height: 15px;"></td>
                    <td style="width: 2.5%;"></td>
                    <td style="height: 15px;"></td>
                    
                </tr>
                <tr style="text-align: center" valign="middle" >
                    <td class="styleB">
                        <img src="Imgs/pLogin/outPuts/Research.png" alt="" style="height: 120px; " />
                        <div class="styleTec1t">
                            <p class="styleTec-p1">RESEARCH</p>
                            <div style="color: black; margin-top: 20px;">Sumário Executivo<br />Diagnóstico reputacional<br />Newsletter<br />Relatório BI<br />Portal</div>
                        </div>
                    </td>
                    <td style="width: 2.5%;"></td>
                    <td class="styleB">
                        <img src="Imgs/pLogin/outPuts/Technology.png" alt="" style="height: 120px; " />
                        <div class="styleTec1t">
                            <p class="styleTec-p1">TECHNOLOGY</p>
                            <div style="color: black; margin-top: 20px;">Sumário Executivo<br />Newsletter | TECH</div>
                        </div>
                    </td>
                    <td style="width:2.5%;"></td>
                    <td class="styleB">
                        <img src="Imgs/pLogin/outPuts/ProjectManager.png" alt="" style="height:120px; " />
                        <div class="styleTec1t">
                            <p class="styleTec-p1">PROJECTS</p>
                            <div style="color: black; margin-top: 20px;">Sumário Executivo<br />Newsletter<br />Alertas | Project</div>
                        </div>
                    </td>
                    <td style="width:2.5%;"></td>
                    <td class="styleB">
                        <img src="Imgs/pLogin/outPuts/Global.png" alt="" style="height:120px; " />
                        <div class="styleTec1t" >
                            <p class="styleTec-p1">GLOBAL</p>
                           <div style="color: black; margin-top: 20px;">Sumário Executivo<br />Newsletter diária<br />Alertas<br />Research</div>
                        </div>
                    </td>
                </tr> 
        </td>
    </tr>
   </table>

<table align="center" cellpadding="0" cellspacing="0" style="width: 1000px" class="auto-style32">
    <%--<tr>
        <td valign="middle" class="auto-style33" style="height:40px;">
            <p style="text-align:center; font-family:Calibri; font-size:28pt; margin-top:30px; margin-bottom:30px; color:White;">Nova tecnologia, novos INSIGHTS</p>
        </td>
    </tr>--%>
	<tr>
		<td class="auto-style33">
		<div class="auto-style41">
			&nbsp;</div>
		
		<%--<asp:ScriptManager ID="script" runat="server" ></asp:ScriptManager>--%>
		
		<table align="center" style="width: 85%">
			<tr>
				<td class="auto-style26" style="width: 30%; height: 30px" valign="top">
                    <div>Angola</div>
                   <%-- <asp:Timer ID="timerAO" OnTick ="timerAO_Tick" runat="server" Interval="1000"></asp:Timer>
                    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerAO" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <div style="font-size: 14pt;"><asp:Label runat="server" ID="lb_tmAo" Text="" ForeColor="#333333"></asp:Label> </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                    
				</td>
				<td class="auto-style38" style="width: 15px; height: 30px" valign="top">
				    &nbsp;</td>
				<td class="auto-style26" style="width: 30%; height: 30px;" valign="top">
				    <div>Cabo Verde</div>
                   <%-- <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerAO" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <div style="font-size: 14pt;"><asp:Label runat="server" ID="lb_tmMz" Text="" ForeColor="#333333"></asp:Label> </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </td>
				<td style="width: 15px; height: 30px;" class="auto-style38" valign="top">
				    &nbsp;</td>
				<td style="width: 30%; height: 30px;" valign="top"" class="auto-style26">
                    <div>Moçambique</div>
                  <%--  <asp:UpdatePanel ID="UpdatePanel3" UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerAO" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <div style="font-size: 14pt;"><asp:Label runat="server" ID="lb_tmPt" Text="" ForeColor="#333333"></asp:Label> </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </td>
			</tr>
			<tr>
				<td style="width: 30%" class="auto-style25" valign="top">		
		
		<%= getTop5AO() %>
				
				</td>
				<td style="width: 15px" class="auto-style25" valign="top">&nbsp;</td>
				<td style="width: 30%" valign="top" class="auto-style25">
				
        <%= getTop5CV() %>

                </td>
				<td style="width: 15px" class="auto-style25" valign="top">&nbsp;</td>
				<td style="width: 30%" class="auto-style25" valign="top">
		 <%= getTop5MZ() %>

			    </td>
			</tr>
            <tr>
                <td><br /></td>
            </tr>

            <tr>
				<td class="auto-style26" style="width: 30%; height: 30px" valign="top">
                    <div>Portugal</div>
                    <%--<asp:UpdatePanel ID="UpdatePanel4" UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerAO" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <div style="font-size: 14pt;"><asp:Label runat="server" ID="lb_tmAS" Text="" ForeColor="#333333"></asp:Label> </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                    
				</td>
				<td class="auto-style38" style="width: 15px; height: 30px" valign="top">
				    &nbsp;</td>
				<td class="auto-style26" style="width: 30%; height: 30px;" valign="top">
				    <div>África do Sul</div>
                   <%-- <asp:UpdatePanel ID="UpdatePanel5" UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerAO" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <div style="font-size: 14pt;"><asp:Label runat="server" ID="lb_tmNB" Text="" ForeColor="#333333"></asp:Label> </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </td>
				<td style="width: 15px; height: 30px;" class="auto-style38" valign="top">
				    &nbsp;</td>
				<td style="width: 30%; height: 30px;" valign="top"" class="auto-style26">
                    <div>Botswana</div>
                   <%-- <asp:UpdatePanel ID="UpdatePanel6" UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerAO" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <div style="font-size: 14pt;"><asp:Label runat="server" ID="lb_aux" Text="" ForeColor="#333333"></asp:Label> </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </td>
			</tr>
			<tr>
				<td style="width: 30%" class="auto-style25" valign="top">		
		
		<%= getTop5PT()%>
				
				</td>
				<td style="width: 15px" class="auto-style25" valign="top">&nbsp;</td>
				<td style="width: 30%" valign="top" class="auto-style25">
				
        <%= getTop5AS()%>

                </td>
				<td style="width: 15px" class="auto-style25" valign="top">&nbsp;</td>
				<td style="width: 30%" class="auto-style25" valign="top">
		
        <%= getTop5BW()%>

			    </td>
			</tr>
            <tr>
                <td><br /></td>
            </tr>
            <tr>
				<td class="auto-style26" style="width: 30%; height: 30px" valign="top">
                    <div>Namibia</div>
                    <%--<asp:UpdatePanel ID="UpdatePanel4" UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerAO" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <div style="font-size: 14pt;"><asp:Label runat="server" ID="lb_tmAS" Text="" ForeColor="#333333"></asp:Label> </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                    
				</td>
				<td class="auto-style38" style="width: 15px; height: 30px" valign="top">
				    &nbsp;</td>
				<td class="auto-style26" style="width: 30%; height: 30px;" valign="top">
				    <div>Zimbabwe</div>
                   <%-- <asp:UpdatePanel ID="UpdatePanel5" UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerAO" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <div style="font-size: 14pt;"><asp:Label runat="server" ID="lb_tmNB" Text="" ForeColor="#333333"></asp:Label> </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </td>
				<td style="width: 15px; height: 30px;" class="auto-style38" valign="top">
				    &nbsp;</td>
				<td style="width: 30%; height: 30px;" valign="top"" class="auto-style26">
                    <div></div>
                   <%-- <asp:UpdatePanel ID="UpdatePanel6" UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerAO" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <div style="font-size: 14pt;"><asp:Label runat="server" ID="lb_aux" Text="" ForeColor="#333333"></asp:Label> </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </td>
			</tr>
			<tr>
				<td style="width: 30%" class="auto-style25" valign="top">		
		
		<%= getTop5NB()%>
				
				</td>
				<td style="width: 15px" class="auto-style25" valign="top">&nbsp;</td>
				<td style="width: 30%" valign="top" class="auto-style25">
				
        <%= getTop5ZB()%>

                </td>
				<td style="width: 15px" class="auto-style25" valign="top">&nbsp;</td>
				<td style="width: 30%" class="auto-style25" valign="top">
		

			    </td>
			</tr>

		</table>
		<br />
		</td>
	</tr>
</table>
<table align="center" cellpadding="0" cellspacing="0" class="auto-style40" style="width: 1000px">
	<tr>
		<td class="auto-style31" style="height: 46px">
				<span class="auto-style31">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; insigte ™ | 2014 | insigte is a registered trademark of one vision consulting</span><br />
		</td>
		<td class="auto-style42" style="height: 46px; text-align: right; visibility:hidden">
			<span class="auto-style14">
                <a href="https://www.linkedin.com/company/insigte" target="_blank"><img alt="" height="32" src="Imgs/linkedinwhite.png" width="32" class="auto-style24" /></a>
                <a href="https://plus.google.com/107453152500726127422" target="_blank"><img alt="" height="32" src="Imgs/googlepluswhite.png" width="32" class="auto-style24" /></a>
            </span>
        </td>
	</tr>
</table>
</form>
</body>
</html>