﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ArticleInfo.aspx.cs" Inherits="Insigte.ArticleInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
    a.fivenews:link /* unvisited link */
    {
        text-decoration:none;
        color:#000000;
    }
    a.fivenews:visited /* unvisited link */
    {
        text-decoration:none;
        color:#000000;
    }
    a.fivenews:hover /* unvisited link */
    {
        text-decoration:none;
        color:#000000;
    }
    a.fivenews:active /* unvisited link */
    {
        text-decoration:none;
        color:#000000;
    }
    
    div.topnews
    {
        float:inherit;
        border:1px dotted #c9c9c9;
        background-color:#e0e0e0;
        width: 98%;
        margin: 4px 0px 4px 0px;
        padding: 2px 2px 2px 2px;
    }
    
    
    a.ent:link /* unvisited link */
    {
        text-decoration:none;
        color:#000000;
        border-bottom:1px dotted #c9c9c9;
        
    }    
    a.ent:visited /* visited link */
    {
        text-decoration:none;
        color:#000000;
        border-bottom:1px dotted #c9c9c9;
    } 
    a.ent:hover /* mouse over link */
    {
        text-decoration:underline;
        color:#000000;
        border-bottom:1px dotted #c9c9c9;
    }   
    a.ent:active /* selected link */
    {
        text-decoration:underline;
        color:#000000;
        border-bottom:1px dotted #c9c9c9;
    }  
</style>
<script language="javascript" type="text/javascript">

    var gAutoPrint = true;

    function processPrint() {

        if (document.getElementById != null) {
            var html = '<HTML>\n<HEAD>\n';
            if (document.getElementsByTagName != null) {
                var headTags = document.getElementsByTagName("head");
                if (headTags.length > 0) html += headTags[0].innerHTML;
            }

            html += '\n</HE' + 'AD>\n<BODY>\n';
            var printReadyElem = document.getElementById("printMe");

            if (printReadyElem != null) html += printReadyElem.innerHTML;
            else {
                alert("Error, no contents.");
                return;
            }

            html += '\n</BO' + 'DY>\n</HT' + 'ML>';
            var printWin = window.open("", "processPrint");
            printWin.document.open();
            printWin.document.write(html);
            printWin.document.close();

            if (gAutoPrint) printWin.print();
        } else alert("Browser not supported.");

    } 
</script> 
    <script type="text/javascript">
        var _gaq = _gaq || [];

        _gaq.push(['_setAccount', 'UA-33483716-1']);

        _gaq.push(['_trackPageview']);

        (function () {

            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

        })();
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="width:810px;" id="printMe">
        <asp:Table runat="server" ID="Table1" Height="100%" Width="100%" CellPadding="12">
            <asp:TableRow> 
            <asp:TableCell Width="60%" VerticalAlign="Top">
                <asp:Label runat="server" ID="lblArticleTitle" Font-Names="Arial" Font-Size="14px" Font-Bold="true"/><br /><br />
                <asp:Label runat="server" ID="lblArticleEditorData" Font-Names="Arial" Font-Size="12px" ForeColor="#000000" Font-Bold="true"/><br /><br />
                <asp:Label runat="server" ID="lblArticleText" Font-Names="Arial" Font-Size="12px" /> 
            </asp:TableCell>
            <asp:TableCell Width="40%" BorderStyle="Dotted" BorderWidth="1px" BorderColor="#c9c9c9">
                <asp:Table runat="server" ID="Table2" Height="100%" Width="100%" CellPadding="0" CellSpacing="0" BorderWidth="0px">
                    <asp:TableRow VerticalAlign="Top">
                        <asp:TableCell>
                            <div id="content">
                                <asp:Label runat="server" ID="lbArticleInfoTitulo" Font-Names="Arial" Font-Size="12px" />
                                <asp:Label runat="server" ID="lbArticleInfoCEO" Font-Names="Arial" Font-Size="12px" /> 
                                <asp:Label runat="server" ID="lbArticleInfoText" Font-Names="Arial" Font-Size="12px" /> 
                                <asp:Label runat="server" ID="lbArticleInfoTelefone" Font-Names="Arial" Font-Size="12px" /> 
                                <asp:Label runat="server" ID="lbArticleInfoFax" Font-Names="Arial" Font-Size="12px" /> 
                                <asp:Label runat="server" ID="lbArticleInfoMorada" Font-Names="Arial" Font-Size="12px" /> 
                                <asp:Label runat="server" ID="lbArticleInfoCidade" Font-Names="Arial" Font-Size="12px" /> 
                                <asp:Label runat="server" ID="lbArticleInfoPais" Font-Names="Arial" Font-Size="12px" /> 
                                <asp:Label runat="server" ID="lbArticleInfoWeb" Font-Names="Arial" Font-Size="12px" /> 
                                <asp:Label runat="server" ID="lbArticleInfoCP" Font-Names="Arial" Font-Size="12px" Visible="false" /> 
                                <asp:Label runat="server" ID="Label1" Font-Names="Arial" Font-Bold="true" Font-Size="12px" Text="Ultima 5 Notícias" /> <br /><br />
                                <asp:Label runat="server" ID="lbArticleInfotop5n" Font-Names="Arial" Font-Size="12px" /> <br />
                            </div>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>

    <br />

    <div id="bot" style="float:left; width:95%;">
        <div style="float:right; padding-right:10px;">
            <asp:Table runat="server" ID="tblActions">
                <asp:TableRow ID="trActions" runat="server">

                    <asp:TableCell ID="tcActions" runat="server" HorizontalAlign="Center" VerticalAlign="Middle">
                        <asp:Image ID="ImgShare" runat="server" Width="16px" Height="16px" ImageUrl="Imgs/icons/black/png/shapes_icon_16.png"/>
                    </asp:TableCell>
                    <asp:TableCell ID="tcActions0" runat="server" HorizontalAlign="Center">
                        <asp:Label runat="server" ID="lblEmailLink" />
                    </asp:TableCell>
                    
                    <asp:TableCell ID="tcActions1" runat="server" HorizontalAlign="Center" VerticalAlign="Middle">
                        <asp:Image ID="ImgPrint" runat="server" Width="16px" Height="16px" ImageUrl="Imgs/icons/black/png/" /> 
                     </asp:TableCell>
                    <asp:TableCell ID="tcActions2" runat="server" HorizontalAlign="Center">
                        <a href="javascript:void(processPrint());" style="font-family:Arial; font-size:12px; text-decoration:none; color:#000000;"><%= getResource("defColImprimir")%></a>
                    </asp:TableCell>
                    
                    <asp:TableCell ID="tcActions3" runat="server" HorizontalAlign="Center" VerticalAlign="Middle"> 
                        <asp:Image runat="server" Width="16px" Height="16px" ID="imgDLorLINK" />
                    </asp:TableCell>
                    <asp:TableCell ID="tcActions4" runat="server" HorizontalAlign="Center">
                        <asp:Label runat="server" ID="lblPath" />
                    </asp:TableCell>
                    
                    <asp:TableCell ID="tcActions5" runat="server" HorizontalAlign="Center" VerticalAlign="Middle">
                        <asp:Image runat="server" ID="imgAddMyArticles" Width="16px" Height="16px" />
                    </asp:TableCell>
                    <asp:TableCell ID="tcActions6" runat="server" HorizontalAlign="Center">
                        <asp:LinkButton ID="LnkMyArticles" runat="server" OnCommand="bt_AddArquivo_Command" CausesValidation="True" style="font-family:Arial; font-size:12px; text-decoration:none; color:#000000;"><asp:Label runat="server" ID="lblAddMyArticle" /></asp:LinkButton>
                    </asp:TableCell>
                    
                    <asp:TableCell ID="tcActions7" runat="server" HorizontalAlign="Center" VerticalAlign="Middle">
                        <asp:Image runat="server" ID="imgPDFMyArticles" Width="16px" Height="16px" />
                    </asp:TableCell>
                    <asp:TableCell ID="tcActions8" runat="server" HorizontalAlign="Center">
                        <asp:LinkButton ID="LnkPDFMyArticles" runat="server" OnCommand="bt_AddPDF_Command" CausesValidation="True" style="font-family:Arial; font-size:12px; text-decoration:none; color:#000000;"><asp:Label runat="server" ID="lblPDFMyArticle" /></asp:LinkButton>
                    </asp:TableCell>
                    
                    <asp:TableCell ID="tcActions9" runat="server" HorizontalAlign="Center" VerticalAlign="Middle">
                        <asp:Image runat="server" ID="imgBackArticles" Width="16px" Height="16px" ImageUrl="Imgs/icons/black/png/arrow_left_icon_16.png" />
                    </asp:TableCell>
                    <asp:TableCell ID="tcActions10" runat="server" HorizontalAlign="Center">
                        <a href="javascript:history.go(-1)" style="font-family:Arial; font-size:12px; text-decoration:none; color:#000000;"><%= getResource("arColVoltar") %></a>
                    </asp:TableCell>

                </asp:TableRow>
            </asp:Table>
        </div>
    </div>



<script type="text/javascript">

</script>

</asp:Content>
