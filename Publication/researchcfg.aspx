﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="researchcfg.aspx.cs" Inherits="Insigte.researchcfg" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>insigte | media intelligence</title>
    <link href="Styles/StyleSheet.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
    function chkconfirmTema() {
        return confirm("Confirma que deseja apagar este Tema?");
    }

    function chkconfirmPasta() {
        return confirm("Confirma que deseja apagar este Relatório?");
    }
</script>

</head>
<body>
<form id="form1" runat="server">
        <% = "<div style=\"width: 400px; float: left; clear: both; color: #ffffff; height: 67px; background-image: url('Cli/" + global_asax.InsigteManager[Session.SessionID].CodCliente + "/fundo_small.png'); background-repeat:no-repeat; background-color: #003f56;\"> "%>
        </div>
    <div style="float:left; width: 380px; padding-top:5px; margin-left:10px; margin-top:5px; ">
        <p>
            <asp:Label ID="Lbl_Info" runat="server" Text="" style="color:#000000; font-size:12px; font-family:Arial;"></asp:Label>
        </p>
        <p>
            <asp:Label ID="Lbl_Pasta" runat="server" Text="Relatório" style="color:#000000; font-size:12px; font-family:Arial;"></asp:Label>
            <asp:DropDownList ID="DDPasta" runat="server" AutoPostBack="True" onselectedindexchanged="DDPasta_SelectedIndexChanged" style="color:#000000; font-size:12px; font-family:Arial;"></asp:DropDownList>
            &nbsp;<asp:LinkButton ID="Lkb_Pasta_Mod" CssClass="btn-pdfs" runat="server" OnClick="Lkb_Pasta_Mod_Click" Text="Modificar" Visible="false" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" Font-Underline="false"></asp:LinkButton>
            &nbsp;<asp:LinkButton ID="Lkb_Pasta_Del" CssClass="btn-pdfs" runat="server" OnClientClick='return chkconfirmPasta();' OnClick="Lkb_Pasta_Del_Click" Text="Apagar" Visible="false" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" Font-Underline="false"></asp:LinkButton>
        </p>
            <div runat="server" id="nov_pasta"  style="color:#000000; font-size:12px; font-family:Arial;">
                <div style="float:left; height:20px; width:100%;margin:2px 2px 2px 2px;">
                    <div style="float:left;width:30%;">
                        Nome Relatório
                    </div>
                    <div style="float:left;width:70%;">
                        <asp:TextBox runat="server" Text="" ID="txt_pasta" style="color:#000000; font-size:12px; font-family:Arial;"></asp:TextBox>
                    </div>
                </div>
                <div style="float:left; height:20px; width:100%;margin:2px 2px 2px 2px;">
                    <div style="float:left;width:30%;">
                        Descrição
                    </div>
                    <div style="float:left;width:70%;">
                        <asp:TextBox runat="server" Text="" ID="txt_pasta_des" style="color:#000000; font-size:12px; font-family:Arial;"></asp:TextBox>
                    </div>
                </div>
                <div style="float:left; height:20px; width:100%; line-height:1.4em; margin:10px 2px 2px 2px;">
                    <div style="float:left;width:30%;">
                        &nbsp;
                    </div>
                    <div style="float:left;width:70%;">
                        <asp:LinkButton ID="LnkPastaCriar" CssClass="btn-pdfs" runat="server" Text="Criar" OnClick="nova_Pasta_click" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" Font-Underline="false"></asp:LinkButton>
                    </div>
                </div>
            </div>
            <div runat="server" id="mod_pasta" visible="false" style="color:#000000; font-size:12px; font-family:Arial;">
                <div style="float:left; height:20px; width:100%;margin:2px 2px 2px 2px;">
                    <div style="float:left;width:30%;">
                        Nome Relatório
                    </div>
                    <div style="float:left;width:70%;">
                        <asp:TextBox runat="server" Text="" ID="txt_mod_nome_pasta" style="color:#000000; font-size:12px; font-family:Arial;"></asp:TextBox>
                    </div>
                </div>
                <div style="float:left; height:20px; width:100%;margin:2px 2px 2px 2px;">
                    <div style="float:left;width:30%;">
                        Descrição
                    </div>
                    <div style="float:left;width:70%;">
                        <asp:TextBox runat="server" Text="" ID="txt_mod_des_pasta" style="color:#000000; font-size:12px; font-family:Arial;"></asp:TextBox>
                    </div>
                </div>
                <div style="float:left; height:20px; width:100%; line-height:1.4em; margin:10px 2px 2px 2px;">
                    <div style="float:left;width:30%;">
                        &nbsp;
                    </div>
                    <div style="float:left;width:70%;">
                        <asp:LinkButton ID="LnkPastaMod" CssClass="btn-pdfs" runat="server" Text="Alterar" OnClick="LnkPastaMod_click" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" Font-Underline="false"></asp:LinkButton>
                    </div>
                </div>
            </div>
        <p>
            <asp:Label ID="Lbl_Tema" runat="server" Text="Tema(s) " style="color:#000000; font-size:12px; font-family:Arial;"></asp:Label>
            <asp:DropDownList ID="DDTema" runat="server" AutoPostBack="True" onselectedindexchanged="DDTema_SelectedIndexChanged" style="color:#000000; font-size:12px; font-family:Arial;"></asp:DropDownList>
            &nbsp;<asp:LinkButton ID="Lkb_Tema_Mod" CssClass="btn-pdfs" runat="server" OnClick="Lkb_Tema_Mod_Click" Text="Modificar" Visible="false" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" Font-Underline="false"></asp:LinkButton>
            &nbsp;<asp:LinkButton ID="Lkb_Tema_Del" CssClass="btn-pdfs" runat="server" OnClientClick='return chkconfirmTema();' OnClick="Lkb_Tema_Del_Click" Text="Apagar" Visible="false" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" Font-Underline="false"></asp:LinkButton>
        </p>
            <div runat="server" id="nov_tema" style="color:#000000; font-size:12px; font-family:Arial;">
                <div style="float:left; height:20px; width:100%;margin:2px 2px 2px 2px;">
                    <div style="float:left;width:30%;">
                        Nome Tema
                    </div>
                    <div style="float:left;width:70%;">
                        <asp:TextBox runat="server" Text="" ID="txt_tema" style="color:#000000; font-size:12px; font-family:Arial;"></asp:TextBox><br />
                    </div>
                </div>
                <div style="float:left; height:20px; width:100%;margin:2px 2px 2px 2px;">
                    <div style="float:left;width:30%;">
                        Descrição
                    </div>
                    <div style="float:left;width:70%;">
                        <asp:TextBox runat="server" Text="" ID="txt_tema_des" style="color:#000000; font-size:12px; font-family:Arial;"></asp:TextBox><br />
                    </div>
                </div>
                <div style="float:left; height:20px; width:100%;line-height:1.4em; margin:10px 2px 2px 2px;">
                    <div style="float:left;width:30%;">
                        &nbsp;
                    </div>
                    <div style="float:left;width:70%;">
                        <asp:LinkButton ID="LnkTemaCriar" CssClass="btn-pdfs" runat="server" Text="Criar" OnClick="novo_Tema_click" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" Font-Underline="false"></asp:LinkButton>
                    </div>
                </div>
            </div>
            <div runat="server" visible="false" id="mod_tema" style="color:#000000; font-size:12px; font-family:Arial;">
                <div style="float:left; height:20px; width:100%;margin:2px 2px 2px 2px;">
                    <div style="float:left;width:30%;">
                        Nome Tema
                    </div>
                    <div style="float:left;width:70%;">
                        <asp:TextBox runat="server" Text="" ID="txt_mod_tema_nome" style="color:#000000; font-size:12px; font-family:Arial;"></asp:TextBox><br />
                    </div>
                </div>
                <div style="float:left; height:20px; width:100%;margin:2px 2px 2px 2px;">
                    <div style="float:left;width:30%;">
                        Descrição
                    </div>
                    <div style="float:left;width:70%;">
                        <asp:TextBox runat="server" Text="" ID="txt_mod_tema_des" style="color:#000000; font-size:12px; font-family:Arial;"></asp:TextBox><br />
                    </div>
                </div>
                <div style="float:left; height:20px; width:100%;line-height:1.4em; margin:10px 2px 2px 2px;">
                    <div style="float:left;width:30%;">
                        &nbsp;
                    </div>
                    <div style="float:left;width:70%;">
                        <asp:LinkButton ID="LnkTemaMod" CssClass="btn-pdfs" runat="server" Text="Alterar" OnClick="LnkTemaMod_click" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" Font-Underline="false"></asp:LinkButton>
                    </div>
                </div>
            </div>
    </div>
    </form>
</body>
</html>
