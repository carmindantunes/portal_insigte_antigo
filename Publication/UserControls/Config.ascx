﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Config.ascx.cs" Inherits="Insigte.UserControls.Config" %>

<div class="ui-corner-top in-cli-usercontrols" style="height: 25px; width: 180px;">
    <div style="padding-top: 4px; padding-left: 5px;float:left;">
        <asp:Label runat="server" Text="<%$Resources:insigte.language,ucConfTitulo%>" ID="Lb_Titulo" ForeColor="white" Font-Names="Arial" Font-Size="12px" />
    </div>
    <div style="padding-top: 4px; padding-left: 5px; padding-right: 5px; float:right">
        <asp:Label runat="server" ForeColor="#00add9" Text="" Font-Bold="true" ID="Label1"  Font-Names="Arial" Font-Size="12px" />
    </div>
</div>
<div class="ui-corner-bottom" style="width: 180px; padding-top: 5px; background-color: #F0F0F0;">
    <asp:Table ID="Table1" runat="server"> 
        
        <asp:TableRow ID="TableRow2" runat="server">
            <asp:TableCell ID="TableCell2" runat="server" VerticalAlign="Middle">
            <%--    <asp:Image ID="Image2" runat="server" ImageUrl="../Imgs/conf_source.png" Width="16px" Height="16px" />--%>
            </asp:TableCell>
            <asp:TableCell ID="TableCell3" runat="server">
                <asp:LinkButton ID="LinkButton2" runat="server" ForeColor="Black" Font-Names="Arial" Font-Size="12px" Text="<%$Resources:insigte.language,ucConfLink1%>" PostBackUrl="../Fontes_gerais.aspx"></asp:LinkButton></asp:TableCell>
        </asp:TableRow>


        <asp:TableRow ID="TableRow1" runat="server">
            <asp:TableCell ID="TableCell0" runat="server" VerticalAlign="Middle">
                <%--<asp:Image ID="Image1" runat="server" ImageUrl="../Imgs/conf_source.png" Width="16px" Height="16px" />--%>
            </asp:TableCell>
            <asp:TableCell ID="TableCell1" runat="server">
                <asp:LinkButton ID="LinkButton1" runat="server" ForeColor="Black" Font-Names="Arial" Font-Size="12px" Text="<%$Resources:insigte.language,ucConfLink2%>" PostBackUrl="../Fontes.aspx"></asp:LinkButton></asp:TableCell>
        </asp:TableRow>

    </asp:Table>
</div>