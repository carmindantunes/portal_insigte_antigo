﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ACNGrafx.ascx.cs" Inherits="Insigte.UserControls.ACNGrafx" %>

<div class="ui-corner-top in-cli-usercontrols" style="height: 25px; width: 180px;">
    <div style="padding-top: 4px; padding-left: 5px;">
        <asp:Label runat="server" Text="<%$Resources:insigte.language,ucGraphTitulo%>" ID="Lb_Titulo" ForeColor="white" Font-Names="Arial" Font-Size="12px" />
    </div>
</div>

<div class="ui-corner-bottom" style="width: 180px; padding-top: 5px;">
    <div style="padding-top: 5px; padding-bottom:5px; padding-left:5px;">
        <asp:Label runat="server" Text="<%$Resources:insigte.language,ucGraphDescChart1%>" Font-Names="Arial" Font-Size="11px" ID="lblACNVCC"/>
    </div>
    <center>
        <asp:Chart runat="server" ID="Chart1" Width="175px" Height="250px" Palette="Fire" Enabled="true" >
            <Series>
                <asp:Series Name="Series1" >
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" >
                    
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
    </center>
    <div style="padding-top: 5px; padding-bottom:5px; padding-left:5px;">
        <asp:Label ID="Label1" runat="server" Text="<%$Resources:insigte.language,ucGraphDescChart2%>" Font-Names="Arial" Font-Size="11px"/>
    </div>
    <center>
    <asp:Chart runat="server" ID="Chart2" Width="175px" Height="175px" Palette="Fire" Enabled="true">
        <Series>
            <asp:Series Name="Series2">
            </asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea2">
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
    </center>
</div>
