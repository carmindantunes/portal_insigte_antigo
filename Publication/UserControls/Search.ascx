﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Search.ascx.cs" Inherits="Insigte.UserControls.Search" %>

<div class="ui-corner-top in-cli-usercontrols" style="height: 25px; width: 180px;">
    <div style="padding-top: 4px; padding-left: 5px;float:left;">
        <asp:Label runat="server" Text="<%$Resources:insigte.language,ucSearchTitulo%>" ID="Lb_Titulo" ForeColor="white" Font-Names="Arial" Font-Size="12px" />
    </div>
    <div style="padding-top: 4px; padding-left: 5px; padding-right: 5px; float:right">
        <asp:Label runat="server" ForeColor="#00add9" Text="" Font-Bold="true" ID="Label1"  Font-Names="Arial" Font-Size="12px" />
    </div>
</div>
<div class="ui-corner-bottom" style="width: 180px; padding-top: 5px; background-color: #F0F0F0;">
    <asp:Table ID="Table1" runat="server"> 
        
        <asp:TableRow ID="TableRow2" runat="server">
            <asp:TableCell ID="TableCell2" runat="server" VerticalAlign="Middle">
            <%--    <asp:Image ID="Image2" runat="server" ImageUrl="../Imgs/conf_source.png" Width="16px" Height="16px" />--%>
            </asp:TableCell>
            <asp:TableCell ID="TableCell3" runat="server">
                <a href="Subject.aspx?tp=5" style="color:#000000; font-size:12px; font-family:Arial;" target="_self"><asp:Literal ID="LtLink1" runat="server" Text="<%$Resources:insigte.language,ucSearchLink1%>" />
                </a>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRow1" runat="server">
            <asp:TableCell ID="TableCell0" runat="server" VerticalAlign="Middle">
                <%--<asp:Image ID="Image1" runat="server" ImageUrl="../Imgs/conf_source.png" Width="16px" Height="16px" />--%>
            </asp:TableCell>
            <asp:TableCell ID="TableCell1" runat="server">
                <a href="Subject.aspx?tp=4" style="color:#000000; font-size:12px; font-family:Arial;" target="_self"><asp:Literal ID="Literal1" runat="server" Text="<%$Resources:insigte.language,ucSearchLink2%>" />
                </a>
                </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow ID="TableRow3" runat="server">
            <asp:TableCell ID="TableCell4" runat="server" VerticalAlign="Middle">
                <%--<asp:Image ID="Image1" runat="server" ImageUrl="../Imgs/conf_source.png" Width="16px" Height="16px" />--%>
            </asp:TableCell>
            <asp:TableCell ID="TableCell5" runat="server">
                <a href="Subject.aspx?tp=2" style="color:#000000; font-size:12px; font-family:Arial;" target="_self"><asp:Literal ID="Literal2" runat="server" Text="<%$Resources:insigte.language,ucSearchLink3%>" />
                </a>
            </asp:TableCell>
        </asp:TableRow>

    </asp:Table>
</div>