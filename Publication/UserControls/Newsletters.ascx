﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Newsletters.ascx.cs"
    Inherits="Insigte.UserControls.Newsletters" %>
<div class="ui-corner-top in-cli-usercontrols" style="height: 25px; width: 180px;">
    <div style="padding-top: 4px; padding-left: 5px; float: left;">
        <asp:Label runat="server" Text="<%$Resources:insigte.language,ucNwlTitulo%>" ID="Lb_Titulo"
            ForeColor="white" Font-Names="Arial" Font-Size="12px" />
    </div>
    <div style="padding-top: 4px; padding-left: 5px; padding-right: 5px; float: right">
        <asp:Label runat="server" ForeColor="#00add9" Text="" Font-Bold="true" ID="Label1"
            Font-Names="Arial" Font-Size="12px" />
    </div>
</div>
<div class="ui-corner-bottom" style="width: 180px; padding-top: 5px; background-color: #F0F0F0;">
    <asp:Table ID="Table1" runat="server">
        <asp:TableRow ID="TableRow1" runat="server">
            <asp:TableCell ID="TableCell0" runat="server" VerticalAlign="Middle">
                <%--<asp:Image ID="Image1" runat="server" ImageUrl="../Imgs/linedpapercheck32.png" Width="16px" Height="16px" />--%>
            </asp:TableCell>
            <asp:TableCell ID="TableCell1" runat="server">
                <asp:LinkButton ID="LinkButton1" runat="server" ForeColor="Black" Font-Names="Arial"
                    Font-Size="12px" Text="<%$Resources:insigte.language,ucNwlLink1%>" PostBackUrl="~/SubscribeNl.aspx"></asp:LinkButton>
            </asp:TableCell>
        </asp:TableRow>
        <%--Escola de noticias para sair no email--%>
        <asp:TableRow ID="NewsChooseRow" runat="server" Visible="false">
            <asp:TableCell ID="TableCell6" runat="server" VerticalAlign="Middle">
                <%--<asp:Image ID="Image1" runat="server" ImageUrl="../Imgs/linedpapercheck32.png" Width="16px" Height="16px" />--%>
            </asp:TableCell>
            <asp:TableCell ID="TableCell7" runat="server">
                <asp:LinkButton ID="NewsChoose" runat="server" Style="margin-left: 10px;" ForeColor="Black"
                    Font-Names="Arial" Font-Size="12px" Text="<%$Resources:insigte.language,ucNwlLink1_1%>"
                    PostBackUrl="../NewsChoose.aspx" Visible="false"></asp:LinkButton>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="Lb_CompostaRow" runat="server" Visible="false">
            <asp:TableCell ID="TableCell4" runat="server" VerticalAlign="Middle">
                <%--<asp:Image ID="Image1" runat="server" ImageUrl="../Imgs/linedpapercheck32.png" Width="16px" Height="16px" />--%>
            </asp:TableCell>
            <asp:TableCell ID="TableCell5" runat="server">
                <asp:LinkButton ID="Lb_Composta" runat="server" ForeColor="Black" Font-Names="Arial"
                    Font-Size="12px" Text="<%$Resources:insigte.language,ucNwlLink2%>" PostBackUrl="../SubscribeCp.aspx"
                    Visible="false"></asp:LinkButton>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="Lb_AlertaRow" runat="server">
            <asp:TableCell ID="TableCell2" runat="server" VerticalAlign="Middle">
                <%--<asp:Image ID="Image2" runat="server" ImageUrl="../Imgs/linedpapercheck32.png" Width="16px" Height="16px" />--%>
            </asp:TableCell>
            <asp:TableCell ID="TableCell3" runat="server">
                <%--Alterado <asp:LinkButton ID="Lb_Alerta" runat="server" ForeColor="Black" Font-Names="Arial" Font-Size="12px" Text="<%$Resources:insigte.language,ucNwlLink3%>"  OnClick="Alertas_Click"></asp:LinkButton> --%>
                <asp:LinkButton ID="Lb_Alerta" runat="server" ForeColor="Black" Font-Names="Arial"
                    Font-Size="12px" Text="<%$Resources:insigte.language,ucNwlLink3%>" PostBackUrl="~/SubscribeLstAl.aspx"></asp:LinkButton>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</div>
