﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Pastas.ascx.cs" Inherits="Insigte.UserControls.Pastas" %>
<script type="text/javascript">
    function openWindow(url)
    {
        var w = window.open(url, '', 'width=400,height=400,toolbar=0,status=0,location=0,menubar=0,directories=0,resizable=1,scrollbars=1');
        w.focus();
}
</script>
<div class="ui-corner-top in-cli-usercontrols" style="height: 25px; width: 180px;">
    <div style="padding-top: 4px; padding-left: 5px; float:left;">
        <asp:Label runat="server" Text="<%$Resources:insigte.language,ucPastasTitulo%>" ID="Lb_Titulo" ForeColor="white" Font-Names="Arial" Font-Size="12px" />
    </div>
    <div style="padding-top: 4px; padding-left: 5px; padding-right: 5px; float:right">
        <asp:Label runat="server" ForeColor="#00add9" Text="" ID="Label1" Font-Bold="true" Font-Names="Arial" Font-Size="12px" />
    </div>
</div>
<div class="ui-corner-bottom" style="width: 180px; padding-top:5px; background-color: #F0F0F0;">
    <asp:Table ID="tblArchives" runat="server">
        <asp:TableRow ID="TableRow10" runat="server">
            <asp:TableCell ID="TableCell0" runat="server" VerticalAlign="Middle" >
                <%--<asp:Image ID="Image1" runat="server" ImageUrl="~/Imgs/folder32.png" Width="16px" Height="20px" ToolTip="Ver Pastas" />--%>
            </asp:TableCell>
            <asp:TableCell ID="TableCell1" runat="server">
                <a href="pastas.aspx" style="color:#000000; font-size:12px; font-family:Arial;" target="_self"><asp:Literal ID="LtLink1" runat="server" Text="<%$Resources:insigte.language,ucPastasLink1%>" />
                </a></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow2" runat="server" Visible="true">
            <asp:TableCell ID="TableCell4" runat="server" VerticalAlign="Middle" >
                <%--<asp:Image ID="Image2" runat="server" ImageUrl="~/Imgs/gPastas.png" Width="16px" Height="16px" ToolTip="Gerir Pastas"/>--%>
            </asp:TableCell>
            <asp:TableCell ID="TableCell5" runat="server">
                <asp:LinkButton runat="server" Text="<%$Resources:insigte.language,ucPastasLink2%>" OnClientClick="openWindow('pastascfg.aspx')" Font-Names="Arial" Font-Size="12px" ForeColor="#000000" Font-Underline="false"></asp:LinkButton><br />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow ID="TableRow3" runat="server"  Visible="false">
            <asp:TableCell ID="TableCell2" runat="server" VerticalAlign="Middle" >
                <%--<asp:Image ID="Image3" runat="server" ImageUrl="~/Imgs/linedpaper32.png" Width="16px" Height="16px" ToolTip="Compilar PDF"/>--%>
            </asp:TableCell>
            <asp:TableCell ID="TableCell3" runat="server"><a href="ListPDF.aspx" style="color:#000000; font-size:12px; font-family:Arial;" target="_self" title="Compilar PDF">Compilar PDF</a></asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</div>